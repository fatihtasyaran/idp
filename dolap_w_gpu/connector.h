#ifndef CONNECTOR_H
#define CONNECTOR_H

#include "common.h"
//GPU//
//#include "gpu_funcs.cu"
//GPU//

extern "C" void connect();
extern "C" void* declare_dB_object(char** col_types, char** col_names, int col_num, int NO_BLOCK_GROUPS);
extern "C" void start_ingest(void* db_addr, int NO_BLOCK_GROUPS, char** col_types, int col_num);
char** GPU_scan(void* db_addr, int target_col_num, int ts_col_num, int NO_BLOCK_GROUPS, int col_num);

/*
class Reporter
{
 public:
  int col_num;
  char** col_names;
  char** join;
  
  Reporter(){
    col_num = 0;
  }
  
  Reporter(int COL_NUM, char** FEATURE_NAMES){
    col_num = COL_NUM;
    
    col_names = new char*[COL_NUM];
    for(int i = 0; i < col_num; i++){
      col_names[i] = FEATURE_NAMES[i];
    }
  }
  
  void col_num_set(int COL_NUM){
    col_num = COL_NUM;
  }
  
  void col_names_set(char** FEATURE_NAMES){
    col_names = FEATURE_NAMES;
  }
};

*/


extern "C" int intParams(int a, int b); //THIS IS TEST
extern "C" char* returnQ(char* query, int Qsize);
extern "C" char* returnQ_S(char* query); //IN SETUP.HPP
extern "C" int report_col_num();
extern "C" char** report_feature_names();
extern "C" void initiate();

#endif
