#ifndef DATABASE_H
#define DATABASE_H

#include "common.h"
#include <sstream>
//#include <boost/lexical_cast.hpp>

#define NO_BLOCKS 30

extern int G_SIZE;

static uint32_t block_ids = -1;

template <typename T>
string myString(T val){
  string out_string;
  std::stringstream ss;
  ss << val;
  out_string = ss.str();
  return out_string;
}

inline int returnSize_d(string type) {
  if(type == "uint8_t")
    return sizeof(UINT8);
  else if(type == "uint16_t")
    return sizeof(UINT16);
  else if(type == "uint32_t")
    return sizeof(UINT32);
  else if(type == "string")
    return SIZEOF_STRING;
  else if(type == "double")
    return sizeof(DOUBLE);
  else if(type == "bool")
    return sizeof(BOOL);
  else if(type == "NONE")
    return 0;
  else
    std::cout << "I don't have " << type << "type" << std::endl;
}

uint32_t get_block_id()
{
  return ++block_ids;
}

struct virtual_block{

  
  virtual ~virtual_block(){
  }

  virtual void insert(int index, uint8_t val){
  }

  virtual void insert(int index, uint16_t val){
  }

  virtual void insert(int index, uint32_t val){
  }

  virtual void insert(int index, double val){
  }

  virtual void insert(int index, float val){
  }

  virtual void insert(int index, string val){
  }

  virtual void insert(int index, bool val){
  }

  virtual void print_get(int index){
  }
  
  virtual string somehow_get(int index){
    string ret = "D U M M Y";
    std::cout << "Erroneous" << std::endl;
    return ret;
  }
  
  
};

template <class T>
struct block: public virtual_block{
  uint32_t id;
  uint32_t size;
  uint32_t accessed;
  string type;
  T block_ptr[1024];

  block()
  {
  }

  block(int B_ID){
    id = B_ID;
    //sketch = new sketch();
    //bloom_filter = new bloom_filter();
  }
  
  void insert(int index, T val){
    //std::cout << "index: " << index << "\n";
    block_ptr[index] = val;
  }
  
  //This is for debug purposes
  //template<typename T>
  void print_get(int index){
    std::cout << block_ptr[index] << std::endl;
  }

  string somehow_get(int index){
    string ret = myString(block_ptr[index]);
    return ret;
  }
  
};

struct dataBase{
  virtual_block* db[100000];
  
  dataBase(type_num_col* type_indexes, int col_num)
  {
    //db = new block_ptrs[col_num];
    string type;
    std::cout << "Constructing Database.." << std::endl;
    for(int i = 0; i < col_num*NO_BLOCK_GROUPS; i++){
      type = type_indexes[i%col_num].first;
      
      if(type == "uint8_t"){
	db[i] = new block<uint8_t>(i);
      }
      
      else if(type == "uint16_t"){
	db[i] = new block<uint16_t>(i);
      }
      
      else if(type == "uint32_t"){
	db[i] = new block<uint32_t>(i);
      }
      
      else if(type == "double"){
	db[i] = new block<double>(i);
      }
      
      else if(type == "float"){
	db[i] = new block<float>(i);
      }
      
      else if(type == "string"){
	db[i] = new block<string>(i);
      }
      
      else if(type == "bool"){
	db[i] = new block<bool>(i);
      }
      
      else{
	std::cout << "Undefined type, exiting" << std::endl;
	exit(1);
      }
    }
  }
  
};

#endif
