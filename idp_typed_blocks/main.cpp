#include <iostream>
#include <string>
#include <stdint.h>
#include <random>
#include <omp.h>
//
#include <fstream>
#include <typeinfo>
#include "common.h"
//
#include "setup.hpp"  
#include "database.hpp"
#include "heterogenous_container.hpp"
#include "connector.h"
//#include "query.hpp" //No query at the time
//
#include <vector>
#include <list>
#include <variant>
#include <map>
#include <iterator>
#include <cstring>
//#include <boost/any.hpp>


template <typename T>
struct temp_type{
  T val;

  temp_type(T VAL){
    val = VAL;
  }


};

void prettyPrintOneRecord(string record_line, int col_num){
  for(int i = 0; i < col_num; i++){
    std::cout << "Column: " << i << " "+acquire_val(record_line, i) << std::endl;
  }
}

/*void separate(string* arr, string line, int col_num){
  int length = line.length();

  std::cout << line << std::endl;
  char GUARD[1000] = {line.c_str()};

  string real_line(line);
  
  int startIn = 0;
  int endIn = 0;
  int in = 0;
  
  for(int i = 0; i < length+1; i++){
    std::cout << "Before seg, in: " << i << "char: ";
    std::cout << "GUARD: " << GUARD[i] << std::endl;

    if(real_line[i] == ',' || real_line[i] == '\n'){
      endIn = i;
      arr[in] = real_line.substr(startIn, endIn-startIn);
      in++;
      startIn = endIn+1;
    }
  }
  }*/




int entry()
{
  std::map<string, int> mapper;
  mapper["uint8_t"] = 0;
  mapper["uint16_t"] = 1;
  mapper["uint32_t"] = 2;
  mapper["double"] = 3;
  mapper["string"] = 4;
  mapper["bool"] = 5;
  
  string types_line;
  string vals_line;
  fstream csv_reader;
  int col_num;

    
  csv_reader.open("/home/fatih/Documents/q_idp/idp/chicago-taxi-rides-2016/chicago_taxi_trips_2016_01.csv");
  //csv_reader.open("/home/data/epa_hap_daily_summary.csv");
  
  getline(csv_reader, types_line);

    
  col_num = coma(types_line);//+2; //Number of variables in database ##+1 -> line_id ##+2 -> Number of accesses

  /*
  /////CREATE CONFIG FILES/////
  std::ofstream out1("config_types.txt");
  string* names = new string[col_num];
  getline(csv_reader, types_line);
  separate(names, types_line, col_num);

  for(int i = 0; i < col_num; i++){
    out1 << names[i];
    out1 << "\n";
  }
  /////CREATE CONFIG FILES/////
  */
  
  type_num_col type_indexes[col_num];  
  
  int counter = 0;

  getline(csv_reader, vals_line);

  typeParse(types_line, type_indexes, 1); //Get types
  typeParse(vals_line, type_indexes, 2); //Get feature names

  int totSize = 0;
  
    
  for(int i = 0; i < col_num; i++){   //Find the total size of database line
    totSize += returnSize(type_indexes[i].first);
    std::cout << type_indexes[i].first << " Returned: " << returnSize(type_indexes[i].first)*8 << " totSize: " << totSize << std::endl;
  }

    
  for(int i = 0; i < col_num; i++){
    std::cout << "Column:" << i << "\tType: " << type_indexes[i].first << "\tColName: " << type_indexes[i].second << std::endl;
    std::cout << std::endl;
  }
  
  std::cout << "Total Size:" << totSize << std::endl;


  

  //REPORTER//
  std::cout << "Col num: " << col_num << std::endl;
  char** f_names = new char*[col_num];
  for(int i = 0; i < col_num; i++){
    f_names[i] = type_indexes[i].second.c_str();
  }
  Reporter connection_report(col_num, f_names);
  //REPORTER//

  
  

  std::cout << "Declaring Database.." << std::endl;
  dataBase *db = new dataBase(type_indexes,col_num);
  std::cout << "Declared Database.." << std::endl;
  
  int record_num = 0;

  
  /*double start = omp_get_wtime();
  
  for(int b = 0; b < 30; b++){ //This writes records one by one, works but slow by bad temporal locality,
    for(int l = 0; l < 1024; l++){ //Creating thread overhead with each record
      getline(csv_reader, vals_line);
      writeOneRecordtoBlocks(*db, vals_line, type_indexes, col_num, record_num);
      //std::cout << "Wrote Record " << (b*1024)+l << std::endl;
      record_num++;
    }
  }

  double end = omp_get_wtime();
  std::cout << "Insert took " << end-start << std::endl;*/

  int num_s_record = 0;
  int num_records = 0;

  double start = omp_get_wtime();

  string record_chunk[1024];
  string check_line;

  double getliner = 0;
  double get_start = 0;
  double get_end = 0;
  
  for(int block_group = 0; block_group < NO_BLOCK_GROUPS; block_group++){
    for(int record = 0; record < 1024; record++){
      get_start = omp_get_wtime();
      getline(csv_reader, record_chunk[record]);
      get_end = omp_get_wtime();
      getliner += get_end-get_start;
      num_s_record++;
      if(num_s_record == 12)
	check_line = record_chunk[record];
    }
    //std::cout << "*****Writing block group: " << block_group << "*****" <<std::endl;
    writeOneRecordChunktoBlocks_better(*db, record_chunk, type_indexes, col_num, 1024, block_group);
    num_records += 1024;
  }
  
  double end = omp_get_wtime();
  std::cout << "Insert took " << end-start << std::endl;
  std::cout << "Number of records inserted: " << num_records << std::endl;
  std::cout << "Number of records per second: " << (1/(end-start))*num_records << std::endl;
  std::cout << "Read time: " << getliner << std::endl;
  std::cout << "According to this: " << (1/(end-start-getliner))*num_records << std::endl;

  
  uint16_t getter = 2;
  string getter_;

  double startt = omp_get_wtime();
  
  db->db[0]->print_get(11);//, getter);
  db->db[1]->print_get(11);//, getter_);
  db->db[2]->print_get(11);//, getter_);
  db->db[3]->print_get(11);//, getter_);
  db->db[4]->print_get(11);//, getter_);
  db->db[5]->print_get(11);//, getter_);
  db->db[6]->print_get(11);//, getter_);
  db->db[7]->print_get(11);//, getter_);

  std::cout << "Somehow Get 0: " << db->db[0]->somehow_get(11) << std::endl;
  std::cout << "Somehow Get: " << db->db[2]->somehow_get(11) << std::endl;
  
  
  db->db[20]->print_get(2);//, getter);
  db->db[21]->print_get(2);//, getter_);
  db->db[22]->print_get(2);//, getter_);

  /*db->db[400]->print_get(1000, getter);
  db->db[401]->print_get(1000, getter_);
  db->db[402]->print_get(1000, getter_);

  db->db[2000]->print_get(1000, getter);
  db->db[2001]->print_get(1001, getter_);
  db->db[2002]->print_get(1001, getter_);

  db->db[8000]->print_get(1000, getter);
  db->db[8001]->print_get(1001, getter_);
  db->db[8002]->print_get(1001, getter_);*/


  std::cin.get();
  std::cin.ignore();
        
  return 0;
}



  
  
