#include <iostream>
#include "connector.h"
//#include "setup.hpp"

extern "C" void connect()
{
  std::cout << "Connected to c++ engine.." << std::endl;
}

//THIS IS TEST//
extern "C" int intParams(int a, int b){
  return a+b;
}
//THIS IS TEST//

extern "C" char* returnQ(char* query, int Qsize){
  string quer(query);
  std::cout << "Param1: " << quer << " Param2: " << Qsize << std::endl;
  char* join = returnQ_S(query);
  return join; 
}

extern "C" int report_col_num(Reporter connection_report){
  std::cout << "This is " << std::endl;
  std::cout << "C++: " << connection_report.col_num << std::endl;
  return connection_report.col_num;
}

