#ifndef SETUP_H
#define SETUP_H

#include "database.hpp"
//#include "query.hpp"
#include <fstream>
#include "common.h"
#include <omp.h>
#include <cstring>
#include <stdio.h>
#include <bsd/string.h>


//#define DEBUG

int returnSize(std::string type) {
  if(type == "uint8_t")
    return sizeof(UINT8);
  else if(type == "uint16_t")
    return sizeof(UINT16);
  else if(type == "uint32_t")
    return sizeof(UINT32);
  else if(type == "string")
    return SIZEOF_STRING;
  else if(type == "double")
    return sizeof(DOUBLE);
  else if(type == "bool")
    return sizeof(BOOL);
  else if(type == "NONE")
    return 0;
  else
    std::cout << "I don't have " << type << "type" << std::endl;
}


int coma(std::string line) {
  
  int comas = 1;
  int length = line.length();
    
  for(int i = 0; i < length; i++){
    if(line[i] == ',')
      comas++;
  }
  return comas;
}

void typeParse(std::string line, type_num_col* indexes, int mod) {
  std::string type;
  
  int length = line.length();
  int current_col = 0;
  int startIndex = 0;
  int endIndex = 0;

  
  for(int i = 0; i < length+1; i++){
    if(line[i] == ',' || i == length){
      endIndex = i;
      
      if(mod == 1){
	indexes[current_col].first = line.substr(startIndex, endIndex-startIndex);  //Type
      }
      else{
	indexes[current_col].second = line.substr(startIndex, endIndex-startIndex); //Feature name
      }

      
      startIndex = endIndex+1;
      current_col++;
      
      //std::cout << "Current Col: " << current_col << " String: " << line.substr(startIndex, endIndex-startIndex) << std::endl; 
    }
  }
}

inline std::string acquire_val(std::string line_to_cut, int order){
#if defined DEBUG
  if(order == 0)
    //std::cout << line_to_cut << std::endl;
#endif
  
  int length = line_to_cut.length();
  int start_index = 0;
  int end_index = 0;

  int found = 0;

  std::string val = "D U M M Y";

  for(int i = 0; i < length; i++){
    if(line_to_cut[i] == ','){
      end_index = i;
      if(order == 0)
	start_index -= 1;
      val = line_to_cut.substr(start_index+1, end_index-start_index-1);
      
      if(found == order)
	return val;

      found++;
      start_index = end_index;
    }
  }

  return val;
}

void writeOneRecordtoBlocks(dataBase db, std::string vals_line, type_num_col* indexes, int col_num, int record_num){

#pragma omp parallel num_threads(col_num) //Write to col_num blocks in parallel
  {
  int tid = omp_get_thread_num();
  
  int block_num = tid;//record_num/1024;
  int index = record_num%1024;
  int c_val;  //Value to cast placeholder
  //auto w_val; //Value to send block's insert function
  std::string type = indexes[tid].first;
  
  std::string val = acquire_val(vals_line, tid);

#if defined DEBUG
  std::cout << "Start parallel ingest" << std::endl;
#pragma omp critical
  {
    std::cout << "Thread " << tid << " before insert, my val: " << val << std::endl;
  }
#endif
  
  if(val == EMPTY){
    ;
  }
  
  else{
    if(type == "uint8_t"){
      c_val = std::stoi(val);
      auto w_val = (uint8_t)c_val;
      db.db[block_num]->insert(index, w_val);
    }
    else if(type == "uint16_t"){
      c_val = std::stoi(val);
    auto w_val = (uint16_t)c_val;
    db.db[block_num]->insert(index, w_val);
    }
    else if(type == "uint32_t"){
      c_val = std::stoi(val);
      auto w_val = (uint32_t)c_val;
      db.db[block_num]->insert(index, w_val);
    }
    else if(type == "double"){
      auto w_val = std::stod(val);
      db.db[block_num]->insert(index, w_val);
    }
    else if(type == "string"){
      auto w_val = val;
      db.db[block_num]->insert(index, w_val);
    }
    else if(type == "bool"){
      if(val == "true"){
	auto w_val = true;
	db.db[block_num]->insert(index, w_val);
      }else{
	auto w_val = false;
	db.db[block_num]->insert(index, w_val);
      }
    }else{
      std::cout << "Undefined type, exiting" << std::endl;
      exit(1);
    }

#if defined DEBUG
#pragma omp critical
    {
      std::cout << "Thread " << tid << " after insert" << std::endl;
    }
#endif
  }
}
  
  }

void writeOneRecordChunktoBlocks(dataBase db, std::string *record_chunk, type_num_col* indexes, int col_num, int chunk_size, int block_group){
#if defined DEBUG
  std::cout << "Block group: " << block_group << " start inserting in parallel" << std::endl;
#endif

#pragma omp parallel num_threads(col_num) //Write to col_num blocks in parallel
  {
  int tid = omp_get_thread_num();
  int block_num = tid+(block_group+block_group*(col_num-1));//record_num/1024;

  for(int i = 0; i < chunk_size; i++){
    int index = i;
    int c_val;  //Value to cast placeholder
    //auto w_val; //Value to send block's insert function
    std::string type = indexes[tid].first;
    
    std::string val = acquire_val(record_chunk[i], tid);
    
#if defined DEBUG
#pragma omp critical
    {
      std::cout << "Thread " << tid << " before insert, my val: " << val << std::endl;
    }
#endif
    
    if(val == EMPTY){
    ;
    }
  
    else{
      if(type == "uint8_t"){
	c_val = std::stoi(val);
	auto w_val = (uint8_t)c_val;
	db.db[block_num]->insert(index, w_val);
      }
      else if(type == "uint16_t"){
	c_val = std::stoi(val);
	auto w_val = (uint16_t)c_val;
	db.db[block_num]->insert(index, w_val);
      }
      else if(type == "uint32_t"){
	c_val = std::stoi(val);
	auto w_val = (uint32_t)c_val;
	db.db[block_num]->insert(index, w_val);
      }
      else if(type == "double"){
	auto w_val = std::stod(val);
	db.db[block_num]->insert(index, w_val);
      }
      else if(type == "string"){
	auto w_val = val;
	db.db[block_num]->insert(index, w_val);
      }
      else if(type == "bool"){
	if(val == "true"){
	  auto w_val = true;
	  db.db[block_num]->insert(index, w_val);
	}else{
	  auto w_val = false;
	  db.db[block_num]->insert(index, w_val);
	}
      }else{
	std::cout << "Undefined type, exiting" << std::endl;
	exit(1);
      }
      
#if defined DEBUG
#pragma omp critical
      {
	std::cout << "Thread " << tid << " after insert" << std::endl;
      }
#endif
    }
  }
#pragma omp barrier
}
  
  }



void writeOneRecordChunktoBlocks_better(dataBase db, std::string *record_chunk, char** col_types, int col_num, int chunk_size, int block_group){ //Threads constantly writes to same blocks, therefore one if - else is enough for 1024 records
#if defined DEBUG
  std::cout << "Block group: " << block_group << " start inserting in parallel" << std::endl;
#endif

  //std::cout << "Writing.." << std::endl;
  //db.db[0]->insert(0,(uint8_t)8);
  //std::cout << "Writing 2.." << std::endl;

#pragma omp parallel num_threads(col_num) //Write to col_num blocks in parallel
  {
  int tid = omp_get_thread_num();
  int block_num = tid+(block_group+block_group*(col_num-1));//record_num/1024;

  
  int c_val;  //Value to cast placeholder
  //auto w_val; //Value to send block's insert function
  //std::string type = col_types[tid]; //This line generates segmentation fault
  //std::string type(col_types[tid]);
  std::string type = col_types[tid];
  //type = col_types[tid];
  
  //std::cout << "Type is: " << col_types[0] << std::endl;

  //std::cout << "Can't get func" << std::endl;
  
  if(type == "uint8_t"){
    for(int index = 0; index < 1024; index++){
      std::string val = acquire_val(record_chunk[index], tid);
      if(val != EMPTY){
	c_val = std::stoi(val);
	  auto w_val = (uint8_t)c_val;
	  db.db[block_num]->insert(index, w_val);
      }
    }
  }
  else if(type == "uint16_t"){
    for(int index = 0; index < 1024; index++){
      std::string val = acquire_val(record_chunk[index], tid);
      if(val != EMPTY){
	c_val = std::stoi(val);
	auto w_val = (uint16_t)c_val;
	db.db[block_num]->insert(index, w_val);
      }
    }
  }
  else if(type == "uint32_t"){
    for(int index = 0; index < 1024; index++){
      std::string val = acquire_val(record_chunk[index], tid);
      if(val != EMPTY){
	c_val = std::stoi(val);
	auto w_val = (uint32_t)c_val;
	db.db[block_num]->insert(index, w_val);
      }
    }
  }
  else if(type == "double"){
    for(int index = 0; index < 1024; index++){
      std::string val = acquire_val(record_chunk[index], tid);
      if(val != EMPTY){
	auto w_val = std::stod(val);
	db.db[block_num]->insert(index, w_val);
      }
    }
  }
  else if(type == "string"){
    for(int index = 0; index < 1024; index++){
      std::string val = acquire_val(record_chunk[index], tid);
      if(val != EMPTY)
	{
	  auto w_val = val;
	  db.db[block_num]->insert(index, w_val);
	}
    }
  }
  else if(type == "bool"){
    for(int index = 0; index < 1024; index++){
      std::string val = acquire_val(record_chunk[index], tid);
      if(val != EMPTY){
	if(val == "true"){
	  auto w_val = true;
	  db.db[block_num]->insert(index, w_val);
	}
	else{
	  auto w_val = false;
	  db.db[block_num]->insert(index, w_val);
	}
      }
    }
  }
  else{
    std::cout << "Undefined type, exiting" << std::endl;
    exit(1);
  }
  
#if defined DEBUG
#pragma omp critical
  {
    std::cout << "Thread " << tid << " after insert" << std::endl;
  }
#endif
#pragma omp barrier
}
  }

void ingest(void* db_addr, int NO_BLOCK_GROUPS, char** col_types, int col_num)
{
  dataBase* dB = db_addr;
  std::string types_line;
  std::string vals_line;
  fstream csv_reader;
  //int col_num;

  std::cout << "In ingest" << std::endl;
    
  csv_reader.open("/home/fatih/Documents/q_idp/idp/chicago-taxi-rides-2016/chicago_taxi_trips_2016_01.csv");
  //csv_reader.open("/home/data/epa_hap_daily_summary.csv");
  
  getline(csv_reader, types_line);
  getline(csv_reader, vals_line);
    
  
  int record_num = 0;
  
  int num_s_record = 0;
  int num_records = 0;
  
  double start = omp_get_wtime();
  
  std::string record_chunk[1024];
  std::string check_line;
  
  double getliner = 0;
  double get_start = 0;
  double get_end = 0;
  
  for(int block_group = 0; block_group < NO_BLOCK_GROUPS; block_group++){
    for(int record = 0; record < 1024; record++){
      get_start = omp_get_wtime();
      getline(csv_reader, record_chunk[record]);
      get_end = omp_get_wtime();
      getliner += get_end-get_start;
      num_s_record++;
      if(num_s_record == 12)
	check_line = record_chunk[record];
    }
    //std::cout << "*****Writing block group: " << block_group << "*****" <<std::endl;
    writeOneRecordChunktoBlocks_better(*dB, record_chunk, col_types, col_num, 1024, block_group);
    num_records += 1024;
  }
  double end = omp_get_wtime();
  std::cout << "Insert took " << end-start << std::endl;
  std::cout << "Number of records inserted: " << num_records << std::endl;
  std::cout << "Number of records per second: " << (1/(end-start))*num_records << std::endl;
  std::cout << "Read time: " << getliner << std::endl;
  std::cout << "According to this: " << (1/(end-start-getliner))*num_records << std::endl;

}

char** scan(void* db_addr, int target_col_num, int ts_col_num, int NO_BLOCK_GROUPS,int col_num){
  std::cout << "Scanning.." << std::endl;
  dataBase* dB = db_addr;

  int max_threads = omp_get_max_threads();
  //int max_threads = 1;

  int total_elem = NO_BLOCK_GROUPS*1024*2;
  int per_thread = total_elem/max_threads;
  
  char** join = new char*[NO_BLOCK_GROUPS*1024*2];
  for(int i = 0; i < NO_BLOCK_GROUPS*1024*2; i++){
    join[i] = new char[100];
  }

  int tso = ts_col_num - target_col_num;  //TIMESERIES OFFSET
  
  int group_per_thread = NO_BLOCK_GROUPS/max_threads;

  std::cout << "Time Series Col: " << ts_col_num << std::endl;
  std::cout << "string query: " << dB->db[5000]->somehow_get(5) << std::endl;
  std::cout << "cstring query: " << dB->db[5002]->somehow_get(5).c_str() << std::endl;
  
#pragma omp parallel num_threads(max_threads)//(max_threads)
  {
    
    std::cout << "Start parallel ingesting" << std::endl;
    int tid = omp_get_thread_num();
    int start_group = group_per_thread*tid;
    int last_group = start_group + group_per_thread; //THIS IS LIMIT, NOT INCLUDED
    int join_index = start_group * 1024 * 2;
#pragma omp critical
    {
      std::cout << "Thread: " << tid << " join_index: " << join_index << std::endl;
    }
    
    if(tid == (max_threads - 1))
      last_group = NO_BLOCK_GROUPS - 1;
    
    for(int b = start_group; b < last_group; b++){
      for(int i = 0; i < 1024; i++){
	int target_block_index = b*col_num + target_col_num;
	int timeseries_block_index = b*col_num + ts_col_num;
	
	strcpy(join[join_index], dB->db[target_block_index]->somehow_get(i).c_str());
	strcpy(join[join_index + 1], dB->db[timeseries_block_index]->somehow_get(i).c_str());
	join_index += 2;
      }
    }
#pragma omp barrier
  }
    
  std::cout << "Scan done!" << std::endl;
  return join;
  
}


/******
THIS WILL REMAIN TRIVIAL FOR SOME MORE TIME
void writeOneRecordChunktoBlocks_better_better(dataBase db, string *record_chunk, type_num_col* indexes, int col_num, int chunk_size, int block_group){ //Threads constantly writes to same blocks, therefore one if - else is enough for 1024 records
#if defined DEBUG
  std::cout << "Block group: " << block_group << " start inserting in parallel" << std::endl;
#endif

  int no_max_threads = omp_get_max_threads();
  std::cout << "Start inserting with " << no_max_threads << " threads " << std::endl;
#pragma omp parallel for num_threads(no_max_threads) schedule(static, 8)
  for(int b = 0; b < col_num*NO_BLOCK_GROUPS; b++){
    auto type_get = db.db[b]->block_ptr[0];
    string val = acquire_val(record_chunk[b], b%col_num);
    auto w_val = (decltype(type_get))val;
    db.db[b]->block_ptr[b%1024] = w_val;
  }
  }
******/

extern "C" char* returnQ_S(char* query){
  char* q = "Hi there you Python";
  return q;
}





#endif
