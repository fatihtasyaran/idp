#include <iostream>
#include "connector.h"
#include "database.hpp"
//#include "main.cpp"
#include "setup.hpp"

using namespace database;

extern "C" void connect()
{
  std::cout << "Connected to c++ engine.." << std::endl;
}

extern "C" void* declare_dB_object(char** col_types, char** col_names, int col_num, int NO_BLOCK_GROUPS){
  std::cout << "In wrapper: " << col_types[0] << std::endl;
  std::cout << "In wrapper col_num: " << col_num << std::endl;
  return declare_dB(col_types, col_names, col_num, NO_BLOCK_GROUPS);
}

extern "C" void addr_test(void* db_addr){
  dataBase* db = db_addr;
  std::cout << db->db[12201]->somehow_get(8) << std::endl;
  std::cout << db->db[12202]->somehow_get(8) << std::endl;
  std::cout << db->db[12205]->somehow_get(8) << std::endl;
  std::cout << db->db[12206]->somehow_get(8) << std::endl;
  std::cout << db->db[12207]->somehow_get(8) << std::endl;
  std::cout << db->db[12208]->somehow_get(8) << std::endl;
  std::cout << db->db[12209]->somehow_get(8) << std::endl;
  std::cout << db->db[12210]->somehow_get(8) << std::endl;
  std::cout << db->db[12211]->somehow_get(8) << std::endl;
  std::cout << db->db[12212]->somehow_get(8) << std::endl;
  std::cout << db->db[12214]->somehow_get(8) << std::endl;
  std::cout << db->db[12215]->somehow_get(8) << std::endl;
  //std::cout << "How about that" << std::endl;
}


extern "C" void start_ingest(void* db_addr, int NO_BLOCK_GROUPS, char** col_types, int col_num){
  std::cout << "In extern: " << col_types[0] << std::endl;
  ingest(db_addr, NO_BLOCK_GROUPS, col_types, col_num);
}

extern "C" char** scan_db(void* db_addr, int target_col_num, int ts_col_num, int NO_BLOCK_GROUPS, int col_num){
  std::cout << "I got scan: col: " << target_col_num << " ts: " << ts_col_num << std::endl;
  char** join = scan(db_addr, target_col_num, ts_col_num, NO_BLOCK_GROUPS ,col_num);
  /*for(int i = 0; i < NO_BLOCK_GROUPS*1024*2; i++)
    std::cout << "In wrapper before return i: " << i << " " <<join[i] << std::endl;*/

  return join;
}

/*
//THIS IS TEST//
extern "C" int intParams(int a, int b){
  return a+b;
}
//THIS IS TEST//

extern "C" char* returnQ(char* query, int Qsize){
  string quer(query);
  std::cout << "Param1: " << quer << " Param2: " << Qsize << std::endl;
  char* join = returnQ_S(query);
  return join; 
}

extern "C" int report_col_num(){
  std::cout << "This is " << std::endl;
  //std::cout << "C++: " << connection_report.col_num << std::endl;
  //return connection_report.col_num;
  return 3;
}
*/




