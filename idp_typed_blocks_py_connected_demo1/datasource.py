import connector 
import json
from flask import Flask, request, jsonify
from datetime import datetime
import time
app = Flask(__name__)


datar = [
    [622,1450754160000],  
    [365,1450774220000],
    [447,1450794220000],
]


@app.route('/')
def health_check():
    return 'Healthy'

@app.route('/search', methods=['POST'])
def search():
    return jsonify(connector.names)

@app.route('/query', methods=['POST'])
def query():
    r = request.get_json()
    print(json.dumps(r, indent=2))

    print("Target: ", r['targets'][0]['target'])
    print(type(r['targets'][0]['target']))
    
    #return jsonify([connector.types])

    ##RESPONSE TIME##
    target = r['targets'][0]['target']
    if(target == "dB Response Time"):
        data = [
        {
            "target":r['targets'][0]['target'], 
            "datapoints":[[connector.return_duration(), time.mktime(datetime.now().timetuple())]]
        }
    ]

        return jsonify(data)
    ##RESPONSE TIME##

    datarizer = connector.scan(r['targets'][0]['target'])
    #Less Weight on Dashboard
    datarizer = datarizer[:40000]
    
    res = []
    no_empty = 0
    print(len(datarizer))
    for i in range(0, len(datarizer), 2):
        if(str(datarizer[i].decode("utf-8")) == ""):
            no_empty += 1
        elif(str(datarizer[i].decode("utf-8")) == ''):
            no_empty += 1
        else:
            dt = datetime.strptime(str(datarizer[i+1].decode("utf-8")), '%Y-%m-%d %H:%M:%S')
            res.append([float(str(datarizer[i].decode("utf-8"))), time.mktime(dt.timetuple())*1000])

        
                
    print("NO_EMPTY: ", no_empty)
    #print(res)

    data = [
        {
            "target":r['targets'][0]['target'], 
            "datapoints":res
        }
    ]

    return jsonify(data)
    

@app.route('/annotations', methods=['POST'])
def annotations():
    req = request.get_json()
    data = [
        {
            "annotation": 'This is the annotation',
        "time": (convert_to_time_ms(req['range']['from']) +
                 convert_to_time_ms(req['range']['to'])) / 2,
            "title": 'Deployment notes',
            "tags": ['tag1', 'tag2'],
            "text": 'Hm, something went wrong...'
        }
    ]
    return jsonify(data)

if __name__ == '__main__':
    app.run(host='127.0.0.1', port=4005, debug=True)
