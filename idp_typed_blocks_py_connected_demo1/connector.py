from datetime import datetime
import socket
import time

from ctypes import*

##PY UTILITIES##
import csv
##PY UTILITIES##
stdc = cdll.LoadLibrary("libc.so.6") # or similar to load c library
stdcpp = cdll.LoadLibrary("libstdc++.so.6") # or similar to load c++ library
libConnect = cdll.LoadLibrary('./libConnect.so')
libConnect.connect()

'''
PRE-TRIES

print(libConnect.intParams(10,20))

#rep = c_wchar_p("first")
#print(rep.value)

libConnect.returnQ.argtypes = [c_char_p,c_int]
libConnect.returnQ.restype = c_char_p

#libConnect.report_col_num.argtypes = [c_wchar_p]
libConnect.report_col_num.restype = c_int

rep = create_string_buffer(10000)
rep = libConnect.returnQ(b"Hi there you C",25)
print(rep)


libConnect.initiate()

print(str(libConnect.report_col_num()))

PRE-TRIES
'''

'''
#####PROCEEDING OO#######

get_col_names()    DONE BY PYTHON
get_col_types()    DONE BY PYTHON
declare_db()       DONE
start_ingest()     DONE
report_col_names() DONE
wait_query()       DOING

#####PROCEEDING OO#######
'''

NO_BLOCK_GROUPS = 650

filer = open('/home/fatih/Documents/q_idp/idp/chicago-taxi-rides-2016/chicago\
_taxi_trips_2016_01.csv')

csv_reader = csv.reader(filer)
types = next(csv_reader)
names = next(csv_reader)
col_num = len(types)

print("types:", types)
print("names:", names)
print("col_num:", col_num)

libConnect.declare_dB_object.argtypes = [POINTER(c_char_p),POINTER(c_char_p),c_int,c_int]

col_types = (c_char_p * col_num)()

col_names = (c_char_p * col_num)()

for i in range(col_num):
    col_types[i] = types[i].encode('utf-8')
    col_names[i] = names[i].encode('utf-8')

##RESPONSE TIME
duration = 0
names.append("dB Response Time")
##RESPONSE TIME

print("In python: " ,col_types[0])
print("In python: " ,col_types[1])
print("In python: " ,col_types[2])

#libConnect.declare_dB_object.restype = c_void_p

#add = libConnect.declare_dB_object(col_types, col_names, col_num, NO_BLOCK_GROUPS)

creator = libConnect.declare_dB_object

creator.restype = c_void_p

db_addr = c_void_p(creator(col_types, col_names, col_num, NO_BLOCK_GROUPS))
print(type(db_addr))

#libConnect.addr_test(db_addr)

libConnect.start_ingest.argtypes = [c_void_p, c_int, POINTER(c_char_p), c_int]
libConnect.start_ingest(db_addr, NO_BLOCK_GROUPS, col_types, col_num)

libConnect.addr_test(db_addr)

#libConnect.start_ingest(NO_BLOCK_GROUPS, col_types)

k = input("Press enter to proceed reporting..")

libConnect.scan_db.argtypes = [c_void_p, c_int, c_int, c_int, c_int]
libConnect.scan_db.restype = POINTER(c_char_p)

def scan(target):
    start = time.process_time()
    datarizer_o = libConnect.scan_db(db_addr, names.index(target), names.index("trip_end_timestamp"), NO_BLOCK_GROUPS ,col_num)
    global duration
    duration = time.process_time() - start
    
    datarizer_l = []

    empty_indexes = []
    
    for i in range(0, NO_BLOCK_GROUPS*1024*2, 2):
        if(datarizer_o[i] == b''):
            empty_indexes.append(i)
            empty_indexes.append(i+1)
        elif(datarizer_o[i+1] == b''):
            empty_indexes.append(i)
            empty_indexes.append(i+1)
        else:
            datarizer_l.append(datarizer_o[i])
            datarizer_l.append(datarizer_o[i+1])
        #print("datarizer_l[i]: ", i, datarizer_l[i])
        #qprint(datarizer_l[i])

    print("Empty indexes: ", len(empty_indexes))

    current_day = datetime.now()
    who = socket.gethostname()

    to_write = str(current_day) + "," + str(duration) + "," + str(target) + "," +str(col_num) + "," + "CPU" + "," + str(who) + "\n"

    f = open("lb_data.txt","a")
    f.write(to_write)
    f.close()

    return datarizer_l
    
def return_duration():
    print("DURATION:", duration)
    return duration
