#include <iostream>
#include <string>


using namespace std;

struct node
{
	int data;
	node* next;
	node* down;
	int col;
	int row;

	node(int x,int y):col(x),row(y){};
	node(int d):data(d),next(nullptr),down(nullptr){};
};
struct row
{
	string name;
	node* next;
	row* down;
	int index;
};

struct col
{
	string name;
	col* next;
	node* down;
	int index;
	col(int x):index(x)
	{
		down=nullptr;
		next=nullptr;
		name="";
	}

	col(int x,string nm):index(x)
	{
		down=nullptr;
		next=nullptr;
		name=nm;
	}
};

class DataFrame
{
public:
	DataFrame();		//Constuctor
	 ~DataFrame();	//Destructor
	 void CreateColumns(int);
	 void insertColumnHead();
	 void nameColumn(int,string);
	 void nameColumn(string,string);
	 void nameAllColumn(string);
	 void printColumns();


private:
	row* rowhead;
	col* colhead;
	int rowNumber;
	int colNumber;
};


string ParseString(string);
void PrintString(string);
int stringElement(string);