#include <stdio.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm>
#include <omp.h>

#define vtype int
#define etype unsigned int

#define X 20
#define N (1024 * 1024 * 256)
#define TpB 256
#define nThr_pGPU (1024 * 256)

#define TIMER

using namespace std;

double computeError(float* curr_vals, float* next_vals, int nov) {
  double sum = 0;
  for(vtype i = 0; i < nov; i++) {
    float err = curr_vals[i]- next_vals[i];
    sum += err * err;
  }
  return sqrt(sum/nov);
}

void computeRWCent(etype* xadj, vtype* adj, vtype nov, float* curr_vals, float* next_vals) {
  for(vtype i = 0; i < nov; i++) {
    etype eptr = xadj[i+1];
    next_vals[i] = 0;
    for(etype ptr = xadj[i]; ptr < eptr; ptr++) {
      vtype j = adj[ptr];
      next_vals[i] += curr_vals[j] / (xadj[j+1] - xadj[j]);
      if(i < 5)
      printf("next_vals[%d]: %f\t curr_vals[%d]: %f\t Division: %d\n", i, next_vals[i], j, curr_vals[j], (xadj[j+1] - xadj[j]));
    }
  }
for (int k = 0; k < 100; k++){
  printf("curr_vals[%d]: %f\n", k, curr_vals[k]);
  printf("next_vals[%d]: %f\n", k, next_vals[k]);
 }
}

bool edgeCompare(const pair<vtype,vtype> &a,const pair<vtype,vtype> &b) {
  if(a.first == b.first) {
    return a.second < b.second;
  } else {
    return a.first < b.first;
  }
}

void printGraph(etype* xadj, vtype* adj) {
  for(int i = 0; i < 5; i++) {
    cout << i << ": ";
    etype eptr = xadj[i+1];
    for(etype e = xadj[i]; e < eptr; e++) {
      cout << adj[e] << " ";
    }
    cout << endl;
  }
}

void readTxtFile(string filename, etype* &xadj, vtype* &adj, int offset, int& nov ) {
  int no_edges = 0;
  int no_vertices = 0;
  
  vector< pair <vtype, vtype> > edges;

  std::ifstream infile(filename.c_str());
  
  std::string line;
  while (std::getline(infile, line)) {
    std::istringstream iss(line);
    vtype a, b;
    if (!(iss >> a >> b)) { continue; } // error
    //  cout << a << " " << b << endl;

    if(a != b) {
      a -= offset;
      b -= offset;

      edges.push_back(make_pair(a, b));
      edges.push_back(make_pair(b, a));
      
      if(a > no_vertices) {no_vertices = a;}
      if(b > no_vertices) {no_vertices = b;}

      no_edges += 2;
    }
  }

  sort(edges.begin(), edges.end(), edgeCompare);

  cout << "Generating graph memory for " << no_vertices << " vertices and " << no_edges << " edges" << endl;
  xadj = new etype[no_vertices + 1];
  adj = new vtype[no_edges];

  vtype* new_vertex_ids = new vtype[no_vertices];
  
  vtype prev = -1;
  nov = 0;
  for(etype i = 0; i < no_edges; i++) {
    vtype u = edges[i].first;
    vtype v = edges[i].second;

    if(u != prev) {
      xadj[nov] = i;
      new_vertex_ids[u] = nov;
      prev = u;
      nov++;
    }
    adj[i] = v;
  }
  xadj[nov] = no_edges;
  
  for(etype i = 0; i < no_edges; i++) {
    adj[i] = new_vertex_ids[adj[i]];
  }

  delete [] new_vertex_ids;
}  

int readBinaryGraph(FILE* bp, etype **pxadj, vtype**padj, vtype* pnov) {

  fread(pnov, sizeof(vtype), 1, bp);

  (*pxadj) = (etype*)malloc(sizeof(etype) * (*pnov + 1));
  fread(*pxadj, sizeof(etype), (size_t)(*pnov + 1), bp);

  (*padj) = (vtype*)malloc(sizeof(vtype) * (*pxadj)[*pnov]);
  fread(*padj, sizeof(vtype), (size_t)(*pxadj)[*pnov], bp);

  return 1;
}

int writeBinaryGraph(FILE* bp, etype *xadj, vtype *adj, vtype nov) {

  fwrite(&nov, sizeof(vtype), (size_t)1, bp);
  fwrite(xadj, sizeof(etype), (size_t)(nov + 1), bp);
  fwrite(adj, sizeof(vtype), (size_t)(xadj[nov]), bp);

  return 1;
}

int main(int argc, char** argv) {
  //variable declarations
  etype* xadj; //Vertex adjacents start locations
  vtype* adj;
  int nov; // Number of Vertices

  //handling parameters
  string filename(argv[1]);
  string bfilename = filename + ".bin";

  
  if(argc >= 3) {
    //handling graph read-write
    FILE* bp = fopen(bfilename.c_str(), "r");
    if(bp == NULL) {
      readTxtFile(filename, xadj, adj, 1, nov);
      bp = fopen(bfilename.c_str(), "w");
      writeBinaryGraph(bp, xadj, adj, nov);
      fclose(bp);
    } else {
      readBinaryGraph(bp, &xadj, &adj, &nov);
    }
    
    cout << "Number of vertices is " << nov << endl;
    cout << "Number of edges is    " << xadj[nov] << endl;
    printGraph(xadj, adj);
    
    cudaDeviceProp prop;
    for(int i = 0; i < 4; i++) {
      cudaSetDevice(i);
      cudaGetDeviceProperties(&prop, i);
      cout << "\tDevice ID: " << i;
      cout << " - props: " << prop.name << ", " << prop.major << "."
	   << prop.minor << ", " << prop.totalGlobalMem/(1000 * 1000 * 1000) << endl;
    }
    
    int nBlocks = nThr_pGPU / TpB;
    
    int ids[4];
    int noGPUs = argc - 2;
    for(int i = 2; i < argc; i++) {
      ids[i - 2] = atoi(argv[i]);
    }
    
    for(int i = 0; i < noGPUs; i++) {
      cout << ids[i] << " ";
    }
    cout << endl;

    // When I lost track
    /*for (etype xe = 0; xe < 1000; xe++){
      printf("adj %d: %d\t xadj %d: %d\n", xe, adj[xe], xe, xadj[xe]);
      }*/
    
    //computer coef
    float* curr_cent = new float[nov];
    float* next_cent = new float[nov];

    for(vtype u = 0; u < nov; u++) {
      curr_cent[u] = 1.0f / nov;
      next_cent[u] = 0;
    }
    
    int iter = 30;      
    for(int i = 0; i < iter; i++) {
      double itime = omp_get_wtime();
      computeRWCent(xadj, adj, nov, curr_cent, next_cent);
      itime = omp_get_wtime() - itime;
      cout << "Iter " << i << " took " << itime << " secs" << " - error: " << computeError(curr_cent, next_cent, nov) << endl;


      float* temp = curr_cent;
      curr_cent = next_cent;
      next_cent = temp;
    }






    int dataPerGPU = N / noGPUs;

    int counter = 0;
    double total;
    for(int i = 0; i < X; i++) {
#ifdef TIMER
      if(i != 0) {cout << "\t" << i << ": "; cout << flush;}
#endif 
      double start = omp_get_wtime();
#pragma omp parallel for num_threads(noGPUs) 
      for(int g = 0; g < noGPUs; g++) {
	int offset = g * dataPerGPU;
	cudaSetDevice(ids[g]);
	//	vectorAdd<<<nBlocks, TpB>>>(a1 + offset,
	//			    a2 + offset,
	//			    a3 + offset,
	//			    dataPerGPU);
	cudaDeviceSynchronize();
#ifdef TIMER
	if(i != 0) {cout << ++counter << " "; cout.flush();}
#endif
      }
      double end = omp_get_wtime();
      if(i != 0) {total += end - start;}
      counter = 0;
#ifdef TIMER
      if(i != 0) {cout << " - " << end - start << " secs." << endl;}
#endif
    }

    cout << "\tElapsed time: " << total << " sec." << endl;
  }
  
  return 0;
}
