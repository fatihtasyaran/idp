#ifndef QUERY_H
#define QUERY_H

#include "common.h"
#include "setup.hpp"
#include "database.hpp"
#include <omp.h>

#define location std::pair<int, int> // first->block_num second->line_index

//**QUERY MODS**//
//0 -> EQUALITY//
//1 -> LESSER THAN//
//2 -> GREATER THAN//

template <class q_class>
void query(dataBase* db, q_class val, int offset, int byte_size, type_num_col* indexes, int col_num, int totSize, int mod){
  
  int join_size = 0;
  location loc[1000];
  line* q_line = db->db_ptr[0]->block_ptr[0]->ptr; //this is to prevent redefinition
  
  unsigned char* byte_val = (unsigned char*)&val;
  unsigned char* byte_check;
  
  int byte_dum = 9092;

  std::cout << "Mod in Query: " << mod << std::endl;
  
  if(byte_size == SIZEOF_STRING){
    string str_dum = "Cash";
    byte_val = str_dum.data();
    std::cout << "I'm in!" << "\n";
    std::cout << (int)(byte_val[0]) << "\n";
    byte_size = str_dum.length();
    byte_dum = byte_size;
    std::cout << "Byte Size Here: " << byte_size << "\n";
  }
  
  std::cout << "In query, val: " << val << "\n";
  
  double start = omp_get_wtime();
  for(int i = 0; i < DB_SIZE; i++){
    for(int j = 0; j < BLOCK_SIZE; j++){
      q_line = db->db_ptr[i]->block_ptr[j];
      q_line->ptr += offset; //Go to query offset
      if(byte_size != byte_dum)
	byte_check = (unsigned char*)(q_line->ptr);
      else{
	string caster;
	caster = *(string*)q_line->ptr;
	byte_check = caster.data();
	//std::cout << "I'm Caster: " << caster << "\n"; 
	//std::cout << (int)(byte_check[0]) << "\n";
      }
      for(int k = 0; k < byte_size; k++){
	if(mod == 0){
	  if(byte_val[k] == byte_check[k])
	    ;
	  //std::cout << "Found!" << " Byte #: " << k <<"\n";
	  else  
	    break;
	  if(k == byte_size - 1){
	    loc[join_size].first = i;
	    loc[join_size].second = j;
	    join_size++;
	  }
	}
	else if(mod == 1){
	  if(byte_val[byte_size-k] <= byte_check[byte_size-k]){
	    loc[join_size].first = i;
	    loc[join_size].second = j;
	    join_size++;
	  }
	  else
	    break;
	}
	
	else if(mod == 2){
	  if(byte_val[byte_size-k] > byte_check[byte_size-k]){
	    loc[join_size].first = i;
	    loc[join_size].second = j;
	    join_size++;
	  }
	  else
	    break;
	}
	if(join_size >= 1000)
	  break;
	
      }
      q_line->ptr -= offset;
      if(join_size >= 1000)
	break;
    }
    if(join_size >= 1000)
      break;
  }
  double end = omp_get_wtime() - start;
  for(int i = 0; i < join_size; i++){
    std::cout << i << "\n";
    printOneLine(db->db_ptr[loc[i].first]->block_ptr[loc[i].second], indexes, col_num, totSize);
  }
  std::cout << "Join Size, nq: " << join_size << "\n";
  std::cout << "Estimated Query Time: " << end << "\n";
  
}



template <class q_class>
int byte_chaser(q_class to_cast){
  return sizeof(to_cast);
}

void typer(dataBase* db, type_num_col* indexes, string val, int f_index, int offset, int totSize, int col_num, int mod){
  std::cout << "In typer: " << "\n";
  std::cout << "indexes.first: " << indexes[f_index].first << " indexes.second: " << indexes[f_index].second << "\n";
  string type = indexes[f_index].first;
  
  int byte_size;
  
  if(type == "uint8_t"){
    uint8_t to_cast;
    byte_size = byte_chaser(to_cast);
    to_cast = std::stoi(val);
    query(db, to_cast, offset, byte_size, indexes, col_num, totSize, mod);
  }
  else if(type == "uint16_t"){
    uint16_t to_cast;
    byte_size = byte_chaser(to_cast);
    to_cast = std::stoi(val);
    query(db, to_cast, offset, byte_size,indexes, col_num, totSize, mod);
    }
  else if(type == "uint32_t"){
    uint32_t to_cast = std::stoi(val);
    byte_size = byte_chaser(to_cast);
    query(db, to_cast, offset, byte_size,indexes, col_num, totSize, mod);
  }
  else if(type == "double"){
    double to_cast;
    byte_size = byte_chaser(to_cast);
    to_cast = std::stod(val);
    query(db, to_cast, offset, byte_size,indexes, col_num, totSize, mod);
  }
  else if(type == "string"){
    string to_cast;
    byte_size = byte_chaser(to_cast);
    to_cast = val;
    std::cout << "In typer, val: " << val << "\n";
    std::cout << "In typer, to_cast: " << to_cast << "\n";
    query(db, to_cast, offset, byte_size,indexes, col_num, totSize, mod);
  }
  else if(type == "bool"){
    bool to_cast;
    byte_size = byte_chaser(to_cast);
  }
  else{std::cout << "No Such Type Encountered in Typer" << "\n";
    exit(1);
  }
  
  std::cout << "Byte Size of Query: " << byte_size << "\n";
  //query(db, to_cast, offset, byte_size);
}
//void query(dataBase* db, q_class val, int offset, int byte_size){

void input_parse(string input, type_num_col *indexes, int col_num ,dataBase *db, int totSize){
  int length = input.length();
  int startIndex = 0;
  int endIndex = 0;
  int f_index = -1; //To be changed
  int offset = 0; // To be changed
  int mod = 0;
  
  string val,feature;
  
  for(int i = 0; i < length+1; i++){
    if(input[i] == '='){
      endIndex = i;
      feature = input.substr(startIndex, endIndex-startIndex);
      val = input.substr(endIndex+1, length-endIndex);
      std::cout << "Equality query" << "\tfeature: " << feature << "\tval: " << val <<std::endl;
      mod = 0;
      break;
    }
    else if(input[i] == '>'){
      endIndex = i;
      feature = input.substr(startIndex, endIndex-startIndex);
      val = input.substr(endIndex+1, length-endIndex);
      std::cout << "Greater than query" << "\tfeature: " << feature << "\tval: " << val <<std::endl;
      mod = 2;
      break;
    }
    
    else if(input[i] == '<'){
      endIndex = i;
      feature = input.substr(startIndex, endIndex-startIndex);
      val = input.substr(endIndex+1, length-endIndex);
      std::cout << "Less than query" << "\tfeature: " << feature << "\tval: " << val <<std::endl;
      mod = 1;
      break;
    }
  }
  for(int i = 0; i < col_num; i++){
    offset += returnSize(indexes[i].first);
    if(feature == indexes[i].second){
      f_index = i;
      break;
    }
  }
  if(f_index == -1){
    std::cout << "No such feature, ending.." << std::endl;
    offset = 0;
  }
  else
    {
      offset -= returnSize(indexes[f_index].first);
      //query(db, indexes, feature, val, f_index, offset, totSize, col_num);
      typer(db, indexes, val, f_index, offset, totSize, col_num, mod);
    }
  
  std::cout << "Feature: " << feature << " val: " << val << " f_index: " << f_index << "\n";
  std::cout << "offset: " << offset << std::endl;
}


#endif
