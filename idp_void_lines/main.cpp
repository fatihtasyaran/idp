#include <iostream>
#include <string>
#include <stdint.h>
#include <random>
#include <omp.h>
//
#include <fstream>
#include <typeinfo>
#include "common.h"
//
#include "database.hpp"
#include "query.hpp"
#include "setup.hpp"  
//
int main()
{
  string types_line;
  string vals_line;
  fstream csv_reader;
  int col_num;
  
  csv_reader.open("chicago-taxi-rides-2016/chicago_taxi_trips_2016_01.csv");
  
  getline(csv_reader, types_line);
  
  col_num = coma(types_line);//+2; //Number of variables in database ##+1 -> line_id ##+2 -> Number of accesses
  type_num_col type_indexes[col_num];  
  /*type_indexes[0].first = "uint32_t";
  type_indexes[0].second = "line_id";

  type_indexes[1].first = "uint32_t";
  type_indexes[1].second = "accessed";*/

  int counter = 0;

  getline(csv_reader, vals_line);

  typeParse(types_line, type_indexes, 1); //Get types
  typeParse(vals_line, type_indexes, 2); //Get feature names

  //std::cout << "Seg " << ++counter <<std::endl;

  int totSize = 0;
  
  for(int i = 0; i < col_num; i++){              //Find the total size of one line pointer
    totSize += returnSize(type_indexes[i].first);
    std::cout << type_indexes[i].first << " Returned: " << returnSize(type_indexes[i].first)*8 << " totSize: " << totSize << std::endl;
  }

    
  for(int i = 0; i < col_num; i++){
    std::cout << "Column:" << i << "\tType: " << type_indexes[i].first << "\tColName: " << type_indexes[i].second << std::endl;
    std::cout << std::endl;
  }
  
  std::cout << "Total Size:" << totSize << std::endl; 
  //std::cin.get();
  //std::cin.ignore();

    
  //line* first = new line(totSize);
  //line* second = new line(totSize);
  //line* third = new line(totSize);
  
  //writeToOneLine(vals_line, first, type_indexes, totSize);
  //printOneLine(first, type_indexes, col_num, totSize);
  
  //delete first;
  
  /*block* f_block = new block(totSize);

  for(int l = 0; l < 1024; l++){
    getline(csv_reader, vals_line);
    writeToOneLine(vals_line, f_block->block_ptr[l], type_indexes, totSize);
  }

  for(int l = 0; l < 1024; l++){
    if(l == 900)
      
  }
  
  delete f_block;*/

  /*dataBase* db = new dataBase(totSize);
  
  for(int b = 0; b < 30; b++){
    for(int l = 0; l < 1024; l++){
      getline(csv_reader, vals_line);
      writeToOneLine(vals_line, db->db_ptr[b]->block_ptr[l], type_indexes, totSize);
    }
  }
  /*std::cout << "This halts" << std::endl;
  printOneLine(db->db_ptr[20]->block_ptr[238], type_indexes, col_num, totSize);
  printOneLine(db->db_ptr[20]->block_ptr[239], type_indexes, col_num, totSize);
  printOneLine(db->db_ptr[20]->block_ptr[240], type_indexes, col_num, totSize);
  std::cout << "One block id: " << db->db_ptr[20]->id << std::endl; */

  string input;

  /*while(std::cin >> input){
    if(input == "end")
      break;
    input_parse(input, type_indexes, col_num, db, totSize);
    }*/
  
  //delete db;
  
  csv_reader.close();
  
  //std::cin.get();
  //std::cin.ignore();
  return 0;
}



  
  
