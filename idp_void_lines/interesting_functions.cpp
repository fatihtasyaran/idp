bool check(line* c_line, string type, string val){
  //c_line += offset;

  int t = 1;
  if(t){
  printByte(c_line, 8);
  printByte(val);
  t=0;
  }

  if(type == "string"){
    string str_val = *((string*)c_line->ptr);
    if(str_val == val){
      return true;
    }		       
  }
  else if(type == "uint8_t"){
    uint8_t u8_val = *((uint8_t*)c_line->ptr);
    uint8_t casted_val = std::stoi(val);
    if(casted_val == u8_val){
      return true;
    }		       
  }
  else if(type == "uint16_t"){
    uint16_t u16_val = *((uint16_t*)c_line->ptr);
    uint16_t casted_val = std::stoi(val);
    //std::cout << "val: " << val << " casted val: " << casted_val << " u16_val: " << u16_val << "\n"; 
    if(casted_val == u16_val){
      return true;
    }		       
  }
  else if(type == "uint32_t"){
    uint32_t u32_val = *((uint32_t*)c_line->ptr);
    uint32_t casted_val = std::stoi(val);
    if(casted_val == u32_val){
      return true;
    }		       
  }
  else if(type == "double"){
    double double_val = *((double*)c_line->ptr);
    double casted_val = std::stod(val);
    /*bool eq = (casted_val==double_val);
    if(eq)
    std::cout << "double_val: " << double_val << "casted_val: " << casted_val << "Equal?: " << eq <<"\n";*/
    if(casted_val == double_val){
      return true;
     }		       
  }
  else if(type == "bool"){
    bool bool_val = *((bool*)c_line->ptr);
    string casted_val;
    if(bool_val)
      casted_val = "true";
    else
      casted_val = "false";
    if(casted_val == val){
      return true;
    }		       
  }
  else{
    std::cout << "I don't have type: " << type << "\n";
  }
  //c_line -= offset;
  return false;
}

void query(dataBase* db, type_num_col *indexes, string feature, string val, int f_index, int offset, int totSize, int col_num){
  double start = omp_get_wtime();
  int no_results = 0;
  int size_join = 0;
  string type = indexes[f_index].first;
  location* loc = new location[1000];
  
  for(int i = 0; i < NO_BLOCKS; i++){
    for(int j = 0; j < 1024; j++){   //1024->block size
      db->db_ptr[i]->block_ptr[j]->ptr += offset; //Go to "the place"
      //std::cout << *((int*)db->db_ptr[i]->block_ptr[j]) << std::endl;
      if(check(db->db_ptr[i]->block_ptr[j], type, val)){
	std::cout << check(db->db_ptr[i]->block_ptr[j], type, val) << "\n";
	//db->db_ptr[i]->block_ptr[j]->ptr -= offset;
	//printOneLine(db->db_ptr[i]->block_ptr[j], indexes, col_num, totSize);
	//db->db_ptr[i]->block_ptr[j]->ptr += offset;
	loc[size_join].first = i;
	loc[size_join].second = j;
	size_join++;
	//*join = db->db_ptr[i]->block_ptr[j];
      }
      db->db_ptr[i]->block_ptr[j]->ptr -= offset; //Back from "the place"
      if(size_join >= 1000)
	break;
    }
    if(size_join >= 1000)
      break;
  }
  //*join--;
  //printOneLine(*join, indexes, col_num, totSize);
  double qtime = omp_get_wtime() - start;
  for(int i = size_join; i > 0; i--){
    std::cout << i << "\n";
    printOneLine(db->db_ptr[loc[i].first]->block_ptr[loc[i].second], indexes, col_num, totSize);
  }
  std::cout << "Done!" << "\n";
  std::cout << "Size of Join: " << size_join << "\n";
  std::cout << "Elapsed Query Time: " << qtime << "\n";
  delete[] loc;
}

