#ifndef SETUP_H
#define SETUP_H

#include "database.hpp"
//#include "query.hpp"
#include "common.h"

int returnSize(string type) {
  if(type == "uint8_t")
    return sizeof(UINT8);
  else if(type == "uint16_t")
    return sizeof(UINT16);
  else if(type == "uint32_t")
    return sizeof(UINT32);
  else if(type == "string")
    return SIZEOF_STRING;
  else if(type == "double")
    return sizeof(DOUBLE);
  else if(type == "bool")
    return sizeof(BOOL);
  else if(type == "NONE")
    return 0;
  else
    std::cout << "I don't have " << type << "type" << std::endl;
}


int coma(string line) {
  
  int comas = 1;
  int length = line.length();
    
  for(int i = 0; i < length; i++){
    if(line[i] == ',')
      comas++;
  }
  return comas;
}

void typeParse(string line, type_num_col* indexes, int mod) {
  string type;
  
  int length = line.length();
  int current_col = 0;
  int startIndex = 0;
  int endIndex = 0;

  
  for(int i = 0; i < length+1; i++){
    if(line[i] == ',' || i == length){
      endIndex = i;
      
      if(mod == 1){
	indexes[current_col].first = line.substr(startIndex, endIndex-startIndex);  //Type
      }
      else{
	indexes[current_col].second = line.substr(startIndex, endIndex-startIndex); //Feature name
      }

      
      startIndex = endIndex+1;
      current_col++;
      
      //std::cout << "Current Col: " << current_col << "String: " << line.substr(startIndex, endIndex) << std::endl; 
    }
  }
}


void writeToOneLine(string vals_line, line *p_line, type_num_col* indexes, int totSize){
  int length = vals_line.length();
  string type;
  string val;
  int current_col = 2;
  int startIndex = 0;
  int endIndex = 0;

  //Giving id
  *((uint32_t*)p_line->ptr) = get_id();
  p_line->ptr += returnSize("uint32_t");

  //Access start at 0
  *((uint32_t*)p_line->ptr) = (uint32_t)0;
  p_line->ptr += returnSize("uint32_t");
  
  for(int i = 0; i < length+1; i++){
    if(vals_line[i] == ',' || i == length){
      endIndex = i;
      val = vals_line.substr(startIndex, endIndex - startIndex);
      type = indexes[current_col].first;
      
      if(val == EMPTY){
	//std::cout << "val:" << val << "space" << std::endl;
      }
      else
	{
	  
	  if(type == "uint8_t"){
	    int t_val = std::stoi(val);
	    //std::cout << "uint8_t val: " << t_val << std::endl;
	    *((uint8_t*)p_line->ptr) = (uint8_t)t_val;
	  }
	  else if(type == "uint16_t"){
	    int t_val = std::stoi(val);
	  //std::cout << "uint16_t val: " << t_val << std::endl;
	    *((uint16_t*)p_line->ptr) = (uint16_t)t_val;
	}
	  else if(type == "uint32_t"){
	    int t_val = std::stoi(val);
	    //std::cout << "uint32_t val: " << t_val << std::endl;
	  *((uint32_t*)p_line->ptr) = (uint32_t)t_val;
	  }
	  else if(type == "string"){
	    //std::cout << "string val:" << val << std::endl;
	    *((string*)p_line->ptr) = val;
	  }
	  else if(type == "double"){
	    double t_val = std::stod(val);
	    //std::cout << "double val: " << t_val << std::endl;
	    *((double*)p_line->ptr) = t_val;
	  }
	  else if(type == "bool"){
	    bool t_val;
	    if(val == "true")
	      t_val = true;
	    else
	      t_val = false;
	  //std::cout << "bool val: " << t_val << std::endl;
	    *((bool*)p_line->ptr) = (bool)t_val;
	  }
	}
      current_col++;
      p_line->ptr += returnSize(type);
      startIndex = endIndex + 1;
      
    }
  }
  p_line->ptr -= totSize;
}

void printOneLine(line *p_line, type_num_col* indexes, int col_num, int totSize){

  string type;
  string name;
  
  for(int i = 0; i < col_num; i++){
    type = indexes[i].first;
    name = indexes[i].second;
    if(type == "uint8_t"){
      std::cout << "Column: " << name << ": " <<*((uint8_t*)p_line->ptr) << std::endl;
    }
    else if(type == "uint16_t"){
      std::cout << "Column: " << name << ": " <<*((uint16_t*)p_line->ptr) << std::endl;     
    }
    else if(type == "uint32_t"){
      std::cout << "Column: " << name << ": " <<*((uint32_t*)p_line->ptr) << std::endl;     
    }
    else if(type == "string"){
      std::cout << "Column: " << name << ": " <<*((string*)p_line->ptr) << std::endl;     
    }
    else if(type == "double"){
      std::cout << "Column: " << name << ": " <<*((double*)p_line->ptr) << std::endl;     
    }
    else if(type == "bool"){
      std::cout << "Column: " << name << ": " <<*((bool*)p_line->ptr) << std::endl;     
    }
    p_line->ptr += returnSize(type);
  }
  p_line->ptr -= totSize;
}

void printByte(line *p_line, int totSize){
  unsigned char *ptr = p_line->ptr;
  for (int i = 0; i < totSize; i++) {
    printf("%02x ", (int)ptr[i]);
  }
  printf("\n");
}

void printByte(string val){
  int casted = std::stoi(val);
  unsigned char *ptr = casted;
  std::cout << ptr[0] << "\n";
}


#endif
