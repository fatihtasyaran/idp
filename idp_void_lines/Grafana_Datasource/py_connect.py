#! /usr/local/bin/python
# -*- coding: utf-8 -*-
from calendar import timegm
from datetime import datetime
import _strptime  # https://bugs.python.org/issue7980
from flask import Flask, request, jsonify
app = Flask(__name__)

app.debug = True

from random import randint

def convert_to_time_ms(timestamp):
    return 1000 * timegm(datetime.strptime(timestamp, '%Y-%m-%dT%H:%M:%S.%fZ').timetuple())



@app.route('/')
def health_check():
    return 'This datasource is healthy.'


@app.route('/search', methods=['POST'])
def search():
    return jsonify(['Series1', 'Series2'])


data_points1 = []
data_points2 = []
req_arr = []
@app.route('/query', methods=['POST'])
def query():
    req = request.get_json()
    ctr = 0
    ctr = ctr+1
    req_arr.append(req)
    print("req: ", req)
    num = req['maxDataPoints']
    data_points1.append([randint(1,1000), convert_to_time_ms(req['range']['from'])])
    data_points1.append([randint(1,1000), convert_to_time_ms(req['range']['to'])])

    data_points2.append([randint(1,1000), convert_to_time_ms(req['range']['from'])])
    data_points2.append([randint(1,1000), convert_to_time_ms(req['range']['to'])])
    data = [
        {
            "target": req['targets'][0]['target'],
            #"datapoints": [[randint(1,1000), convert_to_time_ms(req['range']['from'])],
            #[randint(1,1000), convert_to_time_ms(req['range']['to'])]]
            "datapoints": data_points1
            
        }
        ,
        {
            "target": req['targets'][1]['target'],
            #"datapoints": [[randint(1,1000), convert_to_time_ms(req['range']['from'])],
            #[randint(1,1000), convert_to_time_ms(req['range']['to'])]]
            "datapoints": data_points2
            
        }
    ]
    #print(data)
    return jsonify(data)


@app.route('/annotations', methods=['POST'])
def annotations():
    req = request.get_json()
    data = [
        {
            "annotation": 'This is the annotation',
            "time": (convert_to_time_ms(req['range']['from']) +
                     convert_to_time_ms(req['range']['to'])) / 2,
            "title": 'Deployment notes',
            "tags": ['tag1', 'tag2'],
            "text": 'Hm, something went wrong...'
        }
    ]
    return jsonify(data)


@app.route('/tag-keys', methods=['POST'])
def tag_keys():
    data = [
        {"type": "string", "text": "City"},
        {"type": "string", "text": "Country"}
    ]
    return jsonify(data)


@app.route('/tag-values', methods=['POST'])
def tag_values():
    req = request.get_json()
    if req['key'] == 'City':
        return jsonify([
            {'text': 'Tokyo'},
            {'text': 'São Paulo'},
            {'text': 'Jakarta'}
        ])
    elif req['key'] == 'Country':
        return jsonify([
            {'text': 'China'},
            {'text': 'India'},
            {'text': 'United States'}
        ])


if __name__ == '__main__':
    app.run()
