#ifndef DATABASE_H
#define DATABASE_H

#include "common.h"

#define NO_BLOCKS 30
extern int G_SIZE;

static uint32_t line_ids = -1;
static uint32_t block_ids = -1;


uint32_t get_id()
{
  return ++line_ids;
}

uint32_t get_block_id()
{
  return ++block_ids;
}

/*struct line{
  void *ptr;
  
  
  line(int SIZE)
  {
    //std::cout << "Size: " << SIZE << std::endl;
    ptr = malloc(SIZE);
  }
  
  line()
  {
    std::cout << "G_Size: " << G_SIZE << std::endl;
    ptr = malloc(G_SIZE);
    }

  ~line()
  {
    delete ptr;
    //std::cout << "One line deleted from memory." << std::endl;
  }
  
  };*/

template <class T>
struct block{
  T block_ptr[1024];
  uint32_t id;
  uint32_t accessed;

  block(int LINE_SIZE)
  {
    for(int i = 0; i < 1024; i++){
      block_ptr[i] = new line(LINE_SIZE);
    }
    size = LINE_SIZE;
    id = get_block_id();
    accessed = 0;
  }

  ~block()
  {
    for(int i = 0; i < 1024; i++){
      delete block_ptr[i];
    }
  }
};

struct dataBase{
  block* db_ptr[NO_BLOCKS];

  dataBase(int LINE_SIZE)
    {
      for(int i = 0; i < NO_BLOCKS; i++){
	db_ptr[i] = new block(LINE_SIZE);
      }
    }

  ~dataBase()
    {
      for(int i = 0; i < NO_BLOCKS; i++){
	delete db_ptr[i];
      }
    }
};


#endif
