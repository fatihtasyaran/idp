#include <iostream>
#include "connector.h"
#include "database.hpp"

#define cudaCheckErrors(msg) \
    do { \
        cudaError_t __err = cudaGetLastError(); \
        if (__err != cudaSuccess) { \
            fprintf(stderr, "Fatal error: %s (%s at %s:%d)\n", \
                msg, cudaGetErrorString(__err), \
                __FILE__, __LINE__); \
            fprintf(stderr, "*** FAILED - ABORTING\n"); \
            exit(1); \
        } \
    } while (0)

//==========================GPU_SCAN======================================//

__global__ char** GPU_scan(dataBase* db_addr, int target_col_num, int ts_col_num, int NO_BLOCK_GROUPS, int col_num){
  std::cout << "Let's make a GPU call" << std::endl;

  int id = (blockIdx.x * blockDim.x) + threadIdx.x
//std::cout << "Scanning for: " << target_col_num << std::endl;
  
  dataBase* dB = db_addr;

  //int max_threads = omp_get_max_threads();
  

  int total_elem = NO_BLOCK_GROUPS*1024*2;
  int per_thread = total_elem/max_threads;
  
  char** join = new char*[NO_BLOCK_GROUPS*1024*2];
  for(int i = 0; i < NO_BLOCK_GROUPS*1024*2; i++){
    join[i] = new char[100];
  }

  int tso = ts_col_num - target_col_num;  //TIMESERIES OFFSET
  
  int group_per_thread = NO_BLOCK_GROUPS/max_threads;

//#pragma omp parallel num_threads(max_threads)//(max_threads)
//  {
    int tid = omp_get_thread_num();
    int start_group = group_per_thread*tid;
    int last_group = start_group + group_per_thread; //THIS IS LIMIT, NOT INCLUDED
    int join_index = start_group * 1024 * 2;
    
    if(tid == (max_threads - 1))
      last_group = NO_BLOCK_GROUPS - 1;
    
    for(int b = start_group; b < last_group; b++){
      for(int i = 0; i < 1024; i++){
	int target_block_index = b*col_num + target_col_num;
	int timeseries_block_index = b*col_num + ts_col_num;
	
	strcpy(join[join_index], dB->db[target_block_index]->somehow_get(i).c_str());
	strcpy(join[join_index + 1], dB->db[timeseries_block_index]->somehow_get(i).c_str());
	join_index += 2;
      }
    }
#pragma omp barrier
//  }
  
  std::cout << "Scan done!" << std::endl;
  return join;
}

//========================================================================//

//==========================GPU_SCAN_FILTER===============================//

__global__ char** scan_with_filter(void* db_addr, int target_col_num, int ts_col_num, int NO_BLOCK_GROUPS, int col_num, char* target_value){
  std::string str_value(target_value);
  
  std::cout << "Scanning for: " << target_col_num << std::endl;
  dataBase* dB = db_addr;

  int id = (blockIdx.x * blockDim.x) + threadIdx.x

  //int max_threads = omp_get_max_threads();
  //int max_threads = 1;

  int total_elem = NO_BLOCK_GROUPS*1024*2;
  int per_thread = total_elem/max_threads;
  
  char** join = new char*[NO_BLOCK_GROUPS*1024*2];
  for(int i = 0; i < NO_BLOCK_GROUPS*1024*2; i++){
    join[i] = new char[100];
  }

  int tso = ts_col_num - target_col_num;  //TIMESERIES OFFSET
  
  int group_per_thread = NO_BLOCK_GROUPS/max_threads;

//#pragma omp parallel num_threads(max_threads)//(max_threads)
//  {
    int tid = omp_get_thread_num();
    int start_group = group_per_thread*tid;
    int last_group = start_group + group_per_thread; //THIS IS LIMIT, NOT INCLUDED
    int join_index = start_group * 1024 * 2;
    
    if(tid == (max_threads - 1))
      last_group = NO_BLOCK_GROUPS - 1;
    
    for(int b = start_group; b < last_group; b++){
      for(int i = 0; i < 1024; i++){
	int target_block_index = b*col_num + target_col_num;
	int timeseries_block_index = b*col_num + ts_col_num;

	//std::cout << "dB_val: " << dB->db[target_block_index]->somehow_get(i).c_str() << " target_val: " << str_value << std::endl;
	std::string response(dB->db[target_block_index]->somehow_get(i).c_str());
	
	if(response == target_value){	  
	  strcpy(join[join_index], dB->db[target_block_index]->somehow_get(i).c_str());
	  strcpy(join[join_index + 1], dB->db[timeseries_block_index]->somehow_get(i).c_str());
	  join_index += 2;
	}
      }
    }
#pragma ompOA barrier
// }


  
  std::cout << "Scan done!" << std::endl;
  return join;
}

//========================================================================//
