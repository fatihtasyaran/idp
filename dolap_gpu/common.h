//DEFINITIONS
#include <iostream>
#include <string>
#include <stdint.h>
#include <random>

//#define string std::string
#define NONE "NONE"

//#define string std::string
#define fstream std::fstream

#define EMPTY ""

#define type_num_col std::pair<std::string,std::string>

#define SIZEOF_STRING 32

#define BLOCK_SIZE 1024
#define DB_SIZE 30



//#define NO_BLOCK_GROUPS 500

typedef uint8_t  UINT8;
typedef uint16_t UINT16;
typedef uint32_t UINT32;
typedef std::string   STRING;
typedef double DOUBLE;
typedef float FLOAT;
typedef bool BOOL;

/***************************************************
std::cout << "UINT8T: "<< sizeof(UINT8) << std::endl;
std::cout << "UINT16T: "<< sizeof(UINT16) << std::endl;
std::cout << "UINT32T: "<< sizeof(UINT32) << std::endl;
std::cout << "STRING: " << sizeof(STRING) << std::endl;
std::cout << "DOUBLE: " << sizeof(DOUBLE) << std::endl;
std::cout << "FLOAT: " << sizeof(FLOAT) << std::endl;
std::cout << "BOOL: " << sizeof(BOOL) << std::endl;
****************************************************/


