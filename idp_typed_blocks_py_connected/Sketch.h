#pragma once
#include <iostream>
#include <algorithm>
#include <vector>
#include <array>
#include "MurmurHash3.h"
#include <time.h>
#include "commonn.h"
#include "generator.h"
#include <cmath>
#include <cstdlib>
#include <fstream>
#include "Hasher.h"
#include "omp.h"
#include <chrono>
#include <unistd.h>

using namespace std;

clock_t start, endt;

// Two const doubles for the card estimation process
static double pow_2_32 = 4294967296.0; ///< 2^32
static double neg_pow_2_32 = -4294967296.0; ///< -(2^32)


template <class T>
class sketch {

protected :
	unsigned long long no_stream, no_unique;
	unsigned no_max_threads;
	double epsilon, delta;
	int distNum;
	double zipfAlp;

	//HyperLogLog vars//
	uint8_t bit_width; // register bit width
	uint32_t reg_size; // register size
	double alphaMM; // alpha * m^2
	std::vector<uint8_t> M_regs; // registers
	//================//
	//Bloom Filter's Vars//
	uint8_t n_num_hashes;
	std::vector<uint8_t> m_bits;    // Getting only a vector of bits which has one bit for each item (space efficiency)
	double fpp;
	//================//
public :

	//Constructor for HLL
	sketch(uint8_t b = 4) /*throw (std::invalid_argument)*/ :
		bit_width(b), reg_size(1 << b), M_regs(reg_size, 0) {}
	
	//Constructor for BF
	sketch(uint64_t size, uint8_t numHashes, double falsePosProb) :
		n_num_hashes(numHashes),
		m_bits(size),
		fpp(falsePosProb)
	{
		//sketch BF constructor
	};

	std::array<uint64_t, 2> hashFunc(T item, std::size_t length) {

		std::array<uint64_t, 2> hashValue;
		MurmurHash3_x64_128(item, length, 0, hashValue.data());

		return hashValue;

	}

	uint64_t hash_to_vec(uint8_t n,
		uint64_t hashA,
		uint64_t hashB,
		uint64_t filterSize) {
		return (hashA + n * hashB) % filterSize;
	}

	std::vector<uint32_t> static generate_sizes(std::vector<uint32_t> elements_num, double fpp) {
		std::vector<uint32_t> sizes;
		for (int i = 0; i < elements_num.size(); i++) {
			sizes.push_back((-1)*(elements_num[i] * log(fpp)) / (pow(log(2), 2)));
		}

		return sizes;
	}


	std::vector<int> static optimal_hash_num(std::vector<uint32_t> m, std::vector<uint32_t> n) {
		std::vector<int> hash_funcs;
		for (int i = 0; i < m.size(); i++) {
			hash_funcs.push_back((m[i] / n[i]) * log(2));
		}

		return hash_funcs;
	}


	int parseInputs(int argc, char** argv) {
		if (argc >= 6) {
			no_stream = pow(2, atoi(argv[1]));
			no_unique = pow(2, atoi(argv[2]));
			epsilon = atof(argv[3]);
			delta = atof(argv[4]);
			no_max_threads = atoi(argv[5]);
			distNum = atoi(argv[6]);
			zipfAlp = atof(argv[7]);
			return 0;
		}
		else {
			return -1;
		}
	}

	
	/* HyperLogLog */

#define HLL_HASH_SEED 313
	inline uint8_t _get_leading_zero_count(unsigned long x, uint8_t b) {

	#if defined (_MSC_VER)
		unsigned long leading_zero_len = 32;
		::_BitScanReverse(&leading_zero_len, x);
		--leading_zero_len;
		return std::min(b, (uint8_t)leading_zero_len);
	#else
		uint8_t v = 1;
		while (v <= b && !(x & 0x80000000)) {
			v++;
			x <<= 1;
		}
		return v;
	#endif

	}

	#define _GET_LEADING_ZERO_C(x, b) _get_leading_zero_count(x, b)


	virtual void insert(T* item, uint32_t len) {}
	
	/*Build the HLL sketch
	* using the insert function
	*/
	virtual void build_sketch(dtype* data, uint32_t len, uint32_t n_elements) {}

	/**
	 * Estimates cardinality value.
	 *
	 * @return Estimated cardinality value.
	 */
	virtual double Estimate_card() const {
		return 0.0;
	}

	/**
	 * Clears all internal registers.
	 */
	virtual void clear() {}

	/**
	 * Returns size of register.
	 *
	 * @return Register size
	 */
	virtual uint32_t registerSize() const {
		return reg_size; 
	}

	virtual void HyperLogLog_run(int argc, char** argv, uint32_t elements) {}

	//=============================END OF HYPERLOGLOG=================================//

	//================================BLOOM FILTER====================================//

	virtual int std_bloom_filter(int argc, char** argv) {

	if (this->parseInputs(argc, argv) == -1) {
			std::cout << "Wrong number of arguments" << std::endl;
			std::cout << "Usage: executable [log2(stream size)] [log2(universal set size)] [epsilon] [delta] [no max threads] [distribution no] " << std::endl;
			std::cout << "\tNORMAL 1\n\tUNIFORM 2\n\tPOISSON 3\n\tEXPONENTIAL 4" << std::endl;
			return -1;
		}

		//std::vector<uint32_t> sizes = { 6236, 12471, 31176, 62352, 623522, 6235224, 12470448,
			//12470448, 40265318, 40265318, 40265318, 187056726, 120000000, 120000000 };
		std::vector<uint32_t> elements_num = { 1 << 10, 2 * 1 << 10, 4 * 1 << 10, 8 * 1 << 10, 1 << 20, 2 * 1 << 20, 4 * 1 << 20, 8 * 1 << 20/*, 1 << 25 */ };
		//std::vector<int> num_hashes = { 4, 4, 4, 4, 4, 4, 4, 5, 16, 5, 3, 4, 4, 2 };
		const float fpp = 0.05;
		clock_t time;
		dtype* data;

		//Generate the various filter sizes and its corresponding hash function numbers
		std::vector<uint32_t> sizes = sketch::generate_sizes(elements_num, fpp);
		std::vector<int> num_hashes = sketch::optimal_hash_num(sizes, elements_num);

		// Generate the data
		generateData(*&data, this->no_stream, this->no_unique, this->distNum, this->zipfAlp);
		cout << "The number of items is:" << this->no_stream << endl;


		std::ofstream Log;
		Log.open("log.csv");
		Log << "Number of inserted elements(n)" << "," << "Filter Size (m) in bits" << "," << "Filter Size (m) in bytes" << ","
			<< "Filter Size (m) in Mbytes" << "," << "Number of Hash Funcs(k)" << "," << "Building Time(s)" << "," << "FPP = 5% "
			<< endl;
		uint32_t a = 20;
		uint32_t b = 432;

		//Build the filter for different sizes
		for (int iter = 0; iter < sizes.size(); iter++) {

			sketch bf(sizes[iter], num_hashes[iter], 0.05); // (m,k)
			time = clock();
			bf.build_filter(*&data, 64, elements_num[iter]); //(,,n) 
			time = clock() - time;

			cout << "It took " << ((float)time) / CLOCKS_PER_SEC << " secs to build the filter" << endl;
			cout << "The memory used for " << elements_num[iter] << " is " << sizes[iter] << " bits Requiring "
				<< num_hashes[iter] << " hash functions" << endl;
			float size_bits = (-1)*(elements_num[iter] * (log(0.05) / (pow(log(2), 2))));
			float size_bytes = size_bits / 8;
			float size_Mbytes = size_bits / (8 * 1024 * 1024);
			Log << elements_num[iter] << "," << size_bits << "," << size_bytes << "," << size_Mbytes << "," << num_hashes[iter]
				<< "," << ((float)time) / CLOCKS_PER_SEC << "," << fpp << endl;

		}


		Log.close();


	}

	virtual void build_filter(dtype* data, std::size_t len, uint32_t n) {

		//BFilter bf(n, 3);
		cout << "The number of inserted items is " << n << endl;
#pragma omp parallel for
		for (int i = 0; i < n; i++) {
			insert(data[i], len);
		}
		//return bf;
	}


	//=================Filter's functions====================//
	virtual void insert(T item, std::size_t len) {
		auto hashValues = this->hashFunc((T)item, len);

		for (int n = 0; n < n_num_hashes; n++) {
			m_bits[this->hash_to_vec(n, hashValues[0], hashValues[1], m_bits.size())] = 1;
		}
	}

	virtual bool query(T item, std::size_t len) {
		auto hashValues = this->hashFunc((T)item, len);

		for (int n = 0; n < n_num_hashes; n++) {
			if (m_bits[this->hash_to_vec(n, hashValues[0], hashValues[1], m_bits.size())] != 1) {
				return false;
			}
		}

		return true;
	}


	//=============================END OF BLOOM FILTER================================//
};


template <class T>
class BFilter : public sketch<T>
{

protected:
	uint8_t n_num_hashes;
	std::vector<uint8_t> m_bits;    // Getting only a vector of bits which has one bit for each item (space efficiency)
	double fpp;

public:
	//BFilter(uint64_t BF_size, uint8_t num_hashes);

	//void insert(const uint8_t *item, std::size_t length);
	//bool query(const uint8_t *item, std::size_t length) const;


	int std_bloom_filter(int argc, char** argv) {

		if (this->parseInputs(argc, argv) == -1) {
			std::cout << "Wrong number of arguments" << std::endl;
			std::cout << "Usage: executable [log2(stream size)] [log2(universal set size)] [epsilon] [delta] [no max threads] [distribution no] " << std::endl;
			std::cout << "\tNORMAL 1\n\tUNIFORM 2\n\tPOISSON 3\n\tEXPONENTIAL 4" << std::endl;
			return -1;
		}

		//std::vector<uint32_t> sizes = { 6236, 12471, 31176, 62352, 623522, 6235224, 12470448,
			//12470448, 40265318, 40265318, 40265318, 187056726, 120000000, 120000000 };
		std::vector<uint32_t> elements_num = { 1 << 10, 2 * 1 << 10, 4 * 1 << 10, 8 * 1 << 10, 1 << 20, 2 * 1 << 20, 4 * 1 << 20, 8 * 1 << 20, 1 << 25 };
		//std::vector<int> num_hashes = { 4, 4, 4, 4, 4, 4, 4, 5, 16, 5, 3, 4, 4, 2 };
		const float fpp = 0.05;
		clock_t time;
		dtype* data;

		//Generate the various filter sizes and its corresponding hash function numbers
		std::vector<uint32_t> sizes = BFilter::generate_sizes(elements_num, fpp);
		std::vector<int> num_hashes = BFilter::optimal_hash_num(sizes, elements_num);

		// Generate the data
		generateData(*&data, this->no_stream, this->no_unique, this->distNum, this->zipfAlp);
		cout << "The number of items is:" << this->no_stream << endl;


		std::ofstream Log;
		Log.open("log.csv");
		Log << "Number of inserted elements(n)" << "," << "Filter Size (m) in bits" << "," << "Filter Size (m) in bytes" << ","
			<< "Filter Size (m) in Mbytes" << "," << "Number of Hash Funcs(k)" << "," << "Building Time(s)" << "," << "FPP = 5% "
			<< endl;
		uint32_t a = 20;
		uint32_t b = 432;

		//Build the filter for different sizes
		for (int iter = 0; iter < sizes.size(); iter++) {
			time = 0;
			BFilter bf(sizes[iter], num_hashes[iter], 0.05); // (m,k)
		//	time = clock();
			auto start = std::chrono::high_resolution_clock::now();
			bf.build_filter(*&data, 128, elements_num[iter]); //(,,n) 
			auto finish = std::chrono::high_resolution_clock::now();
		//	time = clock() - time;

	//		cout << "It took " << (float)(time) / CLOCKS_PER_SEC << " secs to build the filter" << endl;
std::chrono::duration<double> elapsed = finish - start;
std::cout << "Elapsed time: " << elapsed.count() << " s\n";
			cout << "The memory used for " << elements_num[iter] << " is " << sizes[iter] << " bits Requiring "
				<< num_hashes[iter] << " hash functions" << endl;
			float size_bits = (-1)*(elements_num[iter] * (log(0.05) / (pow(log(2), 2))));
			float size_bytes = size_bits / 8;
			float size_Mbytes = size_bits / (8 * 1024 * 1024);
			Log << elements_num[iter] << "," << size_bits << "," << size_bytes << "," << size_Mbytes << "," << num_hashes[iter]
				<< "," << ((float)time) / CLOCKS_PER_SEC << "," << fpp << endl;

		}


		Log.close();

	}


	void build_filter(dtype* data, std::size_t len, uint32_t n) {
		//BFilter bf(n, 3);
		cout << "The number of inserted items is " << n << endl;

#pragma omp parallel num_threads(32)
#pragma omp for
		for (int i = 0; i < n; i++) {
			insert((T)data[i], len);
		}
		//return bf;
	}

	BFilter(uint64_t size, uint8_t numHashes, double falsePosProb) :
		n_num_hashes(numHashes),
		m_bits(size),
		fpp(falsePosProb), sketch<T>(size, numHashes, falsePosProb)
	{
		//cout << "Bloom filter Creation" << endl << "--------------------" << endl;

	};


	//=================Filter's functions====================//
	void insert(T item, std::size_t len) {
		auto hashValues = this->hashFunc(item, len);

		for (int n = 0; n < n_num_hashes; n++) {
			m_bits[this->hash_to_vec(n, hashValues[0], hashValues[1], m_bits.size())] = 1;
		}
	}

	bool query(T*item, std::size_t len) {
		auto hashValues = this->hashFunc(item, len);

		for (int n = 0; n < n_num_hashes; n++) {
			if (m_bits[this->hash_to_vec(n, hashValues[0], hashValues[1], m_bits.size())] != 1) {
				return false;
			}
		}

		return true;
	}


	virtual ~BFilter()
	{};


};


template <class T>
class HyperLogLog : public sketch<T>{
public:

	
	/**
	 * Constructor
	 *
	 * @param[in] b bit width (register size will be 2 to the b power).
	 *            This value must be in the range[4,30].Default value is 4.
	 *
	 * @exception std::invalid_argument the argument is out of range.
	 */

	HyperLogLog(uint8_t b ) /*throw (std::invalid_argument)*/ :
		bit_width(b), reg_size(1 << b), M_regs(reg_size, 0), sketch<T>(b) {
		

		if (b < 4 || 32 < b) {
			throw std::invalid_argument("bit width must be in the range [4,32]");
		}

		double alpha;
		switch (this->reg_size) {
		case 16:
			alpha = 0.673;
			break;
		case 32:
			alpha = 0.697;
			break;
		case 64:
			alpha = 0.709;
			break;
		default:
			alpha = 0.7213 / (1.0 + 1.079 / this->reg_size);
			break;
		}
		alphaMM = alpha * this->reg_size * this->reg_size;
	}


	int parseInputs(int argc, char** argv) {
		if (argc >= 6) {
			no_stream = pow(2, atoi(argv[1]));
			no_unique = pow(2, atoi(argv[2]));
			epsilon = atof(argv[3]);
			delta = atof(argv[4]);
			no_max_threads = atoi(argv[5]);
			distNum = atoi(argv[6]);
			zipfAlp = atof(argv[7]);
			return 0;
		}
		else {
			return -1;
		}
	}


		/*
		if (b < 4 || 32 < b) {
			throw std::invalid_argument("bit width must be in the range [4,32]");
		}

		double alpha;
		switch (reg_size) {
		case 16:
			alpha = 0.673;
			break;
		case 32:
			alpha = 0.697;
			break;
		case 64:
			alpha = 0.709;
			break;
		default:
			alpha = 0.7213 / (1.0 + 1.079 / reg_size);
			break;
		}
		alphaMM = alpha * reg_size * reg_size;

		*/
	//}

	/**
	 * Adds element to the estimator
	 *
	 * @param[in] str string to add
	 * @param[in] len length of string
	 */
	void insert(T item, uint32_t len) {
		uint32_t hash;
		MurmurHash3_x86_32(item, len, HLL_HASH_SEED, (void*)&hash);
		//cout << "Width = " << this->bit_width << endl;
		//cout << this->_GET_LEADING_ZERO_C((hash << bit_width), 32 - bit_width) << endl;
		uint32_t index = hash >> (32 - bit_width);
		uint8_t rank = this->_GET_LEADING_ZERO_C((hash << bit_width), 32 - bit_width);
		if (rank > M_regs[index]) { // M is the register or the buckets where to insert the hashes
			M_regs[index] = rank;
		}
	}

	/*Build the HLL sketch
	* using the insert function
	*/
	void build_sketch(dtype* data, uint32_t len, uint32_t n_elements) {
		//cout << "Inside-Check: " << n_elements << endl;
	//int iter;
	//int id = omp_get_thread_num();
//#pragma omp parallel
//{
#pragma omp parallel num_threads(16) shared(data)
#pragma omp for schedule(dynamic)
		for (int iter = 0; iter < n_elements; iter++) {
			//cout << "Check " << endl;
//#pragma omp critical
			insert((T)data[iter], len);

//		}
	//cout << "The number of threads is: " << omp_get_num_threads() << endl;
	}
}

	/**
	 * Estimates cardinality value.
	 *
	 * @return Estimated cardinality value.
	 */
	double Estimate_card() const {
		double estimate;
		double sum = 0.0;
//#pragma omp parallel
//{
//int id = omp_get_thread_num();
//#pragma omp for schedule(static)
		for (int i = 0; i < reg_size; i++) { //uint32_t
			sum += 1.0 / (1 << M_regs[i]);
		}
		estimate = alphaMM / sum; // Using alpha to correct the bias introduced in (reg_size x the harmonic mean)
		if (estimate <= 2.5 * reg_size) {
			uint32_t zeros = 0;
//#pragma omp for
			for (int i = 0; i < reg_size; i++) {
				if (M_regs[i] == 0) {
					zeros++;
				}
			}
			if (zeros != 0) {
				estimate = reg_size * std::log(static_cast<double>(reg_size) / zeros);
			}
		}
		else if (estimate > (1.0 / 30.0) * pow_2_32) {
			estimate = neg_pow_2_32 * log(1.0 - (estimate / pow_2_32));
		}
//}
		return estimate;

	}


	/**
	 * Clears all internal registers.
	 */
	void clear() {
		std::fill(M_regs.begin(), M_regs.end(), 0);
	}

	/**
	 * Returns size of register.
	 *
	 * @return Register size
	 */
	uint32_t registerSize() const {
		return reg_size;
	}

	void HyperLogLog_run(int argc, char** argv, uint32_t n_elements) {

		//Parsing the inputs
		if (parseInputs(argc, argv) == -1) {
			cout << "Error" << endl;
//			return -1;
		}

		//uint32_t elements = 1024;
		dtype* data;
		//cout << this->no_unique << endl;
		generateData(*&data, no_stream, no_unique, distNum, zipfAlp);
		//HyperLogLog<uint32_t> hll(20);
		//cout << "Pre-check: " << n_elements << endl;
		start = clock();
		build_sketch(*&data, 64, n_elements);
		endt = clock();
		cout << "The sketch building time: " << float((endt - start)) / CLOCKS_PER_SEC << " secs" << endl;
		//cout << M_regs[1] << endl;
		//Estimating the cardinalty
		start = clock();
		double cardinality = Estimate_card();
		endt = clock();
		std::cout << "The estimated cardinality is:" << cardinality << std::endl;

		cout << "The cardinality estimation time: " << float((endt - start)) / CLOCKS_PER_SEC << " secs" << endl;

	}

protected:
	uint8_t bit_width; // register bit width
	uint32_t reg_size; // register size
	double alphaMM; // alpha * m^2
	std::vector<uint8_t> M_regs; // registers


	unsigned long long no_stream, no_unique;
	unsigned no_max_threads;
	double epsilon, delta;
	int distNum;
	double zipfAlp;
};


template <class s_type, class d_type>
class CSketch : public sketch <d_type>{

protected:
	unsigned no_rows;
	unsigned no_cols;
	TabularHash* hashes;
	s_type* table;

	CSketch(double epsilon, double delta) {
		no_rows = log2(1 / delta);
		no_cols = (2 / epsilon);

		//prime column count
		bool found = false;
		while (!found) {
			found = true;
			for (unsigned i = 2; i <= sqrt(no_cols); i++) {
				if (no_cols % i == 0) {
					found = false;
					no_cols++;
				}
			}
		}

		table = new s_type[this->no_rows * this->no_cols];
		/*
		D(
		std::cout << "A sketch is created with " << this->no_rows << " rows and " << this->no_cols  << " columns " << std::endl;
		);*/
	}

public:
	virtual void set_hashes(TabularHash* hashes) {
		this->hashes = hashes;
	}

	virtual s_type get(const unsigned& i, const unsigned& j) const {
		return table[i * no_cols + j];
	}

	virtual void add(const unsigned& i, const unsigned& j, const int& val) {
		table[i * no_cols + j] += val;
	}

	virtual void insert_to_row(const d_type& data, unsigned row_id) = 0;
	virtual void insert(const d_type& data) = 0;
	virtual s_type query(const d_type& data) const = 0;

	void insert_stream(dtype* data, unsigned long long no_stream, unsigned no_threads) {
#pragma omp parallel num_threads(no_threads)
		{
#pragma omp for
			for (unsigned i = 0; i < no_rows; i++) {
				for (unsigned long long j = 0; j < no_stream; j++) {
					insert_to_row(data[j], i);
				}
			}
		}
	}

	double getError(std::pair<s_type, d_type>* &freq_decreasing, unsigned hitterRankThreshold) {
		double err = 0.0;

		for (int i = 0; i < hitterRankThreshold; i++) {
			d_type curr = freq_decreasing[i].second;

			s_type actual = freq_decreasing[i].first;
			s_type guess = query(curr);

			double cerr = ((double)(abs(guess - actual)) / actual);
			//std::cout << i << "\t" << guess << " " << actual << " " << cerr << std::endl;      
			err += cerr * cerr;
		}
		return sqrt(err / hitterRankThreshold);
	}

	void print() {
		for (unsigned i = 0; i < no_rows; i++) {
			for (unsigned j = 0; j < no_cols; j++) {
				std::cout << get(i, j) << " ";
			}
			std::cout << std::endl;
		}
	}

	void reset() {
		memset(table, 0, sizeof(s_type) * no_rows * no_cols);
	}

	unsigned get_no_rows() {
		return no_rows;
	}

	unsigned get_no_cols() {
		return no_cols;
	}

	~CSketch() {
		delete[] table;
	}
};


template <class s_type, class d_type>
class CountMinSketch : public CSketch<s_type, d_type> {
public:
	CountMinSketch(double epsilon, double delta) : CSketch<s_type, d_type>(epsilon, delta) {}

	virtual void insert_to_row(const d_type& data, unsigned row_id) {
		unsigned col_id = this->hashes[row_id].hash(data) % this->no_cols;
		this->add(row_id, col_id, 1);
	}

	virtual void insert(const d_type& data) {
		for (unsigned i = 0; i < this->no_rows; i++) {
			insert_to_row(data, i);
		}
	}

	virtual s_type query(const d_type& data) const {
		s_type r_freq, freq = std::numeric_limits<int>::max();

		unsigned h_col;
		for (unsigned i = 0; i < this->no_rows; i++) {
			h_col = this->hashes[i].hash(data) % this->no_cols;
			r_freq = this->get(i, h_col);
			if (r_freq < freq) {
				freq = r_freq;
			}
		}
		return freq;
	}
};


template <class chosenType/*, int NUMHASHES, int k*/>
class Kmin {

protected:
	int numHashes;
	TabularHash * hashesKmin;
	int k;
	dtype* minHashes;
	int t_size;
	unsigned hashDefault = -1;
	unsigned max_hash_index;

public: 
	//Constructor
	Kmin(int size, int k, int NUMHASHES) {

		this->t_size = size;
		this->k = k;
		numHashes = NUMHASHES;

		minHashes = new dtype[t_size];
		minHashes[0] = hashDefault;
		//minHashes.fill(hashDefault);
		max_hash_index = 0;

	}

	void insert(chosenType * item, uint32_t len) {

		uint32_t hashValue;
		MurmurHash3_x86_32(item, len, HLL_HASH_SEED, (void*)&hashValue);
		//Check if the new incoming hashed value is greater than the max one, if yes then exit 
		if (minHashes[max_hash_index] <= hashValue)
			return;

		//Check if the new incoming hashed value already exists in the sketch
		for (int i = 0; i < numHashes; i++) {
			if (minHashes[i] == hashValue)
				return;
		}

		//Replace the max hash
		minHashes[max_hash_index] = hashValue;
		//Find the max hash
		for (unsigned int index = 1; index < minHashes[max_hash_index]; index++)
			max_hash_index = index;

	}

	float CountEstimate(int index) {
		unsigned long long int divInt = pow(2, 32);
		//cout << 1 / ((minHashes[index] - minHashes[index - 1]) / divInt) - 1 << endl;
		return 1 / ((minHashes[index] - minHashes[index - 1]) / divInt) - 1;
	
	}


	//The cardianlity estimate
	void UniqueCountEstimates(float &arithmeticMeanCount, float &geometricMeanCount, float &harmonicMeanCount)
	{
		// calculate the means of the count estimates.  Note that if there we didn't get enough items
		// to fill our m_minHashes array, we are just ignoring the unfilled entries.  In production
		// code, you would probably just want to return the number of items that were filled since that
		// is likely to be a much better estimate.
		// Also, we need to sort the hashes before calculating uniques so that we can get the ranges by
		// using [i]-[i-1] instead of having to search for the next largest item to subtract out
		
		///Write a function to sort the hashes
		///SortHashes();
		
		arithmeticMeanCount = 0.0f;
		geometricMeanCount = 1.0f;
		harmonicMeanCount = 0.0f;
		int numHashes = 0;
		for (unsigned int index = 0; index < numHashes; ++index)
		{
			if (minHashes[index] == hashDefault)
				continue;
			numHashes++;
			float countEstimate = CountEstimate(index);
			arithmeticMeanCount += countEstimate;
			geometricMeanCount *= countEstimate;
			harmonicMeanCount += 1.0f / countEstimate;
		}
		arithmeticMeanCount = arithmeticMeanCount / (float)numHashes;
		geometricMeanCount = pow(geometricMeanCount, 1.0f / (float)numHashes);
		harmonicMeanCount /= (float)numHashes;
		harmonicMeanCount = 1.0f / harmonicMeanCount;
	}



//private:
//	array <T, k> minHashes;
//	size_t max_hash_index;


};
