import connector 
import json
from flask import Flask, request, jsonify
from datetime import datetime
import time
app = Flask(__name__)

import sys

####BF ON-OFF####
BF = int(sys.argv[2])
####BF ON-OFF####

average_durations = 0
no_queries = 0

@app.route('/')
def health_check():
    return 'Healthy'

@app.route('/search', methods=['POST'])
def search():
    return jsonify(connector.names)

@app.route('/tag-keys', methods=['POST'])
def tag_keys():
    print("Ad-hoc asking for columns")
    #k = ["dont","forget","your","keys"]
    return jsonify(connector.names)

@app.route('/tag-values', methods=['POST'])
def tag_values():
    values = ["key", "operator", "value"]
    return jsonify(values)


@app.route('/query', methods=['POST'])
def query():

    ####TAKE QUERY####
    r = request.get_json()
    ####TAKE QUERY####

    ####PRINT QUERY####
    ##print(json.dumps(r, indent=2))
    ##print("Target: ", r['targets'][0]['target'])
    ####PRINT QUERY####

    
    ####RESPONSE TIME####
    target = r['targets'][0]['target']
    if(target == "dB Response Time"):
        data = [
        {
            "target":r['targets'][0]['target'], 
            "datapoints":[[connector.return_duration(), time.mktime(datetime.now().timetuple())]]
        }
    ]

        return jsonify(data)
    ####RESPONSE TIME####

    #####ACCESSES####
    if(target == "block_accesses"):
        filename = sys.argv[1] + ".txt"
        connector.save_accesses(filename)
        exit(1)
    #####ACCESSES####

    
    ####QUERY####
    if (len(r["adhocFilters"]) > 0):
        adhoc_key = r["adhocFilters"][0]["key"]
        adhoc_operator = r["adhocFilters"][0]["operator"]
        adhoc_value = r["adhocFilters"][0]["value"]

        print("Adhoc:" , "key->", adhoc_key, "operator->", adhoc_operator, "value->", adhoc_value)


        if(BF == 0):
            datarizer,durations = connector.scan_with_dependency(r['targets'][0]['target'], adhoc_key, adhoc_value)
        else:
            datarizer,durations = connector.scan_with_bf(r['targets'][0]['target'], adhoc_key, adhoc_value)

        #Less Weight on Dashboard
        datarizer = datarizer[:40000]

        global no_queries
        no_queries += 1
        global average_durations
        average_durations += durations
        #average_durations /= no_queries
    
        res = []
        no_empty = 0
        print(len(datarizer))
        for i in range(0, len(datarizer), 2):
            if(str(datarizer[i].decode("utf-8", errors="ignore").strip()) == ''):
                no_empty += 1
            elif(str(datarizer[i+1].decode("utf-8", errors="ignore").strip()) == ''):
                no_empty += 1
            else:
                #if()
                dt = datetime.strptime(str(datarizer[i+1].decode("utf-8",errors="ignore").strip()), '%Y-%m-%d %H:%M:%S')
                res.append([float(str(datarizer[i].decode("utf-8",errors="ignore").strip())), time.mktime(dt.timetuple())*1000])

        
                
        print("NO_EMPTY: ", no_empty)
        #print(res)

        data = [
            {
                "target":r['targets'][0]['target'], 
                "datapoints":res
            }
        ]

        print("AVERAGE DURATION: ", average_durations/no_queries)
        return jsonify(data)
    ####QUERY####

    
    ####SCAN####
    else:
        datarizer,durations = connector.scan(r['targets'][0]['target'])
        print("Scan total datapoints: ", len(datarizer))
        #Less Weight on Dashboard
        datarizer = datarizer[:40000]
    
        res = []
        no_empty = 0
        print(len(datarizer))
        for i in range(0, len(datarizer), 2):
            if(str(datarizer[i].decode("utf-8")) == ""):
                no_empty += 1
            elif(str(datarizer[i].decode("utf-8")) == ''):
                no_empty += 1
            else:
                dt = datetime.strptime(str(datarizer[i+1].decode("utf-8")), '%Y-%m-%d %H:%M:%S')
                res.append([float(str(datarizer[i].decode("utf-8"))), time.mktime(dt.timetuple())*1000])


        #global no_queries
        #no_queries += 1
        #global average_durations
        #average_durations += durations
                
        print("NO_EMPTY: ", no_empty)
        #print(res)

        data = [
            {
                "target":r['targets'][0]['target'], 
                "datapoints":res
            }
        ]
        
        #print("AVERAGE DURATION: ", average_durations/no_queries)
        return jsonify(data)
    ####SCAN####

@app.route('/annotations', methods=['POST'])
def annotations():
    req = request.get_json()
    data = [
        {
            "annotation": 'This is the annotation',
        "time": (convert_to_time_ms(req['range']['from']) +
                 convert_to_time_ms(req['range']['to'])) / 2,
            "title": 'Deployment notes',
            "tags": ['tag1', 'tag2'],
            "text": 'Hm, something went wrong...'
        }
    ]
    return jsonify(data)

if __name__ == '__main__':
    app.run(host='127.0.0.1', port=4005, debug=True)
