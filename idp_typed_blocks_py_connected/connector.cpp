#include <iostream>
#include "connector.h"
#include "database.hpp"
//#include "main.cpp"
#include "setup.hpp"

using namespace database;

extern "C" void connect()
{
  std::cout << "Connected to c++ engine.." << std::endl;
}

extern "C" void* declare_dB_object(char** col_types, char** col_names, int col_num, int NO_BLOCK_GROUPS){
  return declare_dB(col_types, col_names, col_num, NO_BLOCK_GROUPS);
}

extern "C" void start_ingest(void* db_addr, int NO_BLOCK_GROUPS, char** col_types, int col_num){
  ingest(db_addr, NO_BLOCK_GROUPS, col_types, col_num);
}

extern "C" char** scan_db(void* db_addr, int target_col_num, int ts_col_num, int NO_BLOCK_GROUPS, int col_num){
  //std::cout << "I got scan: col: " << target_col_num << " ts: " << ts_col_num << std::endl;
  char** join = scan(db_addr, target_col_num, ts_col_num, NO_BLOCK_GROUPS ,col_num);
  
  return join;
}

extern "C" char** scan_db_with_filter(void* db_addr, int target_col_num, int ts_col_num, int NO_BLOCK_GROUPS, int col_num, char* value){
  char** join = scan_with_filter(db_addr, target_col_num, ts_col_num, NO_BLOCK_GROUPS, col_num, value);
}

extern "C" char** scan_db_with_dependency(void* db_addr, int target_col_num, int ts_col_num, int NO_BLOCK_GROUPS, int col_num, int filter_col_num, char* filter_value){
  char** join = scan_with_dependency(db_addr, target_col_num, ts_col_num, NO_BLOCK_GROUPS, col_num, filter_col_num, filter_value);
}

extern "C" char** scan_db_with_bf(void* db_addr, int target_col_num, int ts_col_num, int NO_BLOCK_GROUPS, int col_num, int filter_col_num, char* filter_value, char** col_types){
  char** join = scan_with_bf_decider(db_addr, target_col_num, ts_col_num, NO_BLOCK_GROUPS, col_num, filter_col_num, filter_value, col_types);
}

extern "C" int* db_accesses(void* db_addr, int col_num, int NO_BLOCK_GROUPS){
  return return_accesses(db_addr, col_num, NO_BLOCK_GROUPS);
}


//THIS IS FOR TEST//
extern "C" void addr_test(void* db_addr){
  dataBase* db = db_addr;
  std::cout << db->db[12201]->somehow_get(8) << std::endl;
  std::cout << db->db[12202]->somehow_get(8) << std::endl;
  std::cout << db->db[12205]->somehow_get(8) << std::endl;
  std::cout << db->db[12206]->somehow_get(8) << std::endl;
  std::cout << db->db[12207]->somehow_get(8) << std::endl;
  std::cout << db->db[12208]->somehow_get(8) << std::endl;
  std::cout << db->db[12209]->somehow_get(8) << std::endl;
  std::cout << db->db[12210]->somehow_get(8) << std::endl;
  std::cout << db->db[12211]->somehow_get(8) << std::endl;
  std::cout << db->db[12212]->somehow_get(8) << std::endl;
  std::cout << db->db[12214]->somehow_get(8) << std::endl;
  std::cout << db->db[12215]->somehow_get(8) << std::endl;
  //std::cout << "How about that" << std::endl;
}
//THIS IS FOR TEST//




