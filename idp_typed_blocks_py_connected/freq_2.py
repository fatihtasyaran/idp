import matplotlib.pyplot as plt

scan   = [int(line.rstrip('\n')) for line in open("scan_blocks.txt")]                                                                                             
c82_q   = [int(line.rstrip('\n')) for line in open("company82.txt")]           
c82_bf = [int(line.rstrip('\n')) for line in open("company82_bf.txt")]

f20_q   = [int(line.rstrip('\n')) for line in open("fare20.txt")]           
f20_bf = [int(line.rstrip('\n')) for line in open("fare20_bf.txt")]

t20_q   = [int(line.rstrip('\n')) for line in open("taxi_id20.txt")]           
t20_bf = [int(line.rstrip('\n')) for line in open("taxi_id20_bf.txt")]

c103_q   = [int(line.rstrip('\n')) for line in open("company103.txt")]           
c103_bf = [int(line.rstrip('\n')) for line in open("company103_bf.txt")]

RANGE = 2500

x_axis = []
                                                                                  
for i in range(0, RANGE):                                                    
    x_axis.append(i)                                                              

#x_axis = x_axis[:1500]

scan = scan[:RANGE]

c82_q = c82_q[:RANGE]
c82_bf = c82_bf[:RANGE]

f20_q = f20_q[:RANGE]
f20_bf = f20_bf[:RANGE]

t20_q = t20_q[:RANGE]
t20_bf = t20_bf[:RANGE]

c103_q = c103_q[:RANGE]
c103_bf = c103_bf[:RANGE]
    
plt.figure(1)                                                                     

plt.subplot(911)                                                                  
plt.plot(x_axis, scan, color='C1', label='scan')         
plt.legend(loc='upper left')                                                      

plt.subplot(912)                                                                  
plt.plot(x_axis, c82_q, color='C2', label='company=82 query')         
plt.legend(loc='upper left')                                                      
                                                                                  
plt.subplot(913)                                                                  
plt.plot(x_axis, c82_bf, color='C2', label='company=82 bloom filter')
plt.legend(loc='upper left')

plt.subplot(914)                                                                  
plt.plot(x_axis, f20_q, color='C3', label='fare=20 query')         
plt.legend(loc='upper left')                                                      
                                                                                  
plt.subplot(915)                                                                  
plt.plot(x_axis, f20_bf, color='C3', label='fare=20 bloom filter')
plt.legend(loc='upper left')                                                      

plt.subplot(916)                                                                  
plt.plot(x_axis, t20_q, color='C4', label='taxi_id=20 query')         
plt.legend(loc='upper left')                                                      
                                                                                  
plt.subplot(917)                                                                  
plt.plot(x_axis, t20_bf, color='C4', label='taxi_id=20 bloom filter')
plt.legend(loc='upper left')

plt.subplot(918)                                                                  
plt.plot(x_axis, c103_q, color='C5', label='company=103 query')         
plt.legend(loc='upper left')                                                      
                                                                                  
plt.subplot(919)                                                                  
plt.plot(x_axis, c103_bf, color='C5', label='company=103 bloom filter')
plt.legend(loc='upper left')                                                      

plt.savefig("accesses.pdf")
plt.show()    
