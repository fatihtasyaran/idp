from datetime import datetime
import socket
import time

from ctypes import*

##PY UTILITIES##
import csv
##PY UTILITIES##
stdc = cdll.LoadLibrary("libc.so.6") # or similar to load c library
stdcpp = cdll.LoadLibrary("libstdc++.so.6") # or similar to load c++ library
libConnect = cdll.LoadLibrary('./libConnect.so')
libConnect.connect()

NO_BLOCK_GROUPS = 900

filer = open('/home/fatih/idp/chicago-taxi-rides-2016/chicago_taxi_trips_2016_01.csv')

csv_reader = csv.reader(filer)
types = next(csv_reader)
names = next(csv_reader)
col_num = len(types)

libConnect.declare_dB_object.argtypes = [POINTER(c_char_p),POINTER(c_char_p),c_int,c_int]

col_types = (c_char_p * col_num)()

col_names = (c_char_p * col_num)()

for i in range(col_num):
    col_types[i] = types[i].encode('utf-8')
    col_names[i] = names[i].encode('utf-8')

##RESPONSE TIME
duration = 0
names.append("dB Response Time")
names.append("block_accesses")
##RESPONSE TIME

creator = libConnect.declare_dB_object
creator.restype = c_void_p

db_addr = c_void_p(creator(col_types, col_names, col_num, NO_BLOCK_GROUPS))

libConnect.start_ingest.argtypes = [c_void_p, c_int, POINTER(c_char_p), c_int]
libConnect.start_ingest(db_addr, NO_BLOCK_GROUPS, col_types, col_num)

k = input("Press enter to proceed reporting..")

libConnect.scan_db.argtypes = [c_void_p, c_int, c_int, c_int, c_int]
libConnect.scan_db.restype = POINTER(c_char_p)

def scan(target):
    start = time.clock_gettime(1)
    datarizer_o = libConnect.scan_db(db_addr, names.index(target), names.index("trip_end_timestamp"), NO_BLOCK_GROUPS ,col_num)
    global duration
    duration = time.clock_gettime(1) - start
    
    print("DURATION:", duration)
    
    datarizer_l = []

    empty_indexes = []
    
    for i in range(0, NO_BLOCK_GROUPS*1024*2, 2):
        if(datarizer_o[i] == b''):
            empty_indexes.append(i)
            empty_indexes.append(i+1)
        elif(datarizer_o[i+1] == b''):
            empty_indexes.append(i)
            empty_indexes.append(i+1)
        else:
            datarizer_l.append(datarizer_o[i])
            datarizer_l.append(datarizer_o[i+1])
        #print("datarizer_l[i]: ", i, datarizer_l[i])
        #qprint(datarizer_l[i])

    print("Empty indexes: ", len(empty_indexes))

    current_day = datetime.now()
    who = socket.gethostname()

    to_write = str(current_day) + "," + str(duration) + "," + str(target) + "," +str(col_num) + "," + "CPU" + "," + str(who) + "\n"

    f = open("lb_data.txt","a")
    f.write(to_write)
    f.close()

    return datarizer_l,duration

libConnect.scan_db_with_filter.argtypes = [c_void_p, c_int, c_int, c_int, c_int, c_char_p]
libConnect.scan_db_with_filter.restype = POINTER(c_char_p)

def scan_with_filter(target, value):
    start = time.clock_gettime(1)
    datarizer_o = libConnect.scan_db_with_filter(db_addr, names.index(target), names.index("trip_end_timestamp"), NO_BLOCK_GROUPS, col_num, value.encode('utf-8'))
    global duration
    duration = time.clock_gettime(1) - start

    print("DURATION WITH FILTER:", duration)

    datarizer_l = []
    empty_indexes = []

    for i in range(0, NO_BLOCK_GROUPS*1024*2, 2):
        if(datarizer_o[i] == b''):
            empty_indexes.append(i)
            empty_indexes.append(i+1)
        elif(datarizer_o[i+1] == b''):
            empty_indexes.append(i)
            empty_indexes.append(i+1)
        else:
            datarizer_l.append(datarizer_o[i])
            datarizer_l.append(datarizer_o[i+1])

    print("Empty indexes: ", len(empty_indexes))
    print("No returned responses: ", len(datarizer_l))
    #print(datarizer_l[1].decode('utf-8'))

    current_day = datetime.now()
    who = socket.gethostname()

    to_write = str(current_day) + "," + str(duration) + "," + str(target) + "," +str(col_num) + "," + "CPU" + "," + str(who) + "\n"

    f = open("lb_data.txt","a")
    f.write(to_write)
    f.close()

    return datarizer_l

libConnect.scan_db_with_dependency.argtypes = [c_void_p, c_int, c_int, c_int, c_int, c_int, c_char_p]
libConnect.scan_db_with_dependency.restype = POINTER(c_char_p)

def scan_with_dependency(target, filter_col, value):
    start = time.clock_gettime(1)
    datarizer_o = libConnect.scan_db_with_dependency(db_addr, names.index(target), names.index("trip_end_timestamp"), NO_BLOCK_GROUPS, col_num, names.index(filter_col), value.encode('utf-8').strip())
    global duration
    duration = time.clock_gettime(1) - start

    print("DURATION WITH FILTER:", duration)

    datarizer_l = []
    empty_indexes = []

    for i in range(0, NO_BLOCK_GROUPS*1024*2, 2):
        if(datarizer_o[i] == b''):
            empty_indexes.append(i)
            empty_indexes.append(i+1)
        elif(datarizer_o[i+1] == b''):
            empty_indexes.append(i)
            empty_indexes.append(i+1)
        else:
            datarizer_l.append(datarizer_o[i])
            datarizer_l.append(datarizer_o[i+1])

    print("Empty indexes: ", len(empty_indexes))
    print("No returned responses: ", len(datarizer_l))
    print(datarizer_l[1].decode('ISO-8859-1'))

    current_day = datetime.now()
    who = socket.gethostname()

    to_write = str(current_day) + "," + str(duration) + "," + str(target) + "," +str(col_num) + "," + "CPU" + "," + str(who) + ",PLAIN_QUERY" +"\n"

    f = open("lb_data.txt","a")
    f.write(to_write)
    f.close()

    return datarizer_l, duration

libConnect.scan_db_with_bf.argtypes = [c_void_p, c_int, c_int, c_int, c_int, c_int, c_char_p]
libConnect.scan_db_with_bf.restype = POINTER(c_char_p)

def scan_with_bf(target, filter_col, value):
    start = time.clock_gettime(1)
    datarizer_o = libConnect.scan_db_with_bf(db_addr, names.index(target), names.index("trip_end_timestamp"), NO_BLOCK_GROUPS, col_num, names.index(filter_col), value.encode('utf-8').strip(), col_types)
    global duration
    duration = time.clock_gettime(1) - start

    print("DURATION WITH BLOOM FILTER:", duration)

    datarizer_l = []
    empty_indexes = []

    for i in range(0, NO_BLOCK_GROUPS*1024*2, 2):
        if(datarizer_o[i] == b''):
            empty_indexes.append(i)
            empty_indexes.append(i+1)
        elif(datarizer_o[i+1] == b''):
            empty_indexes.append(i)
            empty_indexes.append(i+1)
        else:
            datarizer_l.append(datarizer_o[i])
            datarizer_l.append(datarizer_o[i+1])

    print("Empty indexes: ", len(empty_indexes))
    print("No returned responses: ", len(datarizer_l))
    print(datarizer_l[1].decode('ISO-8859-1'))

    current_day = datetime.now()
    who = socket.gethostname()

    to_write = str(current_day) + "," + str(duration) + "," + str(target) + "," +str(col_num) + "," + "CPU" + "," + str(who) + ",BF_QUERY" +"\n"

    f = open("lb_data.txt","a")
    f.write(to_write)
    f.close()
    

    return datarizer_l, duration

    
def return_duration():
    print("DURATION:", duration)
    return duration

libConnect.db_accesses.argtypes=[c_void_p, c_int, c_int]
libConnect.db_accesses.restype = POINTER(c_int)

def save_accesses(filename):
    accesses = libConnect.db_accesses(db_addr, col_num, NO_BLOCK_GROUPS)

    for i in range(0, col_num*NO_BLOCK_GROUPS):
        print(accesses[i])
    
    with open(filename, 'w') as file:
        for num in accesses[:18000]:
            file.write(str(num))
            file.write("\n")

    exit(1)
    
    
        
