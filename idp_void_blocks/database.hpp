#ifndef DATABASE_H
#define DATABASE_H

#include "common.h"

#define NO_BLOCKS 30

extern int G_SIZE;

static uint32_t block_ids = -1;

inline int returnSize_d(string type) {
  if(type == "uint8_t")
    return sizeof(UINT8);
  else if(type == "uint16_t")
    return sizeof(UINT16);
  else if(type == "uint32_t")
    return sizeof(UINT32);
  else if(type == "string")
    return SIZEOF_STRING;
  else if(type == "double")
    return sizeof(DOUBLE);
  else if(type == "bool")
    return sizeof(BOOL);
  else if(type == "NONE")
    return 0;
  else
    std::cout << "I don't have " << type << "type" << std::endl;
}

uint32_t get_block_id()
{
  return ++block_ids;
}

//template <class T>
struct block{
  uint32_t id;
  uint32_t size;
  uint32_t accessed;
  string type;
  void* block_ptr;

  block()
  {
  }

  block(int SIZE, string TYPE, int B_ID){
    size = SIZE;
    type = TYPE;
    block_ptr = malloc(returnSize_d(TYPE)*1024);
    id = B_ID;
    //sketch = new sketch();
    //bloom_filter = new bloom_filter();
  }

  template<typename T>
  void insert(int index, T val){
    //std::cout << "index: " << index << "\n";
    block_ptr += (index*size);
    *((T*)block_ptr) = val;
    block_ptr -= (index*size);
  }
  
  /*THIS COULD BE FASTER BUT SEGMENTATION FAULT
  template<typename T>
  void offsetted_insert(int index, T val){
    std::cout << "index: " << index << "\n";
    *((T*)block_ptr) = val;
    block_ptr += size;
    if(index == 1023)
      block_ptr -= (1023*size);
      }*/
  
  //This is for debug purposes
  template<typename T>
  void print_get(int index, T typer){
    block_ptr += (index*size);
    std::cout << *((T*)block_ptr) << std::endl;
  }

};

struct dataBase{
   block* db[30000];

  dataBase(type_num_col* type_indexes, int col_num)
  {
    //db = new block_ptrs[col_num];
    string type;
    std::cout << "Constructing Database.." << std::endl;
    for(int i = 0; i < col_num*NO_BLOCK_GROUPS; i++){
      type = type_indexes[i%col_num].first;
      
      if(type == "uint8_t"){
	db[i] = new block(returnSize_d(type), type, i);
      }
      
      else if(type == "uint16_t"){
	db[i] = new block(returnSize_d(type), type, i);
      }
      
      else if(type == "uint32_t"){
	db[i] = new block(returnSize_d(type), type, i);
      }
      
      else if(type == "double"){
	db[i] = new block(returnSize_d(type), type, i);
      }
      
      else if(type == "float"){
	db[i] = new block(returnSize_d(type), type, i);
      }
      
      else if(type == "string"){
	db[i] = new block(returnSize_d(type), type, i);
      }
      
      else if(type == "bool"){
	db[i] = new block(returnSize_d(type), type, i);
      }
      
      else{
	std::cout << "Undefined type, exiting" << std::endl;
	exit(1);
      }
      
    }
  }
};

#endif
