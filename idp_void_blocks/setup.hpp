#ifndef SETUP_H
#define SETUP_H

#include "database.hpp"
//#include "query.hpp"
#include "common.h"
#include <omp.h>

//#define DEBUG

int returnSize(string type) {
  if(type == "uint8_t")
    return sizeof(UINT8);
  else if(type == "uint16_t")
    return sizeof(UINT16);
  else if(type == "uint32_t")
    return sizeof(UINT32);
  else if(type == "string")
    return SIZEOF_STRING;
  else if(type == "double")
    return sizeof(DOUBLE);
  else if(type == "bool")
    return sizeof(BOOL);
  else if(type == "NONE")
    return 0;
  else
    std::cout << "I don't have " << type << "type" << std::endl;
}


int coma(string line) {
  
  int comas = 1;
  int length = line.length();
    
  for(int i = 0; i < length; i++){
    if(line[i] == ',')
      comas++;
  }
  return comas;
}

void typeParse(string line, type_num_col* indexes, int mod) {
  string type;
  
  int length = line.length();
  int current_col = 0;
  int startIndex = 0;
  int endIndex = 0;

  
  for(int i = 0; i < length+1; i++){
    if(line[i] == ',' || i == length){
      endIndex = i;
      
      if(mod == 1){
	indexes[current_col].first = line.substr(startIndex, endIndex-startIndex);  //Type
      }
      else{
	indexes[current_col].second = line.substr(startIndex, endIndex-startIndex); //Feature name
      }

      
      startIndex = endIndex+1;
      current_col++;
      
      //std::cout << "Current Col: " << current_col << "String: " << line.substr(startIndex, endIndex) << std::endl; 
    }
  }
}

inline string acquire_val(string line_to_cut, int order){
#if defined DEBUG
  if(order == 0)
    //std::cout << line_to_cut << std::endl;
#endif
  
  int length = line_to_cut.length();
  int start_index = 0;
  int end_index = 0;

  int found = 0;

  string val = "D U M M Y";

  for(int i = 0; i < length; i++){
    if(line_to_cut[i] == ','){
      end_index = i;
      if(order == 0)
	start_index -= 1;
      val = line_to_cut.substr(start_index+1, end_index-start_index-1);
      
      if(found == order)
	return val;

      found++;
      start_index = end_index;
    }
  }

  return val;
}

void writeOneRecordtoBlocks(dataBase db, string vals_line, type_num_col* indexes, int col_num, int record_num){

#pragma omp parallel num_threads(col_num) //Write to col_num blocks in parallel
  {
  int tid = omp_get_thread_num();
  
  int block_num = tid;//record_num/1024;
  int index = record_num%1024;
  int c_val;  //Value to cast placeholder
  //auto w_val; //Value to send block's insert function
  string type = indexes[tid].first;
  
  string val = acquire_val(vals_line, tid);

#if defined DEBUG
  std::cout << "Start parallel ingest" << std::endl;
#pragma omp critical
  {
    std::cout << "Thread " << tid << " before insert, my val: " << val << std::endl;
  }
#endif
  
  if(val == EMPTY){
    ;
  }
  
  else{
    if(type == "uint8_t"){
      c_val = std::stoi(val);
      auto w_val = (uint8_t)c_val;
      db.db[block_num]->insert(index, w_val);
    }
    else if(type == "uint16_t"){
      c_val = std::stoi(val);
    auto w_val = (uint16_t)c_val;
    db.db[block_num]->insert(index, w_val);
    }
    else if(type == "uint32_t"){
      c_val = std::stoi(val);
      auto w_val = (uint32_t)c_val;
      db.db[block_num]->insert(index, w_val);
    }
    else if(type == "double"){
      auto w_val = std::stod(val);
      db.db[block_num]->insert(index, w_val);
    }
    else if(type == "string"){
      auto w_val = val;
      db.db[block_num]->insert(index, w_val);
    }
    else if(type == "bool"){
      if(val == "true"){
	auto w_val = true;
	db.db[block_num]->insert(index, w_val);
      }else{
	auto w_val = false;
	db.db[block_num]->insert(index, w_val);
      }
    }else{
      std::cout << "Undefined type, exiting" << std::endl;
      exit(1);
    }

#if defined DEBUG
#pragma omp critical
    {
      std::cout << "Thread " << tid << " after insert" << std::endl;
    }
#endif
  }
}
  
  }

void writeOneRecordChunktoBlocks(dataBase db, string *record_chunk, type_num_col* indexes, int col_num, int chunk_size, int block_group){
#if defined DEBUG
  std::cout << "Block group: " << block_group << " start inserting in parallel" << std::endl;
#endif

#pragma omp parallel num_threads(col_num) //Write to col_num blocks in parallel
  {
  int tid = omp_get_thread_num();
  int block_num = tid+(block_group+block_group*(col_num-1));//record_num/1024;

  for(int i = 0; i < chunk_size; i++){
    int index = i;
    int c_val;  //Value to cast placeholder
    //auto w_val; //Value to send block's insert function
    string type = indexes[tid].first;
    
    string val = acquire_val(record_chunk[i], tid);
    
#if defined DEBUG
#pragma omp critical
    {
      std::cout << "Thread " << tid << " before insert, my val: " << val << std::endl;
    }
#endif
    
    if(val == EMPTY){
    ;
    }
  
    else{
      if(type == "uint8_t"){
	c_val = std::stoi(val);
	auto w_val = (uint8_t)c_val;
	db.db[block_num]->insert(index, w_val);
      }
      else if(type == "uint16_t"){
	c_val = std::stoi(val);
	auto w_val = (uint16_t)c_val;
	db.db[block_num]->insert(index, w_val);
      }
      else if(type == "uint32_t"){
	c_val = std::stoi(val);
	auto w_val = (uint32_t)c_val;
	db.db[block_num]->insert(index, w_val);
      }
      else if(type == "double"){
	auto w_val = std::stod(val);
	db.db[block_num]->insert(index, w_val);
      }
      else if(type == "string"){
	auto w_val = val;
	db.db[block_num]->insert(index, w_val);
      }
      else if(type == "bool"){
	if(val == "true"){
	  auto w_val = true;
	  db.db[block_num]->insert(index, w_val);
	}else{
	  auto w_val = false;
	  db.db[block_num]->insert(index, w_val);
	}
      }else{
	std::cout << "Undefined type, exiting" << std::endl;
	exit(1);
      }
      
#if defined DEBUG
#pragma omp critical
      {
	std::cout << "Thread " << tid << " after insert" << std::endl;
      }
#endif
    }
  }
#pragma omp barrier
}
  
  }



void writeOneRecordChunktoBlocks_better(dataBase db, string *record_chunk, type_num_col* indexes, int col_num, int chunk_size, int block_group){ //Threads constantly writes to same blocks, therefore one if - else is enough for 1024 records
#if defined DEBUG
  std::cout << "Block group: " << block_group << " start inserting in parallel" << std::endl;
#endif

#pragma omp parallel num_threads(col_num) //Write to col_num blocks in parallel
  {
  int tid = omp_get_thread_num();
  int block_num = tid+(block_group+block_group*(col_num-1));//record_num/1024;

  
  int c_val;  //Value to cast placeholder
  //auto w_val; //Value to send block's insert function
  string type = indexes[tid].first;
  
  if(type == "uint8_t"){
    for(int index = 0; index < 1024; index++){
      string val = acquire_val(record_chunk[index], tid);
      if(val != EMPTY){
	c_val = std::stoi(val);
	  auto w_val = (uint8_t)c_val;
	  db.db[block_num]->insert(index, w_val);
      }
    }
  }
  else if(type == "uint16_t"){
    for(int index = 0; index < 1024; index++){
      string val = acquire_val(record_chunk[index], tid);
      if(val != EMPTY){
	c_val = std::stoi(val);
	auto w_val = (uint16_t)c_val;
	db.db[block_num]->insert(index, w_val);
      }
    }
  }
  else if(type == "uint32_t"){
    for(int index = 0; index < 1024; index++){
      string val = acquire_val(record_chunk[index], tid);
      if(val != EMPTY){
	c_val = std::stoi(val);
	auto w_val = (uint32_t)c_val;
	db.db[block_num]->insert(index, w_val);
      }
    }
  }
  else if(type == "double"){
    for(int index = 0; index < 1024; index++){
      string val = acquire_val(record_chunk[index], tid);
      if(val != EMPTY){
	auto w_val = std::stod(val);
	db.db[block_num]->insert(index, w_val);
      }
    }
  }
  else if(type == "string"){
    for(int index = 0; index < 1024; index++){
      string val = acquire_val(record_chunk[index], tid);
      if(val != EMPTY)
	{
	  auto w_val = val;
	  db.db[block_num]->insert(index, w_val);
	}
    }
  }
  else if(type == "bool"){
    for(int index = 0; index < 1024; index++){
      string val = acquire_val(record_chunk[index], tid);
      if(val != EMPTY){
	if(val == "true"){
	  auto w_val = true;
	  db.db[block_num]->insert(index, w_val);
	}
	else{
	  auto w_val = false;
	  db.db[block_num]->insert(index, w_val);
	}
      }
    }
  }
  else{
    std::cout << "Undefined type, exiting" << std::endl;
    exit(1);
  }
  
#if defined DEBUG
#pragma omp critical
  {
    std::cout << "Thread " << tid << " after insert" << std::endl;
  }
#endif
#pragma omp barrier
}
  }






#endif
