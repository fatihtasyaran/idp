#include <iostream>
#include <string>
#include <stdint.h>
#include <random>
#include <omp.h>
//
#include <fstream>
#include <typeinfo>
#include "common.h"
//
#include "setup.hpp"  
#include "database.hpp"
#include "heterogenous_container.hpp"
//#include "query.hpp" //No query at the time
//
#include <vector>
#include <list>
#include <variant>
#include <map>
//#include <boost/any.hpp>


/*struct unifiedContainer{

  std::vector<std::variant<uint8_t, uint16_t, uint32_t, double, string, bool>> allowedTypes;

  
  };*/

int basic_hasher(string to_hashed){
  int i = to_hashed[4];
  return i%10;
}



int main()
{
  string types_line;
  string vals_line;
  fstream csv_reader;
  int col_num;
  
  csv_reader.open("/home/fatih/idp/chicago-taxi-rides-2016/chicago_taxi_trips_2016_01.csv");
  
  getline(csv_reader, types_line);
  
  col_num = coma(types_line);//+2; //Number of variables in database ##+1 -> line_id ##+2 -> Number of accesses
  type_num_col type_indexes[col_num];  
  
  int counter = 0;

  getline(csv_reader, vals_line);

  typeParse(types_line, type_indexes, 1); //Get types
  typeParse(vals_line, type_indexes, 2); //Get feature names

  int totSize = 0;
  
  for(int i = 0; i < col_num; i++){   //Find the total size of database line
    totSize += returnSize(type_indexes[i].first);
    std::cout << type_indexes[i].first << " Returned: " << returnSize(type_indexes[i].first)*8 << " totSize: " << totSize << std::endl;
  }

    
  for(int i = 0; i < col_num; i++){
    std::cout << "Column:" << i << "\tType: " << type_indexes[i].first << "\tColName: " << type_indexes[i].second << std::endl;
    std::cout << std::endl;
  }
  
  std::cout << "Total Size:" << totSize << std::endl;

  std::cout << "Declaring Database.." << std::endl;
  dataBase *db = new dataBase(type_indexes,col_num);
  std::cout << "Declared Database.." << std::endl;
  
  int record_num = 0;


  /*double start = omp_get_wtime();
  
  for(int b = 0; b < 30; b++){ //This writes records one by one, works but slow by bad temporal locality,
    for(int l = 0; l < 1024; l++){ //Creating thread overhead with each record
      getline(csv_reader, vals_line);
      writeOneRecordtoBlocks(*db, vals_line, type_indexes, col_num, record_num);
      //std::cout << "Wrote Record " << (b*1024)+l << std::endl;
      record_num++;
    }
  }

  double end = omp_get_wtime();
  std::cout << "Insert took " << end-start << std::endl;*/

  int num_records = 0;

  double start = omp_get_wtime();

  string record_chunk[1024];
  
  for(int block_group = 0; block_group < NO_BLOCK_GROUPS; block_group++){
    for(int record = 0; record < 1024; record++){
      getline(csv_reader, record_chunk[record]);
    }
    //std::cout << "*****Writing block group: " << block_group << "*****" <<std::endl;
    writeOneRecordChunktoBlocks_better(*db, record_chunk, type_indexes, col_num, 1024, block_group);
    num_records += 1024;
  }
  
  double end = omp_get_wtime();
  std::cout << "Insert took " << end-start << std::endl;
  std::cout << "Number of records inserted: " << num_records << std::endl;
  std::cout << "Number of records per second: " << (1/(end-start))*num_records << std::endl;

  
  //WILL TRY BOOST ANY HERE
  //std::vector<std::variant<uint8_t, uint16_t, uint32_t, double, string, bool>> allowedTypes;

  //allowedTypes.push_back("SA");
  //allowedTypes.push_back(2);
  //allowedTypes.push_back(4.3);
  //allowedTypes.push_back(true);


  //string saer = allowedTypes.at(0);// Will need visitor

  //unifiedContainer uC(type_indexes, col_num);
  

  
  uint16_t getter = 2;
  string getter_;
  
  db->db[0]->print_get(11, getter);
  db->db[1]->print_get(11, getter_);
  db->db[2]->print_get(11, getter_);

  db->db[20]->print_get(2, getter);
  db->db[21]->print_get(2, getter_);
  db->db[22]->print_get(2, getter_);

  db->db[400]->print_get(1000, getter);
  db->db[401]->print_get(1000, getter_);
  db->db[402]->print_get(1000, getter_);

  db->db[2000]->print_get(1000, getter);
  db->db[2001]->print_get(1001, getter_);
  db->db[2002]->print_get(1001, getter_);

  db->db[8000]->print_get(1000, getter);
  db->db[8001]->print_get(1001, getter_);
  db->db[8002]->print_get(1001, getter_);

  //std::cout << "Returner(2) returns: " << returner(1) << std::endl;

  /*std::hash<string> hasher;

  std::cout << hasher("string")%10 << std::endl;
  std::cout << hasher("double")%10 << std::endl;
  std::cout << hasher("uint8_t")%10 << std::endl;
  std::cout << hasher("uint16_t")%10 << std::endl;
  std::cout << hasher("uint32_t")%10 << std::endl;
  std::cout << hasher("bool")%10 << std::endl;

  std::cout << "Basic hasher" << "\n";
  
  std::cout << basic_hasher("string")%10 << std::endl;
  std::cout << basic_hasher("double")%10 << std::endl;
  std::cout << basic_hasher("uint8_t")%10 << std::endl;
  std::cout << basic_hasher("uint16_t")%10 << std::endl;
  std::cout << basic_hasher("uint32_t")%10 << std::endl;
  std::cout << basic_hasher("bool")%10 << std::endl;*/

  //HASH collides, trying map

  std::map<string, int> mapper;
  mapper["uint8_t"] = 0;
  mapper["uint16_t"] = 1;
  mapper["uint32_t"] = 2;
  mapper["double"] = 3;
  mapper["string"] = 4;
  mapper["bool"] = 5;

  //std::cout << "First col is: " << mapper[type_indexes[0].first] << " which is " << mapper[0] << std::endl;
  std::cout << "MAP" << std::endl;
  std::cout << mapper["string"] << std::endl;
  

  //std::vector<std::variant> list;
  std::tuple <uint16_t, double, string> types;
  //types = std::make_tuple(2, 2.2, "SA");

  //std::cout << std::get<0>(types) << std::endl;
  //std::cout << std::get<1>(types) << stdasdasd::endl;
  //std::cout << std::get<2>(types) << std::endl;

  std::cout << "Prints: " << std::endl;
  //db->db[8001]->print_get(1001, std::get<2>(types));
  //db->db[8000]->print_get(1000, getter);
  db->db[7001]->print_get(0, std::get<2>(types));
  db->db[7002]->print_get(0, std::get<2>(types));

  std::cin.get();
  std::cin.ignore();
  
  return 0;
}



  
  
