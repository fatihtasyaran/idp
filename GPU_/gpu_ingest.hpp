#include <fstream>
#include "common.h"
#include <omp.h>
#include <cstring>
#include <stdio.h>
#include "gpu_struct.hpp"



inline std::string acquire_val(std::string line_to_cut, int order){
#if defined DEBUG
  if(order == 0)
    //std::cout << line_to_cut << std::endl;
#endif
  
  int length = line_to_cut.length();
  int start_index = 0;
  int end_index = 0;

  int found = 0;

  std::string val = "D U M M Y";

  for(int i = 0; i < line_to_cut.length(); i++){
    if(line_to_cut[i] == ','){
      end_index = i;
      if(order == 0)
        start_index -= 1;
      val = line_to_cut.substr(start_index+1, end_index-start_index-1);

      if(found == order)
        return val;

      found++;
      start_index = end_index;
    }
  }

  return val;
}



void ChunktoBlocks(daba* dbl, std::string *record_chunk, char** col_types, int col_num, int chunk_size, int block_group){

  //std::cout << "Writing.." << std::endl;
  //db.db[0]->insert(0,(uint8_t)8);
  //std::cout << "Writing 2.." << std::endl;

#pragma omp parallel num_threads(col_num) //Write to col_num blocks in parallel
  {
  int tid = omp_get_thread_num();
  int block_num = tid+(block_group+block_group*(col_num-1));//record_num/1024;

  
  int c_val;  //Value to cast placeholder
  //auto w_val; //Value to send block's insert function
  //std::string type = col_types[tid]; //This line generates segmentation fault
  //std::string type(col_types[tid]);
  std::string type = col_types[tid];
  //type = col_types[tid];
  
  //std::cout << "Type is: " << col_types[0] << std::endl;

    
  if(type == "uint8_t"){
    for(int index = 0; index < 1024; index++){
      std::string val = acquire_val(record_chunk[index], tid);
      if(val != EMPTY){
        c_val = std::stoi(val);
          auto w_val = (uint8_t)c_val;
          //db.db[block_num]->insert(index, w_val);
	  dbl->db[block_num] = new dblock<uint8_t>(block_num);
          dbl->db[block_num]->insert(index, w_val);
      }
    }
  }
else if(type == "uint16_t"){
    for(int index = 0; index < 1024; index++){
      std::string val = acquire_val(record_chunk[index], tid);
      if(val != EMPTY){
        c_val = std::stoi(val);
        auto w_val = (uint16_t)c_val;
        //db.db[block_num]->insert(index, w_val);
        dbl->db[block_num] = new dblock<uint16_t>(block_num);
	dbl->db[block_num]->insert(index, w_val);

      }
    }
  }
  else if(type == "uint32_t"){
    for(int index = 0; index < 1024; index++){
      std::string val = acquire_val(record_chunk[index], tid);
      if(val != EMPTY){
        c_val = std::stoi(val);
        auto w_val = (uint32_t)c_val;
        //db.db[block_num]->insert(index, w_val);
        dbl->db[block_num] = new dblock<uint32_t>(block_num);
	dbl->db[block_num]->insert(index, w_val);
     }
    }
  }
  else if(type == "double"){
    for(int index = 0; index < 1024; index++){
      std::string val = acquire_val(record_chunk[index], tid);
      if(val != EMPTY){
        auto w_val = std::stod(val);
        //db.db[block_num]->insert(index, w_val);
        dbl->db[block_num] = new dblock<double>(block_num);
	dbl->db[block_num]->insert(index, w_val);

      }
    }
  }
 else if(type == "string"){
    for(int index = 0; index < 1024; index++){
      std::string val = acquire_val(record_chunk[index], tid);
      if(val != EMPTY)
        {
          auto w_val = val;
          //dbl[block_num] = new dblock<type>(block_num);
	  //db.db[block_num]->insert(index, w_val);
        }
    }
  }
  else if(type == "bool"){
    for(int index = 0; index < 1024; index++){
      std::string val = acquire_val(record_chunk[index], tid);
      if(val != EMPTY){
        if(val == "true"){
          auto w_val = true;
          //db.db[block_num]->insert(index, w_val);
        }
        else{
          auto w_val = false;
          //db.db[block_num]->insert(index, w_val);
        }
      }
    }
  }
  else{
    std::cout << "Undefined type, exiting" << std::endl;
    exit(1);
  }
  

#pragma omp barrier
}
  }

void ingest_GPU(daba* dbl, int NO_BLOCK_GROUPS, char** col_types, int col_num)
{
 // dataBase* dB = db_addr;
  std::string types_line;
  std::string vals_line;
  fstream csv_reader;
  //int col_num;
//  virtual_dblock* d_bl = dblock;

  std::cout << "Ingesting Datasource.." << std::endl;
    
  csv_reader.open("/home/fatih/idp/chicago-taxi-rides-2016/chicago_taxi_trips_2016_01.csv");
  //csv_reader.open("/home/data/epa_hap_daily_summary.csv");
  
  getline(csv_reader, types_line);
  getline(csv_reader, vals_line);
    
  
  int record_num = 0;
  
  int num_s_record = 0;
  int num_records = 0;
  
  double start = omp_get_wtime();
  
  std::string record_chunk[1024];
  std::string check_line;
  
  double getliner = 0;
  double get_start = 0;
  double get_end = 0;
  
  for(int block_group = 0; block_group < NO_BLOCK_GROUPS; block_group++){
    for(int record = 0; record < 1024; record++){
      get_start = omp_get_wtime();
      getline(csv_reader, record_chunk[record]);
      get_end = omp_get_wtime();
      getliner += get_end-get_start;
      num_s_record++;
    }
    //std::cout << "*****Writing block group: " << block_group << "*****" <<std::endl;
    ChunktoBlocks(dbl, record_chunk, col_types, col_num, 1024, block_group);
    num_records += 1024;
  }
  double end = omp_get_wtime();
  std::cout << "Ingest took " << end-start << std::endl;
  std::cout << "Number of records inserted: " << num_records << std::endl;
  std::cout << "Number of records per second: " << (1/(end-start))*num_records << std::endl;
  std::cout << "Read time: " << getliner << std::endl;
  std::cout << "According to this: " << (1/(end-start-getliner))*num_records << std::endl;
  
//  BLOCKS_ADDR = dB->db;
}

