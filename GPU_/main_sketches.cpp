// BloomFilter.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
//#include "BFilter.h"
#include <vector>
#include <array>
#include <time.h>
#include <random>
#include <algorithm>
#include <string.h>
#include "omp.h"
#include <math.h>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <math.h>
#include "commonn.h"
//#include "HyperLogLog.h"
#include "Sketch.h"
#include <set>

using namespace std;


unsigned long long no_stream, no_unique;
unsigned no_max_threads;
double epsilon, delta;
int distNum;
double zipfAlp;


unsigned int choice;

int parseInputs(int argc, char** argv) {
	if (argc >= 6) {
		no_stream = pow(2, atoi(argv[1]));
		no_unique = pow(2, atoi(argv[2]));
		epsilon = atof(argv[3]);
		delta = atof(argv[4]);
		no_max_threads = atoi(argv[5]);
		distNum = atoi(argv[6]);
		zipfAlp = atof(argv[7]);
		choice = atoi(argv[8]);
		return 0;
	}
	else {
		return -1;
	}
}



int main(int argc, char** argv)
{
	//Get the inputs from the command line
	parseInputs(argc, argv);
	
	/** Runnning the chosen sketch
	 *  1: Standard Bloom filter
	 *  2: HyperLogLog
	 *  3: Count-Min (Not included yet)
	 **/

	sketch<uint32_t> * sk;
	uint32_t a = 32;
	BFilter<uint32_t> bf(1 >> 10, 4, 0.05);
	uint32_t bitwidth = 15;
	HyperLogLog<uint32_t> hll(bitwidth);
	uint32_t elem = 1 << bitwidth;
	switch (choice) {
	case 1: 
		//BFilter<uint32_t>::std_bloom_filter(argc, argv); break;
		//sk = &bf;
		sk = new sketch<uint32_t>(1 << 25, 4, 0.05);
		sk->std_bloom_filter(argc, argv);
		//bf.insert(&a, 128); 
		break;
	case 2: 
		sk = &hll;
		sk->HyperLogLog_run(argc, argv, elem);
		//hll.HyperLogLog_run(argc, argv, 1024);
		break;
	case 3: 
	/*
	const char *s_boyNames[] =
		{
			"Loki",
			"Alan",
			"Paul",
			"Stripes",
			"Shelby",
			"Ike",
			"Rafael",
			"Sonny",
			"Luciano",
			"Jason",
			"Brent",
			"Jed",
			"Lesley",
			"Randolph",
			"Isreal",
			"Charley",
			"Valentin",
			"Dewayne",
			"Trent",
			"Abdul",
			"Craig",
			"Andre",
			"Brady",
			"Markus",
			"Randolph",
			"Isreal",
			"Charley",
			"Brenton",
			"Herbert",
			"Rafael",
			"Sonny",
			"Luciano",
			"Joshua",
			"Ramiro",
			"Osvaldo",
			"Monty",
			"Mckinley",
			"Colin",
			"Hyman",
			"Scottie",
			"Tommy",
			"Modesto",
			"Reginald",
			"Lindsay",
			"Alec",
			"Marco",
			"Dee",
			"Randy",
			"Arthur",
			"Hosea",
			"Laverne",
			"Bobbie",
			"Damon",
			"Les",
			"Cleo",
			"Robt",
			"Rick",
			"Alonso",
			"Teodoro",
			"Rodolfo",
			"Ryann",
			"Miki",
			"Astrid",
			"Monty",
			"Mckinley",
			"Colin",
			nullptr
		};
*/

		Kmin<uint32_t> boyCounter(10, 3, 4);
	
		uint32_t aa = 5, bb= 6, cc=7, dd=8, ee=9, ff=10, gg=11;
		boyCounter.insert(&aa, 64);
		boyCounter.insert(&bb, 64);
		boyCounter.insert(&cc, 64);
		boyCounter.insert(&dd, 64);
		boyCounter.insert(&ee, 64);
		boyCounter.insert(&ff, 64);

		float a, g, h;
		boyCounter.UniqueCountEstimates(a, g, h);
		
		cout << "Arithmetic mean = " << a << " \nGeometric mean = " << g << "\nHarmonic mean = " << h << endl;
		break;
		/*
		unsigned int index = 0;
		while (s_boyNames[index] != nullptr)
		{
			boyCounter.insert(s_boyNames[index], 64);
			index++;
		}

		// get our count estimates
		float arithmeticMeanCount, geometricMeanCount, harmonicMeanCount;
		boyCounter.UniqueCountEstimates(arithmeticMeanCount, geometricMeanCount, harmonicMeanCount);

		// get our actual unique count
		std::set<const char*> actualBoyUniques;
		index = 0;
		while (s_boyNames[index] != nullptr)
		{
			actualBoyUniques.insert(s_boyNames[index]);
			index++;
		}

		// print the results!
		
		printf("Boy Names:n%u actual uniquesn", actualBoyUniques.size());
		float actualCount = (float)actualBoyUniques.size();
		printf("\n Estimated counts and percent error:n  \n Arithmetic Mean: %0.2ft%0.2f%%n"
			"  \n Geometric Mean : %0.2ft%0.2f%%n  \nHarmonic Mean  : %0.2ft%0.2f%%n",
			arithmeticMeanCount, 100.0f * (arithmeticMeanCount - actualCount) / actualCount,
			geometricMeanCount, 100.0f * (geometricMeanCount - actualCount) / actualCount,
			harmonicMeanCount, 100.0f * (harmonicMeanCount - actualCount) / actualCount);

	*/

	}
	
}

