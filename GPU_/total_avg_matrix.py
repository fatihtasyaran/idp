from texttable import Texttable

txt_name = ["scenario1_new.txt","scenario2_new.txt","scenario3_new.txt","scenario4_new.txt","scenario5_new.txt","scenario6_new.txt"]

cpu_1_count = 0
cpu_2_count = 0
cpu_3_count = 0

gpu_1_count = 0
gpu_2_count = 0
gpu_3_count = 0

total_time_cpu_1 = 0.0
total_time_cpu_2 = 0.0
total_time_cpu_3 = 0.0
total_time_gpu_1 = 0.0
total_time_gpu_2 = 0.0
total_time_gpu_3 = 0.0
total_query = 0
total_list = []

for txt in txt_name:
    f = open(txt,"r")
    line_list = f.readlines()
    for line in line_list:
        line.strip()
        if "," in line:
            split_list = line.split(",")
            if split_list[2].strip() == "CPU":
                if split_list[3].strip() == "1":
                    total_time_cpu_1 += float(split_list[0])
                    cpu_1_count += 1
                elif split_list[3].strip() == "2":
                    total_time_cpu_2 += float(split_list[0])
                    cpu_2_count += 1
                elif split_list[3].strip() == "3":
                    total_time_cpu_3 += float(split_list[0])
                    cpu_3_count += 1
            elif split_list[2].strip() == "GPU":
                if split_list[3].strip() == "1":
                    total_time_gpu_1 += float(split_list[0])
                    gpu_1_count += 1
                elif split_list[3].strip() == "2":
                    total_time_gpu_2 += float(split_list[0])
                    gpu_2_count += 1
                elif split_list[3].strip() == "3":
                    total_time_gpu_3 += float(split_list[0])
                    gpu_3_count += 1
        else:
            total_list.append(float(line))
                
        total_query += 1
    f.close()
    
try:
    cpu_q1_avg = total_time_cpu_1 / cpu_1_count
except ZeroDivisionError:
    cpu_q1_avg = 0

try:
    cpu_q2_avg = total_time_cpu_2 / cpu_2_count
except ZeroDivisionError:
    cpu_q2_avg = 0
    
try:
    cpu_q3_avg = total_time_cpu_3 / cpu_3_count
except ZeroDivisionError:
    cpu_q3_avg = 0

try:
    gpu_q1_avg = total_time_gpu_1 / gpu_1_count
except ZeroDivisionError:
    gpu_q1_avg = 0
    
try:
    gpu_q2_avg = total_time_gpu_2 / gpu_2_count
except ZeroDivisionError:
    gpu_q2_avg = 0
    
try:
    gpu_q3_avg = total_time_gpu_3 / gpu_3_count
except ZeroDivisionError:
    gpu_q3_avg = 0

t = Texttable()
t.add_rows([['----', 'CPU','GPU','FPGA'], ['Query Type 1', cpu_q1_avg, gpu_q1_avg, "0"], ['Query Type 2', cpu_q2_avg, gpu_q2_avg, "0"],['Query Type 3',cpu_q3_avg, gpu_q3_avg,"0"]])
print("***************************************** GENERAL RESULTS ****************************************")
print(t.draw())
print("----------------------------------------- Number of Query:",total_query,"---------------------------------")
print(total_list)
