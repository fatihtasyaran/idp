#include "device_launch_parameters.h"
#include <stdio.h>
#include <memory>
#include <assert.h>
#include <stddef.h>
#include <cuda.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <bits/stdc++.h>

#include <iostream>
#include "cuda_runtime.h"
#include "data_block.h"
#include <time.h>
#include <chrono>

#define ROW_S 1024
#define THREADS_PER_BLOCK 1024
#define BLOCKS_PER_BL 900
#include <omp.h>

using namespace std;
//#define NO_BLOCK_GROUPS 900
using namespace std::chrono;

#define CUDA_ERROR_CHECK

#define CudaSafeCall( err ) __cudaSafeCall( err, __FILE__, __LINE__ )
#define CudaCheckError()    __cudaCheckError( __FILE__, __LINE__ )


/*inline extern*/
inline void __cudaSafeCall( cudaError err, const char *file, const int line )
{
#ifdef CUDA_ERROR_CHECK
    if ( cudaSuccess != err )
    {
        fprintf( stderr, "cudaSafeCall() failed at %s:%i : %s\n",
                 file, line, cudaGetErrorString( err ) );
        exit( -1 );
    }
#endif

    return;
}


/*inline extern*/
inline void __cudaCheckError( const char *file, const int line )
{
#ifdef CUDA_ERROR_CHECK
    cudaError err = cudaGetLastError();
    if ( cudaSuccess != err )
    {
        fprintf( stderr, "cudaCheckError() failed at %s:%i : %s\n",
                 file, line, cudaGetErrorString( err ) );
        exit( -1 );
    }

    // More careful checking. However, this will affect performance.
    // Comment away if needed.
    err = cudaDeviceSynchronize();
    if( cudaSuccess != err )
    {
        fprintf( stderr, "cudaCheckError() with sync failed at %s:%i : %s\n",
                 file, line, cudaGetErrorString( err ) );
        exit( -1 );
    }
#endif

    return;
}


__host__ __device__ char* my_strcpy(char* dest, char* src){
  int i = 0;
  do {
    dest[i] = src[i];}
  while (src[i++] != 0);
  return dest;
}


__device__ int my_strcmp(char *str_a, char *str_b, unsigned len){
  int match = 0;
  unsigned i = 0;
  unsigned done = 0;
 printf("Here it is\n");  
 while ((i < len) && (match == 0) && !done){
    if ((str_a[i] == 0) || (str_b[i] == 0)) done = 1;
    else if (str_a[i] != str_b[i]){
      match = i+1;
      if ((int)str_a[i] - (int)str_b[i] < 0) match = 0 - (i + 1);}
    i++;
 printf("Here again \n");
}
  
 return match;
}


__device__ int STRCMP( char *p1,  char *p2)
{
  const unsigned char *s1 = (const unsigned char *) p1;
  const unsigned char *s2 = (const unsigned char *) p2;
  unsigned char c1, c2;
  do
    {
      c1 = (unsigned char) *s1++;
      c2 = (unsigned char) *s2++;
      if (c1 == '\0')
        return c1 - c2;
    }
  while (c1 == c2);
  return c1 - c2;
}

//***********************************************GPU KERNELS***********************************************//

//*********************************************************************************************************//
__global__ void GPU_SCAN(char** d_final , int ts_col_num, int NO_BLOCK_GROUPS, int col_num, char** d_join){


  int index = blockIdx.x * blockDim.x + threadIdx.x;

     for(int t=0; t<NO_BLOCK_GROUPS*1024/512; t++)

    { 

//      d_join[(index*39+t)] = db_addr_gpu[blockIdx.x].rows[(index*39+t)];

	d_join[index+t*512] = d_final[index+t*512];
	
    }


}


__global__ void GPU_SCAN_DEP(double* d_final, int ts_col_num, int NO_BLOCK_GROUPS, int col_num, double* d_join, double d_dependency_value){

 int index = blockIdx.x * blockDim.x + threadIdx.x;

     for(int t=0; t<=NO_BLOCK_GROUPS*1024/512; t++) // 1024/1

{
	if(d_dependency_value == d_final[index+t*512]){
//		printf("Match \n");
		d_join[index+t*512] = d_final[index+t*512];
		
	}
    }
}



__global__ void GPU_MULT_QUERY(double* d_col1, double* d_col2, double d_dep_value1, double d_dep_value2, int NO_BLOCK_GROUPS, int num_cols, double* d_join){

 int index = blockIdx.x * blockDim.x + threadIdx.x;

	for(int t=0; t<=NO_BLOCK_GROUPS*1024/(512); t++){
//		if(d_dep_value1 == d_col1[t] && d_dep_value2 == d_col2[t]){
		if(d_dep_value1 == d_col1[index + t*512] && d_dep_value2 == d_col2[index +t*512]){
			d_join[index+t*512] = d_col1[index +t*512];
		   
		}
	}

}


__global__ void GPU_MULT_QUERY2(double* d_col1, double* d_col2, double* d_col3, double* d_vals, int NO_BLOCK_GROUPS, int num_cols, double* d_join){

 int index = blockIdx.x * blockDim.x + threadIdx.x;

        for(int t=0; t<=NO_BLOCK_GROUPS*1024/(512); t++){
//                if(d_vals[0] == d_col1[t] && d_vals[1] == d_col2[t] && d_vals[2] == d_col3[t]){
		if(d_vals[0] == d_col1[index +t*512] && d_vals[1] == d_col2[index +t*512] && d_vals[2] == d_col3[index +t*512]){
			//printf("MATCH \n");
                        d_join[index +t*512] = d_col1[index +t*512];

                }
        }

}


__global__ void GPU_MULT_QUERY3(double* d_col1, double* d_col2, double* d_col3, double* d_col4, double* d_vals, int NO_BLOCK_GROUPS, int num_cols, double* d_join){

 int index = blockIdx.x * blockDim.x + threadIdx.x;

        for(int t=0; t<=NO_BLOCK_GROUPS*1024/(1); t++){
//                if(d_dep_value1 == d_col1[t] && d_dep_value2 == d_col2[t] && d_dep_value3 == d_col3[t]){
                if(d_vals[0] == d_col1[index+t*512] && d_vals[1] == d_col2[index +t*512] && d_vals[2] == d_col3[index+t*512] && d_vals[3] == d_col4[index+t*512]){
			//printf("MATCH \n");
                        d_join[index +t*512] = d_col1[index +t*512];

                }
        }

}


__global__ void GPU_MULT_QUERY4(double* d_col1, double* d_col2, double* d_col3, double* d_col4, double* d_col5, double* d_vals, int NO_BLOCK_GROUPS, int num_cols, double* d_join){

 int index = blockIdx.x * blockDim.x + threadIdx.x;

        for(int t=0; t<=NO_BLOCK_GROUPS*1024/(1); t++){

                if(d_vals[0] == d_col1[t] && d_vals[1] == d_col2[t] && d_vals[2] == d_col3[t] && d_vals[3] == d_col4[t] && d_vals[4] == d_col5[t]){
                        printf("MATCH \n");
                        d_join[t] = d_col1[t];

                }
        }

}


//*********************************************************************************************************//





//char**  GPU_scan_db(dataBase* db_addr, int target_col_num, int ts_col_num, int NO_BLOCK_GROUPS, int col_num, char* col_type, char** timeseries){
char**  GPU_scan_db(dataBase* db_addr, char** target_col_num, int ts_col_num, int NO_BLOCK_GROUPS, int col_num, char* col_type, char** timeseries){

	char** final = target_col_num;

	time_t start, end;		
	high_resolution_clock::time_point t1 = high_resolution_clock::now();

	cout << "The column type is " << col_type << endl;	

time(&start);
	//Allocating memory space for join array (the result of the query) in CPU
//	char** join = new char*[NO_BLOCK_GROUPS*1024];    // Each thread will probably create the join table - create it before calling GPU_s$
	
	char** join = (char**)malloc(sizeof(char*)*NO_BLOCK_GROUPS * 1024);

	for(int i = 0; i < NO_BLOCK_GROUPS*1024; i++){
		join[i] = (char*)malloc(sizeof(char)*100);
	}		
	
	//Allocating memory space for join array (the result of the query) in GPU
	
	int t_SIZE = NO_BLOCK_GROUPS*1024*2;
	char** d_join;
	char** d_final;

	CudaSafeCall(cudaMalloc((void **)&d_join, 900 * 1024 * sizeof(char*)));
	CudaCheckError();

	CudaSafeCall(cudaMalloc((void **)&d_final, 900 * 1024 * sizeof(char*)));
        CudaCheckError();

	CudaSafeCall(cudaMemcpy(d_join, join, 900*1024*sizeof(char*), cudaMemcpyHostToDevice));
	CudaCheckError();
	
	CudaSafeCall(cudaMemcpy(d_final, final, 900*1024*sizeof(char*), cudaMemcpyHostToDevice));
        CudaCheckError();


	int size = NO_BLOCK_GROUPS * 1024 *2;
	char** tempo = (char**)malloc(size * sizeof(char*));
	
	for(int i = 0; i < NO_BLOCK_GROUPS*1024*2; i++){
                tempo[i] = (char*)malloc(sizeof(char)*100);

	}

		
	cout << "Running the GPU-query" << endl;
	//Calling the kernel from GPU_funcs.cu
//	int pos = 0;

	GPU_SCAN<<<64,512>>>(d_final , ts_col_num, NO_BLOCK_GROUPS, col_num, d_join);
        cudaDeviceSynchronize();
	CudaCheckError();

	cout << "Results are recovererd" << endl;		
	cudaMemcpy(join, d_join, NO_BLOCK_GROUPS * 1024 * sizeof(char*), cudaMemcpyDeviceToHost);
	CudaCheckError();

time(&end);
double elapsed_time = double(end - start) / double(CLOCKS_PER_SEC);
//high_resolution_clock::time_point t2 = high_resolution_clock::now();
//duration<std::string> res_time = duration_cast<duration<std::string>>(t2 - t1);

	//int max_threads = omp_get_max_threads();	
	int max_threads = 70;


	for(int i = 0; i < NO_BLOCK_GROUPS*1024/*max_threads*/; i+=2){
               if (join[i]){ 
		tempo[/*tid*2+*/i] = join[/*tid*2+*/i];		
		tempo[/*tid*(2)+*/i+1] = timeseries[/*tid*2+*/i];		
               }
	}
//	strcpy(tempo[0], std::to_string(elapsed_time).c_str());
	return tempo;

}



//char**  GPU_scan_db_dep(dataBase* db_addr, int target_col_num, int ts_col_num, int NO_BLOCK_GROUPS, int col_num, int filter_col_num, char* dependency_value, char** timeseries, char* col_type){
char**  GPU_scan_db_dep(dataBase* db_addr, char** target_col_num, int ts_col_num, int NO_BLOCK_GROUPS, int col_num, int filter_col_num, char* dependency_value, char** timeseries, char* col_type){

time_t start, end, start2, end2, start1, end1;

//high_resolution_clock::time_point t1 = high_resolution_clock::now();

time(&start);

	char** final = (char**)malloc(sizeof(char*) * NO_BLOCK_GROUPS * 1024);
	high_resolution_clock::time_point t2 = high_resolution_clock::now();

	final = target_col_num;
	dataBase* dB = db_addr;		
	//cout << "The column type is " << col_type << endl;	

	cout << "The column type is: " << col_type << endl;
	//Allocating memory space for join array (the result of the query) in CPU
//	char** join = new char*[NO_BLOCK_GROUPS*1024];    // Each thread will probably create the join table - create it before calling GPU_s$
	
	//char** join = (char**)malloc(sizeof(char*)*NO_BLOCK_GROUPS * 1024);
	double *join = (double*)malloc(NO_BLOCK_GROUPS*1024*sizeof(double));
	double *find = (double*)malloc(NO_BLOCK_GROUPS*1024*sizeof(double)); 


cout << "Before converting" << endl;
	for(int i = 0; i < NO_BLOCK_GROUPS*1020; i++){
		//cout << i << endl;
		if(!final[i]){
			find[i] = 0.0;
		}else{
			find[i] = stod(final[i]);
		}
	}
	
	int t_SIZE = NO_BLOCK_GROUPS*1024*2;
	//char** d_join;
	double *d_join;
	char** d_final;
	double* d_find;


cout << "Before allocation " << endl;

//high_resolution_clock::time_point malloc1 = high_resolution_clock::now();

	CudaSafeCall(cudaMalloc((void **)&d_join, 900 * 1024 * sizeof(double)));
	CudaCheckError();
	//cout << "Test 1.5" << endl;
	CudaSafeCall(cudaMalloc((void **)&d_final, 900 * 1024 * sizeof(char*)));
        CudaCheckError();

	CudaSafeCall(cudaMalloc((void **)&d_find, 900 * 1024 * sizeof(double)));
        CudaCheckError();

//high_resolution_clock::time_point malloc2 = high_resolution_clock::now();
//duration<double> malloc_time = duration_cast<duration<double>>(malloc2 - malloc1);
//std::cout << "MEM ALLOCATION TOOK:  " << malloc_time.count() << " seconds " << endl;


//high_resolution_clock::time_point mem_copy1 = high_resolution_clock::now();

	//cout << "Test 2" << endl;
	CudaSafeCall(cudaMemcpy(d_join, join, 900*1024*sizeof(double), cudaMemcpyHostToDevice));
	CudaCheckError();
	//cout << "Test 2.5" << endl;
	CudaSafeCall(cudaMemcpy(d_final, final, 900*1024*sizeof(char*), cudaMemcpyHostToDevice));
        CudaCheckError();

	CudaSafeCall(cudaMemcpy(d_find, find, 900*1024*sizeof(double), cudaMemcpyHostToDevice));
        CudaCheckError();

//high_resolution_clock::time_point mem_copy2 = high_resolution_clock::now();
//duration<double> memcpy_time = duration_cast<duration<double>>(mem_copy2 - mem_copy1);
//std::cout << "MEM COPY TOOK:  " << memcpy_time.count() << " seconds " << endl;


	int size = NO_BLOCK_GROUPS * 1024 *2;
	char** tempo = (char**)malloc(size * sizeof(char*));
	
	for(int i = 0; i < NO_BLOCK_GROUPS*1024*2; i++){
                tempo[i] = (char*)malloc(sizeof(char)*100);

	}

		
	cout << "Running the GPU-query for Query-2" << endl;
	//Calling the kernel from GPU_funcs.cu
//	int pos = 0;
	
	double d_dependency_value = atof(dependency_value);
	cout << "Dep value is: " << d_dependency_value << endl;


	bool exist_bool = false;
	std::string s(dependency_value);
	for (int i=0; i<NO_BLOCK_GROUPS * 1024 ; i++){
		if (dB->db[i]->bf_query(s))
	
	{
			exist_bool = true;
			break;
		}
	}


//time(&end);

	if (exist_bool){
//time(&start1);

	GPU_SCAN_DEP<<<64, 512>>>(d_find , ts_col_num, NO_BLOCK_GROUPS, col_num, d_join, d_dependency_value*1.0);

//time(&end1);
//time(&start2);


//high_resolution_clock::time_point res1 = high_resolution_clock::now();
        cudaDeviceSynchronize();
	CudaCheckError();

	cout << "Results are recovererd" << endl;	

	
	cudaMemcpy(join, d_join, NO_BLOCK_GROUPS * 1024 * sizeof(double), cudaMemcpyDeviceToHost);
	CudaCheckError();

	//int max_threads = omp_get_max_threads();	
	int max_threads = 70;

	for(int i = 0; i < NO_BLOCK_GROUPS*1024/*max_threads*/; i+=2){
//              strcpy(tempo[/*tid*2+*/i] , 
//		if (i==0)
//			strcpy(tempo[i], std::to_string(elapsed_time).c_str());
//		else{
		string s = std::to_string(join[/*tid*2+*/i]);
		s = s.substr(0, 6);
		if(join[i]==d_dependency_value){		
			for (int j = 0; j < sizeof(tempo[i]); j++) { 
				tempo[i][j] = s[j]; 
			}
//		cout << tempo[i] << endl; 
		tempo[/*tid*(2)+*/i+1] = timeseries[/*tid*2+*/i];		
        }else{
		tempo[i] = "";
		//tempo[i+1] = timeseries[i];
//		}
	}
	}

time(&end);
double elapsed_time = double(end - start) / double(CLOCKS_PER_SEC);
//tempo[0] = (char*)malloc(sizeof(char)*100);
//strcpy(tempo[0], std::to_string(elapsed_time).c_str());

//high_resolution_clock::time_point res2 = high_resolution_clock::now();
//duration<double> res_time = duration_cast<duration<double>>(res2 - res1);
//std::cout << "RECOVERING RESULTS TOOK:  " << res_time.count() << " seconds " << endl;


//high_resolution_clock::time_point t2 = high_resolution_clock::now();
//duration<double> res_time = duration_cast<duration<double>>(t2 - t1);

//strcpy(tempo[0], std::to_string(elapsed_time).c_str());

//}
//	double elapsed_time = double(end - start) / double(CLOCKS_PER_SEC);
/*
if(exist_bool){
	double el_time2 = double(end2 - start2) / double(CLOCKS_PER_SEC);
	double total_el_time = el_time2 + elapsed_time;

//	double kernel_time = double(end1 - start1) / double(CLOCKS_PER_SEC);
//	cout << "THE KERNEL TIME IS: "<< fixed << kernel_time << setprecision(5) << endl;

//	high_resolution_clock::time_point t2 = high_resolution_clock::now();
//	duration<double> time_span = duration_cast<duration<double>>(t2 - t1);
//	std::cout << "KERNEL TIME :  " << time_span.count() << " seconds " << endl;
//	cout << "TIME TAKEN IN ALLOCATION AND COMMUNICATION IS: "<< fixed << total_el_time  << setprecision(5) << endl;
}else{
	cout << "TIME TAKEN IN ALLOCATION AND COMMUNICATION IS: "<< fixed << elapsed_time  << setprecision(5) << endl;
}
*/
}

	return tempo;





  }



char**  multi_gpu_query(dataBase* db_addr, int* target_col_num, char** target1, char** target2, int num_cols, int NO_BLOCK_GROUPS, int col_num, char** dependency_values, char** timeseries){

time_t start, start2, end, end2, start1, end1;
time(&start);
high_resolution_clock::time_point t1 = high_resolution_clock::now();

////	cout << "From CPU: " << dependency_values[0] << endl;
////	cout << "From CPU: " << dependency_values[1] << endl;
	dataBase* dB = db_addr;

	char*** cols = (char***)malloc(sizeof(char**)*num_cols);
	high_resolution_clock::time_point t2 = high_resolution_clock::now();

	cols[0] = target1;
	cols[1] = target2;

/*	for(int i=0; i<num_cols; i++){
		cols[i] = copyDataBlock(db_addr, target_col_num[i], NO_BLOCK_GROUPS, col_num);	
		
	}
*/
////	cout << "After copying the data blocks" << endl;

	double* join = (double*)malloc(sizeof(double)*NO_BLOCK_GROUPS * 1024);
//	for(int i = 0; i < NO_BLOCK_GROUPS*1024; i++){
//                join[i] = (char*)malloc(sizeof(char)*100);
	//	cout << cols[1][i] << endl;
//        }

//	cout << "After allocating join array" << endl;


	double* d_join;
	double d_dep_value1;
	double d_dep_value2;
	double* d_col1;
	double* d_col2;
	double* Col0 = (double*)malloc(sizeof(double)*NO_BLOCK_GROUPS * 1024);//= cols[0];
	double* Col1 = (double*)malloc(sizeof(double)*NO_BLOCK_GROUPS * 1024);//= cols[1];
	int cpt = 0;
	for(int i = 0; i < NO_BLOCK_GROUPS*992 ; i++){

                if(!cols[0][i]){
                        Col0[i] = 0.0;
                }else{
                        Col0[i] = stod(cols[0][i]);
		//	cout << Col0[i] << endl;
                }
		cpt ++;
        }
//	cout << "First counter: "<< cpt << endl;
	int cpt1 =0;
	for(int i = 0; i < NO_BLOCK_GROUPS*1024 - 2; i++){

                if(!cols[1][i]){
                        Col1[i] = 0.0;
                }else{
                        Col1[i] = stod(cols[1][i]);
                }
		cpt1++;
        }
//	cout << "Second counter: "<< cpt1 << endl;



high_resolution_clock::time_point malloc1 = high_resolution_clock::now();

	CudaSafeCall(cudaMalloc((void **)&d_join, 900 * 1024 * sizeof(double)));
        CudaCheckError();

	CudaSafeCall(cudaMemcpy(d_join, join, 900*1024*sizeof(double), cudaMemcpyHostToDevice));
        CudaCheckError();

	CudaSafeCall(cudaMalloc((void **)&d_dep_value1, sizeof(double)));
        CudaCheckError();
	CudaSafeCall(cudaMalloc((void **)&d_dep_value2, sizeof(double)));
        CudaCheckError();


	double val1 = stod(dependency_values[0]);
	double val2 = stod(dependency_values[1]);
	
/*	CudaSafeCall(cudaMemcpy(d_dep_value1, val1,  sizeof(double), cudaMemcpyHostToDevice));
	CudaCheckError();
	CudaSafeCall(cudaMemcpy(d_dep_value2, val2,  sizeof(double), cudaMemcpyHostToDevice));
	CudaCheckError();
*/
//	cout << "After Memcpys" << endl;

	CudaSafeCall(cudaMalloc((void **)&d_col1, 900 * 1024 * sizeof(double)));
        CudaCheckError();
	CudaSafeCall(cudaMalloc((void **)&d_col2, 900 * 1024 * sizeof(double)));
        CudaCheckError();

//high_resolution_clock::time_point malloc2 = high_resolution_clock::now();
//duration<double> malloc_time = duration_cast<duration<double>>(malloc1 - malloc2);
//std::cout << "MEM ALLOCATION TOOK:  " << malloc_time.count() << " seconds " << endl;


//	cout << "After d_cols init" << endl;

/*	for(int i=0; i<num_cols; i++){

                CudaSafeCall(cudaMalloc((void **)&(d_cols[0]), 900 * 1024 * sizeof(char*)));
                CudaCheckError();

                CudaSafeCall(cudaMalloc((void **)&(d_cols[1]), 900 * 1024 * sizeof(char*)));
                CudaCheckError();


        }
*/

//	cout << "After d_cols copying" << endl;

//	for(int i=0; i<num_cols; i++){

high_resolution_clock::time_point mem_copy1 = high_resolution_clock::now();

		CudaSafeCall(cudaMemcpy(d_col1, Col0, 900*1024*sizeof(double), cudaMemcpyHostToDevice));
	        CudaCheckError();
		CudaSafeCall(cudaMemcpy(d_col2, Col1, 900*1024*sizeof(double), cudaMemcpyHostToDevice));
	        CudaCheckError();

//high_resolution_clock::time_point mem_copy2 = high_resolution_clock::now();
//duration<double> memcpy_time = duration_cast<duration<double>>(mem_copy1 - mem_copy2);
//std::cout << "MEM COPY TOOK:  " << memcpy_time.count() << " seconds " << endl;

	
//	}	



	int size = NO_BLOCK_GROUPS * 1024 *2;
        char** results = (char**)malloc(size * sizeof(char*));

        for(int i = 0; i < NO_BLOCK_GROUPS*1024*2; i++){
                results[i] = (char*)malloc(sizeof(char)*100);

        }

//	cout << "Before the kernel call" << endl;
	bool exist_bool_1 = false;
	bool exist_bool_2 = false;
	std::string s1(dependency_values[0]);
	std::string s2(dependency_values[1]);


	for (int i=0; i<NO_BLOCK_GROUPS * 1024; i++){
		if (dB->db[i]->bf_query(s1))
		{
			exist_bool_1 = true;
			//break;
		}

		if (dB->db[i]->bf_query(s2)){
                        exist_bool_2 = true;
                        //break;

                }
	if (exist_bool_1 && exist_bool_2)
		break;

	}

/*	if(exist_bool_1)
	for (int i=0; NO_BLOCK_GROUPS * 1024; i++){
		if (dB->db[i]->bf_query(s2)){
			exist_bool_2 = true;
			break;
	
		}
	}
*/
///time(&end);

	if (exist_bool_1 && exist_bool_2){
//time(&start1);

	GPU_MULT_QUERY<<<64, 512>>>(d_col1, d_col2, val1, val2, NO_BLOCK_GROUPS, num_cols, d_join);

//time(&end1);
time(&start2);


	high_resolution_clock::time_point res1 = high_resolution_clock::now();
	cudaDeviceSynchronize();
        CudaCheckError();

	cudaMemcpy(join, d_join, NO_BLOCK_GROUPS * 1024 * sizeof(double), cudaMemcpyDeviceToHost);
        CudaCheckError();

	for(int i = 0; i < NO_BLOCK_GROUPS*1024/*max_threads*/; i+=2){
		string s = std::to_string(join[/*tid*2+*/i]);
                s = s.substr(0, 6);
		if(s.compare("0.0000") != 0){
		for (int j = 0; j < sizeof(results[i]); j++) {
                        results[i][j] = s[j];
                }

//		  results[i] = join[i];
                        results[i+1] = timeseries[i];
		}
		else{
			results[i] = "";
                        //results[i+1] = timeseries[i];
	
		}
        }

//high_resolution_clock::time_point res2 = high_resolution_clock::now();
//duration<double> res_time = duration_cast<duration<double>>(res2 - res1);
//std::cout << "RECOVERING RESULTS TOOK:  " << res_time.count() << " seconds " << endl;


time(&end);
//}

	double elapsed_time = double(end - start) / double(CLOCKS_PER_SEC);
//	high_resolution_clock::time_point t2 = high_resolution_clock::now();
//	duration<double> res_time = duration_cast<duration<double>>(t2 - t1);

//	strncpy(results[0], std::to_string(elapsed_time).c_str(), sizeof(results[0]-1));
/*
if(exist_bool_1 & exist_bool_2){
	double el_time2 = double(end2 - start2) / double(CLOCKS_PER_SEC);
	double total_el_time = elapsed_time + el_time2;

//	double kernel_time = double(end1 - start1) / double(CLOCKS_PER_SEC);
//	cout << "THE KERNEL TIME IS: "<< fixed << kernel_time << setprecision(5) << endl;

//	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	duration<double> time_span = duration_cast<duration<double>>(t2 - t1);
	std::cout << "KERNEL TIME :  " << time_span.count() << " seconds" << endl;
	cout << "TIME TAKEN IN ALLOCATION AND COMMUNICATION IS: "<< fixed << total_el_time  << setprecision(5) << endl;	
}else{

	cout << "TIME TAKEN IN ALLOCATION AND COMMUNICATION IS: "<< fixed << elapsed_time  << setprecision(5) << endl;
}
*/
 }	
//	strcpy(results[0], std::to_string(elapsed_time).c_str());
	return results;
}



char**  multi_gpu_query2(dataBase* db_addr, int* target_col_num, char** target1, char** target2, char** target3, int num_cols, int NO_BLOCK_GROUPS, int col_num, char** dependency_values, char** timeseries){

time_t start, start2, end, end2, start1, end1;
time(&start);
high_resolution_clock::time_point t1 = high_resolution_clock::now(); 

	dataBase* dB = db_addr;
        char*** cols = (char***)malloc(sizeof(char**)*num_cols);
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
/*
        for(int i=0; i<num_cols; i++){
                cols[i] = copyDataBlock(db_addr, target_col_num[i], NO_BLOCK_GROUPS, col_num);

        }
*/
	cols[0] = target1;
	cols[1] = target2;
	cols[2] = target3;

//cout << "After copying data blocks" << endl;

        double* join = (double*)malloc(sizeof(double)*NO_BLOCK_GROUPS * 1024);

//      for(int i = 0; i < NO_BLOCK_GROUPS*1024; i++){
//                join[i] = (char*)malloc(sizeof(char)*100);
        //      cout << cols[1][i] << endl;
//        }


        double* d_join;
        double d_dep_value1;
        double d_dep_value2;
        double d_dep_value3;
        double* d_col1;
        double* d_col2;
        double* d_col3;

        double* Col0 = (double*)malloc(sizeof(double)*NO_BLOCK_GROUPS * 1024);
        double* Col1 = (double*)malloc(sizeof(double)*NO_BLOCK_GROUPS * 1024);
        double* Col2 = (double*)malloc(sizeof(double)*NO_BLOCK_GROUPS * 1024);


//        int cpt = 0;
        for(int i = 0; i < NO_BLOCK_GROUPS*992 ; i++){

                if(!cols[0][i]){
                        Col0[i] = 0.0;
                }else{
                        Col0[i] = stod(cols[0][i]);
                //      cout << Col0[i] << endl;
                }
//                cpt ++;
        }

        for(int i = 0; i < NO_BLOCK_GROUPS*1024 - 2; i++){

                if(!cols[1][i]){
                        Col1[i] = 0.0;
                }else{
                        Col1[i] = stod(cols[1][i]);
                }
        }

//cout << "Before casting " << endl;

        for(int i = 0; i < NO_BLOCK_GROUPS*992; i++){

                if(!cols[2][i]){
                        Col2[i] = 0.0;
                }else{
                        Col2[i] = stod(cols[2][i]);
                }
        }

//cout << "After casting" << endl;


high_resolution_clock::time_point malloc1 = high_resolution_clock::now();

        CudaSafeCall(cudaMalloc((void **)&d_join, 900 * 1024 * sizeof(double)));
        CudaCheckError();

        CudaSafeCall(cudaMemcpy(d_join, join, 900*1024*sizeof(double), cudaMemcpyHostToDevice));
        CudaCheckError();

        CudaSafeCall(cudaMalloc((void **)&d_dep_value1, sizeof(double)));
        CudaCheckError();
	CudaSafeCall(cudaMalloc((void **)&d_dep_value2, sizeof(double)));
        CudaCheckError();
	CudaSafeCall(cudaMalloc((void **)&d_dep_value3, sizeof(double)));
        CudaCheckError();

/*        double val1 = stod(dependency_values[0]);
        double val2 = stod(dependency_values[1]);
        double val3 = stod(dependency_values[2]);
*/

	double* vals = (double*)malloc(sizeof(double)*num_cols);
        double* d_vals;

        for(int i=0; i<num_cols; i++){
                vals[i] = stod(dependency_values[i]);

        }

        CudaSafeCall(cudaMalloc((void **)&d_vals, num_cols * sizeof(double)));
        CudaCheckError();

        CudaSafeCall(cudaMemcpy(d_vals, vals, num_cols * sizeof(double*), cudaMemcpyHostToDevice));
        CudaCheckError();



//cout << "After vals" << endl;

        CudaSafeCall(cudaMalloc((void **)&d_col1, 900 * 1024 * sizeof(double)));
        CudaCheckError();
        CudaSafeCall(cudaMalloc((void **)&d_col2, 900 * 1024 * sizeof(double)));
        CudaCheckError();
        CudaSafeCall(cudaMalloc((void **)&d_col3, 900 * 1024 * sizeof(double)));
        CudaCheckError();

//high_resolution_clock::time_point malloc2 = high_resolution_clock::now();
//duration<double> malloc_time = duration_cast<duration<double>>(malloc1 - malloc2);
//std::cout << "MEM ALLOCATION TOOK:  " << malloc_time.count() << " seconds " << endl;


/*      for(int i=0; i<num_cols; i++){

                CudaSafeCall(cudaMalloc((void **)&(d_cols[0]), 900 * 1024 * sizeof(char*)));
                CudaCheckError();

                CudaSafeCall(cudaMalloc((void **)&(d_cols[1]), 900 * 1024 * sizeof(char*)));
                CudaCheckError();


        }
*/

//cout << "Before d_cols alloc" << endl;

//      for(int i=0; i<num_cols; i++){

high_resolution_clock::time_point mem_copy1 = high_resolution_clock::now();

                CudaSafeCall(cudaMemcpy(d_col1, Col0, 900*1024*sizeof(double), cudaMemcpyHostToDevice));
                CudaCheckError();
                CudaSafeCall(cudaMemcpy(d_col2, Col1, 900*1024*sizeof(double), cudaMemcpyHostToDevice));
                CudaCheckError();
                CudaSafeCall(cudaMemcpy(d_col3, Col2, 900*1024*sizeof(double), cudaMemcpyHostToDevice));
                CudaCheckError();

//high_resolution_clock::time_point mem_copy2 = high_resolution_clock::now();
//duration<double> memcpy_time = duration_cast<duration<double>>(mem_copy1 - mem_copy2);
//std::cout << "MEM COPY TOOK:  " << memcpy_time.count() << " seconds " << endl;


//      }


        int size = NO_BLOCK_GROUPS * 1024 *2;
        char** results = (char**)malloc(size * sizeof(char*));

        for(int i = 0; i < NO_BLOCK_GROUPS*1024*2; i++){
                results[i] = (char*)malloc(sizeof(char)*100);

        }

//cout << "Before the kernel" << endl;

	bool exist_bool_1 = false;
        bool exist_bool_2 = false;
	bool exist_bool_3 = false;
	std::string s1(dependency_values[0]);
	std::string s2(dependency_values[1]);
	std::string s3(dependency_values[2]);


        for (int i=0; i<NO_BLOCK_GROUPS * 1024; i++){
                if (dB->db[i]->bf_query(s1))
                {
                        exist_bool_1 = true;
                        //break;
                }

		if (dB->db[i]->bf_query(s2)){
                        exist_bool_2 = true;
                        //break;

                }

		if (dB->db[i]->bf_query(s3)){
                        exist_bool_3 = true;
                        //break;

                }


	if (exist_bool_1 && exist_bool_2 && exist_bool_3)
		break;

        }

/*        if(exist_bool_1)
        for (int i=0; NO_BLOCK_GROUPS * 1024; i++){
                if (dB->db[i]->bf_query(s2)){
                        exist_bool_2 = true;
                        break;

                }
        }
	if(exist_bool_2)
	for (int i=0; NO_BLOCK_GROUPS * 1024; i++){
                if (dB->db[i]->bf_query(s3)){
                        exist_bool_3 = true;
                        break;

                }
        }
*/

///time(&end);

	if (exist_bool_1 && exist_bool_2 && exist_bool_3){
//time(&start1);

       GPU_MULT_QUERY2<<<64, 512>>>(d_col1, d_col2, d_col3, d_vals, NO_BLOCK_GROUPS, num_cols, d_join);

//time(&end1);
time(&start2);


high_resolution_clock::time_point res1 = high_resolution_clock::now();
        cudaDeviceSynchronize();
        CudaCheckError();
//cout << "The End" << endl;


	cudaMemcpy(join, d_join, NO_BLOCK_GROUPS * 1024 * sizeof(double), cudaMemcpyDeviceToHost);
        CudaCheckError();

        for(int i = 0; i < NO_BLOCK_GROUPS*1024/*max_threads*/; i+=2){
                string s = std::to_string(join[/*tid*2+*/i]);
                s = s.substr(0, 6);
		if(s.compare("0.0000") != 0){
                for (int j = 0; j < sizeof(results[i]); j++) {
                        results[i][j] = s[j];
                }

	                //results[i] = join[i];
                        results[i+1] = timeseries[i];
                }
                else{
                        results[i] = "";
        //                results[i+1] = timeseries[i];

                }
        }
//high_resolution_clock::time_point res2 = high_resolution_clock::now();
//duration<double> res_time = duration_cast<duration<double>>(res2 - res1);
//std::cout << "RECOVERING RESULTS TOOK:  " << res_time.count() << " seconds " << endl;

time(&end);
//}

	double elapsed_time = double(end - start) / double(CLOCKS_PER_SEC);
//	high_resolution_clock::time_point t2 = high_resolution_clock::now();
//	duration<double> res_time = duration_cast<duration<double>>(t2 - t1);

//	strncpy(results[0], std::to_string(elapsed_time).c_str(), sizeof(results[0]-1));
/*
if (exist_bool_1 && exist_bool_2 && exist_bool_3){

	double el_time2 = double(end2 - start2) / double(CLOCKS_PER_SEC);
	double total_el_time = elapsed_time + el_time2;

	double kernel_time = double(end1 - start1) / double(CLOCKS_PER_SEC);
	cout << "THE KERNEL TIME IS: "<< fixed << kernel_time << setprecision(5) << endl;

	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	duration<double> time_span = duration_cast<duration<double>>(t2 - t1);
	std::cout << "KERNEL TIME : " << time_span.count() << " seconds "<< endl;	
	cout << "TIME TAKEN IN ALLOCATION AND COMMUNICATION IS: "<< fixed << total_el_time  << setprecision(5) << endl;

}else{
	cout << "TIME TAKEN IN ALLOCATION AND COMMUNICATION IS: "<< fixed << elapsed_time  << setprecision(5) << endl;
}
*/
 }
        return results;
}



char**  multi_gpu_query3(dataBase* db_addr, int* target_col_num, char** target1, char** target2, char** target3, char** target4, int num_cols, int NO_BLOCK_GROUPS, int col_num, char** dependency_values, char** timeseries){


////	cout << "From CPU: " << dependency_values[0] << endl;
////	cout << "From CPU: " << dependency_values[1] << endl;
////	cout << "From CPU: " << dependency_values[2] << endl;
////	cout << "From CPU: " << dependency_values[3] << endl;

time_t start, start2, end, end2, start1, end1;
time(&start);
high_resolution_clock::time_point t1 = high_resolution_clock::now();

	dataBase* dB = db_addr;
        char*** cols = (char***)malloc(sizeof(char**)*num_cols);
	high_resolution_clock::time_point t2 = high_resolution_clock::now();

/*
        for(int i=0; i<num_cols; i++){
                cols[i] = copyDataBlock(db_addr, target_col_num[i], NO_BLOCK_GROUPS, col_num);

        }
*/
	cols[0] = target1;
	cols[1] = target2;
	cols[2] = target3;
	cols[3] = target4;
	
        double* join = (double*)malloc(sizeof(double)*NO_BLOCK_GROUPS * 1024);
//      for(int i = 0; i < NO_BLOCK_GROUPS*1024; i++){
//                join[i] = (char*)malloc(sizeof(char)*100);
        //      cout << cols[1][i] << endl;
//        }


        double* d_join;
        double* d_dep_values;
        //double d_dep_value2;
        //double d_dep_value3;
        double* d_col1;
        double* d_col2;
        double* d_col3;
        double* d_col4;

        double* Col0 = (double*)malloc(sizeof(double)*NO_BLOCK_GROUPS * 1024);
        double* Col1 = (double*)malloc(sizeof(double)*NO_BLOCK_GROUPS * 1024);
        double* Col2 = (double*)malloc(sizeof(double)*NO_BLOCK_GROUPS * 1024);
        double* Col3 = (double*)malloc(sizeof(double)*NO_BLOCK_GROUPS * 1024);


        for(int i = 0; i < NO_BLOCK_GROUPS*992 ; i++){

                if(!cols[0][i]){
                        Col0[i] = 0.0;
                }else{
                        Col0[i] = stod(cols[0][i]);
                //      cout << Col0[i] << endl;
                }
        }

        for(int i = 0; i < NO_BLOCK_GROUPS*1024 - 2; i++){

                if(!cols[1][i]){
                        Col1[i] = 0.0;
                }else{
                        Col1[i] = stod(cols[1][i]);
                }
        }


	for(int i = 0; i < NO_BLOCK_GROUPS*1024 - 2; i++){

                if(!cols[2][i]){
                        Col2[i] = 0.0;
                }else{
                        Col2[i] = stod(cols[2][i]);
                }

		if(!cols[3][i]){
                        Col3[i] = 0.0;
                }else{
                        Col3[i] = stod(cols[3][i]);
                }

        }



high_resolution_clock::time_point malloc1 = high_resolution_clock::now();

        CudaSafeCall(cudaMalloc((void **)&d_join, 900 * 1024 * sizeof(double)));
        CudaCheckError();

        CudaSafeCall(cudaMemcpy(d_join, join, 900*1024*sizeof(double), cudaMemcpyHostToDevice));
        CudaCheckError();

        CudaSafeCall(cudaMalloc((void **)&d_dep_values, sizeof(double*)));
        CudaCheckError();


/*
        CudaSafeCall(cudaMalloc((void **)&d_dep_value1, sizeof(double)));
        CudaCheckError();
        CudaSafeCall(cudaMalloc((void **)&d_dep_value2, sizeof(double)));
        CudaCheckError();
        CudaSafeCall(cudaMalloc((void **)&d_dep_value3, sizeof(double)));
        CudaCheckError();
*/

	double* vals = (double*)malloc(sizeof(double)*num_cols);
	double* d_vals;

	for(int i=0; i<num_cols; i++){
		vals[i] = stod(dependency_values[i]);

	}

	CudaSafeCall(cudaMalloc((void **)&d_vals, num_cols * sizeof(double)));
	CudaCheckError();

	CudaSafeCall(cudaMemcpy(d_vals, vals, num_cols * sizeof(double*), cudaMemcpyHostToDevice));
	CudaCheckError();	

        double val1 = stod(dependency_values[0]);
        double val2 = stod(dependency_values[1]);
        double val3 = stod(dependency_values[2]);
	double val4 = stod(dependency_values[3]);

        CudaSafeCall(cudaMalloc((void **)&d_col1, 900 * 1024 * sizeof(double)));
        CudaCheckError();
        CudaSafeCall(cudaMalloc((void **)&d_col2, 900 * 1024 * sizeof(double)));
        CudaCheckError();
        CudaSafeCall(cudaMalloc((void **)&d_col3, 900 * 1024 * sizeof(double)));
        CudaCheckError();
        CudaSafeCall(cudaMalloc((void **)&d_col4, 900 * 1024 * sizeof(double)));
        CudaCheckError();

//high_resolution_clock::time_point malloc2 = high_resolution_clock::now();
//duration<double> malloc_time = duration_cast<duration<double>>(malloc1 - malloc2);
//std::cout << "MEM ALLOCATION TOOK:  " << malloc_time.count() << " seconds " << endl;


/*      for(int i=0; i<num_cols; i++){

                CudaSafeCall(cudaMalloc((void **)&(d_cols[0]), 900 * 1024 * sizeof(char*)));
                CudaCheckError();

                CudaSafeCall(cudaMalloc((void **)&(d_cols[1]), 900 * 1024 * sizeof(char*)));
                CudaCheckError();


        }
*/


//      for(int i=0; i<num_cols; i++){

high_resolution_clock::time_point mem_copy1 = high_resolution_clock::now();

                CudaSafeCall(cudaMemcpy(d_col1, Col0, 900*1024*sizeof(double), cudaMemcpyHostToDevice));
                CudaCheckError();
                CudaSafeCall(cudaMemcpy(d_col2, Col1, 900*1024*sizeof(double), cudaMemcpyHostToDevice));
                CudaCheckError();
                CudaSafeCall(cudaMemcpy(d_col3, Col2, 900*1024*sizeof(double), cudaMemcpyHostToDevice));
                CudaCheckError();
                CudaSafeCall(cudaMemcpy(d_col4, Col3, 900*1024*sizeof(double), cudaMemcpyHostToDevice));
                CudaCheckError();

//high_resolution_clock::time_point mem_copy2 = high_resolution_clock::now();
//duration<double> memcpy_time = duration_cast<duration<double>>(mem_copy1 - mem_copy2);
//std::cout << "MEM COPY TOOK:  " << memcpy_time.count() << " seconds " << endl;



//      }



	int size = NO_BLOCK_GROUPS * 1024 *2;
        char** results = (char**)malloc(size * sizeof(char*));

        for(int i = 0; i < NO_BLOCK_GROUPS*1024*2; i++){
                results[i] = (char*)malloc(sizeof(char)*100);

        }


	bool exist_bool_1 = false;
        bool exist_bool_2 = false;
        bool exist_bool_3 = false;
	bool exist_bool_4 = false;
	std::string s1(dependency_values[0]);
	std::string s2(dependency_values[1]);
	std::string s3(dependency_values[2]);
	std::string s4(dependency_values[3]);


        for (int i=0; i<NO_BLOCK_GROUPS * 1024; i++){
                if (dB->db[i]->bf_query(s1))
                {
                        exist_bool_1 = true;
                        //break;
                }

		if (dB->db[i]->bf_query(s2)){
                        exist_bool_2 = true;
                        //break;

                }

		 if (dB->db[i]->bf_query(s3)){
                        exist_bool_3 = true;
                        //break;
                }

		 if (dB->db[i]->bf_query(s4)){
                        exist_bool_3 = true;
                        //break;
                }

	if (exist_bool_1 && exist_bool_2 && exist_bool_3 && exist_bool_4)
		break;
        }

/*        if(exist_bool_1)
        for (int i=0; NO_BLOCK_GROUPS * 1024; i++){
                if (dB->db[i]->bf_query(s2)){
                        exist_bool_2 = true;
                        break;

                }
        }
        if(exist_bool_2)
        for (int i=0; NO_BLOCK_GROUPS * 1024; i++){
                if (dB->db[i]->bf_query(s3)){
                        exist_bool_3 = true;
                        break;
		}
	}
	if (exist_bool_3)
	for (int i=0; NO_BLOCK_GROUPS * 1024; i++){
                if (dB->db[i]->bf_query(s4)){
                        exist_bool_4 = true;
                        break;
                }
        }
*/
///time(&end);

	if (exist_bool_1 && exist_bool_2 && exist_bool_3 && exist_bool_4){
//time(&start1);

        GPU_MULT_QUERY3<<<64, 512>>>(d_col1, d_col2, d_col3, d_col4, d_vals, NO_BLOCK_GROUPS, num_cols, d_join);

//time(&end1);

time(&start2);


high_resolution_clock::time_point res1 = high_resolution_clock::now();
        cudaDeviceSynchronize();
        CudaCheckError();


        cudaMemcpy(join, d_join, NO_BLOCK_GROUPS * 1024 * sizeof(double), cudaMemcpyDeviceToHost);
        CudaCheckError();

	for(int i = 0; i < NO_BLOCK_GROUPS*1024/*max_threads*/; i+=2){
                string s = std::to_string(join[/*tid*2+*/i]);
                s = s.substr(0, 6);
                if(s.compare("0.0000") != 0){
                for (int j = 0; j < sizeof(results[i]); j++) {
                        results[i][j] = s[j];
                }
		//cout << results[i] << endl;
//                results[i] = join[i];
                        results[i+1] = timeseries[i];
                }
                else{
                        results[i] = "";
                        //results[i+1] = timeseries[i];

                }
        }
//high_resolution_clock::time_point res2 = high_resolution_clock::now();
//duration<double> res_time = duration_cast<duration<double>>(res2 - res1);
//std::cout << "RECOVERING RESULTS TOOK:  " << res_time.count() << " seconds " << endl;



time(&end);
//}

	double elapsed_time = double(end - start) / double(CLOCKS_PER_SEC);
//	high_resolution_clock::time_point t2 = high_resolution_clock::now();
//	duration<double> res_time = duration_cast<duration<double>>(t2 - t1);

//	strncpy(results[0], std::to_string(elapsed_time).c_str(), sizeof(results[0]-1));
/*
if (exist_bool_1 && exist_bool_2 && exist_bool_3 && exist_bool_4){

	double el_time2 = double(end2 - start2) / double(CLOCKS_PER_SEC);
	double total_el_time = elapsed_time + el_time2;

	double kernel_time = double(end1 - start1) / double(CLOCKS_PER_SEC);
	cout << "THE KERNEL TIME IS: "<< fixed << kernel_time << setprecision(5) << endl;

	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	duration<double> time_span = duration_cast<duration<double>>(t2 - t1);
	std::cout << "KERNEL TIME :  " << time_span.count() << " seconds" << endl;
	cout << "TIME TAKEN IN ALLOCATION AND COMMUNICATION IS: "<< fixed << total_el_time  << setprecision(5) << endl;

}else{
	cout << "TIME TAKEN IN ALLOCATION AND COMMUNICATION IS: "<< fixed << elapsed_time  << setprecision(5) << endl;
}
*/
}
        return results;
}



char**  multi_gpu_query4(dataBase* db_addr, int* target_col_num, int num_cols, int NO_BLOCK_GROUPS, int col_num, char** dependency_values, char** timeseries){


        char*** cols = (char***)malloc(sizeof(char**)*num_cols);
        for(int i=0; i<num_cols; i++){
                cols[i] = copyDataBlock(db_addr, target_col_num[i], NO_BLOCK_GROUPS, col_num);

        }

        double* join = (double*)malloc(sizeof(double)*NO_BLOCK_GROUPS * 1024);
//      for(int i = 0; i < NO_BLOCK_GROUPS*1024; i++){
//                join[i] = (char*)malloc(sizeof(char)*100);
        //      cout << cols[1][i] << endl;
//        }


        double* d_join;
        double* d_dep_values;
        //double d_dep_value2;
        //double d_dep_value3;
        double* d_col1;
        double* d_col2;
        double* d_col3;
        double* d_col4;
        double* d_col5;

        double* Col0 = (double*)malloc(sizeof(double)*NO_BLOCK_GROUPS * 1024);
        double* Col1 = (double*)malloc(sizeof(double)*NO_BLOCK_GROUPS * 1024);
        double* Col2 = (double*)malloc(sizeof(double)*NO_BLOCK_GROUPS * 1024);
        double* Col3 = (double*)malloc(sizeof(double)*NO_BLOCK_GROUPS * 1024);
        double* Col4 = (double*)malloc(sizeof(double)*NO_BLOCK_GROUPS * 1024);


cout << "Before converting" << endl;
        for(int i = 0; i < NO_BLOCK_GROUPS*992 ; i++){

                if(!cols[0][i]){
                        Col0[i] = 0.0;
                }else{
                        Col0[i] = stod(cols[0][i]);
                //      cout << Col0[i] << endl;
                }
        }

        for(int i = 0; i < NO_BLOCK_GROUPS*1024 - 2; i++){

                if(!cols[1][i]){
                        Col1[i] = 0.0;
                }else{
                        Col1[i] = stod(cols[1][i]);
                }
        }


	for(int i = 0; i < NO_BLOCK_GROUPS*1024 - 2; i++){

                if(!cols[2][i]){
                        Col2[i] = 0.0;
                }else{
                        Col2[i] = stod(cols[2][i]);
                }

		if(!cols[3][i]){
                        Col3[i] = 0.0;
                }else{
                        Col3[i] = stod(cols[3][i]);
                }

		 if(!cols[4][i]){
                        Col4[i] = 0.0;
                }else{
                        Col4[i] = stod(cols[4][i]);
                }


        }
////cout << "After converting" << endl;


        CudaSafeCall(cudaMalloc((void **)&d_join, 900 * 1024 * sizeof(double)));
        CudaCheckError();

        CudaSafeCall(cudaMemcpy(d_join, join, 900*1024*sizeof(double), cudaMemcpyHostToDevice));
        CudaCheckError();

        CudaSafeCall(cudaMalloc((void **)&d_dep_values, sizeof(double*)));
        CudaCheckError();


/*
        CudaSafeCall(cudaMalloc((void **)&d_dep_value1, sizeof(double)));
        CudaCheckError();
        CudaSafeCall(cudaMalloc((void **)&d_dep_value2, sizeof(double)));
        CudaCheckError();
        CudaSafeCall(cudaMalloc((void **)&d_dep_value3, sizeof(double)));
        CudaCheckError();
*/

	double* vals = (double*)malloc(sizeof(double)*num_cols);
	double* d_vals;

	for(int i=0; i<num_cols; i++){
		vals[i] = stod(dependency_values[i]);

	}

	CudaSafeCall(cudaMalloc((void **)&d_vals, num_cols * sizeof(double)));
	CudaCheckError();

	CudaSafeCall(cudaMemcpy(d_vals, vals, num_cols * sizeof(double*), cudaMemcpyHostToDevice));
	CudaCheckError();	


        CudaSafeCall(cudaMalloc((void **)&d_col1, 900 * 1024 * sizeof(double)));
        CudaCheckError();
        CudaSafeCall(cudaMalloc((void **)&d_col2, 900 * 1024 * sizeof(double)));
        CudaCheckError();
        CudaSafeCall(cudaMalloc((void **)&d_col3, 900 * 1024 * sizeof(double)));
        CudaCheckError();
        CudaSafeCall(cudaMalloc((void **)&d_col4, 900 * 1024 * sizeof(double)));
        CudaCheckError();
        CudaSafeCall(cudaMalloc((void **)&d_col5, 900 * 1024 * sizeof(double)));
        CudaCheckError();


////cout << "After allocations 1" << endl;

//      for(int i=0; i<num_cols; i++){

                CudaSafeCall(cudaMemcpy(d_col1, Col0, 900*1024*sizeof(double), cudaMemcpyHostToDevice));
                CudaCheckError();
                CudaSafeCall(cudaMemcpy(d_col2, Col1, 900*1024*sizeof(double), cudaMemcpyHostToDevice));
                CudaCheckError();
                CudaSafeCall(cudaMemcpy(d_col3, Col2, 900*1024*sizeof(double), cudaMemcpyHostToDevice));
                CudaCheckError();
                CudaSafeCall(cudaMemcpy(d_col4, Col3, 900*1024*sizeof(double), cudaMemcpyHostToDevice));
                CudaCheckError();
                CudaSafeCall(cudaMemcpy(d_col5, Col4, 900*1024*sizeof(double), cudaMemcpyHostToDevice));
                CudaCheckError();


//      }

////cout << "After allocations 2" << endl;


	int size = NO_BLOCK_GROUPS * 1024 *2;
        char** results = (char**)malloc(size * sizeof(char*));

        for(int i = 0; i < NO_BLOCK_GROUPS*1024*2; i++){
                results[i] = (char*)malloc(sizeof(char)*100);

        }


        GPU_MULT_QUERY4<<<1, 1>>>(d_col1, d_col2, d_col3, d_col4, d_col5, d_vals, NO_BLOCK_GROUPS, num_cols, d_join);
        cudaDeviceSynchronize();
        CudaCheckError();

        cudaMemcpy(join, d_join, NO_BLOCK_GROUPS * 1024 * sizeof(double), cudaMemcpyDeviceToHost);
        CudaCheckError();

	for(int i = 0; i < NO_BLOCK_GROUPS*1024/*max_threads*/; i+=2){
                string s = std::to_string(join[/*tid*2+*/i]);
                s = s.substr(0, 6);
                if(s.compare("0.0000") != 0){
                for (int j = 0; j < sizeof(results[i]); j++) {
                        results[i][j] = s[j];
                }
		cout << results[i] << endl;
//                results[i] = join[i];
                        results[i+1] = timeseries[i];
                }
                else{
	//	cout << "I am here" << endl;
                        results[i] = "";
                        results[i+1] = timeseries[i];

                }
        }




        return results;
}


