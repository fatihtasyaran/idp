import connector
import json
from flask import Flask, request, jsonify
from datetime import datetime
import time
import random
import hashlib
import psutil
import GPUtil
import nvidia_smi
import queue
from pynvml import *
from texttable import Texttable
app = Flask(__name__)

import sys

####BF ON-OFF####
BF = int(sys.argv[2])
####BF ON-OFF####
gpu_op = int(sys.argv[3])
global pu
global execution_time
type = 0

#pu = {"GPU":0, "CPU":0}
pu = [0, 0]
average_durations = 0
no_queries = 0

@app.route('/')
def health_check():
    return 'Healthy'

@app.route('/search', methods=['POST'])
def search():
    return jsonify(connector.names)

@app.route('/tag-keys', methods=['POST'])
def tag_keys():
    print("Ad-hoc asking for columns")
    #k = ["dont","forget","your","keys"]
    return jsonify(connector.names)

@app.route('/tag-values', methods=['POST'])
def tag_values():
    values = ["key", "operator", "value"]
    return jsonify(values)


def hashQuery(query):
    result = hashlib.sha256(query.encode())
    result = result.hexdigest()
    return result[0:8]


@app.route('/query', methods=['POST'])
def query():
#**************************************************************************************************************************************** 
    # SCENARIO 1

    #query_list=[["fare","30"],["fare","30","tips","7"],["trip_miles","0.9"],["trip_miles","1.3","tips","2"],["extras","1","fare","25"],["dropoff_latitude","744"],["dropoff_latitude","744","fare","30"],["dropoff_longitude","511"],["dropoff_longitude","100"],["dropoff_longitude","511","fare","20","extras","1.5"],["company","101"],["company","80","fare","25"],["pickup_latitude","744"],["pickup_latitude","744","dropoff_latitude","580"],["fare","60"],["tips","30","extras","8"],["trip_miles","1","fare","35"],["tips","1.5","dropoff_longitude","615"],["fare","50","trip_miles","5.7"],["fare","50","company","90"]]

    #txt_name = "scenario1_new.txt"
    #txt_name = "gpu_test1.txt"
    
    #query_list = [['pickup_latitude', '744', 'dropoff_latitude', '580'], ['company', '80', 'fare', '25'], ['fare', '30'], ['tips', '30', 'extras', '8'], ['dropoff_latitude', '744'], ['company', '101'], ['dropoff_longitude', '511', 'fare', '20', 'extras', '1.5'], ['trip_miles', '1.3', 'tips', '2'], ['pickup_latitude', '744', 'dropoff_latitude', '580'], ['trip_miles', '1.3', 'tips', '2'], ['fare', '60'], ['fare', '50', 'company', '90'], ['tips', '30', 'extras', '8'], ['extras', '1', 'fare', '25'], ['fare', '30', 'tips', '7'], ['tips', '30', 'extras', '8'], ['tips', '30', 'extras', '8'], ['company', '101'], ['tips', '30', 'extras', '8'], ['fare', '60'], ['fare', '30'], ['fare', '50', 'trip_miles', '5.7'], ['trip_miles', '1', 'fare', '35'], ['dropoff_longitude', '100'], ['pickup_latitude', '744', 'dropoff_latitude', '580'], ['dropoff_latitude', '744', 'fare', '30'], ['fare', '30'], ['dropoff_longitude', '511', 'fare', '20', 'extras', '1.5'], ['dropoff_latitude', '744', 'fare', '30'], ['fare', '50', 'trip_miles', '5.7'], ['trip_miles', '1', 'fare', '35'], ['company', '80', 'fare', '25'], ['pickup_latitude', '744'], ['tips', '30', 'extras', '8'], ['trip_miles', '0.9'], ['dropoff_longitude', '511', 'fare', '20', 'extras', '1.5'], ['pickup_latitude', '744', 'dropoff_latitude', '580'], ['dropoff_latitude', '744', 'fare', '30'], ['dropoff_latitude', '744'], ['dropoff_latitude', '744', 'fare', '30'], ['company', '80', 'fare', '25'], ['fare', '50', 'trip_miles', '5.7'], ['dropoff_longitude', '100'], ['pickup_latitude', '744', 'dropoff_latitude', '580'], ['pickup_latitude', '744', 'dropoff_latitude', '580'], ['dropoff_longitude', '100'], ['tips', '1.5', 'dropoff_longitude', '615'], ['trip_miles', '1.3', 'tips', '2'], ['trip_miles', '1.3', 'tips', '2'], ['tips', '1.5', 'dropoff_longitude', '615'], ['fare', '50', 'trip_miles', '5.7'], ['fare', '30', 'tips', '7'], ['extras', '1', 'fare', '25'], ['fare', '30'], ['dropoff_longitude', '511'], ['fare', '30'], ['fare', '60'], ['tips', '30', 'extras', '8'], ['trip_miles', '1.3', 'tips', '2'], ['dropoff_longitude', '511'], ['pickup_latitude', '744'], ['fare', '30'], ['trip_miles', '1', 'fare', '35'], ['dropoff_longitude', '511'], ['pickup_latitude', '744'], ['trip_miles', '1', 'fare', '35'], ['pickup_latitude', '744'], ['fare', '30', 'tips', '7'], ['dropoff_longitude', '511'], ['fare', '30'], ['tips', '1.5', 'dropoff_longitude', '615'], ['dropoff_latitude', '744', 'fare', '30'], ['company', '80', 'fare', '25'], ['company', '80', 'fare', '25'], ['fare', '50', 'trip_miles', '5.7'], ['extras', '1', 'fare', '25'], ['dropoff_longitude', '511', 'fare', '20', 'extras', '1.5'], ['fare', '50', 'trip_miles', '5.7'], ['trip_miles', '0.9'], ['fare', '50', 'company', '90'], ['tips', '30', 'extras', '8'], ['tips', '30', 'extras', '8'], ['fare', '30', 'tips', '7'], ['trip_miles', '1.3', 'tips', '2'], ['trip_miles', '1.3', 'tips', '2'], ['dropoff_longitude', '511', 'fare', '20', 'extras', '1.5'], ['trip_miles', '0.9'], ['company', '80', 'fare', '25'], ['fare', '50', 'trip_miles', '5.7'], ['fare', '60'], ['fare', '30', 'tips', '7'], ['fare', '30', 'tips', '7'], ['fare', '50', 'company', '90'], ['company', '80', 'fare', '25'], ['dropoff_longitude', '511', 'fare', '20', 'extras', '1.5'], ['fare', '60'], ['dropoff_longitude', '100'], ['trip_miles', '1', 'fare', '35'], ['dropoff_latitude', '744'], ['fare', '30', 'tips', '7'], ['extras', '1', 'fare', '25'], ['extras', '1', 'fare', '25'], ['tips', '30', 'extras', '8'], ['trip_miles', '1.3', 'tips', '2'], ['fare', '30', 'tips', '7'], ['extras', '1', 'fare', '25'], ['pickup_latitude', '744'], ['dropoff_longitude', '100'], ['tips', '1.5', 'dropoff_longitude', '615'], ['pickup_latitude', '744'], ['trip_miles', '1.3', 'tips', '2'], ['dropoff_longitude', '511', 'fare', '20', 'extras', '1.5'], ['tips', '30', 'extras', '8'], ['dropoff_longitude', '100'], ['company', '80', 'fare', '25'], ['trip_miles', '1', 'fare', '35'], ['company', '80', 'fare', '25'], ['tips', '30', 'extras', '8'], ['fare', '50', 'trip_miles', '5.7'], ['extras', '1', 'fare', '25'], ['fare', '30', 'tips', '7'], ['tips', '30', 'extras', '8'], ['company', '80', 'fare', '25'], ['fare', '50', 'trip_miles', '5.7'], ['dropoff_longitude', '511'], ['trip_miles', '0.9'], ['trip_miles', '1.3', 'tips', '2'], ['dropoff_latitude', '744', 'fare', '30'], ['pickup_latitude', '744', 'dropoff_latitude', '580'], ['extras', '1', 'fare', '25'], ['fare', '50', 'trip_miles', '5.7'], ['company', '101'], ['company', '80', 'fare', '25'], ['fare', '50', 'trip_miles', '5.7'], ['dropoff_longitude', '511'], ['pickup_latitude', '744'], ['tips', '30', 'extras', '8'], ['company', '80', 'fare', '25'], ['fare', '50', 'company', '90'], ['dropoff_longitude', '511'], ['dropoff_longitude', '511', 'fare', '20', 'extras', '1.5'], ['trip_miles', '1.3', 'tips', '2'], ['tips', '1.5', 'dropoff_longitude', '615'], ['fare', '50', 'company', '90'], ['dropoff_longitude', '511', 'fare', '20', 'extras', '1.5'], ['dropoff_latitude', '744'], ['fare', '50', 'company', '90'], ['fare', '60'], ['dropoff_longitude', '100'], ['dropoff_longitude', '100'], ['fare', '60'], ['tips', '1.5', 'dropoff_longitude', '615'], ['trip_miles', '1.3', 'tips', '2'], ['dropoff_latitude', '744', 'fare', '30'], ['fare', '30'], ['dropoff_latitude', '744'], ['tips', '1.5', 'dropoff_longitude', '615'], ['dropoff_longitude', '511', 'fare', '20', 'extras', '1.5'], ['dropoff_latitude', '744', 'fare', '30'], ['dropoff_latitude', '744', 'fare', '30'], ['dropoff_longitude', '511'], ['trip_miles', '1', 'fare', '35'], ['fare', '30', 'tips', '7'], ['pickup_latitude', '744', 'dropoff_latitude', '580'], ['tips', '1.5', 'dropoff_longitude', '615'], ['trip_miles', '1', 'fare', '35'], ['fare', '30'], ['fare', '30', 'tips', '7'], ['dropoff_longitude', '511', 'fare', '20', 'extras', '1.5'], ['fare', '50', 'company', '90'], ['dropoff_latitude', '744', 'fare', '30'], ['trip_miles', '0.9'], ['company', '101'], ['fare', '30'], ['dropoff_longitude', '511', 'fare', '20', 'extras', '1.5'], ['pickup_latitude', '744'], ['company', '80', 'fare', '25'], ['fare', '50', 'company', '90'], ['dropoff_latitude', '744'], ['fare', '30', 'tips', '7'], ['pickup_latitude', '744', 'dropoff_latitude', '580'], ['extras', '1', 'fare', '25'], ['dropoff_longitude', '511'], ['tips', '1.5', 'dropoff_longitude', '615'], ['company', '101'], ['trip_miles', '1.3', 'tips', '2'], ['dropoff_latitude', '744', 'fare', '30'], ['fare', '50', 'company', '90'], ['dropoff_longitude', '100'], ['extras', '1', 'fare', '25'], ['company', '80', 'fare', '25'], ['fare', '60'], ['company', '80', 'fare', '25'], ['dropoff_latitude', '744'], ['dropoff_latitude', '744'], ['fare', '60'], ['dropoff_longitude', '511', 'fare', '20', 'extras', '1.5'], ['extras', '1', 'fare', '25'], ['dropoff_longitude', '100'], ['tips', '1.5', 'dropoff_longitude', '615'], ['company', '80', 'fare', '25'], ['trip_miles', '1', 'fare', '35'], ['extras', '1', 'fare', '25'], ['tips', '30', 'extras', '8'], ['extras', '1', 'fare', '25'], ['fare', '50', 'company', '90'], ['tips', '1.5', 'dropoff_longitude', '615'], ['tips', '30', 'extras', '8'], ['pickup_latitude', '744', 'dropoff_latitude', '580'], ['pickup_latitude', '744'], ['fare', '50', 'company', '90'], ['extras', '1', 'fare', '25'], ['pickup_latitude', '744', 'dropoff_latitude', '580'], ['tips', '1.5', 'dropoff_longitude', '615'], ['trip_miles', '1.3', 'tips', '2'], ['fare', '50', 'trip_miles', '5.7'], ['dropoff_longitude', '511', 'fare', '20', 'extras', '1.5'], ['fare', '30', 'tips', '7'], ['fare', '50', 'trip_miles', '5.7'], ['pickup_latitude', '744', 'dropoff_latitude', '580'], ['fare', '60'], ['company', '101'], ['fare', '60'], ['pickup_latitude', '744'], ['company', '101'], ['company', '80', 'fare', '25'], ['dropoff_longitude', '100'], ['extras', '1', 'fare', '25'], ['pickup_latitude', '744', 'dropoff_latitude', '580'], ['fare', '50', 'trip_miles', '5.7'], ['tips', '1.5', 'dropoff_longitude', '615'], ['trip_miles', '1', 'fare', '35'], ['pickup_latitude', '744', 'dropoff_latitude', '580'], ['fare', '50', 'company', '90'], ['company', '80', 'fare', '25'], ['dropoff_longitude', '100'], ['company', '101'], ['dropoff_longitude', '511'], ['dropoff_longitude', '100'], ['dropoff_longitude', '100'], ['fare', '30', 'tips', '7'], ['fare', '50', 'trip_miles', '5.7'], ['fare', '50', 'trip_miles', '5.7'], ['dropoff_latitude', '744', 'fare', '30'], ['company', '80', 'fare', '25'], ['tips', '30', 'extras', '8'], ['tips', '30', 'extras', '8'], ['dropoff_longitude', '511'], ['tips', '30', 'extras', '8'], ['tips', '1.5', 'dropoff_longitude', '615'], ['tips', '30', 'extras', '8'], ['pickup_latitude', '744', 'dropoff_latitude', '580'], ['dropoff_longitude', '100'], ['tips', '30', 'extras', '8'], ['trip_miles', '1.3', 'tips', '2'], ['tips', '1.5', 'dropoff_longitude', '615'], ['company', '101'], ['dropoff_latitude', '744', 'fare', '30'], ['fare', '50', 'trip_miles', '5.7'], ['company', '101'], ['company', '101'], ['tips', '30', 'extras', '8'], ['dropoff_latitude', '744'], ['extras', '1', 'fare', '25'], ['company', '80', 'fare', '25'], ['dropoff_longitude', '511'], ['company', '80', 'fare', '25'], ['extras', '1', 'fare', '25'], ['company', '101'], ['fare', '50', 'trip_miles', '5.7'], ['dropoff_longitude', '511', 'fare', '20', 'extras', '1.5'], ['fare', '30', 'tips', '7'], ['trip_miles', '1.3', 'tips', '2'], ['trip_miles', '1', 'fare', '35'], ['fare', '30', 'tips', '7'], ['tips', '30', 'extras', '8'], ['company', '80', 'fare', '25'], ['dropoff_longitude', '100'], ['fare', '60'], ['fare', '30', 'tips', '7'], ['dropoff_longitude', '511', 'fare', '20', 'extras', '1.5'], ['fare', '50', 'company', '90'], ['extras', '1', 'fare', '25'], ['extras', '1', 'fare', '25'], ['company', '101'], ['tips', '30', 'extras', '8'], ['dropoff_longitude', '511', 'fare', '20', 'extras', '1.5'], ['dropoff_longitude', '511'], ['pickup_latitude', '744'], ['pickup_latitude', '744'], ['tips', '1.5', 'dropoff_longitude', '615'], ['trip_miles', '0.9'], ['tips', '1.5', 'dropoff_longitude', '615'], ['trip_miles', '1', 'fare', '35'], ['fare', '50', 'company', '90'], ['dropoff_latitude', '744', 'fare', '30'], ['fare', '30', 'tips', '7'], ['dropoff_latitude', '744'], ['dropoff_longitude', '100'], ['trip_miles', '1.3', 'tips', '2']]

    
#*****************************************************************************************************************************************
    # SCENARIO 2

    #query_list=[["fare","50","tips","9","company","85"],["fare","37","tips","5","trip_miles","0.6"],["trip_miles","0.3","extras","2","tips","15"],["trip_miles","5.3","tips","5","fare","25"],["extras","3","fare","25","tips","1"],["dropoff_latitude","857","fare","20"],["dropoff_latitude","484","fare","50"],["dropoff_longitude","381","extras","3","fare","20"],["dropoff_longitude","200","extras","2"],["dropoff_longitude","418","fare","50","extras","1.8"],["company","151","tips","2"],["company","80","fare","45"],["pickup_latitude","416"],["pickup_latitude","589","dropoff_latitude","589"],["fare","60","tips","20"],["tips","20","extras","4"],["trip_miles","5","fare","55"],["tips","1.7","dropoff_longitude","601"],["fare","40","trip_miles","5.9"],["fare","10","company","95"]]

    #txt_name = "scenario2_new.txt"
    #txt_name = "gpu_test2.txt"

    #query_list = [['dropoff_latitude', '857', 'fare', '20'], ['fare', '10', 'company', '95'], ['dropoff_longitude', '418', 'fare', '50', 'extras', '1.8'], ['dropoff_latitude', '857', 'fare', '20'], ['fare', '37', 'tips', '5', 'trip_miles', '0.6'], ['extras', '3', 'fare', '25', 'tips', '1'], ['company', '151', 'tips', '2'], ['dropoff_longitude', '381', 'extras', '3', 'fare', '20'], ['trip_miles', '0.3', 'extras', '2', 'tips', '15'], ['dropoff_latitude', '857', 'fare', '20'], ['dropoff_latitude', '857', 'fare', '20'], ['dropoff_longitude', '418', 'fare', '50', 'extras', '1.8'], ['company', '151', 'tips', '2'], ['tips', '1.7', 'dropoff_longitude', '601'], ['tips', '20', 'extras', '4'], ['fare', '50', 'tips', '9', 'company', '85'], ['fare', '40', 'trip_miles', '5.9'], ['tips', '20', 'extras', '4'], ['extras', '3', 'fare', '25', 'tips', '1'], ['dropoff_latitude', '857', 'fare', '20'], ['dropoff_longitude', '381', 'extras', '3', 'fare', '20'], ['trip_miles', '5', 'fare', '55'], ['fare', '50', 'tips', '9', 'company', '85'], ['trip_miles', '5', 'fare', '55'], ['fare', '37', 'tips', '5', 'trip_miles', '0.6'], ['fare', '10', 'company', '95'], ['tips', '20', 'extras', '4'], ['fare', '40', 'trip_miles', '5.9'], ['extras', '3', 'fare', '25', 'tips', '1'], ['dropoff_longitude', '200', 'extras', '2'], ['fare', '10', 'company', '95'], ['pickup_latitude', '416'], ['fare', '50', 'tips', '9', 'company', '85'], ['dropoff_longitude', '381', 'extras', '3', 'fare', '20'], ['fare', '37', 'tips', '5', 'trip_miles', '0.6'], ['dropoff_longitude', '200', 'extras', '2'], ['dropoff_latitude', '857', 'fare', '20'], ['trip_miles', '0.3', 'extras', '2', 'tips', '15'], ['fare', '50', 'tips', '9', 'company', '85'], ['dropoff_latitude', '484', 'fare', '50'], ['dropoff_longitude', '381', 'extras', '3', 'fare', '20'], ['company', '80', 'fare', '45'], ['company', '80', 'fare', '45'], ['fare', '50', 'tips', '9', 'company', '85'], ['company', '80', 'fare', '45'], ['company', '151', 'tips', '2'], ['dropoff_latitude', '857', 'fare', '20'], ['tips', '1.7', 'dropoff_longitude', '601'], ['company', '80', 'fare', '45'], ['tips', '20', 'extras', '4'], ['dropoff_longitude', '418', 'fare', '50', 'extras', '1.8'], ['tips', '1.7', 'dropoff_longitude', '601'], ['fare', '10', 'company', '95'], ['tips', '20', 'extras', '4'], ['dropoff_longitude', '200', 'extras', '2'], ['pickup_latitude', '416'], ['fare', '40', 'trip_miles', '5.9'], ['trip_miles', '5', 'fare', '55'], ['trip_miles', '5.3', 'tips', '5', 'fare', '25'], ['pickup_latitude', '416'], ['company', '80', 'fare', '45'], ['fare', '10', 'company', '95'], ['trip_miles', '5', 'fare', '55'], ['trip_miles', '0.3', 'extras', '2', 'tips', '15'], ['trip_miles', '5', 'fare', '55'], ['dropoff_longitude', '381', 'extras', '3', 'fare', '20'], ['extras', '3', 'fare', '25', 'tips', '1'], ['fare', '60', 'tips', '20'], ['pickup_latitude', '416'], ['fare', '37', 'tips', '5', 'trip_miles', '0.6'], ['fare', '37', 'tips', '5', 'trip_miles', '0.6'], ['trip_miles', '5.3', 'tips', '5', 'fare', '25'], ['pickup_latitude', '589', 'dropoff_latitude', '589'], ['fare', '10', 'company', '95'], ['dropoff_longitude', '200', 'extras', '2'], ['dropoff_longitude', '418', 'fare', '50', 'extras', '1.8'], ['dropoff_latitude', '484', 'fare', '50'], ['fare', '50', 'tips', '9', 'company', '85'], ['extras', '3', 'fare', '25', 'tips', '1'], ['dropoff_longitude', '381', 'extras', '3', 'fare', '20'], ['fare', '10', 'company', '95'], ['trip_miles', '5', 'fare', '55'], ['dropoff_latitude', '857', 'fare', '20'], ['dropoff_longitude', '381', 'extras', '3', 'fare', '20'], ['fare', '40', 'trip_miles', '5.9'], ['company', '80', 'fare', '45'], ['trip_miles', '5', 'fare', '55'], ['fare', '40', 'trip_miles', '5.9'], ['fare', '60', 'tips', '20'], ['company', '151', 'tips', '2'], ['dropoff_latitude', '857', 'fare', '20'], ['pickup_latitude', '589', 'dropoff_latitude', '589'], ['dropoff_longitude', '381', 'extras', '3', 'fare', '20'], ['pickup_latitude', '589', 'dropoff_latitude', '589'], ['dropoff_latitude', '857', 'fare', '20'], ['dropoff_latitude', '857', 'fare', '20'], ['pickup_latitude', '589', 'dropoff_latitude', '589'], ['fare', '50', 'tips', '9', 'company', '85'], ['fare', '40', 'trip_miles', '5.9'], ['pickup_latitude', '416'], ['dropoff_longitude', '381', 'extras', '3', 'fare', '20'], ['extras', '3', 'fare', '25', 'tips', '1'], ['pickup_latitude', '589', 'dropoff_latitude', '589'], ['fare', '37', 'tips', '5', 'trip_miles', '0.6'], ['trip_miles', '5', 'fare', '55'], ['company', '80', 'fare', '45'], ['trip_miles', '5', 'fare', '55'], ['dropoff_latitude', '857', 'fare', '20'], ['company', '80', 'fare', '45'], ['dropoff_latitude', '484', 'fare', '50'], ['dropoff_longitude', '381', 'extras', '3', 'fare', '20'], ['pickup_latitude', '589', 'dropoff_latitude', '589'], ['tips', '1.7', 'dropoff_longitude', '601'], ['fare', '40', 'trip_miles', '5.9'], ['fare', '40', 'trip_miles', '5.9'], ['trip_miles', '5', 'fare', '55'], ['dropoff_longitude', '200', 'extras', '2'], ['trip_miles', '5', 'fare', '55'], ['dropoff_latitude', '857', 'fare', '20'], ['fare', '37', 'tips', '5', 'trip_miles', '0.6'], ['fare', '50', 'tips', '9', 'company', '85'], ['tips', '1.7', 'dropoff_longitude', '601'], ['fare', '50', 'tips', '9', 'company', '85'], ['company', '80', 'fare', '45'], ['fare', '50', 'tips', '9', 'company', '85'], ['fare', '50', 'tips', '9', 'company', '85'], ['dropoff_longitude', '381', 'extras', '3', 'fare', '20'], ['dropoff_longitude', '200', 'extras', '2'], ['fare', '60', 'tips', '20'], ['fare', '37', 'tips', '5', 'trip_miles', '0.6'], ['dropoff_longitude', '381', 'extras', '3', 'fare', '20'], ['pickup_latitude', '416'], ['pickup_latitude', '589', 'dropoff_latitude', '589'], ['tips', '1.7', 'dropoff_longitude', '601'], ['dropoff_latitude', '857', 'fare', '20'], ['fare', '50', 'tips', '9', 'company', '85'], ['fare', '37', 'tips', '5', 'trip_miles', '0.6'], ['dropoff_latitude', '484', 'fare', '50'], ['dropoff_latitude', '484', 'fare', '50'], ['fare', '60', 'tips', '20'], ['company', '151', 'tips', '2'], ['fare', '40', 'trip_miles', '5.9'], ['tips', '20', 'extras', '4'], ['dropoff_longitude', '200', 'extras', '2'], ['dropoff_longitude', '418', 'fare', '50', 'extras', '1.8'], ['company', '151', 'tips', '2'], ['tips', '1.7', 'dropoff_longitude', '601'], ['trip_miles', '5.3', 'tips', '5', 'fare', '25'], ['trip_miles', '5.3', 'tips', '5', 'fare', '25'], ['dropoff_longitude', '381', 'extras', '3', 'fare', '20'], ['company', '151', 'tips', '2'], ['extras', '3', 'fare', '25', 'tips', '1'], ['fare', '40', 'trip_miles', '5.9'], ['fare', '40', 'trip_miles', '5.9'], ['dropoff_longitude', '418', 'fare', '50', 'extras', '1.8'], ['dropoff_longitude', '418', 'fare', '50', 'extras', '1.8'], ['trip_miles', '5.3', 'tips', '5', 'fare', '25'], ['fare', '37', 'tips', '5', 'trip_miles', '0.6'], ['trip_miles', '0.3', 'extras', '2', 'tips', '15'], ['dropoff_longitude', '418', 'fare', '50', 'extras', '1.8'], ['fare', '40', 'trip_miles', '5.9'], ['dropoff_longitude', '381', 'extras', '3', 'fare', '20'], ['trip_miles', '0.3', 'extras', '2', 'tips', '15'], ['extras', '3', 'fare', '25', 'tips', '1'], ['dropoff_latitude', '857', 'fare', '20'], ['trip_miles', '5.3', 'tips', '5', 'fare', '25'], ['fare', '37', 'tips', '5', 'trip_miles', '0.6'], ['company', '151', 'tips', '2'], ['dropoff_longitude', '418', 'fare', '50', 'extras', '1.8'], ['dropoff_latitude', '857', 'fare', '20'], ['company', '151', 'tips', '2'], ['fare', '60', 'tips', '20'], ['fare', '10', 'company', '95'], ['company', '80', 'fare', '45'], ['company', '151', 'tips', '2'], ['fare', '40', 'trip_miles', '5.9'], ['fare', '10', 'company', '95'], ['dropoff_longitude', '418', 'fare', '50', 'extras', '1.8'], ['tips', '20', 'extras', '4'], ['dropoff_longitude', '200', 'extras', '2'], ['pickup_latitude', '589', 'dropoff_latitude', '589'], ['fare', '10', 'company', '95'], ['tips', '1.7', 'dropoff_longitude', '601'], ['dropoff_longitude', '381', 'extras', '3', 'fare', '20'], ['company', '151', 'tips', '2'], ['trip_miles', '5.3', 'tips', '5', 'fare', '25'], ['trip_miles', '0.3', 'extras', '2', 'tips', '15'], ['fare', '50', 'tips', '9', 'company', '85'], ['dropoff_latitude', '857', 'fare', '20'], ['trip_miles', '0.3', 'extras', '2', 'tips', '15'], ['dropoff_longitude', '381', 'extras', '3', 'fare', '20'], ['company', '151', 'tips', '2'], ['company', '151', 'tips', '2'], ['fare', '50', 'tips', '9', 'company', '85'], ['dropoff_longitude', '200', 'extras', '2'], ['company', '151', 'tips', '2'], ['extras', '3', 'fare', '25', 'tips', '1'], ['tips', '20', 'extras', '4'], ['fare', '10', 'company', '95'], ['trip_miles', '5.3', 'tips', '5', 'fare', '25'], ['fare', '60', 'tips', '20'], ['fare', '40', 'trip_miles', '5.9'], ['fare', '60', 'tips', '20'], ['fare', '10', 'company', '95'], ['fare', '50', 'tips', '9', 'company', '85'], ['tips', '1.7', 'dropoff_longitude', '601'], ['fare', '50', 'tips', '9', 'company', '85'], ['fare', '37', 'tips', '5', 'trip_miles', '0.6'], ['fare', '50', 'tips', '9', 'company', '85'], ['company', '80', 'fare', '45'], ['tips', '1.7', 'dropoff_longitude', '601'], ['trip_miles', '0.3', 'extras', '2', 'tips', '15'], ['fare', '10', 'company', '95'], ['trip_miles', '5.3', 'tips', '5', 'fare', '25'], ['fare', '50', 'tips', '9', 'company', '85'], ['trip_miles', '5', 'fare', '55'], ['dropoff_longitude', '418', 'fare', '50', 'extras', '1.8'], ['trip_miles', '5.3', 'tips', '5', 'fare', '25'], ['extras', '3', 'fare', '25', 'tips', '1'], ['fare', '60', 'tips', '20'], ['company', '151', 'tips', '2'], ['tips', '20', 'extras', '4'], ['trip_miles', '5', 'fare', '55'], ['fare', '60', 'tips', '20'], ['dropoff_latitude', '857', 'fare', '20'], ['trip_miles', '5', 'fare', '55'], ['dropoff_longitude', '200', 'extras', '2'], ['dropoff_longitude', '200', 'extras', '2'], ['dropoff_latitude', '857', 'fare', '20'], ['trip_miles', '5', 'fare', '55'], ['trip_miles', '0.3', 'extras', '2', 'tips', '15'], ['fare', '50', 'tips', '9', 'company', '85'], ['pickup_latitude', '589', 'dropoff_latitude', '589'], ['trip_miles', '5', 'fare', '55'], ['dropoff_longitude', '381', 'extras', '3', 'fare', '20'], ['trip_miles', '5', 'fare', '55'], ['dropoff_latitude', '857', 'fare', '20'], ['fare', '40', 'trip_miles', '5.9'], ['fare', '40', 'trip_miles', '5.9'], ['pickup_latitude', '416'], ['dropoff_latitude', '857', 'fare', '20'], ['tips', '20', 'extras', '4'], ['dropoff_longitude', '200', 'extras', '2'], ['dropoff_longitude', '200', 'extras', '2'], ['dropoff_longitude', '418', 'fare', '50', 'extras', '1.8'], ['fare', '10', 'company', '95'], ['trip_miles', '5.3', 'tips', '5', 'fare', '25'], ['fare', '10', 'company', '95'], ['fare', '10', 'company', '95'], ['fare', '10', 'company', '95'], ['trip_miles', '0.3', 'extras', '2', 'tips', '15'], ['fare', '40', 'trip_miles', '5.9'], ['fare', '60', 'tips', '20'], ['fare', '10', 'company', '95'], ['tips', '1.7', 'dropoff_longitude', '601'], ['extras', '3', 'fare', '25', 'tips', '1'], ['tips', '1.7', 'dropoff_longitude', '601'], ['fare', '10', 'company', '95'], ['fare', '40', 'trip_miles', '5.9'], ['fare', '50', 'tips', '9', 'company', '85'], ['dropoff_longitude', '418', 'fare', '50', 'extras', '1.8'], ['fare', '50', 'tips', '9', 'company', '85'], ['pickup_latitude', '589', 'dropoff_latitude', '589'], ['dropoff_latitude', '484', 'fare', '50'], ['dropoff_latitude', '857', 'fare', '20'], ['dropoff_latitude', '857', 'fare', '20'], ['pickup_latitude', '416'], ['dropoff_longitude', '200', 'extras', '2'], ['fare', '60', 'tips', '20'], ['pickup_latitude', '416'], ['company', '80', 'fare', '45'], ['fare', '10', 'company', '95'], ['fare', '60', 'tips', '20'], ['dropoff_longitude', '200', 'extras', '2'], ['company', '151', 'tips', '2'], ['dropoff_latitude', '857', 'fare', '20'], ['trip_miles', '5', 'fare', '55'], ['trip_miles', '5.3', 'tips', '5', 'fare', '25'], ['pickup_latitude', '589', 'dropoff_latitude', '589'], ['dropoff_longitude', '381', 'extras', '3', 'fare', '20'], ['trip_miles', '0.3', 'extras', '2', 'tips', '15'], ['tips', '1.7', 'dropoff_longitude', '601'], ['trip_miles', '5.3', 'tips', '5', 'fare', '25'], ['dropoff_latitude', '857', 'fare', '20'], ['pickup_latitude', '589', 'dropoff_latitude', '589'], ['fare', '10', 'company', '95'], ['fare', '60', 'tips', '20'], ['tips', '1.7', 'dropoff_longitude', '601'], ['trip_miles', '5.3', 'tips', '5', 'fare', '25'], ['dropoff_longitude', '381', 'extras', '3', 'fare', '20'], ['company', '151', 'tips', '2'], ['pickup_latitude', '416'], ['tips', '1.7', 'dropoff_longitude', '601'], ['company', '151', 'tips', '2'], ['trip_miles', '0.3', 'extras', '2', 'tips', '15'], ['fare', '60', 'tips', '20'], ['fare', '10', 'company', '95'], ['company', '80', 'fare', '45'], ['tips', '20', 'extras', '4'], ['fare', '60', 'tips', '20']]

    
#*****************************************************************************************************************************************
    # SCENARIO 3

    #query_list=[["trip_miles","2"],["fare","20","tips","11"],["extras","1"],["trip_miles","0.9"],["trip_miles","1.3","tips","7"],["trip_miles","1.3","extras","1","fare","35"],["tips","1","fare","17","dropoff_latitude","378"],["dropoff_latitude","789","fare","50","tips","14"],["dropoff_longitude","555"],["dropoff_longitude","100"],["dropoff_longitude","418","fare","43","extras","1.5"],["company","63"],["company","40","fare","65"],["pickup_latitude","121"],["pickup_latitude","744","dropoff_latitude","580"],["fare","70"],["tips","50","extras","8"],["trip_miles","1.8","fare","45"],["tips","1.5","dropoff_longitude","615"],["fare","10","trip_miles","0.7"],["fare","40","company","100","pickup_latitude","744"]]

    #txt_name = "scenario3_new.txt"
    #txt_name = "gpu_test3.txt"
    
    #query_list = [['dropoff_longitude', '100'], ['fare', '10', 'trip_miles', '0.7'], ['pickup_latitude', '121'], ['fare', '20', 'tips', '11'], ['dropoff_longitude', '418', 'fare', '43', 'extras', '1.5'], ['trip_miles', '2'], ['trip_miles', '1.8', 'fare', '45'], ['dropoff_longitude', '555'], ['fare', '20', 'tips', '11'], ['fare', '20', 'tips', '11'], ['dropoff_latitude', '789', 'fare', '50', 'tips', '14'], ['fare', '40', 'company', '100', 'pickup_latitude', '744'], ['tips', '50', 'extras', '8'], ['tips', '1', 'fare', '17', 'dropoff_latitude', '378'], ['tips', '1', 'fare', '17', 'dropoff_latitude', '378'], ['trip_miles', '1.3', 'extras', '1', 'fare', '35'], ['tips', '50', 'extras', '8'], ['tips', '50', 'extras', '8'], ['company', '63'], ['dropoff_longitude', '418', 'fare', '43', 'extras', '1.5'], ['dropoff_longitude', '555'], ['fare', '70'], ['extras', '1'], ['company', '63'], ['company', '40', 'fare', '65'], ['fare', '40', 'company', '100', 'pickup_latitude', '744'], ['tips', '1.5', 'dropoff_longitude', '615'], ['dropoff_latitude', '789', 'fare', '50', 'tips', '14'], ['trip_miles', '1.8', 'fare', '45'], ['pickup_latitude', '121'], ['fare', '20', 'tips', '11'], ['tips', '1', 'fare', '17', 'dropoff_latitude', '378'], ['fare', '10', 'trip_miles', '0.7'], ['dropoff_latitude', '789', 'fare', '50', 'tips', '14'], ['tips', '1', 'fare', '17', 'dropoff_latitude', '378'], ['trip_miles', '0.9'], ['trip_miles', '0.9'], ['pickup_latitude', '121'], ['dropoff_longitude', '100'], ['company', '63'], ['dropoff_latitude', '789', 'fare', '50', 'tips', '14'], ['trip_miles', '0.9'], ['fare', '70'], ['trip_miles', '0.9'], ['fare', '70'], ['company', '40', 'fare', '65'], ['extras', '1'], ['dropoff_longitude', '418', 'fare', '43', 'extras', '1.5'], ['pickup_latitude', '121'], ['trip_miles', '1.8', 'fare', '45'], ['pickup_latitude', '744', 'dropoff_latitude', '580'], ['tips', '1', 'fare', '17', 'dropoff_latitude', '378'], ['tips', '50', 'extras', '8'], ['dropoff_longitude', '418', 'fare', '43', 'extras', '1.5'], ['fare', '20', 'tips', '11'], ['fare', '10', 'trip_miles', '0.7'], ['fare', '20', 'tips', '11'], ['fare', '40', 'company', '100', 'pickup_latitude', '744'], ['tips', '50', 'extras', '8'], ['tips', '1', 'fare', '17', 'dropoff_latitude', '378'], ['fare', '20', 'tips', '11'], ['fare', '40', 'company', '100', 'pickup_latitude', '744'], ['tips', '1', 'fare', '17', 'dropoff_latitude', '378'], ['fare', '70'], ['tips', '1', 'fare', '17', 'dropoff_latitude', '378'], ['fare', '70'], ['tips', '1.5', 'dropoff_longitude', '615'], ['pickup_latitude', '121'], ['dropoff_longitude', '100'], ['tips', '1.5', 'dropoff_longitude', '615'], ['tips', '1.5', 'dropoff_longitude', '615'], ['trip_miles', '1.8', 'fare', '45'], ['pickup_latitude', '121'], ['trip_miles', '2'], ['trip_miles', '1.3', 'tips', '7'], ['trip_miles', '1.3', 'extras', '1', 'fare', '35'], ['trip_miles', '1.3', 'tips', '7'], ['fare', '10', 'trip_miles', '0.7'], ['pickup_latitude', '744', 'dropoff_latitude', '580'], ['fare', '20', 'tips', '11'], ['dropoff_longitude', '100'], ['dropoff_longitude', '100'], ['fare', '70'], ['company', '40', 'fare', '65'], ['tips', '50', 'extras', '8'], ['fare', '40', 'company', '100', 'pickup_latitude', '744'], ['pickup_latitude', '744', 'dropoff_latitude', '580'], ['trip_miles', '0.9'], ['pickup_latitude', '744', 'dropoff_latitude', '580'], ['trip_miles', '1.3', 'extras', '1', 'fare', '35'], ['trip_miles', '1.8', 'fare', '45'], ['trip_miles', '2'], ['trip_miles', '0.9'], ['company', '40', 'fare', '65'], ['trip_miles', '0.9'], ['tips', '50', 'extras', '8'], ['fare', '20', 'tips', '11'], ['fare', '20', 'tips', '11'], ['fare', '10', 'trip_miles', '0.7'], ['dropoff_longitude', '100'], ['dropoff_longitude', '418', 'fare', '43', 'extras', '1.5'], ['dropoff_longitude', '555'], ['company', '63'], ['tips', '1', 'fare', '17', 'dropoff_latitude', '378'], ['pickup_latitude', '121'], ['company', '63'], ['dropoff_latitude', '789', 'fare', '50', 'tips', '14'], ['trip_miles', '1.8', 'fare', '45'], ['trip_miles', '2'], ['fare', '20', 'tips', '11'], ['tips', '1', 'fare', '17', 'dropoff_latitude', '378'], ['pickup_latitude', '744', 'dropoff_latitude', '580'], ['fare', '10', 'trip_miles', '0.7'], ['dropoff_longitude', '555'], ['trip_miles', '1.3', 'extras', '1', 'fare', '35'], ['trip_miles', '2'], ['company', '40', 'fare', '65'], ['company', '63'], ['pickup_latitude', '744', 'dropoff_latitude', '580'], ['dropoff_longitude', '100'], ['trip_miles', '1.3', 'extras', '1', 'fare', '35'], ['trip_miles', '1.8', 'fare', '45'], ['dropoff_longitude', '555'], ['trip_miles', '1.8', 'fare', '45'], ['fare', '20', 'tips', '11'], ['pickup_latitude', '121'], ['trip_miles', '0.9'], ['dropoff_longitude', '418', 'fare', '43', 'extras', '1.5'], ['trip_miles', '0.9'], ['fare', '20', 'tips', '11'], ['fare', '40', 'company', '100', 'pickup_latitude', '744'], ['fare', '40', 'company', '100', 'pickup_latitude', '744'], ['pickup_latitude', '744', 'dropoff_latitude', '580'], ['dropoff_longitude', '100'], ['fare', '40', 'company', '100', 'pickup_latitude', '744'], ['pickup_latitude', '744', 'dropoff_latitude', '580'], ['dropoff_latitude', '789', 'fare', '50', 'tips', '14'], ['pickup_latitude', '744', 'dropoff_latitude', '580'], ['tips', '1', 'fare', '17', 'dropoff_latitude', '378'], ['trip_miles', '1.3', 'tips', '7'], ['fare', '40', 'company', '100', 'pickup_latitude', '744'], ['company', '63'], ['fare', '10', 'trip_miles', '0.7'], ['extras', '1'], ['pickup_latitude', '121'], ['dropoff_latitude', '789', 'fare', '50', 'tips', '14'], ['trip_miles', '1.3', 'tips', '7'], ['dropoff_longitude', '100'], ['extras', '1'], ['company', '63'], ['company', '63'], ['dropoff_longitude', '100'], ['tips', '1', 'fare', '17', 'dropoff_latitude', '378'], ['pickup_latitude', '744', 'dropoff_latitude', '580'], ['tips', '1.5', 'dropoff_longitude', '615'], ['tips', '1', 'fare', '17', 'dropoff_latitude', '378'], ['trip_miles', '0.9'], ['company', '40', 'fare', '65'], ['extras', '1'], ['dropoff_longitude', '555'], ['pickup_latitude', '744', 'dropoff_latitude', '580'], ['dropoff_longitude', '418', 'fare', '43', 'extras', '1.5'], ['company', '40', 'fare', '65'], ['trip_miles', '0.9'], ['pickup_latitude', '744', 'dropoff_latitude', '580'], ['trip_miles', '1.3', 'extras', '1', 'fare', '35'], ['dropoff_longitude', '100'], ['dropoff_longitude', '555'], ['trip_miles', '0.9'], ['trip_miles', '0.9'], ['tips', '1', 'fare', '17', 'dropoff_latitude', '378'], ['tips', '1', 'fare', '17', 'dropoff_latitude', '378'], ['trip_miles', '1.3', 'extras', '1', 'fare', '35'], ['tips', '1.5', 'dropoff_longitude', '615'], ['trip_miles', '1.3', 'tips', '7'], ['dropoff_longitude', '555'], ['extras', '1'], ['trip_miles', '1.8', 'fare', '45'], ['dropoff_longitude', '100'], ['trip_miles', '0.9'], ['tips', '1.5', 'dropoff_longitude', '615'], ['extras', '1'], ['company', '63'], ['tips', '1', 'fare', '17', 'dropoff_latitude', '378'], ['pickup_latitude', '744', 'dropoff_latitude', '580'], ['fare', '70'], ['fare', '40', 'company', '100', 'pickup_latitude', '744'], ['trip_miles', '2'], ['fare', '10', 'trip_miles', '0.7'], ['trip_miles', '0.9'], ['dropoff_longitude', '418', 'fare', '43', 'extras', '1.5'], ['trip_miles', '1.3', 'extras', '1', 'fare', '35'], ['tips', '1.5', 'dropoff_longitude', '615'], ['fare', '40', 'company', '100', 'pickup_latitude', '744'], ['tips', '1.5', 'dropoff_longitude', '615'], ['fare', '10', 'trip_miles', '0.7'], ['dropoff_latitude', '789', 'fare', '50', 'tips', '14'], ['trip_miles', '0.9'], ['trip_miles', '1.8', 'fare', '45'], ['pickup_latitude', '121'], ['dropoff_longitude', '418', 'fare', '43', 'extras', '1.5'], ['fare', '70'], ['trip_miles', '1.8', 'fare', '45'], ['pickup_latitude', '744', 'dropoff_latitude', '580'], ['extras', '1'], ['extras', '1'], ['trip_miles', '1.3', 'extras', '1', 'fare', '35'], ['extras', '1'], ['trip_miles', '0.9'], ['fare', '40', 'company', '100', 'pickup_latitude', '744'], ['fare', '10', 'trip_miles', '0.7'], ['fare', '10', 'trip_miles', '0.7'], ['company', '40', 'fare', '65'], ['fare', '10', 'trip_miles', '0.7'], ['company', '63'], ['fare', '40', 'company', '100', 'pickup_latitude', '744'], ['fare', '20', 'tips', '11'], ['pickup_latitude', '744', 'dropoff_latitude', '580'], ['trip_miles', '1.3', 'extras', '1', 'fare', '35'], ['trip_miles', '1.3', 'extras', '1', 'fare', '35'], ['dropoff_longitude', '100'], ['dropoff_latitude', '789', 'fare', '50', 'tips', '14'], ['pickup_latitude', '744', 'dropoff_latitude', '580'], ['fare', '20', 'tips', '11'], ['company', '63'], ['tips', '1', 'fare', '17', 'dropoff_latitude', '378'], ['fare', '40', 'company', '100', 'pickup_latitude', '744'], ['company', '40', 'fare', '65'], ['dropoff_longitude', '555'], ['trip_miles', '2'], ['company', '40', 'fare', '65'], ['pickup_latitude', '121'], ['dropoff_latitude', '789', 'fare', '50', 'tips', '14'], ['dropoff_longitude', '100'], ['tips', '1', 'fare', '17', 'dropoff_latitude', '378'], ['tips', '1.5', 'dropoff_longitude', '615'], ['trip_miles', '1.8', 'fare', '45'], ['fare', '70'], ['pickup_latitude', '121'], ['trip_miles', '1.3', 'tips', '7'], ['trip_miles', '1.8', 'fare', '45'], ['dropoff_longitude', '100'], ['pickup_latitude', '744', 'dropoff_latitude', '580'], ['trip_miles', '1.3', 'tips', '7'], ['trip_miles', '2'], ['trip_miles', '1.3', 'extras', '1', 'fare', '35'], ['pickup_latitude', '744', 'dropoff_latitude', '580'], ['pickup_latitude', '121'], ['fare', '70'], ['extras', '1'], ['dropoff_longitude', '555'], ['tips', '1', 'fare', '17', 'dropoff_latitude', '378'], ['company', '63'], ['company', '40', 'fare', '65'], ['tips', '1', 'fare', '17', 'dropoff_latitude', '378'], ['fare', '70'], ['dropoff_longitude', '418', 'fare', '43', 'extras', '1.5'], ['pickup_latitude', '744', 'dropoff_latitude', '580'], ['fare', '70'], ['tips', '1', 'fare', '17', 'dropoff_latitude', '378'], ['trip_miles', '1.3', 'tips', '7'], ['trip_miles', '0.9'], ['dropoff_longitude', '418', 'fare', '43', 'extras', '1.5'], ['company', '63'], ['tips', '1', 'fare', '17', 'dropoff_latitude', '378'], ['dropoff_longitude', '100'], ['trip_miles', '0.9'], ['tips', '1', 'fare', '17', 'dropoff_latitude', '378'], ['dropoff_latitude', '789', 'fare', '50', 'tips', '14'], ['dropoff_longitude', '100'], ['trip_miles', '2'], ['tips', '50', 'extras', '8'], ['pickup_latitude', '744', 'dropoff_latitude', '580'], ['fare', '20', 'tips', '11'], ['company', '40', 'fare', '65'], ['pickup_latitude', '744', 'dropoff_latitude', '580'], ['trip_miles', '1.8', 'fare', '45'], ['dropoff_longitude', '100'], ['trip_miles', '0.9'], ['trip_miles', '2'], ['tips', '1', 'fare', '17', 'dropoff_latitude', '378'], ['fare', '40', 'company', '100', 'pickup_latitude', '744'], ['trip_miles', '1.8', 'fare', '45'], ['dropoff_latitude', '789', 'fare', '50', 'tips', '14'], ['pickup_latitude', '121'], ['trip_miles', '1.3', 'tips', '7'], ['dropoff_longitude', '555'], ['trip_miles', '0.9'], ['pickup_latitude', '121'], ['pickup_latitude', '121'], ['dropoff_longitude', '418', 'fare', '43', 'extras', '1.5'], ['pickup_latitude', '744', 'dropoff_latitude', '580'], ['tips', '1', 'fare', '17', 'dropoff_latitude', '378'], ['fare', '10', 'trip_miles', '0.7'], ['trip_miles', '0.9'], ['pickup_latitude', '121'], ['dropoff_latitude', '789', 'fare', '50', 'tips', '14'], ['trip_miles', '1.3', 'extras', '1', 'fare', '35'], ['pickup_latitude', '744', 'dropoff_latitude', '580'], ['extras', '1']]
    
#*****************************************************************************************************************************************
    # SCENARIO 4
    
    #query_list=[["fare","11"],["fare","40"],["fare","33"],["fare","37"],["fare","50"],["fare","51"],["fare","24"],["fare","28"],["fare","39"],["fare","26"],["fare","23"],["fare","54"],["fare","61"],["fare","35"],["fare","10"],["fare","15"],["fare","12"],["fare","21"],["fare","70"],["fare","80"]]

    #txt_name = "scenario4_new.txt"
    #txt_name = "gpu_test4.txt"
    
    #query_list = [['fare', '50'], ['fare', '39'], ['fare', '11'], ['fare', '28'], ['fare', '80'], ['fare', '39'], ['fare', '11'], ['fare', '70'], ['fare', '51'], ['fare', '61'], ['fare', '11'], ['fare', '70'], ['fare', '40'], ['fare', '26'], ['fare', '35'], ['fare', '10'], ['fare', '10'], ['fare', '50'], ['fare', '40'], ['fare', '61'], ['fare', '50'], ['fare', '11'], ['fare', '21'], ['fare', '12'], ['fare', '21'], ['fare', '23'], ['fare', '70'], ['fare', '15'], ['fare', '12'], ['fare', '21'], ['fare', '70'], ['fare', '54'], ['fare', '80'], ['fare', '51'], ['fare', '61'], ['fare', '54'], ['fare', '21'], ['fare', '40'], ['fare', '40'], ['fare', '37'], ['fare', '35'], ['fare', '10'], ['fare', '26'], ['fare', '35'], ['fare', '24'], ['fare', '40'], ['fare', '54'], ['fare', '33'], ['fare', '33'], ['fare', '11'], ['fare', '33'], ['fare', '80'], ['fare', '23'], ['fare', '23'], ['fare', '21'], ['fare', '10'], ['fare', '21'], ['fare', '40'], ['fare', '23'], ['fare', '35'], ['fare', '28'], ['fare', '80'], ['fare', '33'], ['fare', '24'], ['fare', '23'], ['fare', '40'], ['fare', '12'], ['fare', '37'], ['fare', '54'], ['fare', '24'], ['fare', '24'], ['fare', '10'], ['fare', '54'], ['fare', '21'], ['fare', '61'], ['fare', '70'], ['fare', '37'], ['fare', '24'], ['fare', '61'], ['fare', '28'], ['fare', '26'], ['fare', '28'], ['fare', '40'], ['fare', '26'], ['fare', '28'], ['fare', '70'], ['fare', '40'], ['fare', '24'], ['fare', '40'], ['fare', '80'], ['fare', '40'], ['fare', '10'], ['fare', '80'], ['fare', '37'], ['fare', '40'], ['fare', '39'], ['fare', '28'], ['fare', '51'], ['fare', '40'], ['fare', '12'], ['fare', '12'], ['fare', '11'], ['fare', '21'], ['fare', '24'], ['fare', '35'], ['fare', '33'], ['fare', '80'], ['fare', '10'], ['fare', '40'], ['fare', '80'], ['fare', '35'], ['fare', '11'], ['fare', '50'], ['fare', '28'], ['fare', '50'], ['fare', '23'], ['fare', '11'], ['fare', '70'], ['fare', '24'], ['fare', '54'], ['fare', '28'], ['fare', '28'], ['fare', '10'], ['fare', '54'], ['fare', '28'], ['fare', '26'], ['fare', '40'], ['fare', '15'], ['fare', '37'], ['fare', '35'], ['fare', '61'], ['fare', '10'], ['fare', '10'], ['fare', '15'], ['fare', '80'], ['fare', '39'], ['fare', '28'], ['fare', '15'], ['fare', '37'], ['fare', '11'], ['fare', '10'], ['fare', '24'], ['fare', '61'], ['fare', '35'], ['fare', '61'], ['fare', '70'], ['fare', '23'], ['fare', '10'], ['fare', '40'], ['fare', '23'], ['fare', '50'], ['fare', '11'], ['fare', '54'], ['fare', '54'], ['fare', '51'], ['fare', '35'], ['fare', '10'], ['fare', '54'], ['fare', '54'], ['fare', '39'], ['fare', '23'], ['fare', '24'], ['fare', '80'], ['fare', '11'], ['fare', '35'], ['fare', '80'], ['fare', '11'], ['fare', '70'], ['fare', '23'], ['fare', '26'], ['fare', '21'], ['fare', '54'], ['fare', '37'], ['fare', '12'], ['fare', '15'], ['fare', '35'], ['fare', '70'], ['fare', '51'], ['fare', '28'], ['fare', '11'], ['fare', '28'], ['fare', '33'], ['fare', '39'], ['fare', '33'], ['fare', '28'], ['fare', '10'], ['fare', '21'], ['fare', '12'], ['fare', '12'], ['fare', '37'], ['fare', '12'], ['fare', '26'], ['fare', '40'], ['fare', '28'], ['fare', -'15'], ['fare', '33'], ['fare', '23'], ['fare', '33'], ['fare', '80'], ['fare', '33'], ['fare', '35'], ['fare', '12'], ['fare', '28'], ['fare', '51'], ['fare', '26'], ['fare', '24'], ['fare', '24'], ['fare', '37'], ['fare', '24'], ['fare', '51'], ['fare', '21'], ['fare', '54'], ['fare', '28'], ['fare', '39'], ['fare', '10'], ['fare', '23'], ['fare', '51'], ['fare', '50'], ['fare', '15'], ['fare', '54'], ['fare', '23'], ['fare', '33'], ['fare', '37'], ['fare', '12'], ['fare', '26'], ['fare', '10'], ['fare', '26'], ['fare', '12'], ['fare', '11'], ['fare', '54'], ['fare', '15'], ['fare', '54'], ['fare', '24'], ['fare', '54'], ['fare', '61'], ['fare', '80'], ['fare', '80'], ['fare', '28'], ['fare', '10'], ['fare', '28'], ['fare', '12'], ['fare', '39'], ['fare', '10'], ['fare', '50'], ['fare', '35'], ['fare', '54'], ['fare', '61'], ['fare', '15'], ['fare', '28'], ['fare', '37'], ['fare', '61'], ['fare', '54'], ['fare', '40'], ['fare', '21'], ['fare', '61'], ['fare', '70'], ['fare', '37'], ['fare', '61'], ['fare', '24'], ['fare', '40'], ['fare', '70'], ['fare', '28'], ['fare', '21'], ['fare', '40'], ['fare', '12'], ['fare', '28'], ['fare', '40'], ['fare', '51'], ['fare', '70'], ['fare', '21'], ['fare', '26'], ['fare', '40'], ['fare', '80'], ['fare', '61'], ['fare', '24'], ['fare', '15'], ['fare', '11'], ['fare', '54'], ['fare', '12'], ['fare', '12'], ['fare', '50'], ['fare', '28'], ['fare', '39'], ['fare', '51'], ['fare', '51'], ['fare', '37'], ['fare', '35'], ['fare', '11'], ['fare', '50'], ['fare', '80'], ['fare', '40'], ['fare', '50'], ['fare', '35'], ['fare', '12'], ['fare', '24'], ['fare', '24'], ['fare', '40'], ['fare', '70'], ['fare', '24'], ['fare', '23']]
#*****************************************************************************************************************************************
    # SCENARIO 5

    #query_list=[["trip_miles"],["fare"],["extras"],["dropoff_latitude"],["dropoff_longitude"],["company"],["pickup_latitude"]]

    #txt_name = "scenario5_new.txt"
    #txt_name = "gpu_test5.txt"
    
    #query_list = [['extras'], ['trip_miles'], ['pickup_latitude'], ['dropoff_longitude'], ['trip_miles'], ['fare'], ['pickup_latitude'], ['fare'], ['dropoff_longitude'], ['trip_miles'], ['fare'], ['company'], ['dropoff_longitude'], ['company'], ['trip_miles'], ['trip_miles'], ['company'], ['trip_miles'], ['company'], ['pickup_latitude'], ['fare'], ['dropoff_latitude'], ['trip_miles'], ['pickup_latitude'], ['dropoff_latitude'], ['trip_miles'], ['trip_miles'], ['fare'], ['fare'], ['fare'], ['dropoff_latitude'], ['company'], ['dropoff_longitude'], ['company'], ['fare'], ['extras'], ['fare'], ['pickup_latitude'], ['fare'], ['trip_miles'], ['dropoff_longitude'], ['extras'], ['company'], ['company'], ['dropoff_longitude'], ['pickup_latitude'], ['company'], ['trip_miles'], ['fare'], ['trip_miles'], ['pickup_latitude'], ['trip_miles'], ['pickup_latitude'], ['extras'], ['extras'], ['company'], ['fare'], ['company'], ['dropoff_longitude'], ['dropoff_latitude'], ['fare'], ['dropoff_latitude'], ['dropoff_longitude'], ['dropoff_latitude'], ['pickup_latitude'], ['dropoff_longitude'], ['fare'], ['dropoff_latitude'], ['dropoff_longitude'], ['trip_miles'], ['dropoff_longitude'], ['dropoff_longitude'], ['fare'], ['trip_miles'], ['company'], ['company'], ['company'], ['company'], ['dropoff_latitude'], ['dropoff_latitude'], ['company'], ['fare'], ['pickup_latitude'], ['extras'], ['pickup_latitude'], ['extras'], ['dropoff_latitude'], ['fare'], ['pickup_latitude'], ['trip_miles'], ['extras'], ['trip_miles'], ['company'], ['pickup_latitude'], ['dropoff_latitude'], ['dropoff_longitude'], ['trip_miles'], ['trip_miles'], ['pickup_latitude'], ['trip_miles'], ['dropoff_latitude'], ['pickup_latitude'], ['extras'], ['dropoff_latitude'], ['pickup_latitude'], ['pickup_latitude'], ['dropoff_latitude'], ['dropoff_longitude'], ['company'], ['pickup_latitude'], ['fare'], ['trip_miles'], ['dropoff_longitude'], ['extras'], ['extras'], ['dropoff_longitude'], ['pickup_latitude'], ['extras'], ['trip_miles'], ['fare'], ['company'], ['dropoff_latitude'], ['trip_miles'], ['fare'], ['dropoff_longitude'], ['extras'], ['dropoff_longitude'], ['pickup_latitude'], ['extras'], ['company'], ['company'], ['company'], ['fare'], ['company'], ['fare'], ['trip_miles'], ['trip_miles'], ['trip_miles'], ['fare'], ['dropoff_latitude'], ['fare'], ['dropoff_latitude'], ['fare'], ['fare'], ['extras'], ['fare'], ['fare'], ['dropoff_longitude'], ['dropoff_longitude'], ['pickup_latitude'], ['trip_miles'], ['pickup_latitude'], ['dropoff_latitude'], ['extras'], ['trip_miles'], ['dropoff_latitude'], ['dropoff_latitude'], ['extras'], ['dropoff_latitude'], ['trip_miles'], ['trip_miles'], ['dropoff_latitude'], ['trip_miles'], ['company'], ['trip_miles'], ['dropoff_longitude'], ['fare'], ['company'], ['trip_miles'], ['pickup_latitude'], ['dropoff_longitude'], ['trip_miles'], ['trip_miles'], ['dropoff_latitude'], ['pickup_latitude'], ['trip_miles'], ['company'], ['fare'], ['dropoff_longitude'], ['pickup_latitude'], ['fare'], ['company'], ['company'], ['trip_miles'], ['company'], ['pickup_latitude'], ['extras'], ['dropoff_longitude'], ['fare'], ['trip_miles'], ['trip_miles'], ['dropoff_latitude'], ['dropoff_longitude'], ['trip_miles'], ['trip_miles'], ['extras'], ['trip_miles'], ['dropoff_latitude'], ['pickup_latitude'], ['dropoff_longitude'], ['pickup_latitude'], ['trip_miles'], ['trip_miles'], ['pickup_latitude'], ['dropoff_latitude'], ['trip_miles'], ['company'], ['fare'], ['pickup_latitude'], ['dropoff_longitude'], ['dropoff_latitude'], ['company'], ['dropoff_latitude'], ['extras'], ['extras'], ['trip_miles'], ['fare'], ['dropoff_longitude'], ['trip_miles'], ['dropoff_longitude'], ['trip_miles'], ['fare'], ['trip_miles'], ['trip_miles'], ['extras'], ['trip_miles'], ['dropoff_longitude'], ['fare'], ['dropoff_latitude'], ['dropoff_latitude'], ['trip_miles'], ['dropoff_latitude'], ['fare'], ['dropoff_latitude'], ['dropoff_longitude'], ['extras'], ['trip_miles'], ['company'], ['pickup_latitude'], ['pickup_latitude'], ['extras'], ['pickup_latitude'], ['company'], ['extras'], ['dropoff_longitude'], ['extras'], ['trip_miles'], ['dropoff_latitude'], ['fare'], ['company'], ['trip_miles'], ['fare'], ['extras'], ['company'], ['dropoff_latitude'], ['company'], ['extras'], ['dropoff_longitude'], ['dropoff_longitude'], ['fare'], ['trip_miles'], ['trip_miles'], ['dropoff_latitude'], ['company'], ['pickup_latitude'], ['extras'], ['extras'], ['company'], ['company'], ['extras'], ['company'], ['fare'], ['trip_miles'], ['trip_miles'], ['dropoff_latitude'], ['company'], ['dropoff_longitude'], ['pickup_latitude'], ['pickup_latitude'], ['extras'], ['trip_miles'], ['trip_miles'], ['dropoff_latitude'], ['pickup_latitude'], ['dropoff_longitude'], ['pickup_latitude'], ['fare'], ['company'], ['extras'], ['company'], ['fare'], ['pickup_latitude'], ['dropoff_longitude'], ['trip_miles'], ['dropoff_longitude'], ['company'], ['dropoff_longitude'], ['company'], ['fare'], ['dropoff_longitude']]
#**************************************************************************************************************************************** 
    # SCENARIO 6
    
    #query_list=[["fare","30"],["fare","30","tips","7"],["trip_miles","0.9"],["trip_miles","1.3","tips","2"],["extras","1","fare","25"],["dropoff_latitude","744"],["dropoff_latitude","744","fare","30"],["dropoff_longitude","511"],["dropoff_longitude","100"],["dropoff_longitude","511","fare","20","extras","1.5"],["company","101"],["company","80","fare","25"],["pickup_latitude","744"],["pickup_latitude","744","dropoff_latitude","580"],["fare","60"],["tips","30","extras","8"],["trip_miles","1","fare","35"],["tips","1.5","dropoff_longitude","615"],["fare","50","trip_miles","5.7"],["fare","50","company","90"],["fare","50","tips","9","company","85"],["fare","37","tips","5","trip_miles","0.6"],["trip_miles","0.3","extras","2","tips","15"],["trip_miles","5.3","tips","5","fare","25"],["extras","3","fare","25","tips","1"],["dropoff_latitude","857","fare","20"],["dropoff_latitude","484","fare","50"],["dropoff_longitude","381","extras","3","fare","20"],["dropoff_longitude","200","extras","2"],["dropoff_longitude","418","fare","50","extras","1.8"],["company","151","tips","2"],["company","80","fare","45"],["pickup_latitude","416"],["pickup_latitude","589","dropoff_latitude","589"],["fare","60","tips","20"],["tips","20","extras","4"],["trip_miles","5","fare","55"],["tips","1.7","dropoff_longitude","601"],["fare","40","trip_miles","5.9"],["fare","10","company","95"],["fare"],["fare","40"],["fare","33"],["fare","37"],["fare","50"],["fare","51"],["fare","24"],["fare","28"],["fare","39"],["fare","26"],["fare","23"],["fare","54"],["fare","61"],["trip_miles"],["fare"],["tips"],["extras"],["dropoff_latitude"],["dropoff_longitude"],["company"],["pickup_latitude"]]

    #txt_name = "scenario6_new2.txt"
    #txt_name = "gpu_test6.txt"
    
    query_list= [['fare'], ['dropoff_longitude', '511', 'fare', '20', 'extras', '1.5'], ['fare', '61'], ['pickup_latitude', '589', 'dropoff_latitude', '589'], ['dropoff_longitude'], ['dropoff_latitude', '857', 'fare', '20'], ['company', '80', 'fare', '25'], ['fare', '30'], ['company', '151', 'tips', '2'], ['company', '80', 'fare', '25'], ['fare', '50', 'trip_miles', '5.7'], ['tips', '30', 'extras', '8'], ['fare'], ['fare'], ['company', '151', 'tips', '2'], ['fare', '37'], ['pickup_latitude', '416'], ['fare', '39'], ['dropoff_latitude'], ['fare', '39'], ['tips', '30', 'extras', '8'], ['company'], ['pickup_latitude', '589', 'dropoff_latitude', '589'], ['dropoff_longitude', '100'], ['fare', '23'], ['company', '80', 'fare', '45'], ['pickup_latitude'], ['pickup_latitude'], ['pickup_latitude'], ['dropoff_longitude', '511', 'fare', '20', 'extras', '1.5'], ['dropoff_longitude', '511', 'fare', '20', 'extras', '1.5'], ['dropoff_longitude', '381', 'extras', '3', 'fare', '20'], ['dropoff_latitude', '484', 'fare', '50'], ['dropoff_longitude', '418', 'fare', '50', 'extras', '1.8'], ['dropoff_latitude', '857', 'fare', '20'], ['tips', '30', 'extras', '8'], ['fare', '50', 'company', '90'], ['fare', '60'], ['dropoff_longitude', '381', 'extras', '3', 'fare', '20'], ['fare', '37'], ['fare', '30'], ['fare', '28'], ['fare'], ['company', '80', 'fare', '25'], ['extras'], ['dropoff_longitude', '511', 'fare', '20', 'extras', '1.5'], ['fare', '30', 'tips', '7'], ['extras', '1', 'fare', '25'], ['fare', '50'], ['fare', '39'], ['fare', '10', 'company', '95'], ['dropoff_latitude'], ['fare', '54'], ['fare', '10', 'company', '95'], ['company', '101'], ['tips', '1.7', 'dropoff_longitude', '601'], ['fare', '40', 'trip_miles', '5.9'], ['pickup_latitude', '416'], ['trip_miles', '5.3', 'tips', '5', 'fare', '25'], ['trip_miles', '1.3', 'tips', '2'], ['company', '101'], ['fare', '10', 'company', '95'], ['fare', '23'], ['fare', '39'], ['tips', '20', 'extras', '4'], ['dropoff_longitude', '511', 'fare', '20', 'extras', '1.5'], ['dropoff_latitude', '484', 'fare', '50'], ['fare', '37'], ['fare', '61'], ['fare', '51'], ['fare', '40'], ['dropoff_latitude', '857', 'fare', '20'], ['dropoff_longitude', '418', 'fare', '50', 'extras', '1.8'], ['fare', '40', 'trip_miles', '5.9'], ['company', '101'], ['pickup_latitude', '416'], ['trip_miles', '5', 'fare', '55'], ['trip_miles', '5', 'fare', '55'], ['fare'], ['company'], ['pickup_latitude', '744', 'dropoff_latitude', '580'], ['fare', '50', 'company', '90'], ['trip_miles', '0.9'], ['dropoff_latitude', '857', 'fare', '20'], ['tips', '20', 'extras', '4'], ['fare', '30'], ['tips', '20', 'extras', '4'], ['pickup_latitude', '589', 'dropoff_latitude', '589'], ['trip_miles', '1', 'fare', '35'], ['extras', '3', 'fare', '25', 'tips', '1'], ['fare', '33'], ['fare', '40', 'trip_miles', '5.9'], ['fare'], ['dropoff_longitude', '511'], ['trip_miles', '1.3', 'tips', '2'], ['fare', '40', 'trip_miles', '5.9'], ['fare', '50', 'tips', '9', 'company', '85'], ['fare', '50'], ['trip_miles'], ['trip_miles', '1.3', 'tips', '2'], ['fare', '61'], ['fare', '28'], ['fare'], ['trip_miles', '5', 'fare', '55'], ['tips', '30', 'extras', '8'], ['fare'], ['trip_miles', '5.3', 'tips', '5', 'fare', '25'], ['trip_miles', '5.3', 'tips', '5', 'fare', '25'], ['extras'], ['fare', '30', 'tips', '7'], ['fare', '54'], ['fare', '10', 'company', '95'], ['dropoff_latitude', '744', 'fare', '30'], ['fare', '50'], ['fare', '40', 'trip_miles', '5.9'], ['tips', '1.5', 'dropoff_longitude', '615'], ['fare', '61'], ['pickup_latitude', '416'], ['fare', '30', 'tips', '7'], ['trip_miles', '5.3', 'tips', '5', 'fare', '25'], ['company', '101'], ['company'], ['dropoff_latitude', '484', 'fare', '50'], ['fare', '54'], ['dropoff_longitude', '418', 'fare', '50', 'extras', '1.8'], ['company', '80', 'fare', '45'], ['company', '101'], ['trip_miles', '5.3', 'tips', '5', 'fare', '25'], ['fare', '33'], ['fare', '40', 'trip_miles', '5.9'], ['fare', '60', 'tips', '20'], ['company', '80', 'fare', '25'], ['fare', '37'], ['company'], ['fare', '26'], ['company', '101'], ['fare', '50', 'tips', '9', 'company', '85'], ['fare'], ['fare', '30'], ['fare', '23'], ['company', '80', 'fare', '45'], ['company', '80', 'fare', '25'], ['trip_miles', '0.3', 'extras', '2', 'tips', '15'], ['dropoff_latitude', '484', 'fare', '50'], ['fare', '40', 'trip_miles', '5.9'], ['company', '101'], ['company', '80', 'fare', '25'], ['fare', '40'], ['dropoff_latitude', '484', 'fare', '50'], ['fare', '50', 'trip_miles', '5.7'], ['dropoff_longitude', '381', 'extras', '3', 'fare', '20'], ['extras', '3', 'fare', '25', 'tips', '1'], ['tips', '1.5', 'dropoff_longitude', '615'], ['dropoff_longitude', '200', 'extras', '2'], ['extras', '1', 'fare', '25'], ['pickup_latitude', '416'], ['fare'], ['fare', '50', 'tips', '9', 'company', '85'], ['trip_miles', '5', 'fare', '55'], ['fare', '39'], ['dropoff_longitude'], ['fare', '28'], ['dropoff_latitude', '744'], ['fare', '30', 'tips', '7'], ['tips', '1.5', 'dropoff_longitude', '615'], ['dropoff_longitude', '200', 'extras', '2'], ['fare', '30', 'tips', '7'], ['fare', '50'], ['extras', '3', 'fare', '25', 'tips', '1'], ['fare', '40'], ['fare', '26'], ['fare', '26'], ['fare', '40', 'trip_miles', '5.9'], ['fare'], ['fare', '28'], ['trip_miles', '1', 'fare', '35'], ['extras', '3', 'fare', '25', 'tips', '1'], ['trip_miles', '0.9'], ['fare', '30'], ['dropoff_longitude', '381', 'extras', '3', 'fare', '20'], ['trip_miles', '0.3', 'extras', '2', 'tips', '15'], ['tips', '30', 'extras', '8'], ['fare'], ['fare', '50', 'tips', '9', 'company', '85'], ['tips', '30', 'extras', '8'], ['dropoff_longitude', '418', 'fare', '50', 'extras', '1.8'], ['fare', '23'], ['fare'], ['fare'], ['company', '101'], ['dropoff_latitude', '744', 'fare', '30'], ['fare', '37'], ['fare'], ['fare', '40'], ['dropoff_longitude', '511'], ['fare', '37'], ['extras'], ['trip_miles', '5.3', 'tips', '5', 'fare', '25'], ['dropoff_longitude', '100'], ['trip_miles', '5', 'fare', '55'], ['fare', '60'], ['company', '80', 'fare', '25'], ['dropoff_longitude', '418', 'fare', '50', 'extras', '1.8'], ['dropoff_longitude', '200', 'extras', '2'], ['pickup_latitude', '589', 'dropoff_latitude', '589'], ['fare', '40', 'trip_miles', '5.9'], ['fare', '28'], ['fare', '24'], ['dropoff_latitude', '744', 'fare', '30'], ['dropoff_longitude', '418', 'fare', '50', 'extras', '1.8'], ['tips', '1.5', 'dropoff_longitude', '615'], ['fare', '24'], ['company', '80', 'fare', '45'], ['fare', '40', 'trip_miles', '5.9'], ['trip_miles', '1', 'fare', '35'], ['fare', '61'], ['extras'], ['fare'], ['tips', '1.5', 'dropoff_longitude', '615'], ['dropoff_longitude'], ['dropoff_longitude', '511', 'fare', '20', 'extras', '1.5'], ['dropoff_latitude', '744', 'fare', '30'], ['fare'], ['trip_miles'], ['fare', '50', 'tips', '9', 'company', '85'], ['tips', '1.7', 'dropoff_longitude', '601'], ['trip_miles', '1', 'fare', '35'], ['trip_miles', '5', 'fare', '55'], ['company', '80', 'fare', '45'], ['fare'], ['tips', '30', 'extras', '8'], ['fare', '51'], ['tips', '1.5', 'dropoff_longitude', '615'], ['fare', '40'], ['trip_miles'], ['company', '151', 'tips', '2'], ['dropoff_longitude', '200', 'extras', '2'], ['fare', '50', 'company', '90'], ['dropoff_longitude'], ['pickup_latitude', '416'], ['extras', '1', 'fare', '25'], ['company', '80', 'fare', '45'], ['dropoff_latitude', '857', 'fare', '20'], ['fare', '50', 'tips', '9', 'company', '85'], ['tips', '30', 'extras', '8'], ['trip_miles', '1', 'fare', '35'], ['trip_miles', '0.9'], ['trip_miles', '5', 'fare', '55'], ['dropoff_latitude', '857', 'fare', '20'], ['extras', '1', 'fare', '25'], ['dropoff_longitude', '511'], ['fare', '30'], ['fare', '37'], ['company'], ['fare', '40', 'trip_miles', '5.9'], ['extras', '3', 'fare', '25', 'tips', '1'], ['dropoff_longitude', '381', 'extras', '3', 'fare', '20'], ['fare', '33'], ['pickup_latitude', '744'], ['company', '80', 'fare', '25'], ['dropoff_longitude', '418', 'fare', '50', 'extras', '1.8'], ['dropoff_latitude', '744'], ['fare', '40', 'trip_miles', '5.9'], ['pickup_latitude', '744'], ['fare', '30'], ['fare', '60'], ['pickup_latitude'], ['fare', '30', 'tips', '7'], ['company', '80', 'fare', '25'], ['fare', '60'], ['company', '151', 'tips', '2'], ['fare', '33'], ['fare', '33'], ['pickup_latitude', '744'], ['trip_miles', '1', 'fare', '35'], ['trip_miles'], ['pickup_latitude', '744', 'dropoff_latitude', '580'], ['dropoff_longitude', '200', 'extras', '2'], ['fare', '24'], ['extras', '1', 'fare', '25'], ['pickup_latitude', '416'], ['dropoff_longitude', '381', 'extras', '3', 'fare', '20'], ['fare', '54'], ['fare', '60', 'tips', '20'], ['tips', '1.7', 'dropoff_longitude', '601'], ['tips', '1.7', 'dropoff_longitude', '601'], ['company', '101'], ['fare', '50'], ['fare'], ['trip_miles', '0.9'], ['pickup_latitude', '416'], ['extras'], ['fare', '10', 'company', '95'], ['dropoff_latitude', '744'], ['fare', '23'], ['fare', '28'], ['fare', '30', 'tips', '7'], ['trip_miles', '0.3', 'extras', '2', 'tips', '15'], ['fare', '23'], ['tips', '20', 'extras', '4']]


#**************************************************************************************************************************************** 
    #ml_data = "ml_data.txt"
#****************************************************************************************************************************************

    num_of_threads = int(sys.argv[5])
    #query_big =[]
    g_start = time.clock_gettime(1)    
    for query in query_list:
        decision = "GPU"              
        #print("Decision:",decision)

        if(len(query) == 1):     # Query Type 1, Scan for a whole column
            query_type = "1"
            #print("Query Type: 1")
            adhoc_key = query[0]
            q_id = hashQuery(adhoc_key)
            
            if (decision == "GPU"):         # GPU Part
                start = time.clock_gettime(1)
                print("Adhoc:", "key->", adhoc_key)
                print("Scanning for column",adhoc_key)
                datarizer,durations = connector.scan_gpu(query[0],connector.timeseries)
                execution_time = time.clock_gettime(1) - start
                
                nvidia_smi.nvmlInit()
                handle = nvidia_smi.nvmlDeviceGetHandleByIndex(0)
                
                res = nvidia_smi.nvmlDeviceGetUtilizationRates(handle)
                gpu_usage = res.gpu
                gpu_memory_usage = res.memory
                #print(f'GPU USAGE: {res.gpu}%, GPU MEMORY USAGE: {res.memory}%')
                gpu_temp = nvidia_smi.nvmlDeviceGetTemperature(handle, NVML_TEMPERATURE_GPU)
                gpu_powerusage = nvidia_smi.nvmlDeviceGetPowerUsage(handle)
                             
            elif(decision == "CPU"):        # CPU Part
                start = time.clock_gettime(1)
                print("Adhoc:", "key->", adhoc_key)
                print("Scanning for column",adhoc_key)
                #datarizer,durations = connector.scan_with_bf(adhoc_key, query[0]) #, connector.timeseries)
                datarizer,durations = connector.scan(query[0], connector.timeseries)
                execution_time = time.clock_gettime(1) - start

                #print("CPU USAGE: %",psutil.cpu_percent())
                #print("CPU MEMORY USAGE: %",psutil.virtual_memory()[2])
                #print("CPU TEMPERATURE: %",psutil.sensors_temperatures()["coretemp"][0][1])
                
                cpu_usage = psutil.cpu_percent()
                cpu_memory_usage = psutil.virtual_memory()[2]
                cpu_temperature = psutil.sensors_temperatures()["coretemp"][0][1]
     
        elif(len(query)==2):        # Query Type 2, scan with a value
            query_type = "2"
            #print("Query Type: 2")
            adhoc_targets = []
            adhoc_key = query[0]
            adhoc_operator = "="
            adhoc_value = query[1]
            adhoc_targets.append(adhoc_key)
            q_id = hashQuery(adhoc_key+adhoc_value)

            start = time.clock_gettime(1)

            print("Adhoc:" , "key->", adhoc_key, "operator->", adhoc_operator, "value->", adhoc_value)

            if(decision == "GPU"):      # GPU Part
                datarizer,durations = connector.scan_gpu_with_dependency(query[0], adhoc_key, adhoc_value, connector.timeseries)               

                nvidia_smi.nvmlInit()
                handle = nvidia_smi.nvmlDeviceGetHandleByIndex(0)
                
                res = nvidia_smi.nvmlDeviceGetUtilizationRates(handle)
                #print(f'GPU USAGE: {res.gpu}%, GPU MEMORY USAGE: {res.memory}%')
                gpu_usage = res.gpu
                gpu_memory_usage = res.memory
                gpu_temp= nvidia_smi.nvmlDeviceGetTemperature(handle, NVML_TEMPERATURE_GPU)
                gpu_powerusage = nvidia_smi.nvmlDeviceGetPowerUsage(handle)
                
                
            elif(decision == "CPU"):    # CPU Part
                datarizer, durations = connector.scan_with_bf(adhoc_key, adhoc_key, adhoc_value)
                ##datarizer, durations = connector.scan_with_dependency(adhoc_key, adhoc_key, adhoc_value, connector.timeseries)
            
                #print("CPU USAGE: %",psutil.cpu_percent())
                #print("CPU MEMORY USAGE: %",psutil.virtual_memory()[2])
                #print("CPU TEMPERATURE: %",psutil.sensors_temperatures()["coretemp"][0][1])
                
                cpu_usage = psutil.cpu_percent()
                cpu_memory_usage = psutil.virtual_memory()[2]
                cpu_temperature = psutil.sensors_temperatures()["coretemp"][0][1]

                
            global no_queries
            no_queries += 1
            global average_durations
            average_durations += durations

            execution_time = time.clock_gettime(1) - start

            #print("AVERAGE DURATION: ", average_durations/no_queries)

        elif (len(query) > 2):     # Query Type 3, complex query
            query_type = "3"
            adhoc_keys = []
            adhoc_values = []
            adhoc_ops = []
            adhoc_targets = []
            print("Query Type: 3")
            print("There are",len(query)//2,"queries.")
            hash_string = ""
            for i in range(0, len(query),2):
                adhoc_keys.append(query[i])
                adhoc_ops.append("=")
                adhoc_values.append(query[i+1])
                adhoc_targets.append(query[i])
                hash_string += query[i]

            q_id = hashQuery(hash_string)
            
            start = time.clock_gettime(1)

            #print("Adhoc:" , "key->", adhoc_key, "operator->", adhoc_operator, "value->", adhoc_value)
            print("Adhoc:" , "key->", adhoc_keys[1], "operator->", adhoc_ops[1], "value->", adhoc_values[1])

            if(decision == "GPU"):      # GPU Part
                if(len(query)==4):
                    datarizer, durations = connector.scan_gpu_mult_dep(adhoc_keys, adhoc_values, connector.timeseries)
                elif(len(query)==6):
                    datarizer, durations = connector.scan_gpu_mult_dep2(adhoc_keys, adhoc_values, connector.timeseries)
                elif(len(query)==8):
                    datarizer, durations = connector.scan_gpu_mult_dep3(adhoc_keys, adhoc_values, connector.timeseries)
                elif(len(query)==10):
                    datarizer, durations = connector.scan_gpu_mult_dep4(adhoc_keys, adhoc_values, connector.timeseries)
                
                nvidia_smi.nvmlInit()
                handle = nvidia_smi.nvmlDeviceGetHandleByIndex(0)
                
                res = nvidia_smi.nvmlDeviceGetUtilizationRates(handle)
                #print(f'GPU USAGE: {res.gpu}%, GPU MEMORY USAGE: {res.memory}%')
                gpu_usage = res.gpu
                gpu_memory_usage = res.memory
                gpu_temp = nvidia_smi.nvmlDeviceGetTemperature(handle, NVML_TEMPERATURE_GPU)
                gpu_powerusage = nvidia_smi.nvmlDeviceGetPowerUsage(handle)
                
            elif(decision == "CPU"):     # CPU Part
                datarizer, durations = connector.scan_with_dep_bf(adhoc_targets, adhoc_values)
                ##datarizer, durations = connector.scan_with_dependencies(adhoc_targets, adhoc_values, connector.timeseries)

                #print("CPU USAGE: %",psutil.cpu_percent())
                #print("CPU MEMORY USAGE: %",psutil.virtual_memory()[2])                
                #print("CPU TEMPERATURE: %",psutil.sensors_temperatures()["coretemp"][0][1])

                cpu_usage = psutil.cpu_percent()
                cpu_memory_usage = psutil.virtual_memory()[2]
                cpu_temperature = psutil.sensors_temperatures()["coretemp"][0][1]
                
            #global no_queries
            no_queries += 1
            #global average_durations
            average_durations += durations
            #average_durations /= no_queries

            res = []
            no_empty = 0
            if (len(datarizer) != 0):
                #print("Datarizer length:",len(datarizer))
                for i in range(0, len(datarizer), 2):
                    if(str(datarizer[i].decode("utf-8", errors="ignore").strip()) == ''):
                        no_empty += 1
                    elif(str(datarizer[i+1].decode("utf-8", errors="ignore").strip()) == ''):
                        no_empty += 1
                #print("NO_EMPTY: ", no_empty)
            #else:
                    #dt = datetime.strptime(str(datarizer[i+1].decode("utf-8",errors="ignore").strip()), '%Y-%m-%d %H:%M:%S')
                    #res.append([float(str(datarizer[i].decode("utf-8",errors="ignore").strip())), time.mktime(dt.timetuple())*1000])
                    #dt = datetime.strptime(str(datarizer[i+1].decode("utf-8")), '%Y-%m-%d %H:%M:%S')
                    #res.append([float(str(datarizer[i].decode("utf-8"))), time.mktime(dt.timetuple())*1000])


            #print("Length of res: ", len(res))

            
            execution_time = time.clock_gettime(1) - start

        #print("AVERAGE DURATION: ", average_durations/no_queries)

        #print("x_1 Device:",decision,"x_2 Device Utilization:",cpu_usage,"x_3 Device Memory Usage:",cpu_memory_usage,"x_9 Query Type:",query_type,"x_10 Response Time:",execution_time,"x_12 Device Temperature:",cpu_temperature)
 
        if(decision == "GPU"):
            t = Texttable()
            t.add_rows([["x_1","Device",decision],["x_2","Device Utilization (%)",gpu_usage],["x_3","Device Memory Usage (%)",gpu_memory_usage],["x_9","Query Type",query_type],["x_10","Response Time (s)",execution_time],["x_12","Device Temperature (C)",gpu_temp],["x_13","Device Power Consumption (Miliwatts)",gpu_powerusage]])
            print(t.draw())
            print("\n")
            #print("*  x_1 Device:",decision)
            #print("*  x_2 Device Utilization: %",gpu_usage)
            #print("*  x_3 Device Memory Usage: %",gpu_memory_usage)
            #print("*  x_9 Query Type:",query_type)
            #print("*  x_10 Response Time:",execution_time,"second(s)")
            #print("*  x_12 Device Temperature:",gpu_temp,"C")
            #print("*  x_13 Device Power Consumption:",gpu_powerusage,"Miliwatts")
        elif(decision=="CPU"):
            t = Texttable()
            t.add_rows([["x_1","Device",decision],["x_2","Device Utilization (%)",cpu_usage],["x_3","Device Memory Usage (%)",cpu_memory_usage],["x_9","Query Type",query_type],["x_10","Response Time (s)",execution_time],["x_12","Device Temperature (C)",cpu_temperature]])
            print(t.draw())
            print("\n")
            #print("*  x_1 Device:",decision)
            #print("*  x_2 Device Utilization:",cpu_usage)
            #print("*  x_3 Device Memory Usage:",cpu_memory_usage)
            #print("*  x_9 Query Type:",query_type)
            #print("*  x_10 Response Time:",execution_time)
            #print("*  x_12 Device Temperature:",cpu_temperature)
        
        print("QUERY -------->",query_list.index(query))

        queue_length = connector.deneme_py()
        print("******************** QUEUE LENGTH:",queue_length,"********")
        
        
    print("Finished and out ")
    #g_finish = time.clock_gettime(1)- g_start
    #f_lb = open(txt_name,"a")
    #finish = str(g_finish) + "\n"
    #f_lb.write(finish)
    #f_lb.close()

    
@app.route('/annotations', methods=['POST'])
def annotations():
    req = request.get_json()
    data = [
        {
            "annotation": 'This is the annotation',
        "time": (convert_to_time_ms(req['range']['from']) +
                 convert_to_time_ms(req['range']['to'])) / 2,
            "title": 'Deployment notes',
            "tags": ['tag1', 'tag2'],
            "text": 'Hm, something went wrong...'
        }
    ]
    return jsonify(data)

if __name__ == '__main__':
    app.run(host='127.0.0.1', port=4005, debug=True)
