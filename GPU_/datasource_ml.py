import connector
import json
from flask import Flask, request, jsonify
from datetime import datetime
import time
import random
import hashlib
import _thread
import psutil
import multiprocessing
from threading import Thread
import ctypes
from texttable import Texttable
import GPUtil
import nvidia_smi
from pynvml import *
app = Flask(__name__)
#from query_generator import *
import sys
import statistics
#from lb_ml import *
import pandas as pd
from sklearn import linear_model
from sklearn.model_selection import train_test_split
import numpy as np

#class queue_info(Structure):
#    _fields_ = [("queue_length", c_int),("min_load", c_int), ("max_load", c_int), ("avg_load", c_int), ("stdev", c_int)]

####BF ON-OFF####
BF = int(sys.argv[2])
####BF ON-OFF####
gpu_op = int(sys.argv[3])
global pu
global execution_time
type = 0



#pu = {"GPU":0, "CPU":0}
pu = [0, 0]
average_durations = 0
no_queries = 0

@app.route('/')
def health_check():
    return 'Healthy'

@app.route('/search', methods=['POST'])
def search():
    return jsonify(connector.names)

@app.route('/tag-keys', methods=['POST'])
def tag_keys():
    print("Ad-hoc asking for columns")
    #k = ["dont","forget","your","keys"]
    return jsonify(connector.names)

@app.route('/tag-values', methods=['POST'])
def tag_values():
    values = ["key", "operator", "value"]
    return jsonify(values)


def hashQuery(query):
    result = hashlib.sha256(query.encode())
    result = result.hexdigest()
    return result[0:8]


class ThreadWithReturnValue(Thread):
    def __init__(self, group=None, target=None, name=None,
                 args=(), kwargs={}, Verbose=None):
        Thread.__init__(self, group, target, name, args, kwargs)
        self._return = None
    def run(self):
        #print(type(self._target))
        if self._target is not None:
            self._return = self._target(*self._args,**self._kwargs)
    def join(self, *args):
        Thread.join(self, *args)
        return self._return

    
def monitor(target):
#    manager = multiprocessing.Manager()
#    return_dict = manager.dict()
    worker_process = mp.Process(target=target, args=(return_dict))
    worker_process.start()
    p = psutil.Process(worker_process.pid)

    # log cpu usage of `worker_process` every 10 ms
    cpu_percents = []
    while worker_process.is_alive():
        cpu_percents.append(p.cpu_percent())
        time.sleep(0.01)

    worker_process.join()
    return cpu_percents, return_dict["datarizer"], return_dict["durations"]


@app.route('/query', methods=['POST'])
def query():
    # SCENARIO
    query_list= [['fare'], ['dropoff_longitude', '511', 'fare', '20', 'extras', '1.5'], ['fare', '61'], ['pickup_latitude', '589', 'dropoff_latitude', '589'], ['dropoff_longitude'], ['dropoff_latitude', '857', 'fare', '20'], ['company', '80', 'fare', '25'], ['fare', '30'], ['company', '151', 'tips', '2'], ['company', '80', 'fare', '25'], ['fare', '50', 'trip_miles', '5.7'], ['tips', '30', 'extras', '8'], ['fare'], ['fare'], ['company', '151', 'tips', '2'], ['fare', '37'], ['pickup_latitude', '416'], ['fare', '39'], ['dropoff_latitude'], ['fare', '39'], ['tips', '30', 'extras', '8'], ['company'], ['pickup_latitude', '589', 'dropoff_latitude', '589'], ['dropoff_longitude', '100'], ['fare', '23'], ['company', '80', 'fare', '45'], ['pickup_latitude'], ['pickup_latitude'], ['pickup_latitude'], ['dropoff_longitude', '511', 'fare', '20', 'extras', '1.5'], ['dropoff_longitude', '511', 'fare', '20', 'extras', '1.5'], ['dropoff_longitude', '381', 'extras', '3', 'fare', '20'], ['dropoff_latitude', '484', 'fare', '50'], ['dropoff_longitude', '418', 'fare', '50', 'extras', '1.8'], ['dropoff_latitude', '857', 'fare', '20'], ['tips', '30', 'extras', '8'], ['fare', '50', 'company', '90'], ['fare', '60'], ['dropoff_longitude', '381', 'extras', '3', 'fare', '20'], ['fare', '37'], ['fare', '30'], ['fare', '28'], ['fare'], ['company', '80', 'fare', '25'], ['extras'], ['dropoff_longitude', '511', 'fare', '20', 'extras', '1.5'], ['fare', '30', 'tips', '7'], ['extras','1', 'fare', '25'], ['fare', '50'], ['fare', '39'], ['fare', '10', 'company', '95'], ['dropoff_latitude'], ['fare', '54'], ['fare', '10', 'company', '95'], ['company', '101'], ['tips', '1.7', 'dropoff_longitude', '601'], ['fare', '40', 'trip_miles', '5.9'], ['pickup_latitude', '416'], ['trip_miles', '5.3', 'tips', '5', 'fare', '25'], ['trip_miles', '1.3', 'tips', '2'], ['company', '101'], ['fare', '10', 'company', '95'], ['fare', '23'], ['fare', '39'], ['tips', '20', 'extras', '4'], ['dropoff_longitude', '511', 'fare', '20', 'extras', '1.5'], ['dropoff_latitude', '484', 'fare', '50'], ['fare', '37'], ['fare', '61'], ['fare', '51'], ['fare', '40'], ['dropoff_latitude', '857', 'fare', '20'], ['dropoff_longitude', '418', 'fare', '50', 'extras', '1.8'], ['fare', '40', 'trip_miles', '5.9'], ['company', '101'], ['pickup_latitude', '416'], ['trip_miles', '5', 'fare', '55'], ['trip_miles', '5', 'fare', '55'], ['fare'], ['company'], ['pickup_latitude', '744', 'dropoff_latitude', '580'], ['fare', '50', 'company', '90'], ['trip_miles', '0.9'], ['dropoff_latitude', '857', 'fare', '20'], ['tips', '20', 'extras', '4'], ['fare', '30'], ['tips', '20', 'extras', '4'], ['pickup_latitude', '589', 'dropoff_latitude', '589'], ['trip_miles', '1', 'fare', '35'],['extras', '3', 'fare', '25', 'tips', '1'], ['fare', '33'], ['fare', '40', 'trip_miles', '5.9'], ['fare'], ['dropoff_longitude', '511'], ['trip_miles', '1.3', 'tips', '2'], ['fare', '40', 'trip_miles', '5.9'],['fare', '50', 'tips', '9', 'company', '85'], ['fare', '50'], ['trip_miles'], ['trip_miles', '1.3', 'tips', '2'], ['fare', '61'], ['fare', '28'], ['fare'], ['trip_miles', '5', 'fare', '55'], ['tips', '30', 'extras', '8'], ['fare'], ['trip_miles', '5.3', 'tips', '5', 'fare', '25'], ['trip_miles', '5.3', 'tips', '5', 'fare', '25'], ['extras'], ['fare', '30', 'tips', '7'], ['fare', '54'], ['fare', '10', 'company', '95'], ['dropoff_latitude', '744', 'fare', '30'], ['fare', '50'], ['fare', '40', 'trip_miles', '5.9'], ['tips', '1.5', 'dropoff_longitude', '615'], ['fare', '61'], ['pickup_latitude', '416'], ['fare', '30', 'tips','7'], ['trip_miles', '5.3', 'tips', '5', 'fare', '25'], ['company', '101'], ['company'], ['dropoff_latitude', '484', 'fare', '50'], ['fare', '54'], ['dropoff_longitude', '418', 'fare', '50', 'extras', '1.8'], ['company', '80', 'fare', '45'], ['company', '101'], ['trip_miles', '5.3', 'tips', '5', 'fare', '25'], ['fare', '33'], ['fare', '40', 'trip_miles', '5.9'], ['fare', '60', 'tips', '20'], ['company', '80', 'fare', '25'], ['fare', '37'], ['company'], ['fare', '26'], ['company', '101'], ['fare', '50', 'tips', '9', 'company', '85'], ['fare'], ['fare', '30'], ['fare', '23'], ['company', '80', 'fare', '45'], ['company', '80', 'fare', '25'], ['trip_miles', '0.3', 'extras', '2', 'tips', '15'], ['dropoff_latitude', '484', 'fare', '50'], ['fare', '40', 'trip_miles', '5.9'], ['company', '101'], ['company', '80', 'fare', '25'], ['fare', '40'], ['dropoff_latitude', '484', 'fare', '50'], ['fare', '50', 'trip_miles', '5.7'], ['dropoff_longitude', '381', 'extras', '3', 'fare', '20'], ['extras', '3', 'fare', '25', 'tips', '1'], ['tips', '1.5', 'dropoff_longitude', '615'], ['dropoff_longitude', '200', 'extras', '2'], ['extras', '1', 'fare', '25'], ['pickup_latitude', '416'], ['fare'], ['fare', '50', 'tips', '9', 'company', '85'], ['trip_miles', '5', 'fare', '55'], ['fare', '39'], ['dropoff_longitude'], ['fare', '28'], ['dropoff_latitude', '744'], ['fare', '30', 'tips', '7'], ['tips', '1.5', 'dropoff_longitude', '615'], ['dropoff_longitude', '200', 'extras', '2'], ['fare', '30', 'tips', '7'], ['fare', '50'], ['extras', '3', 'fare', '25', 'tips', '1'], ['fare', '40'], ['fare', '26'], ['fare', '26'], ['fare', '40', 'trip_miles', '5.9'], ['fare'], ['fare', '28'], ['trip_miles', '1', 'fare', '35'], ['extras', '3', 'fare', '25', 'tips', '1'], ['trip_miles', '0.9'], ['fare', '30'], ['dropoff_longitude', '381', 'extras', '3', 'fare', '20'], ['trip_miles', '0.3', 'extras', '2', 'tips', '15'], ['tips', '30', 'extras', '8'], ['fare'], ['fare', '50', 'tips', '9', 'company', '85'], ['tips', '30', 'extras', '8'], ['dropoff_longitude', '418', 'fare', '50', 'extras', '1.8'], ['fare', '23'], ['fare'], ['fare'], ['company', '101'], ['dropoff_latitude', '744', 'fare', '30'], ['fare', '37'], ['fare'], ['fare', '40'], ['dropoff_longitude', '511'], ['fare', '37'], ['extras'], ['trip_miles', '5.3', 'tips', '5', 'fare', '25'], ['dropoff_longitude', '100'], ['trip_miles', '5', 'fare', '55'], ['fare', '60'], ['company', '80', 'fare', '25'], ['dropoff_longitude', '418', 'fare', '50', 'extras', '1.8'], ['dropoff_longitude', '200', 'extras', '2'], ['pickup_latitude', '589', 'dropoff_latitude', '589'], ['fare', '40', 'trip_miles', '5.9'], ['fare', '28'], ['fare', '24'], ['dropoff_latitude', '744', 'fare', '30'], ['dropoff_longitude', '418', 'fare', '50', 'extras', '1.8'], ['tips', '1.5', 'dropoff_longitude', '615'], ['fare', '24'], ['company', '80', 'fare', '45'], ['fare', '40', 'trip_miles', '5.9'], ['trip_miles', '1', 'fare', '35'], ['fare', '61'], ['extras'], ['fare'], ['tips', '1.5', 'dropoff_longitude', '615'], ['dropoff_longitude'], ['dropoff_longitude', '511', 'fare', '20', 'extras', '1.5'], ['dropoff_latitude', '744', 'fare', '30'], ['fare'], ['trip_miles'], ['fare', '50', 'tips', '9', 'company', '85'], ['tips', '1.7', 'dropoff_longitude', '601'], ['trip_miles', '1', 'fare', '35'], ['trip_miles', '5', 'fare', '55'], ['company', '80','fare', '45'], ['fare'], ['tips', '30', 'extras', '8'], ['fare', '51'], ['tips', '1.5', 'dropoff_longitude', '615'], ['fare', '40'], ['trip_miles'], ['company', '151', 'tips', '2'], ['dropoff_longitude', '200', 'extras', '2'], ['fare', '50', 'company', '90'], ['dropoff_longitude'], ['pickup_latitude', '416'], ['extras', '1', 'fare', '25'], ['company', '80', 'fare', '45'], ['dropoff_latitude', '857', 'fare', '20'], ['fare', '50', 'tips', '9', 'company', '85'], ['tips', '30', 'extras', '8'], ['trip_miles', '1', 'fare', '35'], ['trip_miles', '0.9'], ['trip_miles', '5', 'fare', '55'], ['dropoff_latitude', '857', 'fare', '20'], ['extras', '1', 'fare', '25'], ['dropoff_longitude', '511'], ['fare', '30'], ['fare', '37'], ['company'], ['fare', '40', 'trip_miles', '5.9'], ['extras', '3', 'fare', '25', 'tips', '1'], ['dropoff_longitude','381', 'extras', '3', 'fare', '20'], ['fare', '33'], ['pickup_latitude', '744'], ['company', '80', 'fare', '25'], ['dropoff_longitude', '418', 'fare', '50', 'extras', '1.8'], ['dropoff_latitude', '744'], ['fare', '40', 'trip_miles', '5.9'], ['pickup_latitude', '744'], ['fare', '30'], ['fare', '60'], ['pickup_latitude'], ['fare', '30', 'tips', '7'], ['company', '80', 'fare', '25'], ['fare', '60'], ['company', '151', 'tips', '2'], ['fare', '33'], ['fare', '33'], ['pickup_latitude', '744'], ['trip_miles', '1', 'fare', '35'], ['trip_miles'], ['pickup_latitude', '744', 'dropoff_latitude', '580'], ['dropoff_longitude', '200', 'extras', '2'], ['fare', '24'], ['extras', '1', 'fare', '25'], ['pickup_latitude', '416'], ['dropoff_longitude', '381', 'extras', '3', 'fare', '20'], ['fare', '54'], ['fare', '60', 'tips', '20'], ['tips', '1.7','dropoff_longitude', '601'], ['tips', '1.7', 'dropoff_longitude', '601'], ['company', '101'], ['fare', '50'], ['fare'], ['trip_miles', '0.9'], ['pickup_latitude', '416'], ['extras'], ['fare', '10', 'company', '95'], ['dropoff_latitude', '744'], ['fare', '23'], ['fare', '28'], ['fare', '30', 'tips', '7'], ['trip_miles', '0.3', 'extras', '2', 'tips', '15'], ['fare', '23'], ['tips', '20', 'extras', '4']]

    
    #query_list = []
    #query_list = query_gen()
    MAX = 0
    MIN = 100
    AVG = 0
    load_list = []
    total_query = 0

    ######## THREAD
    #thread_num = 16
    g_start = time.clock_gettime(1)
    ######## QUERY
    counter = 0
    
########################## DECISION TREE ###############################
    col_names = ['device','cpu_usage','gpu_usage','cpu_temp','gpu_temp','Q2','Q1','execution_time']
    Data= pd.read_csv("/home/anes/DOLAP/idp/GPU_/linear_regression.csv",header=0, names=col_names)
    feature_cols = ['device','cpu_usage','gpu_usage','cpu_temp','gpu_temp','Q2','Q1']
    X = Data[feature_cols]
    y = Data['execution_time']
    
    X_train, X_test, y_train, y_test, = train_test_split(X, y, test_size = 0.2, random_state=1)
    regr = linear_model.LinearRegression()
    regr.fit(X_train,y_train)
    coef_list = regr.coef_
    response_time_cpu = regr.intercept_
    response_time_gpu = regr.intercept_
########################################################################
    for query in query_list:
        counter += 1
        print("QUERY*******",counter)
        info_struct = connector.GET_QUEUE_INFO()
        final_info = ctypes.cast(info_struct, ctypes.POINTER(connector.queue_info))
        condition = final_info.contents.queue_length
        
        if(condition == 0 or condition != 0):
            adhoc_targets = []
            adhoc_keys = []
            adhoc_values = []
        
            for i in range(0,len(query),2):
                adhoc_key = query[i]
                adhoc_operator = "="
                if len(query) != 1:
                    adhoc_value = query[i+1]
                    adhoc_values.append(adhoc_value)
                    q_id = hashQuery(adhoc_key+adhoc_value)
                else:
                    adhoc_values.append("")
                    q_id = hashQuery(adhoc_key)
                    
                adhoc_keys.append(adhoc_key)

            start = time.clock_gettime(1)

            if(len(query)==1):
                parameter = 1
            elif len(query) == 2:
                parameter = 2
            else:
                parameter = 3
            try:
                    
                add_qry_thread = ThreadWithReturnValue(target=connector.INSERT_QUERY, args=(q_id.encode('utf-8').strip(), parameter, adhoc_keys, adhoc_values, ))
                add_qry_thread.start()
                add_qry_thread.join()

            except ValueError:
                print("problem with inserting query thread")
                
            try:

                info_thread = ThreadWithReturnValue(target=connector.GET_QUEUE_INFO, args=( ))
                #dec_start = time.clock_gettime(1)
                info_thread.start()
                info_struct = info_thread.join()
                #dec_finish = time.clock_gettime(1)
                #print("*************************** DEC TIME: ",float(dec_finish - dec_start),"************")
                final_info = ctypes.cast(info_struct, ctypes.POINTER(connector.queue_info))
                gpu_thread = ThreadWithReturnValue(target=connector.GET_GPU_INFO, args=( ))
                gpu_thread.start()
                gpu_struct = gpu_thread.join()
                gpu_info_s = ctypes.cast(gpu_struct, ctypes.POINTER(connector.gpu_info))
            except ValueError:
                print("problem with getting the queue info thread")

            cpu_usage = psutil.cpu_percent()
            cpu_temperature = psutil.sensors_temperatures()["coretemp"][0][1]
            gpu_usage = gpu_info_s.contents.gpu_usage
            gpu_temp = gpu_info_s.contents.gpu_temp
            #print(cpu_usage)
            #print(cpu_temperature)
            #print(gpu_usage)
            #print(gpu_temp)
            
            condition = final_info.contents.queue_length

        if((condition != 0 or condition == 0) and len(query)==1): ############ QUERY TYPE 1
            q_type = 1
            q_id = hashQuery(query[0])
            id_type = q_id +"-"+ str(q_type)
        
            try:
                
                info_thread = ThreadWithReturnValue(target=connector.GET_QUEUE_INFO, args=( ))
                #dec_start = time.clock_gettime(1)
                info_thread.start()
                info_struct = info_thread.join()
                #dec_finish = time.clock_gettime(1)
                #print("*************************** DEC TIME: ",float(dec_finish - dec_start),"************")
                final_info = ctypes.cast(info_struct, ctypes.POINTER(connector.queue_info))
                #q_length = final_info.contents.queue_length
                #q_min = final_info.contents.min_load
                #q_max = final_info.contents.max_load
                #q_avg = final_info.contents.avg_load
                #q_std = final_info.contents.stdev

                gpu_thread = ThreadWithReturnValue(target=connector.GET_GPU_INFO, args=( ))
                gpu_thread.start()
                gpu_struct = gpu_thread.join()
                gpu_info_s = ctypes.cast(gpu_struct, ctypes.POINTER(connector.gpu_info))

            except ValueError:
                print("problem with getting the queue info thread")

            call_rslt = connector.GET_NEXT_QUERY()
            next_query = ctypes.cast(call_rslt, ctypes.POINTER(connector.query_struct))
            c_cols = next_query.contents.columns

###################################################################################################################################################################
            if(counter==1):
                
                dev_decision = next_query.contents.decision
                
            else: 
                
                predict_array_cpu = par1_cpu + [0] + [1]
                predict_array_gpu = par1_gpu + [0] + [1]
                for i in range(1,len(coef_list)):
                    response_time_cpu += coef_list[i] * predict_array_cpu[i-1]
                    response_time_gpu += coef_list[i] * predict_array_gpu[i-1]
                response_gpu = response_time_gpu + (1*coef_list[0])
                print("resp. gpu:",response_gpu)
                print("resp. cpu:",response_time_cpu)
                if(response_gpu < response_time_cpu):
                    dev_decision = 3
                else:
                    dev_decision = 2
                response_time_cpu = regr.intercept_
                response_time_gpu = regr.intercept_
                
###################################################################################################################################################################

            if(dev_decision == 3):  # GPU
                start = time.clock_gettime(1)
                pu[0] += 1
                #print([pu[0]])
                #print("Scanning for column",query[0])
                manager = multiprocessing.Manager()
                return_dict = manager.dict()
                if (next_query.contents.query_type == 1):
                    query_process = multiprocessing.Process(target=connector.scan_gpu, args=(c_cols.decode(), connector.timeseries, return_dict))

                elif (next_query.contents.query_type == 2):
                    c_vals = next_query.contents.values
                    query_process = multiprocessing.Process(target=connector.scan_gpu_with_dependency, args=((c_cols.decode().split(", ")[0], c_cols.decode().split(", ")[0], c_vals.decode().split(", ")[0], connector.timeseries, return_dict)))

                elif (next_query.contents.query_type == 3):
                    c_vals = next_query.contents.values
                    if (next_query.contents.size == 2):
                        query_process = multiprocessing.Process(target=connector.scan_gpu_mult_dep, args=((c_cols.decode().split(", "), c_vals.decode().split(", "), connector.timeseries, return_dict)))
                    elif (next_query.contents.size == 3):
                        query_process = multiprocessing.Process(target=connector.scan_gpu_mult_dep2, args=((c_cols.decode().split(", "), c_vals.decode().split(", "), connector.timeseries, return_dict)))
                    elif (next_query.contents.size == 4):
                        query_process = multiprocessing.Process(target=connector.scan_gpu_mult_dep3, args=((c_cols.decode().split(", "), c_vals.decode().split(", "), connector.timeseries, return_dict)))

                query_process.start()

                p = psutil.Process(query_process.pid)
                cpu_percents = []

                while query_process.is_alive():
                    cpu_percents.append(p.cpu_percent()/psutil.cpu_count())
                    time.sleep(0.01)
                    
                query_process.join()
                cpu_perc = sum(cpu_percents)/len(cpu_percents)
                if cpu_perc < MIN:
                    MIN = cpu_perc
                if cpu_perc > MAX:
                    MAX = cpu_perc

                q_min = MIN
                q_max = MAX
                load_list.append(cpu_perc)
                q_avg = sum(load_list) / len(load_list)
                if len(load_list) > 1:
                    q_std = statistics.stdev(load_list)

                execution_time = time.clock_gettime(1) - start

                cpu_usage = psutil.cpu_percent()
                cpu_temperature = psutil.sensors_temperatures()["coretemp"][0][1]
                gpu_usage = gpu_info_s.contents.gpu_usage
                gpu_temp = gpu_info_s.contents.gpu_temp

            elif(dev_decision == 2):#CPU
                start = time.clock_gettime(1)
                pu[1] += 1

                manager = multiprocessing.Manager()
                return_dict = manager.dict()

                if (next_query.contents.query_type == 1):
                    query_process = multiprocessing.Process(target=connector.scan, args=(c_cols.decode(), connector.timeseries, return_dict))

                elif (next_query.contents.query_type == 2):
                    c_vals = next_query.contents.values
                    query_process = multiprocessing.Process(target=connector.scan_with_filter, args=((c_cols.decode().split(", ")[0], c_vals.decode().split(", ")[0], return_dict)))

                    #query_process = multiprocessing.Process(target=connector.scan_with_dependency, args=((c_cols.decode().split(", ")[0], c_cols.decode().split(", ")[0], c_vals.decode().split(", ")[0], connector.timeseries, return_dict)))
                elif (next_query.contents.query_type == 3):
                    c_vals = next_query.contents.values
                    query_process = multiprocessing.Process(target=connector.scan_with_dependencies, args=((c_cols.decode().split(", "), c_vals.decode().split(", "), connector.timeseries, return_dict)))
                
#                query_process = multiprocessing.Process(target=connector.scan, args=(query[0], connector.timeseries, return_dict))
                query_process.start()
                try:
                    p = psutil.Process(query_process.pid)
                    cpu_percents = []

                    while query_process.is_alive():
                        cpu_percents.append(p.cpu_percent()/psutil.cpu_count())
                        time.sleep(0.01)
                except psutil.NoSuchProcess:
                    print("Process has been deleted !")

                query_process.join()
                cpu_perc = sum(cpu_percents)/len(cpu_percents)

                if cpu_perc < MIN:
                    MIN = cpu_perc
                if cpu_perc > MAX:
                    MAX = cpu_perc

                q_min = MIN
                q_max = MAX

                load_list.append(cpu_perc)
                q_avg = sum(load_list) / len(load_list)
                if len(load_list) > 1:
                    q_std = statistics.stdev(load_list)

                cpu_usage = psutil.cpu_percent()
                cpu_temperature = psutil.sensors_temperatures()["coretemp"][0][1]
                gpu_usage = gpu_info_s.contents.gpu_usage
                gpu_temp = gpu_info_s.contents.gpu_temp

                res = []
                no_empty = 0
                execution_time = time.clock_gettime(1) - start
                        
        elif ((condition != 0 or condition == 0) and len(query) == 2):  ########### QUERY TYPE 2
            q_type = 2
            adhoc_targets = []
            adhoc_key = query[0]
            adhoc_operator = "="
            adhoc_value = query[1]
            q_id = hashQuery(adhoc_key+adhoc_value)
            
            start = time.clock_gettime(1)

            adhoc_keys = []
            adhoc_values = []
            adhoc_keys.append(adhoc_key)
            adhoc_values.append(adhoc_value)
        
            try:
                info_thread = ThreadWithReturnValue(target=connector.GET_QUEUE_INFO, args=( ))
                #dec_start = time.clock_gettime(1)
                info_thread.start()
                info_struct = info_thread.join()
                #dec_finish = time.clock_gettime(1)
                #print("*************************** DEC TIME: ",float(dec_finish - dec_start),"************")
                final_info = ctypes.cast(info_struct, ctypes.POINTER(connector.queue_info))
                #q_length = final_info.contents.queue_length
                #q_min = final_info.contents.min_load
                #q_max = final_info.contents.max_load
                #q_avg = final_info.contents.avg_load
                #q_std = final_info.contents.stdev

                gpu_thread = ThreadWithReturnValue(target=connector.GET_GPU_INFO, args=( ))
                gpu_thread.start()
                gpu_struct = gpu_thread.join()
                gpu_info_s = ctypes.cast(gpu_struct, ctypes.POINTER(connector.gpu_info))
                
            except ValueError:
                print("problem with getting the queue info thread")
        
            call_rslt = connector.GET_NEXT_QUERY()
            next_query = ctypes.cast(call_rslt, ctypes.POINTER(connector.query_struct))

###################################################################################################################################################################
            if(counter==1):
                
                dev_decision = next_query.contents.decision
                
            else:
                
                predict_array_cpu = par1_cpu + [1] + [0]
                predict_array_gpu = par1_gpu + [1] + [0]
                for i in range(1,len(coef_list)):
                    response_time_cpu += coef_list[i] * predict_array_cpu[i-1]
                    response_time_gpu += coef_list[i] * predict_array_gpu[i-1]
                response_gpu = response_time_gpu + (1*coef_list[0])
                print("resp. gpu:",response_gpu)
                print("resp. cpu:",response_time_cpu)
                if(response_gpu < response_time_cpu):
                    dev_decision = 3
                else:
                    dev_decision = 2
                response_time_cpu = regr.intercept_
                response_time_gpu = regr.intercept_
                
###################################################################################################################################################################

            c_cols = next_query.contents.columns
            c_vals = next_query.contents.values

            if(BF == 0 or BF == 3):
                if dev_decision == 2:
                    manager = multiprocessing.Manager()
                    return_dict = manager.dict()
                    
                    if (next_query.contents.query_type == 1):
                        query_process = multiprocessing.Process(target=connector.scan, args=(c_cols.decode(), connector.timeseries, return_dict))

                    elif (next_query.contents.query_type == 2):
                        c_vals = next_query.contents.values
                        query_process = multiprocessing.Process(target=connector.scan_with_filter, args=((c_cols.decode().split(", ")[0], c_vals.decode().split(", ")[0], return_dict)))
    #                    query_process = multiprocessing.Process(target=connector.scan_with_dependency, args=((c_cols.decode().split(", ")[0], c_cols.decode().split(", ")[0], c_vals.decode().split(", ")[0], connector.timeseries, return_dict)))

                    elif (next_query.contents.query_type == 3):
                        c_vals = next_query.contents.values
                        query_process = multiprocessing.Process(target=connector.scan_with_dependencies, args=((c_cols.decode().split(", "), c_vals.decode().split(", "), connector.timeseries, return_dict)))

#                    query_process = multiprocessing.Process(target=connector.scan_with_dependency, args=((c_cols.decode().split(", ")[0], c_cols.decode().split(", ")[0],  c_vals.decode().split(", ")[0], connector.timeseries, return_dict)))
                    query_process.start()
                    try:
                        p = psutil.Process(query_process.pid)
                        cpu_percents = []

                        while query_process.is_alive():
                            cpu_percents.append(p.cpu_percent()/psutil.cpu_count())
                            time.sleep(0.01)
                    except psutil.NoSuchProcess:
                        print("Process has been deleted!")
                        
                    query_process.join()

                    cpu_perc = sum(cpu_percents)/len(cpu_percents)

                    if cpu_perc < MIN:
                        MIN = cpu_perc
                    if cpu_perc > MAX:
                        MAX = cpu_perc

                    q_min = MIN
                    q_max = MAX
                    load_list.append(cpu_perc)
                    q_avg = sum(load_list) / len(load_list)
                    if len(load_list) > 1:
                        q_std = statistics.stdev(load_list)
                    
                    cpu_usage = psutil.cpu_percent()
                    cpu_temperature = psutil.sensors_temperatures()["coretemp"][0][1]
                    gpu_usage = gpu_info_s.contents.gpu_usage
                    gpu_temp = gpu_info_s.contents.gpu_temp

                elif dev_decision == 3:
                    manager = multiprocessing.Manager()
                    return_dict = manager.dict()

                    if (next_query.contents.query_type == 1):
                        query_process = multiprocessing.Process(target=connector.scan_gpu, args=(c_cols.decode(), connector.timeseries, return_dict))

                    elif (next_query.contents.query_type == 2):
                        c_vals = next_query.contents.values
                        query_process = multiprocessing.Process(target=connector.scan_gpu_with_dependency, args=((c_cols.decode().split(", ")[0], c_cols.decode().split(", ")[0], c_vals.decode().split(", ")[0], connector.timeseries, return_dict)))

                    elif (next_query.contents.query_type == 3):
                        c_vals = next_query.contents.values
                        if (next_query.contents.size == 2):
                            query_process = multiprocessing.Process(target=connector.scan_gpu_mult_dep, args=((c_cols.decode().split(", "), c_vals.decode().split(", "), connector.timeseries, return_dict)))
                        elif (next_query.contents.size == 3):
                            query_process = multiprocessing.Process(target=connector.scan_gpu_mult_dep2, args=((c_cols.decode().split(", "), c_vals.decode().split(", "), connector.timeseries, return_dict)))
                        elif (next_query.contents.size == 4):
                            query_process = multiprocessing.Process(target=connector.scan_gpu_mult_dep3, args=((c_cols.decode().split(", "), c_vals.decode().split(", "), connector.timeseries, return_dict)))
                        
#                        query_process = multiprocessing.Process(target=connector.scan_gpu_mult_dep2, args=((c_cols.decode().split(", "), c_vals.decode().split("\, "), connector.timeseries, return_dict)))

                    
#                    query_process = multiprocessing.Process(target=connector.scan_gpu_with_dependency, args=((c_cols.decode().split(", ")[0], c_cols.decode().split(", ")[0], c_vals.decode().split(", ")[0], connector.timeseries, return_dict)))

                    query_process.start()
                    try:
                        p = psutil.Process(query_process.pid)
                        cpu_percents = []

                        while query_process.is_alive():
                            cpu_percents.append(p.cpu_percent()/psutil.cpu_count())
                            time.sleep(0.01)
                    except psutil.NoSuchProcess:
                        print("Process has been deleted!")
                    
                    query_process.join()

                    cpu_perc = sum(cpu_percents)/len(cpu_percents)

                    if cpu_perc < MIN:
                        MIN = cpu_perc
                    if cpu_perc > MAX:
                        MAX = cpu_perc

                    q_min = MIN
                    q_max = MAX
                    load_list.append(cpu_perc)
                    q_avg = sum(load_list) / len(load_list)
                    if len(load_list) > 1:
                        q_std = statistics.stdev(load_list)

                    cpu_usage = psutil.cpu_percent()
                    cpu_temperature = psutil.sensors_temperatures()["coretemp"][0][1]
                    gpu_usage = gpu_info_s.contents.gpu_usage
                    gpu_temp = gpu_info_s.contents.gpu_temp

            elif(BF == 1):
                datarizer,durations = connector.scan_with_bf(c_cols.decode().split(", ")[0], c_cols.decode().split(", ")[0], c_vals.decode().split(", ")[0])
                cpu_usage = psutil.cpu_percent()
                cpu_temperature = psutil.sensors_temperatures()["coretemp"][0][1]
                gpu_usage = gpu_info_s.contents.gpu_usage
                gpu_temp = gpu_info_s.contents.gpu_temp
            elif(BF == 2):
                if dev_decision == 2:
                    manager = multiprocessing.Manager()
                    return_dict = manager.dict()

                    if (next_query.contents.query_type == 1):
                        query_process = multiprocessing.Process(target=connector.scan, args=(c_cols.decode(), connector.timeseries, return_dict))

                    elif (next_query.contents.query_type == 2):
                        c_vals = next_query.contents.values
                        query_process = multiprocessing.Process(target=connector.scan_with_filter, args=((c_cols.decode().split(", ")[0], c_vals.decode().split(", ")[0], return_dict)))

                    elif (next_query.contents.query_type == 3):
                        c_vals = next_query.contents.values
                        query_process = multiprocessing.Process(target=connector.scan_with_dependencies, args=((c_cols.decode().split(", "), c_vals.decode().split("\, "), connector.timeseries, return_dict)))


                    query_process.start()
                    try:
                        p = psutil.Process(query_process.pid)
                        cpu_percents = []

                        while query_process.is_alive():
                            cpu_percents.append(p.cpu_percent()/psutil.cpu_count())
                            time.sleep(0.01)
                    except psutil.NoSuchProcess:
                        print("Process has been deleted!")
                    
                    query_process.join()

                    cpu_perc = sum(cpu_percents)/len(cpu_percents)

                    if cpu_perc < MIN:
                        MIN = cpu_perc
                    if cpu_perc > MAX:
                        MAX = cpu_perc

                    q_min = MIN
                    q_max = MAX
                    load_list.append(cpu_perc)
                    q_avg = sum(load_list) / len(load_list)
                    if len(load_list) > 1:
                        q_std = statistics.stdev(load_list)

                    cpu_usage = psutil.cpu_percent()
                    cpu_temperature = psutil.sensors_temperatures()["coretemp"][0][1]
                    gpu_usage = gpu_info_s.contents.gpu_usage
                    gpu_temp = gpu_info_s.contents.gpu_temp

                elif dev_decision == 3:
                    manager = multiprocessing.Manager()
                    return_dict = manager.dict()

                    if (next_query.contents.query_type == 1):
                        query_process = multiprocessing.Process(target=connector.scan_gpu, args=(c_cols.decode(), connector.timeseries, return_dict))

                    elif (next_query.contents.query_type == 2):
                        c_vals = next_query.contents.values
                        query_process = multiprocessing.Process(target=connector.scan_gpu_with_dependency, args=((c_cols.decode().split(", ")[0], c_cols.decode().split(", ")[0], c_vals.decode().split(", ")[0], connector.timeseries, return_dict)))

                    elif (next_query.contents.query_type == 3):
                        c_vals = next_query.contents.values

                        if (next_query.contents.size == 2):
                            query_process = multiprocessing.Process(target=connector.scan_gpu_mult_dep, args=((c_cols.decode().split(", "), c_vals.decode().split(", "), connector.timeseries, return_dict)))
                        elif (next_query.contents.size == 3):
                            query_process = multiprocessing.Process(target=connector.scan_gpu_mult_dep2, args=((c_cols.decode().split(", "), c_vals.decode().split(", "), connector.timeseries, return_dict)))
                        elif (next_query.contents.size == 4):
                            query_process = multiprocessing.Process(target=connector.scan_gpu_mult_dep3, args=((c_cols.decode().split(", "), c_vals.decode().split(", "), connector.timeseries, return_dict)))
                        
                    query_process.start()
                    try:
                        p = psutil.Process(query_process.pid)
                        cpu_percents = []

                        while query_process.is_alive():
                            cpu_percents.append(p.cpu_percent()/psutil.cpu_count())
                            time.sleep(0.01)
                    except psutil.NoSuchProcess:
                        print("Process has been deleted!")
                    
                    query_process.join()

                    cpu_perc = sum(cpu_percents)/len(cpu_percents)

                    if cpu_perc < MIN:
                        MIN = cpu_perc
                    if cpu_perc > MAX:
                        MAX = cpu_perc


                    q_min = MIN
                    q_max = MAX
                    load_list.append(cpu_perc)
                    q_avg = sum(load_list) / len(load_list)
                    if len(load_list) > 1:
                        q_std = statistics.stdev(load_list)
                        
                    cpu_usage = psutil.cpu_percent()
                    cpu_temperature = psutil.sensors_temperatures()["coretemp"][0][1]
                    gpu_usage = gpu_info_s.contents.gpu_usage
                    gpu_temp = gpu_info_s.contents.gpu_temp

            global no_queries
            no_queries += 1
            global average_durations

            res = []
            no_empty = 0

            execution_time = time.clock_gettime(1) - start

            
        elif ((condition != 0 or condition == 0) and len(query) > 2): ######## QUERY TYPE 3
            q_type = 3
            adhoc_keys = []
            adhoc_values = []
            adhoc_ops = []
            adhoc_targets = []

            if (len(query) > 2):
                for i in range(0, len(query),2):
                    adhoc_keys.append(query[i])
                    adhoc_ops.append("=")
                    adhoc_values.append(query[i+1])
                    adhoc_targets.append(query[i])
            
            q_id = hashQuery(adhoc_keys[0]+adhoc_values[0])

            start = time.clock_gettime(1)
            
            try:
                info_thread = ThreadWithReturnValue(target=connector.GET_QUEUE_INFO, args=( ))
                
                #dec_start = time.clock_gettime(1)
                info_thread.start()
                info_struct = info_thread.join()
                #dec_finish = time.clock_gettime(1)
                #print("*************************** DEC TIME: ",float(dec_finish - dec_start),"************")
                final_info = ctypes.cast(info_struct, ctypes.POINTER(connector.queue_info))
                #q_length = final_info.contents.queue_length
                #q_min = final_info.contents.min_load
                #q_max = final_info.contents.max_load
                #q_avg = final_info.contents.avg_load
                #q_std = final_info.contents.stdev
                
                gpu_thread = ThreadWithReturnValue(target=connector.GET_GPU_INFO, args=( ))
                gpu_thread.start()
                gpu_struct = gpu_thread.join()
                gpu_info_s = ctypes.cast(gpu_struct, ctypes.POINTER(connector.gpu_info))
                
            except ValueError:
                print("problem with getting the queue info thread")

            call_rslt = connector.GET_NEXT_QUERY()
            next_query = ctypes.cast(call_rslt, ctypes.POINTER(connector.query_struct))
###################################################################################################################################################################
            if(counter==1):
                
                dev_decision = next_query.contents.decision
                
            else:
                
                predict_array_cpu = par1_cpu + [0] + [0]
                predict_array_gpu = par1_gpu + [0] + [0]
                for i in range(1,len(coef_list)):
                    response_time_cpu += coef_list[i] * predict_array_cpu[i-1]
                    response_time_gpu += coef_list[i] * predict_array_gpu[i-1]
                response_gpu = response_time_gpu + (1*coef_list[0])
                print("resp. gpu:",response_gpu)
                print("resp. cpu:",response_time_cpu)
                if(response_gpu < response_time_cpu):
                    dev_decision = 3
                else:
                    dev_decision = 2
                response_time_cpu = regr.intercept_
                response_time_gpu = regr.intercept_
                
###################################################################################################################################################################
            
            c_cols = next_query.contents.columns
            c_vals = next_query.contents.values

            adhoc_keys = c_cols.decode().split(", ")
            adhoc_values = c_vals.decode().split(", ")


            if(BF == 0):

                datarizer,durations = connector.scan_with_dependencies(adhoc_keys, adhoc_values, connector.timeseries)
                cpu_usage = psutil.cpu_percent()
                cpu_temperature = psutil.sensors_temperatures()["coretemp"][0][1]
                gpu_usage = gpu_info_s.contents.gpu_usage
                gpu_temp = gpu_info_s.contents.gpu_temp
                
            elif(BF == 2):

                if dev_decision == 2:

                    manager = multiprocessing.Manager()
                    return_dict = manager.dict()

                    if (next_query.contents.query_type == 1):
                        query_process = multiprocessing.Process(target=connector.scan, args=(c_cols.decode(), connector.timeseries, return_dict))
                        
                    elif (next_query.contents.query_type == 2):
                        c_vals = next_query.contents.values
                        query_process = multiprocessing.Process(target=connector.scan_with_filter, args=((c_cols.decode().split(", ")[0], c_vals.decode().split(", ")[0], return_dict)))

#                        query_process = multiprocessing.Process(target=connector.scan_with_dependency, args=((c_cols.decode().split(", ")[0], c_cols.decode().split(", ")[0], c_vals.decode().split(", ")[0], connector.timeseries, return_dict)))

                    elif (next_query.contents.query_type == 3):
                        c_vals = next_query.contents.values
                        query_process = multiprocessing.Process(target=connector.scan_with_dependencies, args=((c_cols.decode().split(", "), c_vals.decode().split(", "), connector.timeseries, return_dict)))
                        
#                        query_process = multiprocessing.Process(target=connector.scan_with_dependencies, args=((adhoc_keys, adhoc_values,  connector.timeseries, return_dict)))
                    query_process.start()
                    try:
                        p = psutil.Process(query_process.pid)
                        cpu_percents = []
                    
                        while query_process.is_alive():
                            cpu_percents.append(p.cpu_percent()/psutil.cpu_count())
                            time.sleep(0.01)
                    except psutil.NoSuchProcess:
                        print("Process has been deleted!")
                    
                    query_process.join()
                    cpu_perc = sum(cpu_percents)/len(cpu_percents)

                    if cpu_perc < MIN:
                        MIN = cpu_perc
                    if cpu_perc > MAX:
                        MAX = cpu_perc

                    q_min = MIN
                    q_max = MAX
                    load_list.append(cpu_perc)
                    q_avg = sum(load_list) / len(load_list)
                    if len(load_list) > 1:
                        q_std = statistics.stdev(load_list)

                    cpu_usage = psutil.cpu_percent()
                    cpu_temperature = psutil.sensors_temperatures()["coretemp"][0][1]
                    gpu_usage = gpu_info_s.contents.gpu_usage
                    gpu_temp = gpu_info_s.contents.gpu_temp

                elif dev_decision == 3:

                        manager = multiprocessing.Manager()
                        return_dict = manager.dict()

                        if (next_query.contents.query_type == 1):
                            query_process = multiprocessing.Process(target=connector.scan_gpu, args=(c_cols.decode(), connector.timeseries, return_dict))

                        elif (next_query.contents.query_type == 2):
                            c_vals = next_query.contents.values
                            query_process = multiprocessing.Process(target=connector.scan_gpu_with_dependency, args=((c_cols.decode().split(", ")[0], c_cols.decode().split(", ")[0], c_vals.decode().split(", ")[0], connector.timeseries, return_dict)))

                        elif (next_query.contents.query_type == 3):
                            c_vals = next_query.contents.values

                            if (next_query.contents.size == 2):
                                query_process = multiprocessing.Process(target=connector.scan_gpu_mult_dep, args=((c_cols.decode().split(", "), c_vals.decode().split(", "), connector.timeseries, return_dict)))
                            elif (next_query.contents.size == 3):
                                query_process = multiprocessing.Process(target=connector.scan_gpu_mult_dep2, args=((c_cols.decode().split(", "), c_vals.decode().split(", "), connector.timeseries, return_dict)))
                            elif (next_query.contents.size == 4):
                                query_process = multiprocessing.Process(target=connector.scan_gpu_mult_dep3, args=((c_cols.decode().split(", "), c_vals.decode().split(", "), connector.timeseries, return_dict)))

                        query_process.start()
                        try:
                            p = psutil.Process(query_process.pid)
                            cpu_percents = []
                        
                            while query_process.is_alive():
                                cpu_percents.append(p.cpu_percent()/psutil.cpu_count())
                                time.sleep(0.01)
                        except psutil.NoSuchProcess:
                            print("Process has been deleted!")
                        query_process.join()

                        cpu_perc = sum(cpu_percents)/len(cpu_percents)

                        if cpu_perc < MIN:
                            MIN = cpu_perc
                        if cpu_perc > MAX:
                            MAX = cpu_perc

                        q_min = MIN
                        q_max = MAX
                        load_list.append(cpu_perc)
                        q_avg = sum(load_list) / len(load_list)
                        if len(load_list) > 1:
                            q_std = statistics.stdev(load_list)

                        cpu_usage = psutil.cpu_percent()
                        cpu_temperature = psutil.sensors_temperatures()["coretemp"][0][1]
                        gpu_usage = gpu_info_s.contents.gpu_usage
                        gpu_temp = gpu_info_s.contents.gpu_temp

            elif(BF == 3):
                datarizer, durations = connector.scan_with_dep_bf(adhoc_keys, adhoc_values)
                cpu_usage = psutil.cpu_percent()
                cpu_temperature = psutil.sensors_temperatures()["coretemp"][0][1]
                gpu_usage = gpu_info_s.contents.gpu_usage
                gpu_temp = gpu_info_s.contents.gpu_temp

            no_queries += 1
    
            res = []
            no_empty = 0

            execution_time = time.clock_gettime(1) - start
   
######### WRITING DATA TO TXT FILES #############
        
        print("******EXECUTION TIME: ",execution_time)
        print("\n")
        #txt_data = str(dev_decision)+","+str(cpu_usage)+","+str(gpu_usage)+","+str(q_min)+","+str(q_max)+","+str(q_avg)+","+str(q_std)+","+str(q_type)+","+str(cpu_temperature)+","+str(gpu_temp)+"\n"
        
        par1_gpu = [0,gpu_usage,0,gpu_temp]
        par1_cpu = [cpu_usage,0,cpu_temperature,0]
                          
        txt_data = str(dev_decision)+","+str(cpu_usage)+","+str(gpu_usage)+","+str(cpu_temperature)+","+str(gpu_temp)+","+str(q_type)+","+str(execution_time)+"\n"
        
        f_lb = open("LR_ml.txt","a")
        f_lb.write(txt_data)
        f_lb.close()

        if(dev_decision==3):
            if(q_type == 1):
                txt_data2 = "1" +","+"0"+","+str(gpu_usage)+","+"0"+","+str(gpu_temp)+","+"0"+","+"1"+","+str(execution_time)+"\n"
            elif(q_type==2):
                txt_data2 = "1" +","+"0"+","+str(gpu_usage)+","+"0"+","+str(gpu_temp)+","+"1"+","+"0"+","+str(execution_time)+"\n"
            elif(q_type==3):
                txt_data2 = "1" +","+"0"+","+str(gpu_usage)+","+"0"+","+str(gpu_temp)+","+"0"+","+"0"+","+str(execution_time)+"\n"
        elif(dev_decision==2):
            if(q_type == 1):
                txt_data2 = "0" +","+str(cpu_usage)+","+"0"+","+str(cpu_temperature)+","+"0"+","+"0"+","+"1"+","+str(execution_time)+"\n"
            elif(q_type==2):
                txt_data2 = "0" +","+str(cpu_usage)+","+"0"+","+str(cpu_temperature)+","+"0"+","+"1"+","+"0"+","+str(execution_time)+"\n"
            elif(q_type==3):
                txt_data2 = "0" +","+str(cpu_usage)+","+"0"+","+str(cpu_temperature)+","+"0"+","+"0"+","+"0"+","+str(execution_time)+"\n"
        f_lb = open("linear_regression.txt","a")
        f_lb.write(txt_data2)
        f_lb.close()
        
        total_query += 1
        
    g_finish= time.clock_gettime(1) - g_start
    f_lb = open("LR_ml_test.txt","a")
    finish = str(g_finish) +" & "+str(total_query)+ "\n"
    f_lb.write(finish)
    f_lb.close()

@app.route('/annotations', methods=['POST'])
def annotations():
    req = request.get_json()
    data = [
        {
            "annotation": 'This is the annotation',
        "time": (convert_to_time_ms(req['range']['from']) +
                 convert_to_time_ms(req['range']['to'])) / 2,
            "title": 'Deployment notes',
            "tags": ['tag1', 'tag2'],
            "text": 'Hm, something went wrong...'
        }
    ]
    return jsonify(data)

if __name__ == '__main__':
    app.run(host='127.0.0.1', port=4001, debug=True)
