from texttable import Texttable
import statistics

txt_name = "cpu_test6.txt"

f = open(txt_name,"r")
line_list = f.readlines()
f.close()
    
thread_res = {}
thread_res["16_1"] = [0.0,0]
thread_res["16_2"] = [0.0,0]
thread_res["16_3"] = [0.0,0]


thread_16_list = []

for line in line_list:
    line.strip()
    if "," in line:
        split_list = line.split(",")
        if split_list[1].strip() == "Scan":        # Function Name (Query Type)
            if split_list[2].strip() == "16":
                thread_res["16_1"][0] += float(split_list[0])
                thread_res["16_1"][1] += 1
                thread_16_list.append(float(split_list[0]))                                    
        elif split_list[1].strip() == "Scan_with_bf":       # Query Type
            if split_list[2].strip() == "16":
                thread_res["16_2"][0] += float(split_list[0])
                thread_res["16_2"][1] += 1
                thread_16_list.append(float(split_list[0]))                    
                
        elif split_list[1].strip() == "Scan_with_dep_bf":       # Query Type     
            if split_list[2].strip() == "16":
                thread_res["16_3"][0] += float(split_list[0])
                thread_res["16_3"][1] += 1
                thread_16_list.append(float(split_list[0]))                    
                    
###################################### Q1 #################################################

try:
    cpu_q1_t16_avg = thread_res["16_1"][0] / thread_res["16_1"][1]
except ZeroDivisionError:
    cpu_q1_t16_avg = 0

###################################### Q2 #################################################
try:
    cpu_q2_t16_avg = thread_res["16_2"][0] / thread_res["16_2"][1]
except ZeroDivisionError:
    cpu_q2_t16_avg = 0

###################################### Q3 #################################################

try:
    cpu_q3_t16_avg = thread_res["16_3"][0] / thread_res["16_3"][1]
except ZeroDivisionError:
    cpu_q3_t16_avg = 0



thread_16_total = thread_res["16_1"][0] + thread_res["16_2"][0] + thread_res["16_3"][0]

thread_16_noq = thread_res["16_1"][1] + thread_res["16_2"][1] + thread_res["16_3"][1]

print("******************************* Resulst for",txt_name,"****************************************")    
t = Texttable()
t.add_rows([['Number of Threads','16'], ['Scan(Query Type 1)', cpu_q1_t16_avg], ['Scan_with_bf (Query Type 2)', cpu_q2_t16_avg],['Scan_with_dep_bf (Query Type 3)', cpu_q3_t16_avg],['Number of query', thread_16_noq],['Max',max(thread_16_list)],['Min',min(thread_16_list)],['Median',statistics.median(thread_16_list)],['Mean',statistics.mean(thread_16_list)]])
print(t.draw())
