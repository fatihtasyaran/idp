import connector
import json
from flask import Flask, request, jsonify
from datetime import datetime
import time
import random
import hashlib
import _thread
from threading import Thread
import ctypes


app = Flask(__name__)

import sys

#class queue_info(Structure):
#    _fields_ = [("queue_length", c_int),("min_load", c_int), ("max_load", c_int), ("avg_load", c_int), ("stdev", c_int)]

####BF ON-OFF####
BF = int(sys.argv[2])
####BF ON-OFF####
gpu_op = int(sys.argv[3])
global pu
global execution_time
type = 0

#pu = {"GPU":0, "CPU":0}
pu = [0, 0]
average_durations = 0
no_queries = 0

@app.route('/')
def health_check():
    return 'Healthy'

@app.route('/search', methods=['POST'])
def search():
    return jsonify(connector.names)

@app.route('/tag-keys', methods=['POST'])
def tag_keys():
    print("Ad-hoc asking for columns")
    #k = ["dont","forget","your","keys"]
    return jsonify(connector.names)

@app.route('/tag-values', methods=['POST'])
def tag_values():
    values = ["key", "operator", "value"]
    return jsonify(values)


def hashQuery(query):
    result = hashlib.sha256(query.encode())
    result = result.hexdigest()
    return result[0:8]


class ThreadWithReturnValue(Thread):
    def __init__(self, group=None, target=None, name=None,
                 args=(), kwargs={}, Verbose=None):
        Thread.__init__(self, group, target, name, args, kwargs)
        self._return = None
    def run(self):
        #print(type(self._target))
        if self._target is not None:
            self._return = self._target(*self._args,
                                                **self._kwargs)
    def join(self, *args):
        Thread.join(self, *args)
        return self._return

@app.route('/query', methods=['POST'])
def query():
    #gpu_op = random.randint(0,1)
   

    ####TAKE QUERY####
    r = request.get_json()
    ####TAKE QUERY####

    ####PRINT QUERY####
    print(json.dumps(r, indent=2))
    ##print("Target: ", r['targets'][0]['target'])
    ####PRINT QUERY####

    
    ####RESPONSE TIME####
    target = r['targets'][0]['target']
    if(target == "dB Response Time"):
        data = [
        {
            "target":r['targets'][0]['target'], 
            "datapoints":[[connector.return_duration(), time.mktime(datetime.now().timetuple())]]
        }
    ]
        return jsonify(data)
   
    #cols += 1
    ####RESPONSE TIME####
    #####ACCESSES####
    if(target == "block_accesses"):
        filename = sys.argv[1] + ".txt"
        connector.save_accesses(filename)
        exit(1)
    #####ACCESSES####

    
    ####QUERY####
    if (len(r["adhocFilters"]) == 1):
        
        #type = 1
        #connector.get_query_type(type)
        #if (gpu_op ==0):
        #adhoc_keys = []
        #adhoc_values = []
        #adhoc_ops = []
        adhoc_targets = []
        print(len(r["adhocFilters"]))
        
        adhoc_key = r["adhocFilters"][0]["key"]
        adhoc_operator = r["adhocFilters"][0]["operator"]
        adhoc_value = r["adhocFilters"][0]["value"]
        #if (len(r["adhocFilters"]) > 1):
            #adhoc_keys.append(adhoc_key)
            #adhoc_values.append(adhoc_value)
            #adhoc_ops.append(adhoc_operator)
        #    for i in range(0, len(r["adhocFilters"])):
        #        adhoc_keys.append(r["adhocFilters"][i]["key"])
        #        adhoc_ops.append(r["adhocFilters"][i]["operator"])
        #        adhoc_values.append(r["adhocFilters"][i]["value"])
        #    adhoc_targets.append(r['targets'][0]['target'])
            
        q_id = hashQuery(adhoc_key+adhoc_value)

        start = time.clock_gettime(1)

        print("Adhoc:" , "key->", adhoc_key, "operator->", adhoc_operator, "value->", adhoc_value)
        #print("Adhoc:" , "key->", adhoc_keys[1], "operator->", adhoc_ops[1], "value->", adhoc_values[1])

        adhoc_keys = []
        adhoc_values = []
        adhoc_keys.append(adhoc_key)
        adhoc_values.append(adhoc_value)
        
        # Insert a query to the queue
        print("JUST BEFORE INSERTING THE QUERY TO THE QUEUE")
        try:
            add_qry_thread = ThreadWithReturnValue(target=connector.INSERT_QUERY, args=(q_id.encode('utf-8').strip(), BF, adhoc_keys, adhoc_values, ))
            add_qry_thread.start()
            add_qry_thread.join()
#            _thread.start_new_thread(connector.INSERT_QUERY, (q_id.encode('utf-8').strip(), BF, adhoc_keys, adhoc_values, ))

        except ValueError:
            print("problem with inserting query thread")

        # Getting the info of the queue
        try:
            info_thread = ThreadWithReturnValue(target=connector.GET_QUEUE_INFO, args=( ))
#            info_thread = Thread(target=connector.GET_QUEUE_INFO, args=())

            info_thread.start()
            info_struct = info_thread.join()
            final_info = ctypes.cast(info_struct, ctypes.POINTER(connector.queue_info))
            print("The queue length in python : ", final_info.contents.queue_length)
            print("The queue's min load is: ", final_info.contents.min_load)
            print("The queue's max load is: ", final_info.contents.max_load)
            print("The queue's avg load is: ", final_info.contents.avg_load)
            print("The queue's stdev is: ", final_info.contents.stdev)
            #_thread.start_new_thread(connector.GET_QUEUE_INFO, ())
        except ValueError:
            print("problem with getting the queue info thread")
        #connector.INSERT_QUERY(q_id.encode('utf-8').strip(), BF, adhoc_keys, adhoc_values)            


        
        call_rslt = connector.GET_NEXT_QUERY()
        next_query = ctypes.cast(call_rslt, ctypes.POINTER(connector.query_struct))
        print("THE QUERY ID IS: ", (next_query.contents.query_id).decode())
        print("THE QUERY TYPE IS: ", next_query.contents.query_type)
        dev_decision = next_query.contents.decision
        print("THE DEVICE TO BE RUNNING IT IS: ", next_query.contents.decision)
        c_cols = next_query.contents.columns
        c_vals = next_query.contents.values
#        p_cols = [c_cols[i] for i in range(next_query.contents.size)]
        print("THE COLUMNS ARE: ", c_cols.decode())
        print("THE VALUES ARE: ", c_vals.decode())

        
        if(BF == 0 or BF == 3):
            if(len(r["adhocFilters"])==1):
                if dev_decision == 2:
                    datarizer, durations = connector.scan_with_dependency(c_cols.decode(), c_cols.decode(), c_vals.decode(), connector.timeseries)
#                    datarizer, durations = connector.scan_with_dependency(r['targets'][0]['target'], adhoc_key, adhoc_value, connector.timeseries)
                elif dev_decision == 3:
                    datarizer,durations = connector.scan_gpu_with_dependency(c_cols.decode(), c_cols.decode(),  c_vals.decode(), connector.timeseries)
        elif(BF == 1):
            datarizer,durations = connector.scan_with_bf(c_cols.decode(), c_cols.decode(), c_vals.decode())
        elif(BF == 2):
            if(len(r["adhocFilters"])==1):
                if dev_decision == 2:
                    datarizer, durations = connector.scan_with_dependency(c_cols.decode(), c_cols.decode(), c_vals.decode(), connector.timeseries)
#                    datarizer, durations = connector.scan_with_dependency(r['targets'][0]['target'], adhoc_key, adhoc_value, connector.timeseries)
                elif dev_decision == 3:
                    datarizer,durations = connector.scan_gpu_with_dependency(c_cols.decode(), c_cols.decode(),  c_vals.decode(), connector.timeseries)
#                datarizer,durations = connector.scan_gpu_with_dependency(r['targets'][0]['target'], adhoc_key, adhoc_value, connector.timeseries)
            
            elif(len(r["adhocFilters"])==2):
        #elif(BF == 3):
                adhoc_keys = c_cols.decode().split(",").strip()
                adhoc_values = c_vals.decode().split(",").strip()
                
                if dev_decision == 2:
                    datarizer, durations = connector.scan_with_dependencies(adhoc_keys, adhoc_values, connector.timeseries)
                elif dev_decision == 3:
                    datarizer, durations = connector.scan_gpu_mult_dep(adhoc_keys, adhoc_values, connector.timeseries)

#datarizer, durations = connector.scan_gpu_mult_dep(adhoc_keys, adhoc_values, connector.timeseries)

            elif(len(r["adhocFilters"])==3):
        #elif(BF == 4):
                datarizer, durations = connector.scan_gpu_mult_dep2(adhoc_keys, adhoc_values, connector.timeseries)
            elif(len(r["adhocFilters"])==4):
        #elif(BF == 5):
                datarizer, durations = connector.scan_gpu_mult_dep3(adhoc_keys, adhoc_values, connector.timeseries)
        #elif(BF == 3):
        #    datarizer, durations = connector.scan_with_dep_bf(adhoc_key, adhoc_value)

        global no_queries
        no_queries += 1
        global average_durations
        average_durations += durations
        datarizer = datarizer[:40000]


        #average_durations /= no_queries

        res = []
        no_empty = 0
        print(len(datarizer))
        for i in range(0, len(datarizer), 2):
            if(str(datarizer[i].decode("utf-8", errors="ignore").strip()) == ''):
                no_empty += 1
            elif(str(datarizer[i+1].decode("utf-8", errors="ignore").strip()) == ''):
                no_empty += 1
            else:
                #if()
                dt = datetime.strptime(str(datarizer[i+1].decode("utf-8",errors="ignore").strip()), '%Y-%m-%d %H:%M:%S')
                res.append([float(str(datarizer[i].decode("utf-8",errors="ignore").strip())), time.mktime(dt.timetuple())*1000])
                #dt = datetime.strptime(str(datarizer[i+1].decode("utf-8")), '%Y-%m-%d %H:%M:%S')
                #res.append([float(str(datarizer[i].decode("utf-8"))), time.mktime(dt.timetuple())*1000])


        print("Length of res: ", len(res))

        print("NO_EMPTY: ", no_empty)
        #print(res)

        data = [
            {
                "target":r['targets'][0]['target'], 
                "datapoints":res
            }
        ]
        execution_time = time.clock_gettime(1) - start

        print("AVERAGE DURATION: ", average_durations/no_queries)
        if(BF == 2 and len(r["adhocFilters"]) == 1):
            write_lb = str(execution_time) +", " + str(q_id) +", " + "GPU" +", "+ "2"+ "\n"
            f_lb = open("exectime.txt","a")
            f_lb.write(write_lb)
            f_lb.close()
        elif(len(r["adhocFilters"]) > 1):
            write_lb = str(execution_time) +", " + str(q_id) +", " + "GPU" +", "+ "3"+ "\n"
            f_lb = open("exectime.txt","a")
            f_lb.write(write_lb)
            f_lb.close()
        else:
            write_lb = str(execution_time) +", " + str(q_id) +", " + "CPU" +", "+ "2"+ "\n"
            f_lb = open("exectime.txt","a")
            f_lb.write(write_lb)
            f_lb.close() 

        return jsonify(data)
        
    ####QUERY####

    
    ####SCAN####
    elif (len(r["adhocFilters"]) > 1):
        #type = 1
        #connector.get_query_type(type)
        #if (gpu_op ==0):
        adhoc_keys = []
        adhoc_values = []
        adhoc_ops = []
        adhoc_targets = []
        print(len(r["adhocFilters"]))
        
        #adhoc_key = r["adhocFilters"][0]["key"]
        #adhoc_operator = r["adhocFilters"][0]["operator"]
        #adhoc_value = r["adhocFilters"][0]["value"]
        if (len(r["adhocFilters"]) > 1):
            #adhoc_keys.append(adhoc_key)
            #adhoc_values.append(adhoc_value)
            #adhoc_ops.append(adhoc_operator)
            for i in range(0, len(r["adhocFilters"])):
                adhoc_keys.append(r["adhocFilters"][i]["key"])
                adhoc_ops.append(r["adhocFilters"][i]["operator"])
                adhoc_values.append(r["adhocFilters"][i]["value"])
            adhoc_targets.append(r['targets'][0]['target'])
            
        q_id = hashQuery(adhoc_keys[0]+adhoc_values[0])

        start = time.clock_gettime(1)

        #print("Adhoc:" , "key->", adhoc_key, "operator->", adhoc_operator, "value->", adhoc_value)
        print("Adhoc:" , "key->", adhoc_keys[len(r["adhocFilters"])-1], "operator->", adhoc_ops[1], "value->", adhoc_values[len(r["adhocFilters"])-1])

        # Insert a query to the queue
        print("JUST BEFORE INSERTING THE QUERY TO THE QUEUE")
        try:
            add_qry_thread = ThreadWithReturnValue(target=connector.INSERT_QUERY, args=(q_id.encode('utf-8').strip(), 2, adhoc_keys, adhoc_values, ))
            add_qry_thread.start()
            add_qry_thread.join()
#            _thread.start_new_thread(connector.INSERT_QUERY, (q_id.encode('utf-8').strip(), BF, adhoc_keys, adhoc_values, ))

        except ValueError:
            print("problem with inserting query thread")

        # Getting the info of the queue
        try:
            info_thread = ThreadWithReturnValue(target=connector.GET_QUEUE_INFO, args=( ))
#            info_thread = Thread(target=connector.GET_QUEUE_INFO, args=())

            info_thread.start()
            info_struct = info_thread.join()
            final_info = ctypes.cast(info_struct, ctypes.POINTER(connector.queue_info))
            print("The queue length in python : ", final_info.contents.queue_length)
            print("The queue's min load is: ", final_info.contents.min_load)
            print("The queue's max load is: ", final_info.contents.max_load)
            print("The queue's avg load is: ", final_info.contents.avg_load)
            print("The queue's stdev is: ", final_info.contents.stdev)
            #_thread.start_new_thread(connector.GET_QUEUE_INFO, ())
        except ValueError:
            print("problem with getting the queue info thread")

        call_rslt = connector.GET_NEXT_QUERY()
        next_query = ctypes.cast(call_rslt, ctypes.POINTER(connector.query_struct))
        print("THE QUERY ID IS: ", (next_query.contents.query_id).decode())
        print("THE QUERY TYPE IS: ", next_query.contents.query_type)
        dev_decision = next_query.contents.decision
        print("THE DEVICE TO BE RUNNING IT IS: ", next_query.contents.decision)
        c_cols = next_query.contents.columns
        c_vals = next_query.contents.values
#        p_cols = [c_cols[i] for i in range(next_query.contents.size)]
        print("THE COLUMNS ARE: ", c_cols.decode())
        print("THE VALUES ARE: ", c_vals.decode())
        adhoc_keys = c_cols.decode().split(",")
        adhoc_values = c_vals.decode().split(",")
        print("adhoc_keys: ", adhoc_keys)
        print("adhoc_values: ", adhoc_values)

        if(BF == 0):
            #if(len(r["adhocFilters"]) == 1):
            #    datarizer,durations = connector.scan_with_dependency(r['targets'][0]['target'], adhoc_key, adhoc_value)
            if(len(r["adhocFilters"]) > 1):
                print("In CPU query T2")
                datarizer,durations = connector.scan_with_dependencies(adhoc_keys, adhoc_values, connector.timeseries)
        elif(BF == 2):
            if(len(r["adhocFilters"])==2):
                #elif(BF == 3):
#                adhoc_keys = c_cols.decode().split(",").strip()
#                adhoc_values = c_vals.decode().split(",").strip()
                if dev_decision == 2:
                    print("IN CPU 2")
                    datarizer,durations = connector.scan_with_dependencies(adhoc_keys, adhoc_values, connector.timeseries)
                elif dev_decision == 3:
                    print("IN GPU 3")
                    datarizer, durations = connector.scan_gpu_mult_dep(adhoc_keys, adhoc_values, connector.timeseries)

            elif(len(r["adhocFilters"])==3):
                if dev_decision == 2:
                    datarizer,durations = connector.scan_with_dependencies(adhoc_keys, adhoc_values, connector.timeseries)
                elif dev_decision == 3:
                    datarizer, durations = connector.scan_gpu_mult_dep2(adhoc_keys, adhoc_values, connector.timeseries)
                #elif(BF == 4):
#                datarizer, durations = connector.scan_gpu_mult_dep2(adhoc_keys, adhoc_values, connector.timeseries)
            elif(len(r["adhocFilters"])==4):
                if dev_decision == 2:
                    datarizer,durations = connector.scan_with_dependencies(adhoc_keys, adhoc_values, connector.timeseries)
                elif dev_decision == 3:
                    datarizer, durations = connector.scan_gpu_mult_dep3(adhoc_keys, adhoc_values, connector.timeseries)
                #elif(BF == 5):
#                datarizer, durations = connector.scan_gpu_mult_dep3(adhoc_keys, adhoc_values, connector.timeseries)
            elif(len(r["adhocFilters"])==5):
                if dev_decision == 2:
                    datarizer,durations = connector.scan_with_dependencies(adhoc_keys, adhoc_values, connector.timeseries)
                elif dev_decision == 3:
                    datarizer, durations = connector.scan_gpu_mult_dep4(adhoc_keys, adhoc_values, connector.timeseries)
                #elif(BF == 5):
#                datarizer, durations = connector.scan_gpu_mult_dep4(adhoc_keys, adhoc_values, connector.timeseries)
        elif(BF == 3):
            datarizer, durations = connector.scan_with_dep_bf(adhoc_keys, adhoc_values)
            print("In the right place !!")


#        if(BF == 0):
#            datarizer,durations = connector.scan_with_dependency(r['targets'][0]['target'], adhoc_key, adhoc_value)
#        elif(BF == 1):
#            datarizer,durations = connector.scan_with_bf(r['targets'][0]['target'], adhoc_key, adhoc_value)
#        elif(BF == 2):
#            if(len(r["adhocFilters"])==1):
#                datarizer,durations = connector.scan_gpu_with_dependency(r['targets'][0]['target'], adhoc_key, adhoc_value, connector.timeseries)
            

        #global no_queries
        no_queries += 1
        #global average_durations
        average_durations += durations
        #average_durations /= no_queries
    
        res = []
        no_empty = 0
        print(len(datarizer))
        for i in range(0, len(datarizer), 2):
            if(str(datarizer[i].decode("utf-8", errors="ignore")) == ""):
                no_empty += 1
            elif(str(datarizer[i+1].decode("utf-8", errors="ignore")) == ''):
                no_empty += 1
            else:
                dt = datetime.strptime(str(datarizer[i+1].decode("utf-8")), '%Y-%m-%d %H:%M:%S')
                res.append([float(str(datarizer[i].decode("utf-8"))), time.mktime(dt.timetuple())*1000])

#        for i in range(0, len(datarizer), 2):
            ##print(datarizer[i+1])
#            if(str(datarizer[i].decode("utf-8", errors="ignore").strip()) == ''):
#                no_empty += 1
#            elif(str(datarizer[i+1].decode("utf-8", errors="ignore").strip()) == ''):
#                no_empty += 1
#            else:
         #       #if()
#                dt = datetime.strptime(str(datarizer[i+1].decode("utf-8",errors="ignore").strip()), '%Y-%m-%d %H:%M:%S')
#                res.append([float(str(datarizer[i].decode("utf-8",errors="ignore").strip())), time.mktime(dt.timetuple())*1000])
      #          #dt = datetime.strptime(str(datarizer[i+1].decode("utf-8")), '%Y-%m-%d %H:%M:%S')
      #          #res.append([float(str(datarizer[i].decode("utf-8"))), time.mktime(dt.timetuple())*1000])


        print("Length of res: ", len(res))

        print("NO_EMPTY: ", no_empty)
        #print(res)

        data = [
            {
                "target":r['targets'][0]['target'], 
                "datapoints":res
            }
        ]
        execution_time = time.clock_gettime(1) - start

        print("AVERAGE DURATION: ", average_durations/no_queries)
        if(BF == 2 and len(r["adhocFilters"]) == 1):
            write_lb = str(execution_time) +", " + str(q_id) +", " + "GPU" +", "+ "2"+ "\n"
            f_lb = open("exectime.txt","a")
            f_lb.write(write_lb)
            f_lb.close()
        elif(len(r["adhocFilters"]) > 1):
            write_lb = str(execution_time) +", " + str(q_id) +", " + "GPU" +", "+ "3"+ "\n"
            f_lb = open("exectime.txt","a")
            f_lb.write(write_lb)
            f_lb.close()
        else:
            if(BF == 3):
                write_lb = str(execution_time) +", " + str(q_id) +", " + "CPU" +", "+ "3"+ "\n"
                f_lb = open("exectime.txt","a")
                f_lb.write(write_lb)
                f_lb.close()
            else:
                write_lb = str(execution_time) +", " + str(q_id) +", " + "CPU" +", "+ "2"+ "\n"
                f_lb = open("exectime.txt","a")
                f_lb.write(write_lb)
                f_lb.close()
                
        return jsonify(data)
        

    elif (len(r["adhocFilters"]) > 2):
        #type = 1
        #connector.get_query_type(type)
        #if (gpu_op ==0):
        adhoc_keys = []
        adhoc_values = []
        adhoc_ops = []
        adhoc_targets = []
        print(len(r["adhocFilters"]))
        
        adhoc_key = r["adhocFilters"][0]["key"]
        adhoc_operator = r["adhocFilters"][0]["operator"]
        adhoc_value = r["adhocFilters"][0]["value"]
        if (len(r["adhocFilters"]) > 1):
            #adhoc_keys.append(adhoc_key)
            #adhoc_values.append(adhoc_value)
            #adhoc_ops.append(adhoc_operator)
            for i in range(0, len(r["adhocFilters"])):
                adhoc_keys.append(r["adhocFilters"][i]["key"])
                adhoc_ops.append(r["adhocFilters"][i]["operator"])
                adhoc_values.append(r["adhocFilters"][i]["value"])
            adhoc_targets.append(r['targets'][0]['target'])
            
        q_id = hashQuery(adhoc_key+adhoc_value)

        start = time.clock_gettime(1)

        print("Adhoc:" , "key->", adhoc_key, "operator->", adhoc_operator, "value->", adhoc_value)
        print("Adhoc:" , "key->", adhoc_keys[1], "operator->", adhoc_ops[1], "value->", adhoc_values[1])

        # Insert a query to the queue
        print("JUST BEFORE INSERTING THE QUERY TO THE QUEUE")
        try:
            add_qry_thread = ThreadWithReturnValue(target=connector.INSERT_QUERY, args=(q_id.encode('utf-8').strip(), BF, adhoc_keys, adhoc_values, ))
            add_qry_thread.start()
            add_qry_thread.join()
#            _thread.start_new_thread(connector.INSERT_QUERY, (q_id.encode('utf-8').strip(), BF, adhoc_keys, adhoc_values, ))

        except ValueError:
            print("problem with inserting query thread")

        # Getting the info of the queue
        try:
            info_thread = ThreadWithReturnValue(target=connector.GET_QUEUE_INFO, args=( ))
#            info_thread = Thread(target=connector.GET_QUEUE_INFO, args=())

            info_thread.start()
            info_struct = info_thread.join()
            final_info = ctypes.cast(info_struct, ctypes.POINTER(connector.queue_info))
            print("The queue length in python : ", final_info.contents.queue_length)
            print("The queue's min load is: ", final_info.contents.min_load)
            print("The queue's max load is: ", final_info.contents.max_load)
            print("The queue's avg load is: ", final_info.contents.avg_load)
            print("The queue's stdev is: ", final_info.contents.stdev)
            #_thread.start_new_thread(connector.GET_QUEUE_INFO, ())
        except ValueError:
            print("problem with getting the queue info thread")

        call_rslt = connector.GET_NEXT_QUERY()
        next_query = ctypes.cast(call_rslt, ctypes.POINTER(connector.query_struct))
        print("THE QUERY ID IS: ", (next_query.contents.query_id).decode())
        print("THE QUERY TYPE IS: ", next_query.contents.query_type)
        dev_decision = next_query.contents.decision
        print("THE DEVICE TO BE RUNNING IT IS: ", next_query.contents.decision)
        c_cols = next_query.contents.columns
        c_vals = next_query.contents.values
#        p_cols = [c_cols[i] for i in range(next_query.contents.size)]
        print("THE COLUMNS ARE: ", c_cols.decode())
        print("THE VALUES ARE: ", c_vals.decode())
        adhoc_keys = c_cols.decode().split(",")
        adhoc_values = c_vals.decode().split(",")



        if(BF == 0):
            datarizer,durations = connector.scan_with_dependency(c_cols.decode(), c_cols.decode(), c_vals.decode(), connector.timeseries)
        elif(BF == 1):
            datarizer,durations = connector.scan_with_bf(c_cols.decode(), c_cols.decode(), c_vals.decode())
        elif(BF == 2):
            if(len(r["adhocFilters"])==1):
                if dev_decision == 2:
                    datarizer,durations = connector.scan_with_dependency(c_cols.decode(), c_cols.decode(), c_vals.decode(), connector.timeseries)
                elif dev_decision == 3:
                    datarizer,durations = connector.scan_gpu_with_dependency(c_cols.decode(), c_cols.decode(), c_vals.decode(), connector.timeseries)
                    
            elif(len(r["adhocFilters"])==2):
                if dev_decision == 2:
                     datarizer,durations = connector.scan_with_dependency(c_cols.decode(), c_cols.decode(), c_vals.decode(), connector.timeseries)
                elif dev_decision == 3:
                     datarizer,durations = connector.scan_gpu_mult_dep(adhoc_keys, adhoc_values, connector.timeseries)
        #elif(BF == 3):
               
            elif(len(r["adhocFilters"])==3):
                if dev_decision == 2:
                    datarizer,durations = connector.scan_with_dependency(c_cols.decode(), c_cols.decode(), c_vals.decode(), connector.timeseries)
                elif dev_decision == 3:
                    datarizer, durations = connector.scan_gpu_mult_dep2(adhoc_keys, adhoc_values, connector.timeseries)
            elif(len(r["adhocFilters"])==4):
                if dev_decision == 2:
                    datarizer,durations = connector.scan_with_dependency(c_cols.decode(), c_cols.decode(), c_vals.decode(), connector.timeseries)
                elif dev_decision == 3:
                    datarizer, durations = connector.scan_gpu_mult_dep3(adhoc_keys, adhoc_values, connector.timeseries)

        #global no_queries
        no_queries += 1
        #global average_durations
        average_durations += durations
        #average_durations /= no_queries
    
        res = []
        no_empty = 0
        print(len(datarizer))
        for i in range(0, len(datarizer), 2):
            if(str(datarizer[i].decode("utf-8", errors="ignore").strip()) == ''):
                no_empty += 1
            elif(str(datarizer[i+1].decode("utf-8", errors="ignore").strip()) == ''):
                no_empty += 1
            else:
                #if()
                dt = datetime.strptime(str(datarizer[i+1].decode("utf-8",errors="ignore").strip()), '%Y-%m-%d %H:%M:%S')
                res.append([float(str(datarizer[i].decode("utf-8",errors="ignore").strip())), time.mktime(dt.timetuple())*1000])
                #dt = datetime.strptime(str(datarizer[i+1].decode("utf-8")), '%Y-%m-%d %H:%M:%S')
                #res.append([float(str(datarizer[i].decode("utf-8"))), time.mktime(dt.timetuple())*1000])


        print("Length of res: ", len(res))

        print("NO_EMPTY: ", no_empty)
        #print(res)

        data = [
            {
                "target":r['targets'][0]['target'], 
                "datapoints":res
            }
        ]
        execution_time = time.clock_gettime(1) - start

        print("AVERAGE DURATION: ", average_durations/no_queries)
        if(BF == 2 and len(r["adhocFilters"]) == 1):
            write_lb = str(execution_time) +", " + str(q_id) +", " + "GPU" +", "+ "2"+ "\n"
            f_lb = open("exectime.txt","a")
            f_lb.write(write_lb)
            f_lb.close()
        elif(len(r["adhocFilters"]) > 1):
            write_lb = str(execution_time) +", " + str(q_id) +", " + "GPU" +", "+ "3"+ "\n"
            f_lb = open("exectime.txt","a")
            f_lb.write(write_lb)
            f_lb.close()
        else:
            write_lb = str(execution_time) +", " + str(q_id) +", " + "CPU" +", "+ "2"+ "\n"
            f_lb = open("exectime.txt","a")
            f_lb.write(write_lb)
            f_lb.close() 

        return jsonify(data)

    elif (len(r["adhocFilters"]) > 3):
        #type = 1
        #connector.get_query_type(type)
        #if (gpu_op ==0):
        adhoc_keys = []
        adhoc_values = []
        adhoc_ops = []
        adhoc_targets = []
        print(len(r["adhocFilters"]))
        
        adhoc_key = r["adhocFilters"][0]["key"]
        adhoc_operator = r["adhocFilters"][0]["operator"]
        adhoc_value = r["adhocFilters"][0]["value"]
        if (len(r["adhocFilters"]) > 1):
            #adhoc_keys.append(adhoc_key)
            #adhoc_values.append(adhoc_value)
            #adhoc_ops.append(adhoc_operator)
            for i in range(0, len(r["adhocFilters"])):
                adhoc_keys.append(r["adhocFilters"][i]["key"])
                adhoc_ops.append(r["adhocFilters"][i]["operator"])
                adhoc_values.append(r["adhocFilters"][i]["value"])
            adhoc_targets.append(r['targets'][0]['target'])
            
        q_id = hashQuery(adhoc_key+adhoc_value)

        start = time.clock_gettime(1)

        print("Adhoc:" , "key->", adhoc_key, "operator->", adhoc_operator, "value->", adhoc_value)
        print("Adhoc:" , "key->", adhoc_keys[1], "operator->", adhoc_ops[1], "value->", adhoc_values[1])

        # Insert a query to the queue
        print("JUST BEFORE INSERTING THE QUERY TO THE QUEUE")
        try:
            add_qry_thread = ThreadWithReturnValue(target=connector.INSERT_QUERY, args=(q_id.encode('utf-8').strip(), BF, adhoc_keys, adhoc_values, ))
            add_qry_thread.start()
            add_qry_thread.join()
#            _thread.start_new_thread(connector.INSERT_QUERY, (q_id.encode('utf-8').strip(), BF, adhoc_keys, adhoc_values, ))

        except ValueError:
            print("problem with inserting query thread")

        # Getting the info of the queue
        try:
            info_thread = ThreadWithReturnValue(target=connector.GET_QUEUE_INFO, args=( ))
#            info_thread = Thread(target=connector.GET_QUEUE_INFO, args=())

            info_thread.start()
            info_struct = info_thread.join()
            final_info = ctypes.cast(info_struct, ctypes.POINTER(connector.queue_info))
            print("The queue length in python : ", final_info.contents.queue_length)
            print("The queue's min load is: ", final_info.contents.min_load)
            print("The queue's max load is: ", final_info.contents.max_load)
            print("The queue's avg load is: ", final_info.contents.avg_load)
            print("The queue's stdev is: ", final_info.contents.stdev)
            #_thread.start_new_thread(connector.GET_QUEUE_INFO, ())
        except ValueError:
            print("problem with getting the queue info thread")

        call_rslt = connector.GET_NEXT_QUERY()
        next_query = ctypes.cast(call_rslt, ctypes.POINTER(connector.query_struct))
        print("THE QUERY ID IS: ", (next_query.contents.query_id).decode())
        print("THE QUERY TYPE IS: ", next_query.contents.query_type)
        dev_decision = next_query.contents.decision
        print("THE DEVICE TO BE RUNNING IT IS: ", next_query.contents.decision)
        c_cols = next_query.contents.columns
        c_vals = next_query.contents.values
#        p_cols = [c_cols[i] for i in range(next_query.contents.size)]
        print("THE COLUMNS ARE: ", c_cols.decode())
        print("THE VALUES ARE: ", c_vals.decode())
        adhoc_keys = c_cols.decode().split(",")
        adhoc_values = c_vals.decode().split(",")


        if(BF == 0):
            datarizer,durations = connector.scan_with_dependency(c_cols.decode(), c_cols.decode(), c_vals.decode(), connector.timeseries)
        elif(BF == 1):
            datarizer,durations = connector.scan_with_bf(c_cols.decode(), c_cols.decode(), c_vals.decode())
        elif(BF == 2):
            if(len(r["adhocFilters"])==1):
                if dev_decision == 2:
                    datarizer,durations = connector.scan_with_dependency(c_cols.decode(), c_cols.decode(), c_vals.decode(), connector.timeseries)
                elif dev_decision == 3:
                    datarizer,durations = connector.scan_gpu_with_dependency(c_cols.decode(), c_cols.decode(), c_vals.decode(), connector.timeseries)
            elif(len(r["adhocFilters"])==2):
                if dev_decision == 2:
                    datarizer,durations = connector.scan_with_dependency(c_cols.decode(), c_cols.decode(), c_vals.decode(), connector.timeseries)
                elif dev_decision == 3:
                    datarizer, durations = connector.scan_gpu_mult_dep(adhoc_keys, adhoc_values, connector.timeseries)
            elif(len(r["adhocFilters"])==3):
                if dev_decision == 2:
                    datarizer,durations = connector.scan_with_dependency(c_cols.decode(), c_cols.decode(), c_vals.decode(), connector.timeseries)
                elif dev_decision == 3:
                    datarizer, durations = connector.scan_gpu_mult_dep2(adhoc_keys, adhoc_values, connector.timeseries)
            elif(len(r["adhocFilters"])==4):
                if dev_decision == 2:
                    datarizer,durations = connector.scan_with_dependency(c_cols.decode(), c_cols.decode(), c_vals.decode(), connector.timeseries)
                elif dev_decision == 3:
                    datarizer, durations = connector.scan_gpu_mult_dep3(adhoc_keys, adhoc_values, connector.timeseries)

        #global no_queries
        no_queries += 1
        #global average_durations
        average_durations += durations
        #average_durations /= no_queries
    
        res = []
        no_empty = 0
        print(len(datarizer))
        for i in range(0, len(datarizer), 2):
            if(str(datarizer[i].decode("utf-8", errors="ignore").strip()) == ''):
                no_empty += 1
            elif(str(datarizer[i+1].decode("utf-8", errors="ignore").strip()) == ''):
                no_empty += 1
            else:
                #if()
                dt = datetime.strptime(str(datarizer[i+1].decode("utf-8",errors="ignore").strip()), '%Y-%m-%d %H:%M:%S')
                res.append([float(str(datarizer[i].decode("utf-8",errors="ignore").strip())), time.mktime(dt.timetuple())*1000])
                #dt = datetime.strptime(str(datarizer[i+1].decode("utf-8")), '%Y-%m-%d %H:%M:%S')
                #res.append([float(str(datarizer[i].decode("utf-8"))), time.mktime(dt.timetuple())*1000])


        print("Length of res: ", len(res))

        print("NO_EMPTY: ", no_empty)
        #print(res)

        data = [
            {
                "target":r['targets'][0]['target'], 
                "datapoints":res
            }
        ]
        execution_time = time.clock_gettime(1) - start

        print("AVERAGE DURATION: ", average_durations/no_queries)
        if(BF == 2 and len(r["adhocFilters"]) == 1):
            write_lb = str(execution_time) +", " + str(q_id) +", " + "GPU" +", "+ "2"+ "\n"
            f_lb = open("exectime.txt","a")
            f_lb.write(write_lb)
            f_lb.close()
        elif(len(r["adhocFilters"]) > 1):
            write_lb = str(execution_time) +", " + str(q_id) +", " + "GPU" +", "+ "3"+ "\n"
            f_lb = open("exectime.txt","a")
            f_lb.write(write_lb)
            f_lb.close()
        else:
            write_lb = str(execution_time) +", " + str(q_id) +", " + "CPU" +", "+ "2"+ "\n"
            f_lb = open("exectime.txt","a")
            f_lb.write(write_lb)
            f_lb.close() 

        return jsonify(data)

    elif (len(r["adhocFilters"]) > 4):
        #type = 1
        #connector.get_query_type(type)
        #if (gpu_op ==0):
        adhoc_keys = []
        adhoc_values = []
        adhoc_ops = []
        adhoc_targets = []
        print(len(r["adhocFilters"]))
        
        adhoc_key = r["adhocFilters"][0]["key"]
        adhoc_operator = r["adhocFilters"][0]["operator"]
        adhoc_value = r["adhocFilters"][0]["value"]
        if (len(r["adhocFilters"]) > 1):
            #adhoc_keys.append(adhoc_key)
            #adhoc_values.append(adhoc_value)
            #adhoc_ops.append(adhoc_operator)
            for i in range(0, len(r["adhocFilters"])):
                adhoc_keys.append(r["adhocFilters"][i]["key"])
                adhoc_ops.append(r["adhocFilters"][i]["operator"])
                adhoc_values.append(r["adhocFilters"][i]["value"])
            adhoc_targets.append(r['targets'][0]['target'])
            
        q_id = hashQuery(adhoc_key+adhoc_value)

        start = time.clock_gettime(1)

        print("Adhoc:" , "key->", adhoc_key, "operator->", adhoc_operator, "value->", adhoc_value)
        print("Adhoc:" , "key->", adhoc_keys[1], "operator->", adhoc_ops[1], "value->", adhoc_values[1])

        # Insert a query to the queue
        print("JUST BEFORE INSERTING THE QUERY TO THE QUEUE")
        try:
            add_qry_thread = ThreadWithReturnValue(target=connector.INSERT_QUERY, args=(q_id.encode('utf-8').strip(), BF, adhoc_keys, adhoc_values, ))
            add_qry_thread.start()
            add_qry_thread.join()
#            _thread.start_new_thread(connector.INSERT_QUERY, (q_id.encode('utf-8').strip(), BF, adhoc_keys, adhoc_values, ))

        except ValueError:
            print("problem with inserting query thread")

        # Getting the info of the queue
        try:
            info_thread = ThreadWithReturnValue(target=connector.GET_QUEUE_INFO, args=( ))
#            info_thread = Thread(target=connector.GET_QUEUE_INFO, args=())

            info_thread.start()
            info_struct = info_thread.join()
            final_info = ctypes.cast(info_struct, ctypes.POINTER(connector.queue_info))
            print("The queue length in python : ", final_info.contents.queue_length)
            print("The queue's min load is: ", final_info.contents.min_load)
            print("The queue's max load is: ", final_info.contents.max_load)
            print("The queue's avg load is: ", final_info.contents.avg_load)
            print("The queue's stdev is: ", final_info.contents.stdev)
            #_thread.start_new_thread(connector.GET_QUEUE_INFO, ())
        except ValueError:
            print("problem with getting the queue info thread")

        call_rslt = connector.GET_NEXT_QUERY()
        next_query = ctypes.cast(call_rslt, ctypes.POINTER(connector.query_struct))
        print("THE QUERY ID IS: ", (next_query.contents.query_id).decode())
        print("THE QUERY TYPE IS: ", next_query.contents.query_type)
        dev_decision = next_query.contents.decision
        print("THE DEVICE TO BE RUNNING IT IS: ", next_query.contents.decision)
        c_cols = next_query.contents.columns
        c_vals = next_query.contents.values
#        p_cols = [c_cols[i] for i in range(next_query.contents.size)]
        print("THE COLUMNS ARE: ", c_cols.decode())
        print("THE VALUES ARE: ", c_vals.decode())
        adhoc_keys = c_cols.decode().split(",")
        adhoc_values = c_vals.decode().split(",")


        if(BF == 0):
            datarizer,durations = connector.scan_with_dependency(c_cols.decode(), c_cols.decode(), c_vals.decode(), connector.timeseries)
        elif(BF == 1):
            datarizer,durations = connector.scan_with_bf(c_cols.decode(), c_cols.decode(), c_vals.decode())
        elif(BF == 2):
            if(len(r["adhocFilters"])==1):
                if dev_decision == 2:
                    datarizer,durations = connector.scan_with_dependency(c_cols.decode(), c_cols.decode(), c_vals.decode(), connector.timeseries)
                elif dev_decision == 3:
                    datarizer,durations = connector.scan_gpu_with_dependency(c_cols.decode(), c_cols.decode(), c_vals.decode(), connector.timeseries)
            elif(len(r["adhocFilters"])==2):
                if dev_decision == 2:
                    datarizer,durations = connector.scan_with_dependency(c_cols.decode(), c_cols.decode(), c_vals.decode(), connector.timeseries)
                elif dev_decision == 3:
                    datarizer, durations = connector.scan_gpu_mult_dep(adhoc_keys, adhoc_values, connector.timeseries)
            elif(len(r["adhocFilters"])==3):
                if dev_decision == 2:
                    datarizer,durations = connector.scan_with_dependency(c_cols.decode(), c_cols.decode(), c_vals.decode(), connector.timeseries)
                elif dev_decision == 3:
                    datarizer, durations = connector.scan_gpu_mult_dep2(adhoc_keys, adhoc_values, connector.timeseries)
            elif(len(r["adhocFilters"])==4):
                if dev_decision == 2:
                    datarizer,durations = connector.scan_with_dependency(c_cols.decode(), c_cols.decode(), c_vals.decode(), connector.timeseries)
                elif dev_decision == 3:
                    datarizer, durations = connector.scan_gpu_mult_dep3(adhoc_keys, adhoc_values, connector.timeseries)
            elif(len(r["adhocFilters"])==5):
                if dev_decision == 2:
                    datarizer,durations = connector.scan_with_dependency(c_cols.decode(), c_cols.decode(), c_vals.decode(), connector.timeseries)
                elif dev_decision == 3:
                    datarizer, durations = connector.scan_gpu_mult_dep4(adhoc_keys, adhoc_values, connector.timeseries)

        #global no_queries
        no_queries += 1
        #global average_durations
        average_durations += durations
        #average_durations /= no_queries
    
        res = []
        no_empty = 0
        print(len(datarizer))
        for i in range(0, len(datarizer), 2):
            if(str(datarizer[i].decode("utf-8", errors="ignore").strip()) == ''):
                no_empty += 1
            elif(str(datarizer[i+1].decode("utf-8", errors="ignore").strip()) == ''):
                no_empty += 1
            else:
                #if()
                dt = datetime.strptime(str(datarizer[i+1].decode("utf-8",errors="ignore").strip()), '%Y-%m-%d %H:%M:%S')
                res.append([float(str(datarizer[i].decode("utf-8",errors="ignore").strip())), time.mktime(dt.timetuple())*1000])
                #dt = datetime.strptime(str(datarizer[i+1].decode("utf-8")), '%Y-%m-%d %H:%M:%S')
                #res.append([float(str(datarizer[i].decode("utf-8"))), time.mktime(dt.timetuple())*1000])


        print("Length of res: ", len(res))

        print("NO_EMPTY: ", no_empty)
        #print(res)

        data = [
            {
                "target":r['targets'][0]['target'], 
                "datapoints":res
            }
        ]
        execution_time = time.clock_gettime(1) - start

        print("AVERAGE DURATION: ", average_durations/no_queries)
        if(BF == 2 and len(r["adhocFilters"]) == 1):
            write_lb = str(execution_time) +", " + str(q_id) +", " + "GPU" +", "+ "2"+ "\n"
            f_lb = open("exectime.txt","a")
            f_lb.write(write_lb)
            f_lb.close()
        elif(len(r["adhocFilters"]) > 1):
            write_lb = str(execution_time) +", " + str(q_id) +", " + "GPU" +", "+ "3"+ "\n"
            f_lb = open("exectime.txt","a")
            f_lb.write(write_lb)
            f_lb.close()
        else:
            write_lb = str(execution_time) +", " + str(q_id) +", " + "CPU" +", "+ "2"+ "\n"
            f_lb = open("exectime.txt","a")
            f_lb.write(write_lb)
            f_lb.close() 

        return jsonify(data)

    else:
        type = 1
        #"Before the decision"
        q_id = hashQuery(r['targets'][0]['target'])
        print(q_id)
        id_type = q_id +"-"+ str(type)
        print("Id type : ", id_type)
        decision = 3 #connector.get_query_type(id_type)
        print(decision)
        #"After the decision"
        #elif(gpu_op == 1):
        if(decision == 2):
            start = time.clock_gettime(1)
            #gpu_op = random.randint(0,1)
            pu[0] += 1
            print([pu[0]])
            if(target == "GPU Usage"):
                #print("******************************HERE GPU*****************")
                data0 = [
                    {
                        "target":r['targets'][0]['target'], 
                        "datapoints":[[pu[0]],time.mktime(datetime.now().timetuple())]
                    }
                ]
                #print("*******************END GPU************************")
                return jsonify(data0)
            else:
                print("Scanning for column",r['targets'][0]['target'])
#                datarizer,durations, q_id = connector.scan_gpu(r['targets'][0]['target'], connector.timeseries)
                datarizer,durations = connector.scan_gpu(r['targets'][0]['target'], connector.timeseries)
                print("Scan total datapoints: ", len(datarizer))
                #Less Weight on Dashboard
                datarizer = datarizer[:40000]

                res = []
                no_empty = 0
                print(len(datarizer))
                for i in range(0, len(datarizer), 2):
                    if(str(datarizer[i].decode("utf-8")) == ""):
                        no_empty += 1
                    elif(str(datarizer[i].decode("utf-8")) == ''):
                        no_empty += 1
                    else:
                        dt = datetime.strptime(str(datarizer[i+1].decode("utf-8")), '%Y-%m-%d %H:%M:%S')
                        res.append([float(str(datarizer[i].decode("utf-8"))), time.mktime(dt.timetuple())*1000])



                print("NO_EMPTY: ", no_empty)

                data = [
                    {
                        "target":r['targets'][0]['target'], 
                        "datapoints":res
                    }
                ]
                execution_time = time.clock_gettime(1) - start
                #print(execution_time)

                write_lb = str(execution_time) +", " + str(q_id) +", " + "GPU" +", "+ "1"+ "\n"
                f_lb = open("exectime.txt","a")
                f_lb.write(write_lb)
                f_lb.close()

                return jsonify(data)

        elif(decision == 3):
            start = time.clock_gettime(1)
            #gpu_op = random.randint(0,1)
            pu[1] += 1
            print(pu[1])
       
            if(target == "CPU Usage"):
                #print("***************HERE****************")
                data1 = [
                    {
                        "target":r['targets'][0]['target'], 
                        "datapoints":[[pu[1]], time.mktime(datetime.now().timetuple())]
                    }
                ]
                return jsonify(data1)
            else:

                #print("Scanning for column",r['targets'][0]['target'])
                datarizer, durations = connector.scan(r['targets'][0]['target'], connector.timeseries)
                print("Scan total datapoints: ", len(datarizer))
                #Less Weight on Dashboard
                datarizer = datarizer[:40000]
        
                res = []
                no_empty = 0
                #print(len(datarizer))
                for i in range(0, len(datarizer), 2):
                    if(str(datarizer[i].decode("utf-8")) == ""):
                        no_empty += 1
                    elif(str(datarizer[i].decode("utf-8")) == ''):
                        no_empty += 1
                    else:
                        dt = datetime.strptime(str(datarizer[i+1].decode("utf-8")), '%Y-%m-%d %H:%M:%S')
                        res.append([float(str(datarizer[i].decode("utf-8"))), time.mktime(dt.timetuple())*1000])

                    #global no_queries
                    #no_queries += 1
                    #global average_durations
                    #average_durations += durations

                print("NO_EMPTY: ", no_empty)
                #print(res)

                data = [
                    {
                        "target":r['targets'][0]['target'], 
                        "datapoints":res
                    }
                ]
                #print("***********HERE END************************")
        
                #print("AVERAGE DURATION: ", average_durations/no_queries)
                execution_time = time.clock_gettime(1) - start
                print(execution_time)
                write_lb = str(execution_time) +", " + str(q_id) +", " + "CPU" + ", " +"1"+ "\n"
                f_lb = open("exectime.txt","a")
                f_lb.write(write_lb)
                f_lb.close()
            
                return jsonify(data)
    ####SCAN###
   

@app.route('/annotations', methods=['POST'])
def annotations():
    req = request.get_json()
    data = [
        {
            "annotation": 'This is the annotation',
        "time": (convert_to_time_ms(req['range']['from']) +
                 convert_to_time_ms(req['range']['to'])) / 2,
            "title": 'Deployment notes',
            "tags": ['tag1', 'tag2'],
            "text": 'Hm, something went wrong...'
        }
    ]
    return jsonify(data)

if __name__ == '__main__':
    app.run(host='127.0.0.1', port=4006, debug=True)
