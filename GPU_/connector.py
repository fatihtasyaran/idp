from datetime import datetime
import socket
import time
import hashlib
import array
import numpy as np
from ctypes import*
from pynvml import *
from threading import Thread

def hashQuery(query):
    result = hashlib.sha256(str(query).encode())
    result = result.hexdigest()
    return result[0:8]

class ThreadWithReturnValue(Thread):
    def __init__(self, group=None, target=None, name=None,
                 args=(), kwargs={}, Verbose=None):
        Thread.__init__(self, group, target, name, args, kwargs)
        self._return = None
    def run(self):
        #print(type(self._target))
        if self._target is not None:
            self._return = self._target(*self._args,**self._kwargs)
    def join(self, *args):
        Thread.join(self, *args)
        return self._return


def return_all_cols(db_addr, libGPU, NO_BLOCK_GROUPS, col_num, names):
    db_cols = []
#    print("LIST OF COLUMNS: ", names[:col_num])
    for i in range(col_num):
        db_cols.append(libGPU.getDataBlock(db_addr, names.index(names[i]), NO_BLOCK_GROUPS, col_num))
    #print(db_cols[9][0:5])
    return db_cols

##PY UTILITIES##
import csv
##PY UTILITIES##
stdc = cdll.LoadLibrary("libc.so.6") # or similar to load c library
stdcpp = cdll.LoadLibrary("libstdc++.so.6") # or similar to load c++ library
libConnect = cdll.LoadLibrary('./libConnect.so')
libConnect.connect()

libGPU = cdll.LoadLibrary('./GPU.so')

#gpu_query = cdll.LoadLibrary('./gpo.so')

NO_BLOCK_GROUPS = 900


filer = open('/home/fatih/idp/chicago-taxi-rides-2016/chicago_taxi_trips_2016_01.csv')
#filer = open('GSM1659156_cutadapt_sorted.csv')

csv_reader = csv.reader(filer)
types = next(csv_reader)
names = next(csv_reader)
col_num = len(types)

libConnect.declare_dB_object.argtypes = [POINTER(c_char_p),POINTER(c_char_p),c_int,c_int]

col_types = (c_char_p * col_num)()

col_names = (c_char_p * col_num)()

for i in range(col_num):
    col_types[i] = types[i].encode('utf-8')
    col_names[i] = names[i].encode('utf-8')

##RESPONSE TIME
duration = 0
names.append("dB Response Time")
names.append("block_accesses")
names.append("GPU Usage")
names.append("CPU Usage")
##RESPONSE TIME


class queue_info(Structure):
    _fields_ = [("queue_length", c_int),("min_load", c_int), ("max_load", c_int), ("avg_load", c_int), ("stdev", c_int)]

class query_struct(Structure):
    _fields_ = [("query_id", c_char_p),("query_type", c_int),("columns", c_char_p),("values", c_char_p),("decision", c_int),("size", c_int)]

class gpu_info(Structure):
    _fields_ = [("gpu_usage", c_int), ("gpu_mem", c_int), ("gpu_power", c_int), ("gpu_temp", c_int)]

    
creator = libConnect.declare_dB_object
creator.restype = c_void_p

db_addr = c_void_p(creator(col_types, col_names, col_num, NO_BLOCK_GROUPS))

libConnect.start_ingest.argtypes = [c_void_p, c_int, POINTER(c_char_p), c_int]
libConnect.start_ingest(db_addr, NO_BLOCK_GROUPS, col_types, col_num)

k = input("Press enter to proceed reporting..")

libConnect.scan_db.argtypes = [c_void_p, c_int, c_int, c_int, c_int, c_int, POINTER(c_char_p)]
libConnect.scan_db.restype = POINTER(c_char_p)

libGPU.getDataBlock.argtypes = [c_void_p, c_int, c_int, c_int]
libGPU.getDataBlock.restype = POINTER(c_char_p)
global timeseries
timeseries = libGPU.getDataBlock(db_addr, names.index("trip_end_timestamp"), NO_BLOCK_GROUPS, col_num)


try:
    collecting_thread = ThreadWithReturnValue(target=return_all_cols, args=(db_addr, libGPU, NO_BLOCK_GROUPS, col_num, names, ))
    collecting_thread.start()
    all_cols = collecting_thread.join()

except ValueError:
    print("Problem while getting the database columns")
#db_cols = []
#print("LIST OF COLUMNS: ", names)
#for i in range(1, col_num-4):
#    db_cols.append(libGPU.getDataBlock(db_addr, names.index(names[i]), NO_BLOCK_GROUPS, col_num))

##print("After calling the thread:", all_cols[9][:5])
def scan(target, timeseries, return_dict, num_threads):
    is_int = False
    if isinstance(target, int) == False:
        for char in target:
            if char.isdigit():
                is_int = True

    
    if isinstance(target, str) and is_int != True:
        target = names.index(target)
        
    start = time.clock_gettime(1)
    datarizer_o = libConnect.scan_db(db_addr, int(target), names.index("trip_end_timestamp"), NO_BLOCK_GROUPS ,col_num, num_threads, timeseries)
#    datarizer_o = libConnect.scan_db(db_addr, names.index(target), names.index("trip_end_timestamp"), NO_BLOCK_GROUPS ,col_num, timeseries)
    global duration
    duration = time.clock_gettime(1) - start
    
    print("DURATION:", duration)
    
    datarizer_l = []

    empty_indexes = []
    
    for i in range(0, NO_BLOCK_GROUPS*1024*2, 2):
        if(datarizer_o[i] == b''):
            empty_indexes.append(i)
            empty_indexes.append(i+1)
        elif(datarizer_o[i+1] == b''):
            empty_indexes.append(i)
            empty_indexes.append(i+1)
        else:
            datarizer_l.append(datarizer_o[i])
            datarizer_l.append(datarizer_o[i+1])
        #print("datarizer_l[i]: ", i, datarizer_l[i])
        #qprint(datarizer_l[i])

    print("Empty indexes: ", len(empty_indexes))

    current_day = datetime.now()
    who = socket.gethostname()

    to_write = str(current_day) + "," + str(duration) + "," + str(target) + "," +str(col_num) + "," + "CPU" + "," + str(who) + "\n"

    f = open("lb_data.txt","a")
    f.write(to_write)
    f.close()

    q_id = hashQuery(str(target))
    write_lb = str(q_id) + " , " + str(duration) + " , " + str(target) + " , " + "CPU" + "\n"

    f_lb = open("lBalancer.txt","a")
    f_lb.write(write_lb)
    f_lb.close()
    timeseries = libGPU.getDataBlock(db_addr, names.index("trip_end_timestamp"), NO_BLOCK_GROUPS, col_num)

    return_dict["datarizer"] = datarizer_l
    return_dict["durations"] = duration
    return datarizer_l,duration

########### Get Query Type ###############
#libConnect.set_query_type.argtypes = [c_char_p]
#libConnect.set_query_type.restype = c_int

#def get_query_type(type):
#    decision = libConnect.set_query_type(type.encode('utf-8'))
#    return decision


##########################################

############ Query for GPU ################

#db_addr = c_void_p(creator(col_types, col_names, col_num, NO_BLOCK_GROUPS))
libConnect.queue_info_wrapper.restype = POINTER(queue_info)

def GET_QUEUE_INFO():
    return libConnect.queue_info_wrapper()

libConnect.gpu_info_wrapper.restype = POINTER(gpu_info)

def GET_GPU_INFO():
    return libConnect.gpu_info_wrapper()

libConnect.get_query.restype = POINTER(query_struct)

def GET_NEXT_QUERY():
    return libConnect.get_query()

libGPU.GPU_scan.argtypes = [c_void_p, POINTER(c_char_p), c_int, c_int, c_int]
libGPU.GPU_scan.restype = POINTER(c_char_p)

def scan_gpu(target, timeseries, return_dict):
    start = time.clock_gettime(1)
    
    datarizer_o = libGPU.GPU_scan(db_addr, all_cols[int(target)], names.index("trip_end_timestamp"), NO_BLOCK_GROUPS ,col_num, col_types[int(target)],timeseries)
    
#    datarizer_o = libGPU.GPU_scan(db_addr, int(target), names.index("trip_end_timestamp"), NO_BLOCK_GROUPS ,col_num, col_types[int(target)],timeseries)
    #datarizer_o = libGPU.GPU_scan(db_addr, names.index(target), names.index("trip_end_timestamp"), NO_BLOCK_GROUPS ,col_num, col_types[names.index(target)], timeseries)
    print("Datarizer is collected ")
    global duration
    duration = time.clock_gettime(1) - start

    print("DURATION:", duration)

    datarizer_l = []

    empty_indexes = []

    for i in range(0, NO_BLOCK_GROUPS*1024*2, 2):

        if(datarizer_o[i] == b''):
            empty_indexes.append(i)
            empty_indexes.append(i+1)
        elif(datarizer_o[i+1] == b''):
            empty_indexes.append(i)
            empty_indexes.append(i+1)
        else:
            datarizer_l.append(datarizer_o[i])
            datarizer_l.append(datarizer_o[i+1])
        #print("datarizer_l[i]: ", i, datarizer_l[i])
        #qprint(datarizer_l[i])

    print("Empty indexes: ", len(empty_indexes))

    current_day = datetime.now()
    who = socket.gethostname()

    to_write = str(current_day) + "," + str(duration) + "," + str(target) + "," +str(col_num) + "," + "GPU" + "," + str(who) + "\n"

    f = open("lb_data.txt","a")
    f.write(to_write)
    f.close()

    q_id = hashQuery(target)
    write_lb = str(q_id) + " , " + str(duration) + " , " + str(target) + " , " + "GPU" + "\n"

    f_lb = open("lBalancer.txt","a")
    f_lb.write(write_lb)
    f_lb.close()

    write_new = str(duration) + "," + str(q_id) + "," + "GPU" +","+ "1" + "\n"
    f_new = open("gpu_test1.txt","a")
    f_new.write(write_new)
    f_new.close()
    
    return_dict["datarizer"] = datarizer_l
    return_dict["durations"] = duration
    return datarizer_l,duration


###########################################


libConnect.scan_db_with_filter.argtypes = [c_void_p, c_int, c_int, c_int, c_int, c_int, c_char_p]
libConnect.scan_db_with_filter.restype = POINTER(c_char_p)

def scan_with_filter(target, value, return_dict, num_threads):
    start = time.clock_gettime(1)

    is_int = False
    if isinstance(target, int) == False:
        for char in target:
            if char.isdigit():
                is_int = True

    if isinstance(target, str) and is_int != True:
        target = names.index(target)
    
    datarizer_o = libConnect.scan_db_with_filter(db_addr, int(target), names.index("trip_end_timestamp"), NO_BLOCK_GROUPS, col_num, num_threads, value.encode('utf-8'))
#    datarizer_o = libConnect.scan_db_with_filter(db_addr, names.index(target), names.index("trip_end_timestamp"), NO_BLOCK_GROUPS, col_num, value.encode('utf-8'))
    global duration
    duration = time.clock_gettime(1) - start

    print("DURATION WITH FILTER:", duration)

    datarizer_l = []
    empty_indexes = []

    for i in range(0, NO_BLOCK_GROUPS*1024*2, 2):
        if(datarizer_o[i] == b''):
            empty_indexes.append(i)
            empty_indexes.append(i+1)
        elif(datarizer_o[i+1] == b''):
            empty_indexes.append(i)
            empty_indexes.append(i+1)
        else:
            datarizer_l.append(datarizer_o[i])
            datarizer_l.append(datarizer_o[i+1])

    print("Empty indexes: ", len(empty_indexes))
    print("No returned responses: ", len(datarizer_l))
    #print(datarizer_l[1].decode('utf-8'))

    current_day = datetime.now()
    who = socket.gethostname()

    to_write = str(current_day) + "," + str(duration) + "," + str(target) + "," +str(col_num) + "," + "CPU" + "," + str(who) + "\n"

    f = open("lb_data.txt","a")
    f.write(to_write)
    f.close()
    return_dict["datarizer"] = datarizer_l
    return_dict["durations"] = duration
    return datarizer_l, duration

libConnect.scan_db_with_dependency.argtypes = [c_void_p, c_int, c_int, c_int, c_int, c_int, c_char_p, c_int, POINTER(c_char_p)]
libConnect.scan_db_with_dependency.restype = POINTER(c_char_p)

def scan_with_dependency(target, filter_col, value, timeseries, return_dict, num_threads):
    start = time.clock_gettime(1)
    print(filter_col, value)

    is_int = False
    if isinstance(target, int) == False:
        for char in target:
            if char.isdigit():
                is_int = True

    if isinstance(target, str) and is_int != True:
        target = names.index(target)
#    datarizer_o = libConnect.scan_db_with_dependency(db_addr, names.index(target), names.index("trip_end_timestamp"), NO_BLOCK_GROUPS, col_num, names.index(filter_col), value.encode('utf-8').strip(), timeseries)
    datarizer_o = libConnect.scan_db_with_dependency(db_addr, int(target), names.index("trip_end_timestamp"), NO_BLOCK_GROUPS, col_num, int(filter_col), value.encode('utf-8').strip(), num_threads, timeseries)
    global duration
    duration = time.clock_gettime(1) - start

    print("DURATION WITH FILTER:", duration)

    datarizer_l = []
    empty_indexes = []

    for i in range(0, NO_BLOCK_GROUPS*1024*2, 2):
        if(datarizer_o[i] == b''):
            empty_indexes.append(i)
            empty_indexes.append(i+1)
        elif(datarizer_o[i+1] == b''):
            empty_indexes.append(i)
            empty_indexes.append(i+1)
        else:
            datarizer_l.append(datarizer_o[i])
            datarizer_l.append(datarizer_o[i+1])

    print("Empty indexes: ", len(empty_indexes))
    print("No returned responses: ", len(datarizer_l))
    ###print(datarizer_l[1].decode('ISO-8859-1'))

    current_day = datetime.now()
    who = socket.gethostname()

    to_write = str(current_day) + "," + str(duration) + "," + str(target) + "," +str(col_num) + "," + "CPU" + "," + str(who) + ",PLAIN_QUERY" +"\n"

    f = open("lb_data.txt","a")
    f.write(to_write)
    f.close()

    return_dict["datarizer"] = datarizer_l
    return_dict["durations"] = duration
    return datarizer_l, duration



libConnect.scan_db_with_dependencies.argtypes = [c_void_p, POINTER(c_int), c_int, c_int, c_int, c_int, POINTER(c_int), POINTER(c_char_p), c_int, POINTER(c_char_p)]
libConnect.scan_db_with_dependencies.restype = POINTER(c_char_p)

def scan_with_dependencies(targets, values, timeseries, return_dict, num_threads):

    name_ind = []
    vals_enc = []
    
    for target in targets:
        is_int = False
        if isinstance(target, int) == False:
            for char in target:
                if char.isdigit():
                    is_int = True
            if isinstance(target, str) and is_int != True:
                target = names.index(target)
    #arr_filter_cols = []
    print("VALUES IN DEPS: ", values)
    for i in range(len(targets)):
        name_ind.append(int(targets[i]))
        vals_enc.append(values[i].encode('utf-8').strip())
        #arr_filter_cols.append(filter_cols[i])
    names_arr = (c_int * len(name_ind))(*name_ind)
    #filt_cols = (c_int * len(arr_filter_cols))(*arr_filter_cols)
    encoded_values = (c_char_p * (len(vals_enc)))()
    encoded_values[:] = vals_enc

    print("Number of columns queried is: ", len(values))
    start = time.clock_gettime(1)
    datarizer_o = libConnect.scan_db_with_dependencies(db_addr, names_arr, names.index("trip_end_timestamp"), len(values), NO_BLOCK_GROUPS, col_num, names_arr, encoded_values, num_threads, timeseries)
    global duration
    duration = time.clock_gettime(1) - start

    print("DURATION WITH FILTER:", duration)

    datarizer_l = []
    empty_indexes = []

    for i in range(0, NO_BLOCK_GROUPS*1024*2, 2):
        if(datarizer_o[i] == b''):
            empty_indexes.append(i)
            empty_indexes.append(i+1)
        elif(datarizer_o[i+1] == b''):
            empty_indexes.append(i)
            empty_indexes.append(i+1)
        else:
            datarizer_l.append(datarizer_o[i])
            datarizer_l.append(datarizer_o[i+1])

    print("Empty indexes: ", len(empty_indexes))
    print("No returned responses: ", len(datarizer_l))
    ###print(datarizer_l[1].decode('ISO-8859-1'))

    current_day = datetime.now()
    who = socket.gethostname()

    to_write = str(current_day) + "," + str(duration) + "," + str(targets) + "," +str(col_num) + "," + "CPU" + "," + str(who) + ",PLAIN_QUERY" +"\n"

    f = open("lb_data.txt","a")
    f.write(to_write)
    f.close()

    return_dict["datarizer"] = datarizer_l
    return_dict["durations"] = duration
    return datarizer_l, duration



libGPU.GPU_scan_dependency.argtypes = [c_void_p, POINTER(c_char_p), c_int, c_int, c_int, c_int, c_char_p]
libGPU.GPU_scan_dependency.restype = POINTER(c_char_p)

def scan_gpu_with_dependency(target, filter_col, value, timeseries, return_dict):
    start = time.clock_gettime(1)
    is_int = False
    if isinstance(target, int) == False:
        for char in target:
            if char.isdigit():
                is_int = True

    if isinstance(target, str) and is_int != True:
        target = names.index(target)
#    datarizer_o = libGPU.GPU_scan_dependency(db_addr, names.index(target), names.index("trip_end_timestamp"), NO_BLOCK_GROUPS, col_num, names.index(filter_col), value.encode('utf-8').strip(), timeseries, col_types[names.index(filter_col)])

#    datarizer_o = libGPU.GPU_scan_dependency(db_addr, int(target), names.index("trip_end_timestamp"), NO_BLOCK_GROUPS, col_num, int(filter_col), value.encode('utf-8').strip(), timeseries, col_types[int(filter_col)])
    datarizer_o = libGPU.GPU_scan_dependency(db_addr, all_cols[int(target)], names.index("trip_end_timestamp"), NO_BLOCK_GROUPS, col_num, int(filter_col), value.encode('utf-8').strip(), timeseries, col_types[int(filter_col)]) 
    global duration
    duration = time.clock_gettime(1) - start

    print("DURATION WITH FILTER:", duration)

    datarizer_l = []
    empty_indexes = []

    for i in range(0, NO_BLOCK_GROUPS*1024*2, 2):
        if(datarizer_o[i] == b''):
            empty_indexes.append(i)
            empty_indexes.append(i+1)
        elif(datarizer_o[i+1] == b''):
            empty_indexes.append(i)
            empty_indexes.append(i+1)
        else:
            datarizer_l.append(datarizer_o[i])
            datarizer_l.append(datarizer_o[i+1])

    print("Empty indexes: ", len(empty_indexes))
    print("No returned responses: ", len(datarizer_l))
    #print(datarizer_l[1].decode('ISO-8859-1'))

    current_day = datetime.now()
    who = socket.gethostname()

    to_write = str(current_day) + "," + str(duration) + "," + str(target) + "," +str(col_num) + "," + "CPU" + "," + str(who) + ",PLAIN_QUERY-T2" +"\n"

    f = open("lb_data.txt","a")
    f.write(to_write)
    f.close()

    q_id = hashQuery(target)
    write_lb = str(q_id) + " , " + str(duration) + " , " + str(target) + " , " + "GPU" + "\n"

    f_lb = open("lBalancer.txt","a")
    f_lb.write(write_lb)
    f_lb.close()

    write_new = str(duration) + "," + str(q_id) + "," + "GPU" + ","+"2" + "\n"
    f_new = open("gpu_test1.txt","a")
    f_new.write(write_new)
    f_new.close()

    return_dict["datarizer"] = datarizer_l
    return_dict["durations"] = duration
    return datarizer_l, duration

#libGPU.GPU_scan_mult_dep.argtypes = [c_void_p, POINTER(c_int), c_int, c_int, c_int, POINTER(c_char_p), POINTER(c_char_p)]
libGPU.GPU_scan_mult_dep.argtypes = [c_void_p, POINTER(c_int), POINTER(c_char_p), POINTER(c_char_p), c_int, c_int, c_int, POINTER(c_char_p), POINTER(c_char_p)]
libGPU.GPU_scan_mult_dep.restype = POINTER(c_char_p)

def scan_gpu_mult_dep(targets, values, timeseries, return_dict):
    name_ind = []
    vals_enc = []
    for i in range(len(targets)):
        name_ind.append(int(targets[i]))
        vals_enc.append(values[i].encode('utf-8').strip())

    names_arr = (c_int * len(name_ind))(*name_ind)
    encoded_values = (c_char_p * (len(vals_enc)))()
    encoded_values[:] = vals_enc
    print("JUST BEFORE MULT DEP")
    start = time.clock_gettime(1)
#    datarizer_o = libGPU.GPU_scan_mult_dep(db_addr, names_arr, 2, NO_BLOCK_GROUPS, col_num, encoded_values, timeseries)
    datarizer_o = libGPU.GPU_scan_mult_dep(db_addr, names_arr, all_cols[int(targets[0])], all_cols[int(targets[1])],2, NO_BLOCK_GROUPS, col_num, encoded_values, timeseries)
    global duration
    duration = time.clock_gettime(1) - start

    print("DURATION WITH FILTER:", duration)

    datarizer_l = []
    empty_indexes = []

    for i in range(0, NO_BLOCK_GROUPS*1024*2, 2):
        if(datarizer_o[i] == b''):
            empty_indexes.append(i)
            empty_indexes.append(i+1)
        elif(datarizer_o[i+1] == b''):
            empty_indexes.append(i)
            empty_indexes.append(i+1)
        else:
            datarizer_l.append(datarizer_o[i])
            datarizer_l.append(datarizer_o[i+1])

    print("Empty indexes: ", len(empty_indexes))
    print("No returned responses: ", len(datarizer_l))
    #print(datarizer_l[1].decode('ISO-8859-1'))

    current_day = datetime.now()
    who = socket.gethostname()

    to_write = str(current_day) + "," + str(duration) + "," + str(targets) + "," +str(col_num) + "," + "GPU" + "," + str(who) + ",PLAIN_QUERY-T2" +"\n"

    f = open("lb_data.txt","a")
    f.write(to_write)
    f.close()

    q_id = hashQuery(targets[0])
    write_lb = str(q_id) + " , " + str(duration) + " , " + str(targets) + " , " + "GPU" + "\n"

    f_lb = open("lBalancer.txt","a")
    f_lb.write(write_lb)
    f_lb.close()

    write_new = str(duration) + "," + str(q_id) + "," + "GPU" +","+ "3" + "\n"
    f_new = open("gpu_test1.txt","a")
    f_new.write(write_new)
    f_new.close()

    return_dict["datarizer"] = datarizer_l
    return_dict["durations"] = duration
    return datarizer_l, duration



#libGPU.GPU_scan_mult_dep2.argtypes = [c_void_p, POINTER(c_int), c_int, c_int, c_int, POINTER(c_char_p), POINTER(c_char_p)]
libGPU.GPU_scan_mult_dep2.argtypes = [c_void_p, POINTER(c_int), POINTER(c_char_p), POINTER(c_char_p), POINTER(c_char_p),c_int, c_int, c_int, POINTER(c_char_p), POINTER(c_char_p)]
libGPU.GPU_scan_mult_dep2.restype = POINTER(c_char_p)

def scan_gpu_mult_dep2(targets, values, timeseries, return_dict):
    name_ind = []
    vals_enc = []
    print("VALUES IN DEPS 2: ", values)
    print("LEN OF TARGETS IN DEP2: ", len(targets))
    for i in range(len(targets)):
        name_ind.append(int(targets[i]))
        vals_enc.append(values[i].encode('utf-8').strip())

    names_arr = (c_int * len(name_ind))(*name_ind)
    encoded_values = (c_char_p * (len(vals_enc)))()
    encoded_values[:] = vals_enc
    
    start = time.clock_gettime(1)
    datarizer_o = libGPU.GPU_scan_mult_dep2(db_addr, names_arr, all_cols[int(targets[0])], all_cols[int(targets[1])], all_cols[int(targets[2])], 3, NO_BLOCK_GROUPS, col_num, encoded_values, timeseries)
    global duration
    duration = time.clock_gettime(1) - start

    print("DURATION WITH FILTER:", duration)

    datarizer_l = []
    empty_indexes = []

    for i in range(0, NO_BLOCK_GROUPS*1024, 2):
        if(isinstance(datarizer_o[i], int) != True):
            if(datarizer_o[i] == b''):
                empty_indexes.append(i)
                empty_indexes.append(i+1)
            elif(datarizer_o[i+1] == b''):
                empty_indexes.append(i)
                empty_indexes.append(i+1)
            else:
                datarizer_l.append(datarizer_o[i])
                datarizer_l.append(datarizer_o[i+1])

    print("Empty indexes: ", len(empty_indexes))
    print("No returned responses: ", len(datarizer_l))
    #print(datarizer_l[1].decode('ISO-8859-1'))

    current_day = datetime.now()
    who = socket.gethostname()

    to_write = str(current_day) + "," + str(duration) + "," + str(targets) + "," +str(col_num) + "," + "GPU" + "," + str(who) + ",PLAIN_QUERY-T2" +"\n"

    f = open("lb_data.txt","a")
    f.write(to_write)
    f.close()

    q_id = hashQuery(targets[0])
    write_lb = str(q_id) + " , " + str(duration) + " , " + str(targets) + " , " + "GPU" + "\n"

    f_lb = open("lBalancer.txt","a")
    f_lb.write(write_lb)
    f_lb.close()

    return_dict["datarizer"] = datarizer_l
    return_dict["durations"] = duration
    return datarizer_l, duration



#libGPU.GPU_scan_mult_dep3.argtypes = [c_void_p, POINTER(c_int), c_int, c_int, c_int, POINTER(c_char_p), POINTER(c_char_p)]
libGPU.GPU_scan_mult_dep3.argtypes = [c_void_p, POINTER(c_int), POINTER(c_char_p), POINTER(c_char_p), POINTER(c_char_p), c_int, c_int, c_int, POINTER(c_char_p), POINTER(c_char_p)]
libGPU.GPU_scan_mult_dep3.restype = POINTER(c_char_p)

def scan_gpu_mult_dep3(targets, values, timeseries):
    name_ind = []
    vals_enc = []
    for i in range(len(values)):
        name_ind.append(int(targets[i]))
        vals_enc.append(values[i].encode('utf-8').strip())

    names_arr = (c_int * len(name_ind))(*name_ind)
    encoded_values = (c_char_p * (len(vals_enc)))()
    encoded_values[:] = vals_enc
    
    start = time.clock_gettime(1)
    datarizer_o = libGPU.GPU_scan_mult_dep3(db_addr, names_arr, all_cols[int(targets[0])], all_cols[int(targets[1])], all_cols[int(targets[2])], all_cols[int(targets[3])], 4, NO_BLOCK_GROUPS, col_num, encoded_values, timeseries)
    global duration
    duration = time.clock_gettime(1) - start

    print("DURATION WITH FILTER:", duration)

    datarizer_l = []
    empty_indexes = []
    print(datarizer_o)
    for i in range(0, NO_BLOCK_GROUPS*1024*2, 2):
        
        if(datarizer_o[i] == b''):
            empty_indexes.append(i)
            empty_indexes.append(i+1)
        elif(datarizer_o[i+1] == b''):
            empty_indexes.append(i)
            empty_indexes.append(i+1)
        else:
            datarizer_l.append(datarizer_o[i])
            datarizer_l.append(datarizer_o[i+1])


    print("Empty indexes: ", len(empty_indexes))
    print("No returned responses: ", len(datarizer_l))
    #print(datarizer_l[1].decode('ISO-8859-1'))

    current_day = datetime.now()
    who = socket.gethostname()

    to_write = str(current_day) + "," + str(duration) + "," + str(targets) + "," +str(col_num) + "," + "GPU" + "," + str(who) + ",PLAIN_QUERY-T2" +"\n"

    f = open("lb_data.txt","a")
    f.write(to_write)
    f.close()

    q_id = hashQuery(targets[0])
    write_lb = str(q_id) + " , " + str(duration) + " , " + str(targets) + " , " + "GPU" + "\n"

    f_lb = open("lBalancer.txt","a")
    f_lb.write(write_lb)
    f_lb.close()


    return datarizer_l, duration


libGPU.GPU_scan_mult_dep4.argtypes = [c_void_p, POINTER(c_int), c_int, c_int, c_int, POINTER(c_char_p), POINTER(c_char_p)]
libGPU.GPU_scan_mult_dep4.restype = POINTER(c_char_p)

def scan_gpu_mult_dep4(targets, values, timeseries):
    name_ind = []
    vals_enc = []
    for i in range(len(values)):
        name_ind.append(int(targets[i]))
        vals_enc.append(values[i].encode('utf-8').strip())

    names_arr = (c_int * len(name_ind))(*name_ind)
    encoded_values = (c_char_p * (len(vals_enc)))()
    encoded_values[:] = vals_enc
    
    start = time.clock_gettime(1)
    datarizer_o = libGPU.GPU_scan_mult_dep4(db_addr, names_arr, 5, NO_BLOCK_GROUPS, col_num, encoded_values, timeseries)
    global duration
    duration = time.clock_gettime(1) - start

    print("DURATION WITH FILTER:", duration)

    datarizer_l = []
    empty_indexes = []
    print(datarizer_o)
    for i in range(0, NO_BLOCK_GROUPS*1024*2, 2):
        
        if(datarizer_o[i] == b''):
            empty_indexes.append(i)
            empty_indexes.append(i+1)
        elif(datarizer_o[i+1] == b''):
            empty_indexes.append(i)
            empty_indexes.append(i+1)
        else:
            datarizer_l.append(datarizer_o[i])
            datarizer_l.append(datarizer_o[i+1])


    print("Empty indexes: ", len(empty_indexes))
    print("No returned responses: ", len(datarizer_l))
    #print(datarizer_l[1].decode('ISO-8859-1'))

    current_day = datetime.now()
    who = socket.gethostname()

    to_write = str(current_day) + "," + str(duration) + "," + str(targets) + "," +str(col_num) + "," + "GPU" + "," + str(who) + ",PLAIN_QUERY-T2" +"\n"

    f = open("lb_data.txt","a")
    f.write(to_write)
    f.close()

    q_id = hashQuery(targets[0])
    write_lb = str(q_id) + " , " + str(duration) + " , " + str(targets) + " , " + "GPU" + "\n"

    f_lb = open("lBalancer.txt","a")
    f_lb.write(write_lb)
    f_lb.close()


    return datarizer_l, duration


libConnect.scan_db_with_bf.argtypes = [c_void_p, c_int, c_int, c_int, c_int, c_int, c_char_p]
libConnect.scan_db_with_bf.restype = POINTER(c_char_p)

def scan_with_bf(target, filter_col, value):
    start = time.clock_gettime(1)
    datarizer_o = libConnect.scan_db_with_bf(db_addr, names.index(target), names.index("trip_end_timestamp"), NO_BLOCK_GROUPS, col_num, names.index(filter_col), value.encode('utf-8').strip(), col_types)
    global duration
    duration = time.clock_gettime(1) - start

    print("DURATION WITH BLOOM FILTER:", duration)

    datarizer_l = []
    empty_indexes = []

    for i in range(0, NO_BLOCK_GROUPS*1024*2, 2):
        if(datarizer_o[i] == b''):
            empty_indexes.append(i)
            empty_indexes.append(i+1)
        elif(datarizer_o[i+1] == b''):
            empty_indexes.append(i)
            empty_indexes.append(i+1)
        else:
            datarizer_l.append(datarizer_o[i])
            datarizer_l.append(datarizer_o[i+1])

    print("Empty indexes: ", len(empty_indexes))
    print("No returned responses: ", len(datarizer_l))
    ###print(datarizer_l[1].decode('ISO-8859-1'))

    current_day = datetime.now()
    who = socket.gethostname()

    to_write = str(current_day) + "," + str(duration) + "," + str(target) + "," +str(col_num) + "," + "CPU" + "," + str(who) + ",BF_QUERY" +"\n"

    f = open("lb_data.txt","a")
    f.write(to_write)
    f.close()
    

    return datarizer_l, duration


libConnect.scan_db_with_dep_bf.argtypes = [c_void_p, POINTER(c_int), c_int, c_int, c_int, c_int, POINTER(c_int), POINTER(c_char_p), POINTER(c_char_p)]
libConnect.scan_db_with_dep_bf.restype = POINTER(c_char_p)

def scan_with_dep_bf(targets, values):

    name_ind = []
    vals_enc = []
    if(len(targets) == 1):
        name_ind.append(int(targets[0]))
        vals_enc.append(values[0].encode('utf-8').strip())
    else:
        for i in range(len(values)):
            name_ind.append(int(targets[i]))
            vals_enc.append(values[i].encode('utf-8').strip())

    names_arr = (c_int * len(name_ind))(*name_ind)
    encoded_values = (c_char_p * (len(vals_enc)))()
    encoded_values[:] = vals_enc

    start = time.clock_gettime(1)
    datarizer_o = libConnect.scan_db_with_dep_bf(db_addr, names_arr, names.index("trip_end_timestamp"), len(encoded_values) , NO_BLOCK_GROUPS, col_num, names_arr, encoded_values, col_types)
    global duration
    duration = time.clock_gettime(1) - start

    print("DURATION WITH BLOOM FILTER:", duration)

    datarizer_l = []
    empty_indexes = []

    for i in range(0, NO_BLOCK_GROUPS*1024*2, 2):
        if(datarizer_o[i] == b''):
            empty_indexes.append(i)
            empty_indexes.append(i+1)
        elif(datarizer_o[i+1] == b''):
            empty_indexes.append(i)
            empty_indexes.append(i+1)
        else:
            datarizer_l.append(datarizer_o[i])
            datarizer_l.append(datarizer_o[i+1])

    print("Empty indexes: ", len(empty_indexes))
    print("No returned responses: ", len(datarizer_l))
    #print(datarizer_l[1].decode('ISO-8859-1'))

    current_day = datetime.now()
    who = socket.gethostname()

    to_write = str(current_day) + "," + str(duration) + "," + str(targets) + "," +str(col_num) + "," + "CPU" + "," + str(who) + ",BF_QUERY" +"\n"

    f = open("lb_data.txt","a")
    f.write(to_write)
    f.close()
    

    return datarizer_l, duration


libConnect.insert_query1.argtypes= [c_char_p, c_int, POINTER(c_char_p)]
libConnect.insert_query.argtypes = [c_char_p, c_int, POINTER(c_char_p), POINTER(c_char_p)]
#libConnect.insert_query.restype = c_void_p

def INSERT_QUERY(query_id, query_type, columns, values):

    print("QUERY TYPE: ", query_type)
    print("COLUMNS: ", columns)
    print("VALUES: ", values)
    name_ind = []
    vals_enc = []
##    print("=================COLUMNS================", columns)
##    print("=================VALUES=================", values)
#    print("CHECKPOINT 1")
#    if (len(values) != 1) or (len(values)==1 and values[0] != ""):
    if query_type != 1:
        for i in range(len(columns)):
            name_ind.append(str(names.index(columns[i])).encode('utf-8').strip())
            vals_enc.append(values[i].encode('utf-8').strip())
    else:
        name_ind.append(str(names.index(columns[0])).encode('utf-8').strip())
        
        #arr_filter_cols.append(filter_cols[i])

#    print("CHECKPOINT 2")
#    names_arr = (c_char_p * len(name_ind))(*name_ind)
    names_arr = (c_char_p * len(name_ind))()
    names_arr[:] = name_ind  
    #filt_cols = (c_int * len(arr_filter_cols))(*arr_filter_cols)
#    print("CHECKPOINT 3")
    if query_type != 1:
        encoded_values = (c_char_p * (len(vals_enc)))()
        encoded_values[:] = vals_enc
##    print("===============NAMES_ARR================", names_arr[1])
##    print("===============ENC_VALS=================", encoded_values[1])
#    print("CHECKPOINT 4")
#    print("names_arr: ", names_arr)
#    print("encoded_values: ", encoded_values)
    if query_type != 1:
        libConnect.insert_query(query_id, query_type, names_arr, encoded_values, len(values))
    else:
        print("SENT TO INSERT QUERY 1")
        libConnect.insert_query1(query_id, query_type, names_arr, len(values))
#    print("CHECKPOINT 5")


libConnect.deneme_wrapper.restype = c_int

def deneme_py():
    return libConnect.deneme_wrapper()
    
    
def return_duration():
    print("DURATION:", duration)
    return duration


libConnect.db_accesses.argtypes=[c_void_p, c_int, c_int]
libConnect.db_accesses.restype = POINTER(c_int)

def save_accesses(filename):
    accesses = libConnect.db_accesses(db_addr, col_num, NO_BLOCK_GROUPS)

    for i in range(0, col_num*NO_BLOCK_GROUPS):
        print(accesses[i])
    
    with open(filename, 'w') as file:
        for num in accesses[:18000]:
            file.write(str(num))
            file.write("\n")

    exit(1)    
    
        
