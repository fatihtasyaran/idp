from texttable import Texttable
import sys

txt_name = "lb_ml_dt.txt"

f = open(txt_name,"r")
line_list = f.readlines()
f.close()

total_usage_cpu = 0.0
total_usage_gpu = 0.0
cpu_max = 0.0
gpu_max = 0.0
cpu_min = 10.0
gpu_min = 10.0

total_count = 0

for line in line_list:
    line.strip()
    if "_" not in line:
        split_list = line.split(",")
        total_usage_cpu += float(split_list[1])
        total_usage_gpu += float(split_list[2])        
        total_count += 1
        if(float(split_list[1])>cpu_max):
            cpu_max = float(split_list[1])
        if(float(split_list[1])<cpu_min):
            cpu_min = float(split_list[1])
        if(float(split_list[2])>gpu_max):
            gpu_max = float(split_list[1])
        if(float(split_list[2])<gpu_min):
            gpu_min = float(split_list[1])


try:
    cpu_avg = total_usage_cpu / total_count
except ZeroDivisionError:
    cpu_avg = 0

try:
    gpu_avg = total_usage_gpu / total_count
except ZeroDivisionError:
    gpu_avg = 0

print("CPU AVG:",cpu_avg)
print("GPU AVG:",gpu_avg)
print("CPU MAX-MIN:",cpu_max," , ",cpu_min)
print("GPU MAX-MIN:",gpu_max, " , ",gpu_min)
print(total_count)
