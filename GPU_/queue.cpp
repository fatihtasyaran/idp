#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <sstream>
#include <fstream>
#include <stdlib.h>
#include <thread>
#include <chrono>
#include <stdio.h>
#include "/usr/include/nvml.h"

using namespace std;


struct gpu_infos{

	unsigned int gpu_usage;
	unsigned int gpu_mem;
	unsigned int gpu_power;
	unsigned int gpu_temp;

	gpu_infos(unsigned int usage,unsigned int mem,unsigned int power,unsigned int temp):

		gpu_usage(usage),
		gpu_mem(mem),
		gpu_power(power),
		gpu_temp(temp){}

};


struct query{
	string query_id;
	uint8_t query_type;
	vector<string> columns;
	vector<string> query_strings;
	int device;

	query(string Query_id, uint8_t Query_type, vector<string> Columns, vector<string> Query, int Device):
  		query_id(Query_id),
		query_type(Query_type),
		columns(Columns),
		query_strings(Query),
		device(Device){}

};


template <class T>
class Queue{

public:
  Queue(){};
  ~Queue(){};
  T pop();
  void push(query& element);
  void remove(std::string Q_id);
  int get_min_load();
  int get_max_load();
  int get_avg_load();
  int standard_deviation();
  int queue_length();


  vector<query> Q;

};


template <class T>
T Queue<T>::pop(){
  return Q.front();
}

template <class T>
void Queue<T>::push(query& element){
  Q.push_back(element);
}

template <class T>
void Queue<T>::remove(std::string Q_id){
//  Queue.erase(std::remove(Queue.begin(), Queue.end(), Q.query_id), Queue.end());
	for( typename std::vector<T>::iterator iter = Q.begin(); iter != Q.end(); ++iter )
	{
    		if( (*iter).query_id == (Q_id) )
    		{
        		Q.erase( iter );
        		break;
    }
}

}

template <class T>
int Queue<T>::get_min_load(){

  return 10;

}


template <class T>
int Queue<T>::get_max_load(){

  return 100;

}

template <class T>
int Queue<T>::get_avg_load(){

  return 55;

}

template <class T>
int Queue<T>::standard_deviation(){

  return 27;

}



template <class T>
int Queue<T>::queue_length(){
  //  cout << "The queue size from c++ "<< endl;
  return Q.size();

}

void printQueue(Queue<query>* qq){

	for(int i =0; i <qq->Q.size(); i++){

		cout << qq->Q.at(i).query_id << endl; 

	}

}


int ReadNumbers( std::string & s, std::vector <std::string> & v ) {
    istringstream is(s);
    std::string n;
    while( is >> n ) {
        v.push_back( n );
    }
    return v.size();
}



int get_minimum_exectime(const char* filename_X, const char* filename_gpu, vector<std::string>& vv,vector <std::string>& v, int& rows, int& cols, 
				 int query_type){
    ifstream file_Y;
    std::string linee;

    file_Y.open(filename_gpu);
    if (file_Y.is_open())
    {
        int i=0;
        getline(file_Y, linee, '\n');
        cols = ReadNumbers( linee, vv );
//        cout << "cols:" << cols << endl;

        for ( i=1;i<32767;i++){
            if ( !getline(file_Y, linee, '\n') ) break;
            ReadNumbers( linee, vv );
        }
        rows=i;
//        cout << "rows :" << rows << endl;
        if(rows >32766) ////cout<< "N must be smaller than MAX_INT";

        file_Y.close();
    }
    else{
////        cout << "file open failed";
    }

//    cout << "vv:" << endl;
////    for (int i=0;i<rows;i++){
////        for (int j=0;j<cols;j++){
//            cout << vv[i*cols+j] << "\t" ;
////        }
//        cout << endl;
////    }

    ifstream file_X;
    std::string line;

    file_X.open(filename_X);
    if (file_X.is_open())
    {
        int i=0;
        getline(file_X, line, '\n');
        cols =ReadNumbers( line, v );
//        cout << "cols:" << cols << endl;

        for ( i=1;i<32767;i++){
            if ( !getline(file_X, line, '\n') ) break;
            ReadNumbers( line, v );
        }
        rows=i;
//        cout << "rows :" << rows << endl;
        if(rows >32766) ////cout<< "N must be smaller than MAX_INT";

        file_X.close();
    }
    else{
        cout << "file open failed";
    }

//    cout << "v:" << endl;
////    for (int i=0;i<rows;i++){
////        for (int j=0;j<cols;j++){
//            cout << v[i*cols+j] << "\t" ;
////        }
//        cout << endl;
////    }

std::vector<std::vector<double>> matrix_gpu;
for (int j=0; j<2; j++){
    std::vector<double> vec;
    std::string temp;

    size_t i = 0, start = 0, end;

        do {
                end = vv[j+2].find_first_of ( ' ', start );
                temp = vv[j+2].substr( start, end );
                if ( isdigit ( temp[0] ) )
                {
                        vec.push_back ( atof ( temp.c_str ( ) ) );
                        ++i;
                }
                start = end + 1;
        } while ( start );


//        for ( i = 0; i < vec.size ( ); ++i )
//                std::cout << vec[i] << '\n';

   matrix_gpu.push_back(vec);

}
/*
    auto min_gpu = std::min_element(std::begin(matrix_gpu[1]), std::end(matrix_gpu[1]));
    std::cout << "Gpu min time avg is " << *min_gpu << endl;
    auto pos_gpu = std::distance(std::begin(matrix_gpu[1]), min_gpu);
    std::cout << "Fpu min pos is " << pos_gpu << endl;
*/

//std::cout << "BETWEEEEEEEEEEEEEEEEEEEEEEN "<< endl;

std::vector<std::vector<double>> matrix;

for (int j=0; j<3; j++){
    std::vector<double> vec1;
    std::string temp;

    size_t i = 0, start = 0, end;

	do {
		end = v[j+1].find_first_of ( ',', start );
		temp = v[j+1].substr( start, end );
		if ( isdigit ( temp[0] ) )
		{
			vec1.push_back ( atof ( temp.c_str ( ) ) );
			++i;
		}
		start = end + 1;
	} while ( start );


//	for ( i = 0; i < vec1.size ( ); ++i )
//		std::cout << vec1[i] << '\n';

   matrix.push_back(vec1);

}
    auto min_cpu = std::min_element(std::begin(matrix[0]), std::end(matrix[0]));
    auto min_cpu1 = std::min_element(std::begin(matrix[1]), std::end(matrix[1]));

    auto min_gpu = std::min_element(std::begin(matrix_gpu[0]), std::end(matrix_gpu[0]));
//    std::cout << "BEFORE CALS"<< endl;

    auto min_gpu1 = std::min_element(std::begin(matrix_gpu[1]), std::end(matrix_gpu[1]));

    auto min_cpu2 = std::min_element(std::begin(matrix[2]), std::end(matrix[2]));

//    std::cout << "Min time avg is " << *min_cpu1 << *min_gpu<< endl;
    auto pos = std::distance(std::begin(matrix[1]), min_cpu);
//    std::cout << "Min pos is " << pos << endl;
   
//    cout << vec << endl; 

if (query_type == 1)
	//return std::min_element(std::begin(matrix[1]), std::end(matrix[1]));
	return 2;
else if (query_type == 2){
//	cout << std::min(std::min_element(std::begin(matrix[2]), std::end(matrix[2])), std::min_element(std::begin(matrix_gpu[1]), std::end(matrix_gpu[1]))) << endl;

//	return (std::min(std::min_element(std::begin(matrix[2]), std::end(matrix[2])), std::min_element(std::begin(matrix_gpu[1]), std::end(matrix_gpu[1])))
//			== std::min_element(std::begin(matrix[2]), std::end(matrix[2]))) ? 2 : 3;}
	return (std::min(*min_cpu1, *min_gpu) == *min_cpu1 ? 2 : 3);
}
else if (query_type == 3)
//	return (std::min(std::min_element(std::begin(matrix[3]), std::end(matrix[3])), std::min_element(std::begin(matrix_gpu[2]), std::end(matrix_gpu[2])))
//                        == std::min_element(std::begin(matrix[3]), std::end(matrix[3]))) ? 2 : 3;
	return (std::min(*min_cpu2, *min_gpu1) == *min_cpu2 ? 2 : 3);
else
	//In hazardous cases, just return GPU
	return 3;
}


int device_decision(int query_type){

	vector <std::string> v, vv;
	int rows=0;
    	int cols=0;
	return get_minimum_exectime("Matrix.txt", "matrix_gpu.txt", vv, v, rows, cols, query_type);
//	return 2;
}

const int NUM_CPU_STATES = 10;

enum CPUStates
{
	S_USER = 0,
	S_NICE,
	S_SYSTEM,
	S_IDLE,
	S_IOWAIT,
	S_IRQ,
	S_SOFTIRQ,
	S_STEAL,
	S_GUEST,
	S_GUEST_NICE
};

typedef struct CPUData
{
	std::string cpu;
	size_t times[NUM_CPU_STATES];
} CPUData;


void ReadStatsCPU(std::vector<CPUData> & entries)
{
	std::ifstream fileStat("/proc/stat");

	std::string line;

	const std::string STR_CPU("cpu");
	const std::size_t LEN_STR_CPU = STR_CPU.size();
	const std::string STR_TOT("tot");

	while(std::getline(fileStat, line))
	{
		// cpu stats line found
		if(!line.compare(0, LEN_STR_CPU, STR_CPU))
		{
			std::istringstream ss(line);

			// store entry
			entries.emplace_back(CPUData());
			CPUData & entry = entries.back();

			// read cpu label
			ss >> entry.cpu;

			// remove "cpu" from the label when it's a processor number
			if(entry.cpu.size() > LEN_STR_CPU)
				entry.cpu.erase(0, LEN_STR_CPU);
			// replace "cpu" with "tot" when it's total values
			else
				entry.cpu = STR_TOT;

			// read times
			for(int i = 0; i < NUM_CPU_STATES; ++i)
				ss >> entry.times[i];
		}
	}
}

size_t GetIdleTime(const CPUData & e)
{
	return	e.times[S_IDLE] + 
			e.times[S_IOWAIT];
}

size_t GetActiveTime(const CPUData & e)
{
	return	e.times[S_USER] +
			e.times[S_NICE] +
			e.times[S_SYSTEM] +
			e.times[S_IRQ] +
			e.times[S_SOFTIRQ] +
			e.times[S_STEAL] +
			e.times[S_GUEST] +
			e.times[S_GUEST_NICE];
}


float PrintStats(const std::vector<CPUData> & entries1, const std::vector<CPUData> & entries2)
{
	const size_t NUM_ENTRIES = entries1.size();
	float AVG_TOTAL_TIME = 0.0;

	for(size_t i = 0; i < NUM_ENTRIES; ++i)
	{
		const CPUData & e1 = entries1[i];
		const CPUData & e2 = entries2[i];

//		std::cout.width(3);
//		std::cout << e1.cpu << "] ";

		const float ACTIVE_TIME	= static_cast<float>(GetActiveTime(e2) - GetActiveTime(e1));
		const float IDLE_TIME	= static_cast<float>(GetIdleTime(e2) - GetIdleTime(e1));
		const float TOTAL_TIME	= ACTIVE_TIME + IDLE_TIME;

//		std::cout << "active: ";
//		std::cout.setf(std::ios::fixed, std::ios::floatfield);
//		std::cout.width(6);
//		std::cout.precision(2);
//		std::cout << (100.f * ACTIVE_TIME / TOTAL_TIME) << "%";
		if (i != 0)
			AVG_TOTAL_TIME += (100.f * ACTIVE_TIME / TOTAL_TIME);
//		std::cout << " - idle: ";
//		std::cout.setf(std::ios::fixed, std::ios::floatfield);
//		std::cout.width(6);
//		std::cout.precision(2);
//		std::cout << (100.f * IDLE_TIME / TOTAL_TIME) << "%" << std::endl;
	}

//	std::cout << "AVG CPU USAGE IS: " << (AVG_TOTAL_TIME / (NUM_ENTRIES-1)) << endl;
//	std::cout << "TOTAL ENTRIES: " << NUM_ENTRIES-1 << endl;

	return (AVG_TOTAL_TIME / (NUM_ENTRIES-1));
}

float cpu_utilization(){

	std::vector<CPUData> entries1;
	std::vector<CPUData> entries2;
//std::cout << "Inside CPU utils "<< endl; 

	// snapshot 1
	ReadStatsCPU(entries1);

	// 100ms pause
	std::this_thread::sleep_for(std::chrono::milliseconds(100));
	// snapshot 2
	ReadStatsCPU(entries2);

	return PrintStats(entries1, entries2); 
}


//unsigned int gpu_utilization(){
gpu_infos* gpu_utilization(){
// GPU UTILIZATION
//std::cout << "Inside GPU utils "<< endl; 

    nvmlReturn_t result, pow_result, gpu_temp;
    unsigned int device_count;
//    nvmlTemperatureSensors_t sensorType;
    result = nvmlInit();
//    if (result != NVML_SUCCESS)
//        return NULL;

    result = nvmlDeviceGetCount(&device_count);
//    if (result != NVML_SUCCESS)
//        return NULL;

    nvmlDevice_t device;
    //for (int i = 0; i < device_count; ++i) {
        result = nvmlDeviceGetHandleByIndex(0, &device);
//        if (result != NVML_SUCCESS)
//            return NULL;

        char device_name[NVML_DEVICE_NAME_BUFFER_SIZE];
        result = nvmlDeviceGetName(device, device_name, NVML_DEVICE_NAME_BUFFER_SIZE);
//        if (result != NVML_SUCCESS)
 //           return NULL;

////        std::printf("Device name: %s\n", device_name);

        nvmlUtilization_st device_utilization;
        result = nvmlDeviceGetUtilizationRates(device, &device_utilization);
// GPU POWER CONSUMPTION
	unsigned int power;
	pow_result = nvmlDeviceGetPowerUsage(device, &power);

// GPU TEMPERATURE
	unsigned int GPU_temp;
	gpu_temp = nvmlDeviceGetTemperature(device, NVML_TEMPERATURE_GPU, &GPU_temp);

//        if (result != NVML_SUCCESS)
//            return NULL;
	unsigned int gpu_usage, gpu_mem;
	gpu_usage = device_utilization.gpu;
	gpu_mem = device_utilization.memory;
////        std::printf("GPU Util: %u, Mem Util: %u\n, GPU Power usage: %u\n, GPU Temp: %u\n", device_utilization.gpu, device_utilization.memory, power, GPU_temp);
   // }
//    nvmlShutdown();

	gpu_infos* infos = new gpu_infos(device_utilization.gpu, device_utilization.memory, power, GPU_temp);
	result = nvmlDeviceGetUtilizationRates(device, &device_utilization);

return new gpu_infos(device_utilization.gpu, device_utilization.memory, power, GPU_temp);
//return device_utilization.gpu;

}


int main(){


vector<string> vec;
vec.push_back("6");
vec.push_back("7");

vector<string> vec_vals;
vec_vals.push_back("20");
vec_vals.push_back("2");

query* q1 = new query("23SDFM34", 1, vec, vec_vals, 2);
query* q2 = new query("23SDFMsdfsd34", 2, vec, vec_vals, 3);
query* q3 = new query("23SDFsjdkjfkjM34", 3, vec, vec_vals, 2);

//query q4 = new query("23SDFM34", 1, vec, "20");

Queue<query>* Q = new Queue<query>();

Q->push(*q1);
Q->push(*q2);
Q->push(*q3);
//cout << "After inserting the queries "<< endl; 
printQueue(Q);
//cout << "The query length before deletion is: "<< Q->queue_length() << endl;

Q->remove(q2->query_id);
//cout << "After deleting the queries "<< endl; 
printQueue(Q);

//cout << "The query length is: "<< Q->queue_length() << endl;


vector <std::string> v, vv;
    int rows=0;
    int cols=0;

int decision = get_minimum_exectime("Matrix.txt", "matrix_gpu.txt", vv, v, rows, cols, 1);

//cout << "The Decision is: "<< decision << endl;


std::vector<CPUData> entries1;
std::vector<CPUData> entries2;

// snapshot 1
ReadStatsCPU(entries1);

// 100ms pause
std::this_thread::sleep_for(std::chrono::milliseconds(100));

// snapshot 2
ReadStatsCPU(entries2);

// print output
float CPU_USAGE = PrintStats(entries1, entries2);
	std::cout << "AVG CPU USAGE IS: " << CPU_USAGE << endl;

//unsigned int GPU_USAGE = gpu_utilization();
//	std::cout << "AVG GPU USAGE IS; " << GPU_USAGE<< endl;



return 0;


}
