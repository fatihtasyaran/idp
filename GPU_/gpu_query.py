import pycuda.driver as drv
import pycuda.tools
import pycuda.autoinit
import numpy
from pycuda.compiler import SourceModule

module = SourceModule("""
__device__ char* my_strcpy(char* dest, char* src){
  int i = 0;
  do {
    dest[i] = src[i];}
  while (src[i++] != 0);
  return dest;
}


""")

module2 = SourceModule("""
__global__ void GPU_SCAN(dataBase* db_addr, int target_col_num, int ts_col_num, int NO_BLOCK_GROUPS, int col_num, char** join){
  //std::cout << "Let's make a GPU call" << std::endl;

  int id = (blockIdx.x * blockDim.x) + threadIdx.x;
//std::cout << "Scanning for: " << target_col_num << std::endl;
  
  dataBase* dB = db_addr;

  

  int total_elem = NO_BLOCK_GROUPS*1024*2;
  //int per_thread = total_elem/max_threads;
  
 /* char** join = new char*[NO_BLOCK_GROUPS*1024*2];    // Each thread will probably create the join table - create it before calling GPU_scan
  for(int i = 0; i < NO_BLOCK_GROUPS*1024*2; i++){
    join[i] = new char[100];
  }
  */

  int tso = ts_col_num - target_col_num;  //TIMESERIES OFFSET
  
  int group_per_thread = NO_BLOCK_GROUPS/BLOCKS_PER_BL;     //max_threads;


    
    int start_group = THREADS_PER_BLOCK*id;
    int last_group = start_group + THREADS_PER_BLOCK; //THIS IS LIMIT, NOT INCLUDED
    int join_index = start_group * 1024 * 2;
    
    /*if(tid == (max_threads - 1))
      last_group = NO_BLOCK_GROUPS - 1;
    */

    //for(int b = start_group; b < last_group; b++){
 //// for(int i = 0; i < 1024; i++){
	int target_block_index = /*b*/ blockIdx.x*col_num + target_col_num;
	int timeseries_block_index = /*b*/ blockIdx.x*col_num + ts_col_num;
	char* value = new char[100];

//db_addr->db[target_block_index]->somehow_getG(i, value); 
//db_addr->db[timeseries_block_index]->somehow_getG(i, value);

//dB->db[target_block_index]->somehow_getG(i, value);
//dB->db[timeseries_block_index]->somehow_getG(i, value);
	my_strcpy(join[join_index], db_addr->db[target_block_index]->getElems()[id%1024]);
	my_strcpy(join[join_index + 1], db_addr->db[timeseries_block_index]->getElems()[id%1024]);

	//my_strcpy(join[join_index], somehow_get_gpu(dB, target_block_index , i));

	join_index += 2;
 //// }

    //}

//  }
  
  ////std::cout << "Scan done!" << std::endl;

  //return join;
}

""")

