#include <iostream>
#include "connector.h"
#include "database.hpp"

#define THREADS_PER_BLOCK 1024
#define BLOCKS_PER_BL 900

#define cudaCheckErrors(msg) \
    do { \
        cudaError_t __err = cudaGetLastError(); \
        if (__err != cudaSuccess) { \
            fprintf(stderr, "Fatal error: %s (%s at %s:%d)\n", \
                msg, cudaGetErrorString(__err), \
                __FILE__, __LINE__); \
            fprintf(stderr, "*** FAILED - ABORTING\n"); \
            exit(1); \
        } \
    } while (0)


__host__ __device__ char* my_strcpy(char* dest, char* src){
  int i = 0;
  do {
    dest[i] = src[i];}
  while (src[i++] != 0);
  return dest;
}



__global__ void GPU_SCAN(dataBase* db_addr, int target_col_num, int ts_col_num, int NO_BLOCK_GROUPS, int col_num, char** join){

  int id = (blockIdx.x * blockDim.x) + threadIdx.x;
  dataBase* dB = db_addr;

  

  int total_elem = NO_BLOCK_GROUPS*1024*2;

  int tso = ts_col_num - target_col_num;  //TIMESERIES OFFSET
  
  int group_per_thread = NO_BLOCK_GROUPS/BLOCKS_PER_BL;     //max_threads;
    
  int start_group = THREADS_PER_BLOCK*id;
  int last_group = start_group + THREADS_PER_BLOCK; //THIS IS LIMIT, NOT INCLUDED
  int join_index = start_group * 1024 * 2;
    
  int target_block_index = /*b*/ blockIdx.x*col_num + target_col_num;
  int timeseries_block_index = /*b*/ blockIdx.x*col_num + ts_col_num;
  char* value = new char[100];
  
//  int i =0;
//  do{
//	join[join_index][i] = db_addr->db[target_block_index]->getElems()[id%1024][i];
//	join[join_index + 1][i] = db_addr->db[timeseries_block_index]->getElems()[id%1024][i];
//  }
//  while(db_addr->db[target_block_index]->getElems()[id%1024][i] !=0);

  my_strcpy(join[join_index], db_addr->db[target_block_index]->getElems()[id%1024]);
  my_strcpy(join[join_index + 1], db_addr->db[timeseries_block_index]->getElems()[id%1024]);
  
  //my_strcpy(join[join_index], db_addr->db[target_block_index]->somehow_get(id%1024).c_str());

  join_index += 2;

}


