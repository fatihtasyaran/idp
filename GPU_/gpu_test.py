from texttable import Texttable
import statistics

#txt_name = "gpu_test1.txt"
#txt_name = "gpu_test2.txt"
#txt_name = "gpu_test3.txt"
#txt_name = "gpu_test4.txt"
#txt_name = "gpu_test5.txt"
txt_name = "gpu_test6.txt"

f = open(txt_name,"r")
line_list = f.readlines()
f.close()

gpu_1_count = 0
gpu_2_count = 0
gpu_3_count = 0

total_time_gpu_1 = 0.0
total_time_gpu_2 = 0.0
total_time_gpu_3 = 0.0

gpu_q1_list =[]
gpu_q2_list =[]
gpu_q3_list =[]

for line in line_list:
    line.strip()
    if "," in line:
        split_list = line.split(",")
        if split_list[2].strip() == "GPU":      # DEVICE
            if split_list[3].strip() == "1":    # QUERY TYPE
                total_time_gpu_1 += float(split_list[0])
                gpu_1_count += 1
                gpu_q1_list.append(float(split_list[0]))
            elif split_list[3].strip() == "2":  # QUERY TYPE
                total_time_gpu_2 += float(split_list[0])
                gpu_2_count += 1
                gpu_q2_list.append(float(split_list[0]))
            elif split_list[3].strip() == "3":  # QUERY TYPE
                total_time_gpu_3 += float(split_list[0])
                gpu_3_count += 1
                gpu_q3_list.append(float(split_list[0]))                


try:
    gpu_q1_avg = total_time_gpu_1 / gpu_1_count
except ZeroDivisionError:
    gpu_q1_avg = 0
    
try:
    gpu_q2_avg = total_time_gpu_2 / gpu_2_count
except ZeroDivisionError:
    gpu_q2_avg = 0
    
try:
    gpu_q3_avg = total_time_gpu_3 / gpu_3_count
except ZeroDivisionError:
    gpu_q3_avg = 0

t = Texttable()
t.add_rows([
    ['----', 'Average Time','Max','Min','Median','Mean'],
    ['Query Type 2', gpu_q2_avg,max(gpu_q2_list),min(gpu_q2_list),statistics.median(gpu_q2_list),statistics.mean(gpu_q2_list)],
    ['Query Type 3', gpu_q3_avg,max(gpu_q3_list),min(gpu_q3_list),statistics.median(gpu_q3_list),statistics.mean(gpu_q3_list)]]) #['Query Type 1', gpu_q1_avg,max(gpu_q1_list),min(gpu_q1_list),statistics.median(gpu_q1_list),statistics.mean(gpu_q1_list)] ,['Query Type 2', gpu_q2_avg,max(gpu_q2_list),min(gpu_q2_list),statistics.median(gpu_q2_list),statistics.mean(gpu_q2_list)], ['Query Type 3', gpu_q3_avg,max(gpu_q3_list),min(gpu_q3_list),statistics.median(gpu_q3_list),statistics.mean(gpu_q3_list)]])
print("********************************* Results for",txt_name,"*****************************")
print(t.draw())

f = open("matrix_gpu.txt", "wt")
f.write(" ".join( ['Query Type 2', str(gpu_q2_avg),str(max(gpu_q2_list)),str(min(gpu_q2_list)),str(statistics.median(gpu_q2_list)),str(statistics.mean(gpu_q2_list))])+'\n')
f.write(" ".join( ['Query Type 3', str(gpu_q3_avg),str(max(gpu_q3_list)),str(min(gpu_q3_list)),str(statistics.median(gpu_q3_list)),str(statistics.mean(gpu_q3_list))])+'\n')

f.close()
