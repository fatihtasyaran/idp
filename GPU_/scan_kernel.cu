#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>
#include <memory>
#include <assert.h>
#include <stddef.h>
#include <cuda.h>
//#include "thirdP.hpp"
//#include "gpu_struct.hpp"

#include <iostream>
//#include "connector.h"
#include "database.hpp"
//#include "cuda_runtime.h"

#define THREADS_PER_BLOCK 1024
#define BLOCKS_PER_BL 900

using namespace std;
//#define NO_BLOCK_GROUPS 900



#define CUDA_ERROR_CHECK

#define CudaSafeCall( err ) __cudaSafeCall( err, __FILE__, __LINE__ )
#define CudaCheckError()    __cudaCheckError( __FILE__, __LINE__ )

inline void __cudaSafeCall( cudaError err, const char *file, const int line )
{
#ifdef CUDA_ERROR_CHECK
    if ( cudaSuccess != err )
    {
        fprintf( stderr, "cudaSafeCall() failed at %s:%i : %s\n",
                 file, line, cudaGetErrorString( err ) );
        exit( -1 );
    }
#endif

    return;
}

inline void __cudaCheckError( const char *file, const int line )
{
#ifdef CUDA_ERROR_CHECK
    cudaError err = cudaGetLastError();
    if ( cudaSuccess != err )
    {
        fprintf( stderr, "cudaCheckError() failed at %s:%i : %s\n",
                 file, line, cudaGetErrorString( err ) );
        exit( -1 );
    }

    // More careful checking. However, this will affect performance.
    // Comment away if needed.
    err = cudaDeviceSynchronize();
    if( cudaSuccess != err )
    {
        fprintf( stderr, "cudaCheckError() with sync failed at %s:%i : %s\n",
                 file, line, cudaGetErrorString( err ) );
        exit( -1 );
    }
#endif

    return;
}


//============================================================================//


__host__ __device__ char* my_strcpy(char* dest, char* src){
  int i = 0;
  do {
    dest[i] = src[i];}
  while (src[i++] != 0);
  return dest;
}

__global__ void GPU_SCAN(dataBase* db_addr, int target_col_num, int ts_col_num, int NO_BLOCK_GROUPS, int col_num, char** join){

  int id = (blockIdx.x * blockDim.x) + threadIdx.x;
  //daba* dB = db_addr;

  

  int total_elem = NO_BLOCK_GROUPS*1024*2;

  int tso = ts_col_num - target_col_num;  //TIMESERIES OFFSET
  
  int group_per_thread = NO_BLOCK_GROUPS/BLOCKS_PER_BL;     //max_threads;
    
  int start_group = THREADS_PER_BLOCK*id;
  int last_group = start_group + THREADS_PER_BLOCK; //THIS IS LIMIT, NOT INCLUDED
  int join_index = start_group * 1024 * 2;
  //printf("%d \n", start_group);
  int target_block_index = /*b*/ blockIdx.x*col_num + target_col_num;
  int timeseries_block_index = /*b*/ blockIdx.x*col_num + ts_col_num;

  char* value = new char[100];
  
  char* valu = new char[100];

  //value = db_addr->db[target_block_index]->items[id%1024];	

  //printf("%c \n", value);

  my_strcpy(join[join_index], db_addr->db[target_block_index]->somehow_getG(id%1024));
  
  //my_strcpy(join[join_index + 1], db_addr->db[timeseries_block_index]->items[id%1024]);
  
  //my_strcpy(join[join_index], db_addr->db[target_block_index]->somehow_get(id%1024).c_str());

  join_index += 2;

}
