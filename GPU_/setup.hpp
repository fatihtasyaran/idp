#ifndef SETUP_H
#define SETUP_H
#include <iostream>
#include <stdlib.h>
#include "database.hpp"
//#include "query.hpp"
#include <fstream>
#include "common.h"
#include <omp.h>
#include <cstring>
#include <stdio.h>
#include <string.h>

//#include "gpu_struct.hpp"
//template<class T>
dataBase* BLOCKS_ADDR;
//virtual_dblock dbk[100000]; 

//#define DEBUG

using namespace std;

//*********************************************************** HERE **********************************************************//
int thread = 8;

int returnSize(std::string type) {
  if(type == "uint8_t")
    return sizeof(UINT8);
  else if(type == "uint16_t")
    return sizeof(UINT16);
  else if(type == "uint32_t")
    return sizeof(UINT32);
  else if(type == "string")
    return SIZEOF_STRING;
  else if(type == "double")
    return sizeof(DOUBLE);
  else if(type == "bool")
    return sizeof(BOOL);
  else if(type == "NONE")
    return 0;
  else
    std::cout << "I don't have " << type << "type" << std::endl;
}


int coma(std::string line) {
  
  int comas = 1;
  int length = line.length();
    
  for(int i = 0; i < length; i++){
    if(line[i] == ',')
      comas++;
  }
  return comas;
}

void typeParse(std::string line, type_num_col* indexes, int mod) {
  std::string type;
  
  int length = line.length();
  int current_col = 0;
  int startIndex = 0;
  int endIndex = 0;

  

  for(int i = 0; i < length+1; i++){
    if(line[i] == ',' || i == length){
      endIndex = i;
      
      if(mod == 1){
	indexes[current_col].first = line.substr(startIndex, endIndex-startIndex);  //Type
      }
      else{
	indexes[current_col].second = line.substr(startIndex, endIndex-startIndex); //Feature name
      }

      
      startIndex = endIndex+1;
      current_col++;
      
      //std::cout << "Current Col: " << current_col << " String: " << line.substr(startIndex, endIndex-startIndex) << std::endl; 
    }
  }
}

//inline char* acquire_val(std::string line_to_cut, int order){
inline std::string acquire_val(std::string line_to_cut, int order){
#if defined DEBUG
  if(order == 0)
    //std::cout << line_to_cut << std::endl;
#endif
  
  int length = line_to_cut.length();
  int start_index = 0;
  int end_index = 0;

  int found = 0;

  std::string val = "D U M M Y";

  for(int i = 0; i < line_to_cut.length(); i++){
    if(line_to_cut[i] == ','){
      end_index = i;
      if(order == 0)
	start_index -= 1;
      val = line_to_cut.substr(start_index+1, end_index-start_index-1);
      
      if(found == order){
	//char * tmp = (char*)malloc(sizeof(char) * (val.size()+1));
	//strcpy(tmp, val.c_str());
	return val;
	}

      found++;
      start_index = end_index;
    }
  }

//  char * tmp = (char*)malloc(sizeof(char) * (val.size()+1));
//  strcpy(tmp, val.c_str());
  //return val;
  return val;
}

void writeOneRecordtoBlocks(dataBase db, std::string vals_line, type_num_col* indexes, int col_num, int record_num){

#pragma omp parallel num_threads(col_num) //Write to col_num blocks in parallel
  {
  int tid = omp_get_thread_num();
  
  int block_num = tid;//record_num/1024;
  int index = record_num%1024;
  int c_val;  //Value to cast placeholder
  //auto w_val; //Value to send block's insert function
  std::string type = indexes[tid].first;
  
  std::string val = acquire_val(vals_line, tid);

#if defined DEBUG
  std::cout << "Start parallel ingest" << std::endl;
#pragma omp critical
  {
    std::cout << "Thread " << tid << " before insert, my val: " << val << std::endl;
  }
#endif
  
  if(val == EMPTY){
    ;
  }
  
  else{
    if(type == "uint8_t"){
      c_val = std::stoi(val);
      auto w_val = (uint8_t)c_val;
      db.db[block_num]->insert(index, w_val);
    }
    else if(type == "uint16_t"){
      c_val = std::stoi(val);
    auto w_val = (uint16_t)c_val;
    db.db[block_num]->insert(index, w_val);
    }
    else if(type == "uint32_t"){
      c_val = std::stoi(val);
      auto w_val = (uint32_t)c_val;
      db.db[block_num]->insert(index, w_val);
    }
    else if(type == "double"){
      auto w_val = std::stod(val);
      db.db[block_num]->insert(index, w_val);
    }
    else if(type == "string"){
      auto w_val = val;
      db.db[block_num]->insert(index, w_val);
    }
    else if(type == "bool"){
      if(val == "true"){
	auto w_val = true;
	db.db[block_num]->insert(index, w_val);
      }else{
	auto w_val = false;
	db.db[block_num]->insert(index, w_val);
      }
    }else{
      std::cout << "Undefined type, exiting" << std::endl;
      exit(1);
    }

#if defined DEBUG
#pragma omp critical
    {
      std::cout << "Thread " << tid << " after insert" << std::endl;
    }
#endif
  }
}
  
  }

void writeOneRecordChunktoBlocks(dataBase db, std::string *record_chunk, type_num_col* indexes, int col_num, int chunk_size, int block_group){
#if defined DEBUG
  std::cout << "Block group: " << block_group << " start inserting in parallel" << std::endl;
#endif

#pragma omp parallel num_threads(col_num) //Write to col_num blocks in parallel
  {
  int tid = omp_get_thread_num();
  int block_num = tid+(block_group+block_group*(col_num-1));//record_num/1024;

  for(int i = 0; i < chunk_size; i++){
    int index = i;
    int c_val;  //Value to cast placeholder
    //auto w_val; //Value to send block's insert function
    std::string type = indexes[tid].first;
    
    std::string val = acquire_val(record_chunk[i], tid);
    
#if defined DEBUG
#pragma omp critical
    {
      std::cout << "Thread " << tid << " before insert, my val: " << val << std::endl;
    }
#endif
    
    if(val == EMPTY){
    ;
    }
  
    else{
      if(type == "uint8_t"){
	c_val = std::stoi(val);
	auto w_val = (uint8_t)c_val;
	db.db[block_num]->insert(index, w_val);
      }
      else if(type == "uint16_t"){
	c_val = std::stoi(val);
	auto w_val = (uint16_t)c_val;
	db.db[block_num]->insert(index, w_val);
      }
      else if(type == "uint32_t"){
	c_val = std::stoi(val);
	auto w_val = (uint32_t)c_val;
	db.db[block_num]->insert(index, w_val);
      }
      else if(type == "double"){
	auto w_val = std::stod(val);
	db.db[block_num]->insert(index, w_val);
      }
      else if(type == "string"){
	auto w_val = val;
	db.db[block_num]->insert(index, w_val);
      }
      else if(type == "bool"){
	if(val == "true"){
	  auto w_val = true;
	  db.db[block_num]->insert(index, w_val);
	}else{
	  auto w_val = false;
	  db.db[block_num]->insert(index, w_val);
	}
      }else{
	std::cout << "Undefined type, exiting" << std::endl;
	exit(1);
      }
      
#if defined DEBUG
#pragma omp critical
      {
	std::cout << "Thread " << tid << " after insert" << std::endl;
      }
#endif
    }
  }
#pragma omp barrier
}
  
  }



void writeOneRecordChunktoBlocks_better(dataBase db, std::string *record_chunk, char** col_types, int col_num, int chunk_size, int block_group){ //Threads constantly writes to same blocks, therefore one if - else is enough for 1024 records

  //std::cout << "Writing.." << std::endl;
  //db.db[0]->insert(0,(uint8_t)8);
  //std::cout << "Writing 2.." << std::endl;

#pragma omp parallel num_threads(col_num) //Write to col_num blocks in parallel
  {
  int tid = omp_get_thread_num();
  int block_num = tid+(block_group+block_group*(col_num-1));//record_num/1024;

  
  int c_val;  //Value to cast placeholder
  //auto w_val; //Value to send block's insert function
  //std::string type = col_types[tid]; //This line generates segmentation fault
  //std::string type(col_types[tid]);
  std::string type = col_types[tid];
  //type = col_types[tid];
  
  //std::cout << "Type is: " << col_types[0] << std::endl;

//std::cout << "Entered the ingestion " << endl;
  if(type == "uint8_t"){
    for(int index = 0; index < 1024; index++){
      std::string val = acquire_val(record_chunk[index], tid);
     // char * c_val = (char*)malloc(sizeof(char)*100);
     // c_val = acquire_val(record_chunk[index], tid);
      if(val != EMPTY){
	  c_val = std::stoi(val);
	  auto w_val = (uint8_t)c_val;
	  db.db[block_num]->insert(index, w_val);
	//  dbk[block_num].insert(index, w_val);
      }
    }
  }
  else if(type == "uint16_t"){
    for(int index = 0; index < 1024; index++){
        std::string val = acquire_val(record_chunk[index], tid);
	//char * c_val = (char*)malloc(sizeof(char)*100);
	//c_val = acquire_val(record_chunk[index], tid);

      if(val != EMPTY){
	c_val = std::stoi(val);
	auto w_val = (uint16_t)c_val;
	db.db[block_num]->insert(index, w_val);
	//dbk[block_num].insert(index, w_val);

      }
    }
  }
  else if(type == "uint32_t"){
    for(int index = 0; index < 1024; index++){
      std::string val = acquire_val(record_chunk[index], tid);
      //char * c_val = (char*)malloc(sizeof(char)*100);
    //  c_val = acquire_val(record_chunk[index], tid);

      if(val != EMPTY){
	c_val = std::stoi(val);
	auto w_val = (uint32_t) c_val;
	db.db[block_num]->insert(index, w_val);
       	// dbk[block_num].insert(index, w_val);
     }
    }
  }
  else if(type == "double"){
    for(int index = 0; index < 1024; index++){
      std::string val = acquire_val(record_chunk[index], tid);
      //char * c_val = (char*)malloc(sizeof(char)*100);
      //c_val = acquire_val(record_chunk[index], tid);

      if(val != EMPTY){
	auto w_val = std::stod(val);
	db.db[block_num]->insert(index, w_val);
//	dbk[block_num].insert(index, w_val);

      }
    }
  }
  else if(type == "string"){
    for(int index = 0; index < 1024; index++){
      std::string val = acquire_val(record_chunk[index], tid);
      //char * tmp = (char*) malloc(sizeof(char)* (val.size()+1));
      //strcpy(tmp, val.c_str());
      ////char * c_val = (char*)malloc(sizeof(char)*100); 
      ////c_val = acquire_val(record_chunk[index], tid);
      if(val != EMPTY)
	{
	  auto w_val = val;
	  ///auto w_val = (char*)c_val;
	  db.db[block_num]->insert(index, w_val);
	}
    }
  }
  else if(type == "bool"){
    for(int index = 0; index < 1024; index++){
      std::string val = acquire_val(record_chunk[index], tid);
      ////char * c_val = (char*)malloc(sizeof(char)*100);
      //c_val = acquire_val(record_chunk[index], tid);
       if(val != EMPTY){
	if(val == "true"){
	  auto w_val = true;
	  db.db[block_num]->insert(index, w_val);
	}
	else{
	  auto w_val = false;
	  db.db[block_num]->insert(index, w_val);
	}
      }
    }
  }
  else{
    std::cout << "Undefined type, exiting" << std::endl;
    exit(1);
  }
  

//#pragma omp barrier
}
  }

//====================================================================================
/*
void ChunktoBlocks(std::string *record_chunk, char** col_types, int col_num, int chunk_size, int block_group){

  //std::cout << "Writing.." << std::endl;
  //db.db[0]->insert(0,(uint8_t)8);
  //std::cout << "Writing 2.." << std::endl;

#pragma omp parallel num_threads(col_num) //Write to col_num blocks in parallel
  {
  int tid = omp_get_thread_num();
  int block_num = tid+(block_group+block_group*(col_num-1));//record_num/1024;

  
  int c_val;  //Value to cast placeholder
  //auto w_val; //Value to send block's insert function
  //std::string type = col_types[tid]; //This line generates segmentation fault
  //std::string type(col_types[tid]);
  std::string type = col_types[tid];
  //type = col_types[tid];
  
  //std::cout << "Type is: " << col_types[0] << std::endl;

    
  if(type == "uint8_t"){
    for(int index = 0; index < 1024; index++){
      std::string val = acquire_val(record_chunk[index], tid);
      if(val != EMPTY){
        c_val = std::stoi(val);
          auto w_val = (uint8_t)c_val;
          //db.db[block_num]->insert(index, w_val);
          dbk[block_num].insert(index, w_val);
      }
    }
  }
  else if(type == "uint16_t"){
    for(int index = 0; index < 1024; index++){
      std::string val = acquire_val(record_chunk[index], tid);
      if(val != EMPTY){
        c_val = std::stoi(val);
        auto w_val = (uint16_t)c_val;
        //db.db[block_num]->insert(index, w_val);
        dbk[block_num].insert(index, w_val);

      }
    }
  }
  else if(type == "uint32_t"){
    for(int index = 0; index < 1024; index++){
      std::string val = acquire_val(record_chunk[index], tid);
      if(val != EMPTY){
        c_val = std::stoi(val);
        auto w_val = (uint32_t)c_val;
        //db.db[block_num]->insert(index, w_val);
        dbk[block_num].insert(index, w_val);
     }
    }
  }

else if(type == "double"){
    for(int index = 0; index < 1024; index++){
      std::string val = acquire_val(record_chunk[index], tid);
      if(val != EMPTY){
        auto w_val = std::stod(val);
        //db.db[block_num]->insert(index, w_val);
        dbk[block_num].insert(index, w_val);

      }
    }
  }
  else if(type == "string"){
    for(int index = 0; index < 1024; index++){
      std::string val = acquire_val(record_chunk[index], tid);
      if(val != EMPTY)
        {
          auto w_val = val;
          //db.db[block_num]->insert(index, w_val);
        }
    }
  }
  else if(type == "bool"){
    for(int index = 0; index < 1024; index++){
      std::string val = acquire_val(record_chunk[index], tid);
      if(val != EMPTY){
        if(val == "true"){
          auto w_val = true;
          //db.db[block_num]->insert(index, w_val);
        }
        else{
          auto w_val = false;
          //db.db[block_num]->insert(index, w_val);
        }
      }
    }
  }
  else{
    std::cout << "Undefined type, exiting" << std::endl;
    exit(1);
  }
  

#pragma omp barrier
}
  }


//==================================================================================== 

void ingest_GPU(int NO_BLOCK_GROUPS, char** col_types, int col_num)
{
 // dataBase* dB = db_addr;
  std::string types_line;
  std::string vals_line;
  fstream csv_reader;
  //int col_num;
//  virtual_dblock* d_bl = dblock;

  std::cout << "Ingesting Datasource.." << std::endl;
    
  csv_reader.open("/home/fatih/idp/chicago-taxi-rides-2016/chicago_taxi_trips_2016_01.csv");
  //csv_reader.open("/home/data/epa_hap_daily_summary.csv");
  
  getline(csv_reader, types_line);
  getline(csv_reader, vals_line);
    
  
  int record_num = 0;
  
  int num_s_record = 0;
  int num_records = 0;
  
  double start = omp_get_wtime();
  
  std::string record_chunk[1024];
  std::string check_line;
  
  double getliner = 0;
  double get_start = 0;
  double get_end = 0;
  
  for(int block_group = 0; block_group < NO_BLOCK_GROUPS; block_group++){
    for(int record = 0; record < 1024; record++){
      get_start = omp_get_wtime();
      getline(csv_reader, record_chunk[record]);
      get_end = omp_get_wtime();
      getliner += get_end-get_start;
      num_s_record++;
    }
    //std::cout << "*****Writing block group: " << block_group << "*****" <<std::endl;
    ChunktoBlocks(record_chunk, col_types, col_num, 1024, block_group);
    num_records += 1024;
  }
  double end = omp_get_wtime();
  std::cout << "Ingest took " << end-start << std::endl;
  std::cout << "Number of records inserted: " << num_records << std::endl;
  std::cout << "Number of records per second: " << (1/(end-start))*num_records << std::endl;
  std::cout << "Read time: " << getliner << std::endl;
  std::cout << "According to this: " << (1/(end-start-getliner))*num_records << std::endl;
  
//  BLOCKS_ADDR = dB->db;
}
*/

//====================================================================================

//template<class T>
void ingest(void* db_addr,  int NO_BLOCK_GROUPS, char** col_types, int col_num)
{
  dataBase* dB = db_addr;
  std::string types_line;
  std::string vals_line;
  fstream csv_reader;
  //int col_num;
//  virtual_dblock* d_bl = dblock;

  std::cout << "Ingesting Datasource.." << std::endl;
    
  csv_reader.open("/home/fatih/idp/chicago-taxi-rides-2016/chicago_taxi_trips_2016_01.csv");
//  csv_reader.open("/home/fatih/idp/chicago-taxi-rides-2016/chicago-taxi-rides-2016.csv");
  //csv_reader.open("/home/data/epa_hap_daily_summary.csv");
  
  getline(csv_reader, types_line);
  getline(csv_reader, vals_line);
    
  
  int record_num = 0;
  
  int num_s_record = 0;
  int num_records = 0;
  
  double start = omp_get_wtime();
  
  std::string record_chunk[1024];
  std::string check_line;
  
  double getliner = 0;
  double get_start = 0;
  double get_end = 0;
  
  for(int block_group = 0; block_group < NO_BLOCK_GROUPS; block_group++){
    for(int record = 0; record < 1024; record++){
      get_start = omp_get_wtime();
      getline(csv_reader, record_chunk[record]);
      get_end = omp_get_wtime();
      getliner += get_end-get_start;
      num_s_record++;
    }
    //std::cout << "*****Writing block group: " << block_group << "*****" <<std::endl;
    writeOneRecordChunktoBlocks_better(*dB, record_chunk, col_types, col_num, 1024, block_group);
    num_records += 1024;
  }
  double end = omp_get_wtime();
  std::cout << "Ingest took " << end-start << std::endl;
  std::cout << "Number of records inserted: " << num_records << std::endl;
  std::cout << "Number of records per second: " << (1/(end-start))*num_records << std::endl;
  std::cout << "Read time: " << getliner << std::endl;
  std::cout << "According to this: " << (1/(end-start-getliner))*num_records << std::endl;
  
//  BLOCKS_ADDR = dB->db;
}

char** scan(void* db_addr, int target_col_num, int ts_col_num, int NO_BLOCK_GROUPS,int col_num, int num_threads, char** timeseries){
  std::cout << "Scanning for: " << target_col_num << std::endl;
  dataBase* dB = db_addr;
 
  //*************************************************************** HERE ****************************************************//
  //int max_threads = omp_get_max_threads();
  //  int max_threads = thread;

  std::ofstream myfile;
  myfile.open("cpu_test6.txt",fstream::out | fstream::app);

  /*int total_elem = NO_BLOCK_GROUPS*1024*2;
  int per_thread = total_elem/max_threads;
  */
  char** join = new char*[NO_BLOCK_GROUPS*1024*2+1];
  for(int i = 0; i < NO_BLOCK_GROUPS*1024*2+1; i++){
    join[i] = new char[20];
  }

  //  int tso = ts_col_num - target_col_num;  //TIMESERIES OFFSET
  
  //  int group_per_thread = NO_BLOCK_GROUPS/max_threads;
  int baa;
  double start = omp_get_wtime();
#pragma omp parallel for schedule(dynamic, 8) num_threads(num_threads)
  for(int b = 0; b < NO_BLOCK_GROUPS; b++) {
    //if(b == 0) { std::cout << "IN par " << omp_get_num_threads() << " " << NO_BLOCK_GROUPS << "\n";}
    int target_block_index = (b * col_num) + target_col_num;    
    int join_index = b * 1024 * 2;
    for(int i = 0; i < 1024; i++){
         strncpy(join[join_index++], dB->db[target_block_index]->somehow_get(i).c_str(), sizeof(char) * 20);
	
	//join[join_index++] = &(dB->db[target_block_index]->somehow_get(i)[0]);
	////join[join_index++] = &(timeseries[b * i][0]);

//	join[join_index++] = dB->db[target_block_index]->somehow_get(i).c_str();
//	join[join_index++] = dB->db[target_block_index]->somehow_get(i);
	strcpy(join[join_index++], timeseries[b * i]);
//	cout << dB->db[target_block_index]->somehow_get(i) << endl;
    }
  }
  /*
  
#pragma omp parallel num_threads(max_threads)//(max_threads)
  {
    int tid = omp_get_thread_num();
    int start_group = group_per_thread * tid;
    int last_group = start_group + group_per_thread; //THIS IS LIMIT, NOT INCLUDED
    int join_index = start_group * 1024 * 2;
    
    if(tid == (max_threads - 1)) {
      last_group = NO_BLOCK_GROUPS - 1;
    }

    //#pragma omp for schedule(static)
    for(int b = start_group; b < last_group; b++){
      int target_block_index = (b * col_num) + target_col_num;
    //  for (int j=0; j<NO_BLOCK_GROUPS; j++){
       for(int i = 0; i < 1024; i++){

	//int timeseries_block_index = b*col_num + ts_col_num;
	
	strcpy(join[join_index], dB->db[target_block_index]->somehow_get(i).c_str());
	//strcpy(join[join_index + 1], dB->db[timeseries_block_index]->somehow_get(i).c_str());
	strcpy(join[join_index + 1], timeseries[i*b]);
	join_index += 2;
      }
    }
  }
  */
  double end = omp_get_wtime();
  std::cout << "Scan done! (Scan) " << end - start << std::endl;

  myfile << std::to_string(end - start) +", "+ "Scan" +", "+ std::to_string(thread)<<std::endl;
  myfile.close();
//  delete[] join;
//  strcpy(join[0], std::to_string(end-start).c_str());
  cout << "|||||||||||||FINISHED||||||||||||||"<<endl;
  return join;
}

char** scan_with_filter(void* db_addr, int target_col_num, int ts_col_num, int NO_BLOCK_GROUPS, int col_num, int num_threads, char* target_value){
  std::string str_value(target_value);
  
  std::cout << "Scanning for: " << target_col_num << std::endl;
  dataBase* dB = db_addr;
//*********************************************************************************************** HERE ****************************************************//
  //int max_threads = omp_get_max_threads();
  int max_threads = thread;

  std::ofstream myfile;
  myfile.open("cpu_test6.txt",fstream::out | fstream::app);

  int total_elem = NO_BLOCK_GROUPS*1024*2;
  int per_thread = total_elem/max_threads;
 
  char** join = new char*[NO_BLOCK_GROUPS*1024*2+1];
  for(int i = 0; i < NO_BLOCK_GROUPS*1024*2+1; i++){
    join[i] = new char[100];
  }

  int tso = ts_col_num - target_col_num;  //TIMESERIES OFFSET
  
  int group_per_thread = NO_BLOCK_GROUPS/max_threads;
  double start = omp_get_wtime();
#pragma omp parallel num_threads(num_threads)//(max_threads)
  {
    int tid = omp_get_thread_num();
    int start_group = group_per_thread*tid;
    int last_group = start_group + group_per_thread; //THIS IS LIMIT, NOT INCLUDED
    int join_index = start_group * 1024 * 2;
    
    if(tid == (max_threads - 1))
      last_group = NO_BLOCK_GROUPS - 1;
    
    for(int b = start_group; b < last_group; b++){
      for(int i = 0; i < 1024; i++){
	int target_block_index = b*col_num + target_col_num;
	int timeseries_block_index = b*col_num + ts_col_num;
	if(dB->db[target_block_index]->bf_query(str_value))
	{
	//std::cout << "dB_val: " << dB->db[target_block_index]->somehow_get(i).c_str() << " target_val: " << str_value << std::endl;
	std::string response(dB->db[target_block_index]->somehow_get(i).c_str());
	//char* response = (char*)malloc(sizeof(char)* 100);
	//response = dB->db[target_block_index]->somehow_get(i);
	if(response == target_value){
	  strcpy(join[join_index], dB->db[target_block_index]->somehow_get(i).c_str());
//          join[join_index] = dB->db[target_block_index]->somehow_get(i);
	  strcpy(join[join_index + 1], dB->db[timeseries_block_index]->somehow_get(i).c_str());
	  join_index += 2;
	}
       }
      }
    }

  }
  double end = omp_get_wtime();
  std::cout << "Scan done! (Scan with filter) " << end - start << std::endl;
//  strcpy(join[0], std::to_string(end-start).c_str());
  myfile << std::to_string(end - start) +", "+ "Scan_with_filter" +", "+ std::to_string(thread) <<std::endl;
  myfile.close();
  return join;
}

char** scan_with_dependency(void* db_addr, int target_col_num, int ts_col_num, int NO_BLOCK_GROUPS, int col_num, int filter_col_num, char* dependency_value, int num_threads, char** timeseries){
  std::string filter_value(dependency_value);
  std::cout << filter_value << endl; 
  std::cout << "Scanning for: " << target_col_num << std::endl;
  dataBase* dB = db_addr;
//*********************************************************************************************** HERE ****************************************************//
  //int max_threads = omp_get_max_threads();
  int max_threads = thread;

  std::ofstream myfile;
  //  myfile.open("cpu_test6.txt",fstream::out | fstream::app);

  int total_elem = 900*1024*2;
  int per_thread = total_elem/max_threads;


  char** join = new char*[NO_BLOCK_GROUPS*1024*2+1];
  
  for(int i = 0; i < NO_BLOCK_GROUPS*1024*2+1; i++){
    join[i] = new char[100];
  }

  

  int tso = ts_col_num - target_col_num;//TIMESERIES OFFSET
  
  int group_per_thread = NO_BLOCK_GROUPS/max_threads;
  double start = omp_get_wtime();
#pragma omp parallel num_threads(num_threads)//(max_threads)
  {
    int tid = omp_get_thread_num();
    int start_group = group_per_thread*tid;
    int last_group = start_group + group_per_thread; //THIS IS LIMIT, NOT INCLUDED
    int join_index = start_group * 1024 * 2;
    
    if(tid == (max_threads - 1))
      last_group = NO_BLOCK_GROUPS - 1;
    
    for(int b = start_group; b < last_group; b++){

      int target_block_index = b*col_num + target_col_num;
      int timeseries_block_index = b*col_num + ts_col_num;
      int filter_block_index = b*col_num + filter_col_num;    
    //for(int b = start_group; b < last_group; b++){
      //   #pragma omp for schedule(dynamic)
      //    for(int j =0; j<NO_BLOCK_GROUPS; j++){
       for(int i = 0; i < 1024; i++){
	 //	int target_block_index = j*col_num + target_col_num;
	 //	int timeseries_block_index = j*col_num + ts_col_num;
	 //	int filter_block_index = j*col_num + filter_col_num;
	//std::cout << "dB_val: " << dB->db[target_block_index]->somehow_get(i).c_str() << " target_val: " << str_value << std::endl;
	char* tmp = new char[10];
	tmp = dB->db[filter_block_index]->somehow_get(i).c_str();
	std::string response(tmp);
	//char* response = (char*)malloc(sizeof(char)* 100);
        //response = dB->db[target_block_index]->somehow_get(i);

	if(response == filter_value){	  
	 strcpy(join[join_index], tmp);
	 //	 strcpy(join[join_index], dB->db[target_block_index]->somehow_get(i).c_str());
//	  strcpy(join[join_index + 1], dB->db[timeseries_block_index]->somehow_get(i).c_str());
	  
	 // join[join_index] = dB->db[target_block_index]->somehow_get(i);
//	  strcpy(join[join_index + 1], timeseries[i*b]);
	  join_index += 2;
	}
      }
    }
  }
  double end = omp_get_wtime();
  std::cout << "Scan done! (Scan with dependency) " << end - start << std::endl;
  strcpy(join[0], std::to_string(end-start).c_str());
 //myfile << std::to_string(end - start) +", "+ "Scan_with_dependency" +", "+ std::to_string(thread)  <<std::endl;
 // myfile.close();
  //delete[] join;
  return join;
}



char** scan_with_dependencies(void* db_addr, int* target_col_nums, int ts_col_num, int num_cols, int NO_BLOCK_GROUPS, 
int col_num, int* filter_col_num, char** dependency_values, int num_threads, char** timeseries){

  std::string dependencies[num_cols];

std::cout << "I am here len: " << num_cols << std::endl;

  for(int i = 0; i < num_cols; i++){
    //std::string filter_value(dependency_values[i]);
//    std::cout << i << " : " << dependency_values[i] << std::endl;
    dependencies[i] = dependency_values[i];
  }

  dataBase* dB = db_addr;
//*********************************************************************************************** HERE ****************************************************//
  //int max_threads = omp_get_max_threads();
  int max_threads = thread;

  std::ofstream myfile;
  myfile.open("cpu_test6.txt",fstream::out | fstream::app);

  int total_elem = NO_BLOCK_GROUPS*1024*2;
  int per_thread = total_elem/max_threads;

  std::cout << "Scanning for: " << target_col_nums[0] << std::endl;
  
  char** join = new char*[NO_BLOCK_GROUPS*1024*2+1];
  for(int i = 0; i < NO_BLOCK_GROUPS*1024*2+1; i++){
    join[i] = new char[100];
  }

  int tso = ts_col_num - target_col_nums[0];//TIMESERIES OFFSET
  
  int group_per_thread = NO_BLOCK_GROUPS/max_threads;

  double start = omp_get_wtime();
std::cout << dependencies[1] << std::endl;

#pragma omp parallel num_threads(num_threads)//(max_threads)
  {
    int tid = omp_get_thread_num();
    int start_group = group_per_thread*tid;
    int last_group = start_group + group_per_thread; //THIS IS LIMIT, NOT INCLUDED
    int join_index = start_group * 1024 * 2;
    
    if(tid == (max_threads - 1))
      last_group = NO_BLOCK_GROUPS - 1;

#pragma omp for schedule(static)
    for(int j =0; j<NO_BLOCK_GROUPS; j++){   
      //for(int b = start_group; b < last_group; b++){
        int target_block_indices[num_cols];
	int filter_block_indices[num_cols];
	int timeseries_block_indices; //[num_cols];

	for(int i=0; i< num_cols; i++){
          target_block_indices[i] = j*col_num + target_col_nums[i];
          filter_block_indices[i] = j*col_num + filter_col_num[i];
	  timeseries_block_indices = j*col_num + ts_col_num;

        }

      for(int i = 0; i < 1024; i++){
	//std::cout << "dB_val: " << dB->db[target_block_index]->somehow_get(i).c_str() << " target_val: " << str_value << std::endl;

        std::string responses[num_cols];
/*        char** responses = (char**)malloc(sizeof(char*)*num_cols);
	responses[0] = (char*)malloc(sizeof(char));
	responses[1] = (char*)malloc(sizeof(char));
	responses[2] = (char*)malloc(sizeof(char));
	responses[3] = (char*)malloc(sizeof(char));
*/
	bool check = true;
/*	for (int l=0; l<num_cols; l++){
		if(!dB->db[filter_block_indices[l]]->bf_query(dependencies[l]))
			check = false;
			break;
	}
*/
///////
	if (check){
	for(int k=0; k < num_cols; k++){
	  responses[k] = (dB->db[filter_block_indices[k]]->somehow_get(i).c_str());
//	  responses[k] = dB->db[filter_block_indices[k]]->somehow_get(i);
	}

	for (int i=0; i < num_cols; i++)
	  if(responses[i] != dependencies[i]){
	    check = false;
	  }
//#pragma omp critical
	if(check){ 
	  strcpy(join[join_index], dB->db[target_block_indices[0]]->somehow_get(i).c_str());
	 ////join[join_index] = dB->db[target_block_indices[0]]->somehow_get(i);

	 // char * tmp = dB->db[timeseries_block_indices[0]]->somehow_get(i).c_str();
	 // if(!tmp)
	  struct tm tm;
        if (strptime(dB->db[timeseries_block_indices]->somehow_get(i).c_str(), "%Y-%m-%d %H:%M:%S", &tm))
	//  	strcpy(join[join_index + 1], dB->db[timeseries_block_indices]->somehow_get(i).c_str());
	  strcpy(join[join_index + 1], timeseries[i*j]);
	  join_index += 2;

//	  std::cout << join[join_index] << std::endl;
//	  std::cout << join[join_index + 1] << std::endl;
	}
       }
      }
    }
  }
  double end = omp_get_wtime();
  //free(dependencies);
  std::cout << "Scan done! (Scan with dependencies) " << end - start << std::endl;
 // strcpy(join[0], std::to_string(end-start).c_str());
  myfile << std::to_string(end - start) +", "+ "Scan_with_dependencies" +", "+ std::to_string(thread) <<std::endl;
  myfile.close();
  return join;
}




template <class T>
char** scan_with_bf(void* db_addr, int target_col_num, int ts_col_num, int NO_BLOCK_GROUPS, int col_num, int filter_col_num, char* chr_filter_value, T filter_value){

  std::cout << "Scanning for: " << target_col_num << std::endl;
  std::string str_filter_value(chr_filter_value); 
  dataBase* dB = db_addr;
//*********************************************************************************************** HERE ****************************************************//  
  //int max_threads = omp_get_max_threads();
  int max_threads = thread;

  std::ofstream myfile;
  myfile.open("cpu_test6.txt",fstream::out | fstream::app);
  
  int total_elem = NO_BLOCK_GROUPS*1024*2;
  int per_thread = total_elem/max_threads;
  
  char** join = new char*[NO_BLOCK_GROUPS*1024*2];
  for(int i = 0; i < NO_BLOCK_GROUPS*1024*2; i++){
    join[i] = new char[100];
  }
  
  int tso = ts_col_num - target_col_num;//TIMESERIES OFFSET
  
  int group_per_thread = NO_BLOCK_GROUPS/max_threads;

  int no_denied_blocks = 0;
  double start = omp_get_wtime();
#pragma omp parallel num_threads(max_threads)//(max_threads)
  {
    int tid = omp_get_thread_num();
    int start_group = group_per_thread*tid;
    int last_group = start_group + group_per_thread; //THIS IS LIMIT, NOT INCLUDED
    int join_index = start_group * 1024 * 2;
    
    if(tid == (max_threads - 1))
      last_group = NO_BLOCK_GROUPS - 1;
    
    for(int b = start_group; b < last_group; b++){
      
      int target_block_index = b*col_num + target_col_num;
      int timeseries_block_index = b*col_num + ts_col_num;
      int filter_block_index = b*col_num + filter_col_num;
       
      if(dB->db[filter_block_index]->bf_query(filter_value))

	{
	  for(int i = 0; i < 1024; i++){
	    
	    //std::cout << "dB_val: " << dB->db[target_block_index]->somehow_get(i).c_str() << " target_val: " << str_value << std::endl;
	    ///////char* res = dB->db[filter_block_index]->somehow_get(i);
	    std::string response(dB->db[filter_block_index]->somehow_get(i).c_str());
	    
	    if(response == chr_filter_value){ /////// <=====	  
//////	    if(res == chr_filter_value){ /////// <=====	  
	      strcpy(join[join_index], dB->db[target_block_index]->somehow_get(i).c_str());
	   /////   join[join_index] = dB->db[target_block_index]->somehow_get(i);

              strcpy(join[join_index + 1], dB->db[timeseries_block_index]->somehow_get(i).c_str());
	      join_index += 2;
	    }
	  }
	}
      //else//{
	;
	/*std::cout << "Block " << filter_block_index << " denied" << std::endl;
	#pragma omp critical
	{
	no_denied_blocks++;
	}
	}*/
    }
  }
  double end = omp_get_wtime();
  std::cout << "Scan done! (Scan with bf) " << end - start << std::endl;
  //std::cout << "No denied blocks: " << no_denied_blocks << std::endl;
  myfile << std::to_string(end - start) +", "+ "Scan_with_bf" +", "+ std::to_string(thread)  << std::endl;
  myfile.close();
  return join;
}


//template <class T>
char** scan_with_dep_bf(void* db_addr, int* target_col_nums, int ts_col_num, int num_cols, int NO_BLOCK_GROUPS, int col_num, int* filter_col_num, char** dependency_values, char** col_types){

  std::string dependencies[num_cols];

std::cout << "I am here len: " << num_cols << std::endl;

  for(int i = 0; i < num_cols; i++){
    //std::string filter_value(dependency_values[i]);
//    std::cout << i << " : " << dependency_values[i] << std::endl;
    dependencies[i] = dependency_values[i];
  }

std::string types[num_cols];

for (int i=0; i<num_cols; i++){

        types[i] = col_types[filter_col_num[i]];
        std::cout << "Type for Bloom filter of value " << i << " is: "<< types[i] << std::endl;

}


  dataBase* dB = db_addr;
  //******************************************************** HERE **********************************************************//
  //int max_threads = omp_get_max_threads();
  int max_threads = thread;

  std::ofstream myfile;
  myfile.open("cpu_test6.txt",fstream::out | fstream::app);

  int total_elem = NO_BLOCK_GROUPS*1024*2;
  int per_thread = total_elem/max_threads;

  std::cout << "Scanning for: " << target_col_nums[0] << std::endl;
  
  char** join = new char*[NO_BLOCK_GROUPS*1024*2];
  for(int i = 0; i < NO_BLOCK_GROUPS*1024*2; i++){
    join[i] = new char[100];
  }

  int tso = ts_col_num - target_col_nums[0];//TIMESERIES OFFSET
  
  int group_per_thread = NO_BLOCK_GROUPS/max_threads;


//std::cout << dependencies[1] << std::endl;
  double start = omp_get_wtime();
#pragma omp parallel num_threads(max_threads)//(max_threads)
  {
    int tid = omp_get_thread_num();
    int start_group = group_per_thread*tid;
    int last_group = start_group + group_per_thread; //THIS IS LIMIT, NOT INCLUDED
    int join_index = start_group * 1024 * 2;
    
    if(tid == (max_threads - 1))
      last_group = NO_BLOCK_GROUPS - 1;

    
    for(int b = start_group; b < last_group; b++){
        int target_block_indices[num_cols];
	int filter_block_indices[num_cols];
	int timeseries_block_indices; //[num_cols];

	for(int j=0; j< num_cols; j++){
          target_block_indices[j] = b*col_num + target_col_nums[j];
          filter_block_indices[j] = b*col_num + filter_col_num[j];
	  timeseries_block_indices = b*col_num + ts_col_num;

        }
  bool bf_bool = true;
  for(int i = 0; i < num_cols; i++){
    std::string s(dependencies[i]);
    if(types[i] == "uint8_t")
        uint8_t s = (uint8_t)std::stoi(dependencies[i]);
    else if (types[i] == "uint16_t")
        uint16_t s = (uint16_t)std::stoi(dependencies[i]);
    else if (types[i] == "uint32_t")
        uint32_t s = (uint32_t)std::stoi(dependencies[i]);
    else if (types[i] == "double")
        double s = (double)std::stod(dependencies[i]);
    else if (types[i] == "float")
        float s = (float)std::stod(dependencies[i]);
    else if (types[i] == "string")
        std::string s(dependencies[i]);
    else
        std::string s(dependencies[i]);

    if(!dB->db[filter_block_indices[i]]->bf_query(s)){
	bf_bool = false;
	break;
	}

  }
 if (bf_bool)
{
      for(int i = 0; i < 1024; i++){
	//std::cout << "dB_val: " << dB->db[target_block_index]->somehow_get(i).c_str() << " target_val: " << str_value << std::endl;

        std::string responses[num_cols];
/*	char** responses = (char**)malloc(sizeof(char*)*num_cols);
        responses[0] = (char*)malloc(sizeof(char));
        responses[1] = (char*)malloc(sizeof(char));
        responses[2] = (char*)malloc(sizeof(char));
        responses[3] = (char*)malloc(sizeof(char));
*/
        bool check = true;

	for(int k=0; k < num_cols; k++){
	  responses[k] = (dB->db[filter_block_indices[k]]->somehow_get(i).c_str());
////	  responses[k] = dB->db[filter_block_indices[k]]->somehow_get(i);
///	  responsesS[k] = responses[k];

	}

	for (int i=0; i < num_cols; i++)
	  if(responses[i] != dependencies[i]){ ////////////////
	    check = false;
	  }
//#pragma omp critical
	if(check){ 
	 // std::string tmp = dB->db[target_block_indices[0]]->somehow_get(i).c_str();
	  strcpy(join[join_index], dB->db[target_block_indices[0]]->somehow_get(i).c_str());
	      ////join[join_index] = dB->db[target_block_indices[0]]->somehow_get(i);
	 // char * tmp = dB->db[timeseries_block_indices[0]]->somehow_get(i).c_str();
	 // if(!tmp)
	  struct tm tm;
	if (strptime(dB->db[timeseries_block_indices]->somehow_get(i).c_str(), "%Y-%m-%d %H:%M:%S", &tm) && dB->db[timeseries_block_indices]->somehow_get(i).c_str() != ""){
////#pragma omp critical
	  	strcpy(join[join_index + 1], dB->db[timeseries_block_indices]->somehow_get(i).c_str());
 }
	  join_index += 2;

//	  std::cout << join[join_index] << std::endl;
//	  std::cout << join[join_index + 1] << std::endl;
	}
      }
}
    }
  }
  double end = omp_get_wtime();
  //free(dependencies);
  std::cout << "Scan done! (Scan with dep bf) " << end - start << std::endl;

  myfile << std::to_string(end - start) +", "+ "Scan_with_dep_bf" +", "+ std::to_string(thread) <<std::endl;
  myfile.close();
  return join;

}


char** scan_with_dep_bf_decider(void* db_addr, int* target_col_nums, int ts_col_num, int num_cols, int NO_BLOCK_GROUPS, int col_num, int* filter_col_num, char** dependency_values, char** col_types){

/*
  std::string type(col_types[filter_col_num]);
  std::string filter_value(chr_filter_value);
  std::cout << "Type for Bloom filter: " << type << std::endl;
  */
/*
std::string types[num_cols];

for (int i=0; i<num_cols; i++){

	types[i] = col_types[filter_col_num[i]];
	std::cout << "Type for Bloom filter of value " << i << " is: "<< types[i] << std::endl;

}
*/

    return scan_with_dep_bf(db_addr, target_col_nums, ts_col_num, num_cols, NO_BLOCK_GROUPS, col_num, filter_col_num, dependency_values, col_types);

/*

  if(type == "uint8_t"){
    uint8_t val = (uint8_t)std::stoi(filter_value);
    return scan_with_bf(db_addr, target_col_num, ts_col_num, NO_BLOCK_GROUPS, col_num, filter_col_num, chr_filter_value, val);
  }
  else if(type == "uint16_t"){
    uint16_t val = (uint16_t)std::stoi(filter_value);
    return scan_with_bf(db_addr, target_col_num, ts_col_num, NO_BLOCK_GROUPS, col_num, filter_col_num, chr_filter_value, val);
  }
  else if(type == "uint32_t"){
    uint32_t val = (uint32_t)std::stoi(filter_value);
    return scan_with_bf(db_addr, target_col_num, ts_col_num, NO_BLOCK_GROUPS, col_num, filter_col_num, chr_filter_value, val);
  }
  else if(type == "double"){
    double val = std::stod(filter_value);
    return scan_with_bf(db_addr, target_col_num, ts_col_num, NO_BLOCK_GROUPS, col_num, filter_col_num, chr_filter_value, val);
  }
  else if(type == "float"){
    float val = (float)std::stod(filter_value);
    return scan_with_bf(db_addr, target_col_num, ts_col_num, NO_BLOCK_GROUPS, col_num, filter_col_num, chr_filter_value, val);
  }
  else if(type == "string"){
    return scan_with_bf(db_addr, target_col_num, ts_col_num, NO_BLOCK_GROUPS, col_num, filter_col_num, chr_filter_value, filter_value);
  }
  //else if(type == "bool"){
  //  if(filter_value == "true")
  //    return scan_with_bf(db_addr, target_col_num, ts_col_num, NO_BLOCK_GROUPS, col_num, filter_col_num, chr_filter_value, true);
  //  else
  //    return scan_with_bf(db_addr, target_col_num, ts_col_num, NO_BLOCK_GROUPS, col_num, filter_col_num, chr_filter_value, false);
  //    }
  else
    std::cout << "Pre bf caller saw undefined type, embrace the run time error." << std::endl;

  char** adj;
  return adj;
*/
}




char** scan_with_bf_decider(void* db_addr, int target_col_num, int ts_col_num, int NO_BLOCK_GROUPS, int col_num, int filter_col_num, char* chr_filter_value, char** col_types){
  
  std::string type(col_types[filter_col_num]);
  std::string filter_value(chr_filter_value);
  std::cout << "Type for Bloom filter: " << type << std::endl;
  
  if(type == "uint8_t"){
    uint8_t val = (uint8_t)std::stoi(filter_value);
    return scan_with_bf(db_addr, target_col_num, ts_col_num, NO_BLOCK_GROUPS, col_num, filter_col_num, chr_filter_value, val);
  }
  else if(type == "uint16_t"){
    uint16_t val = (uint16_t)std::stoi(filter_value);
    return scan_with_bf(db_addr, target_col_num, ts_col_num, NO_BLOCK_GROUPS, col_num, filter_col_num, chr_filter_value, val);
  }
  else if(type == "uint32_t"){
    uint32_t val = (uint32_t)std::stoi(filter_value);
    return scan_with_bf(db_addr, target_col_num, ts_col_num, NO_BLOCK_GROUPS, col_num, filter_col_num, chr_filter_value, val);
  }
  else if(type == "double"){
    double val = std::stod(filter_value);
    return scan_with_bf(db_addr, target_col_num, ts_col_num, NO_BLOCK_GROUPS, col_num, filter_col_num, chr_filter_value, val);
  }
  else if(type == "float"){
    float val = (float)std::stod(filter_value);
    return scan_with_bf(db_addr, target_col_num, ts_col_num, NO_BLOCK_GROUPS, col_num, filter_col_num, chr_filter_value, val);
  }
  else if(type == "string"){
    return scan_with_bf(db_addr, target_col_num, ts_col_num, NO_BLOCK_GROUPS, col_num, filter_col_num, chr_filter_value, filter_value);
  }
  /*else if(type == "bool"){
    if(filter_value == "true")
      return scan_with_bf(db_addr, target_col_num, ts_col_num, NO_BLOCK_GROUPS, col_num, filter_col_num, chr_filter_value, true);
    else
      return scan_with_bf(db_addr, target_col_num, ts_col_num, NO_BLOCK_GROUPS, col_num, filter_col_num, chr_filter_value, false);
      }*/
  else
    std::cout << "Pre bf caller saw undefined type, embrace the run time error." << std::endl;

  char** adj;
  return adj;
  
}




/*void setAddress(dataBase* db){
	ADDR = db;
}
*/

int* return_accesses(void* db_addr, int col_num, int NO_BLOCK_GROUPS){
  dataBase* dB = db_addr;
  //setAddress(dB);
  return dB->accesses(col_num, NO_BLOCK_GROUPS);
}
  
  

//template<class T>
dataBase* getdb_address(){

	return BLOCKS_ADDR;
}
/*
virtual_dblock* get_Blocks(){

return dbk;
}
*/
/******
THIS WILL REMAIN TRIVIAL FOR SOME MORE TIME
void writeOneRecordChunktoBlocks_better_better(dataBase db, string *record_chunk, type_num_col* indexes, int col_num, int chunk_size, int block_group){ //Threads constantly writes to same blocks, therefore one if - else is enough for 1024 records
#if defined DEBUG
  std::cout << "Block group: " << block_group << " start inserting in parallel" << std::endl;
#endif

  int no_max_threads = omp_get_max_threads();
  std::cout << "Start inserting with " << no_max_threads << " threads " << std::endl;
#pragma omp parallel for num_threads(no_max_threads) schedule(static, 8)
  for(int b = 0; b < col_num*NO_BLOCK_GROUPS; b++){
    auto type_get = db.db[b]->block_ptr[0];
    string val = acquire_val(record_chunk[b], b%col_num);
    auto w_val = (decltype(type_get))val;
    db.db[b]->block_ptr[b%1024] = w_val;
  }
  }
******/

extern "C" char* returnQ_S(char* query){
  char* q = "Hi there you Python";
  return q;
}





#endif
