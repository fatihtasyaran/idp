import connector
import json
from flask import Flask, request, jsonify
from datetime import datetime
from memory_profiler import profile
import time
import random
import hashlib
import _thread
import psutil
import multiprocessing
from threading import Thread
import ctypes
from texttable import Texttable
import GPUtil
import nvidia_smi
from pynvml import *
app = Flask(__name__)
from query_generator import *
import sys
import statistics
import statsmodels.api as sm
import pandas as pd
from sklearn import linear_model
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.ensemble import RandomForestRegressor
import numpy as np
import csv
from multiprocessing import Value
import matplotlib.pyplot as plt
import math

#class queue_info(Structure):
#    _fields_ = [("queue_length", c_int),("min_load", c_int), ("max_load", c_int), ("avg_load", c_int), ("stdev", c_int)]


####BF ON-OFF####
BF = int(sys.argv[2])
####BF ON-OFF####
gpu_op = int(sys.argv[3])
global pu
global execution_time
type = 0

filer = open('/home/fatih/idp/chicago-taxi-rides-2016/chicago_taxi_trips_2016_01.csv')
csv_reader = csv.reader(filer)
types = next(csv_reader)
names = next(csv_reader)

#pu = {"GPU":0, "CPU":0}
pu = [0, 0]
average_durations = 0
no_queries = 0

#################
CPU_THRESHOLD = 10
GPU_THRESHOLD = 30
MAX_CPU_Q = Value('i', 0)
MAX_GPU_Q = Value('i', 0)
queue_length = Value('i',0)
################ 

@app.route('/')
def health_check():
    return 'Healthy'

@app.route('/search', methods=['POST'])
def search():
    return jsonify(connector.names)

@app.route('/tag-keys', methods=['POST'])
def tag_keys():
    print("Ad-hoc asking for columns")
    #k = ["dont","forget","your","keys"]
    return jsonify(connector.names)

@app.route('/tag-values', methods=['POST'])
def tag_values():
    values = ["key", "operator", "value"]
    return jsonify(values)

def hashQuery(query):
    result = hashlib.sha256(query.encode())
    result = result.hexdigest()
    return result[0:8]


class ThreadWithReturnValue(Thread):
    def __init__(self, group=None, target=None, name=None,
                 args=(), kwargs={}, Verbose=None):
        Thread.__init__(self, group, target, name, args, kwargs)
        self._return = None
    def run(self):
        #print(type(self._target))
        if self._target is not None:
            self._return = self._target(*self._args,**self._kwargs)
    def join(self, *args):
        Thread.join(self, *args)
        return self._return

    
def monitor(target):
#    manager = multiprocessing.Manager()
#    return_dict = manager.dict()
    worker_process = mp.Process(target=target, args=(return_dict))
    worker_process.start()
    p = psutil.Process(worker_process.pid)

    # log cpu usage of `worker_process` every 10 ms
    cpu_percents = []
    while worker_process.is_alive():
        cpu_percents.append(p.cpu_percent())
        time.sleep(0.01)

    worker_process.join()
    return cpu_percents, return_dict["datarizer"], return_dict["durations"]

delay = 0

@profile
def split_processing(items, MAX_CPU_Q, MAX_GPU_Q, MIN, AVG, load_list, total_query, dev_decision_parameter, num_splits, timestamp, interval,  queue_length, counter=0, tau=15):

    queue_wait_time = 0.00
    
    split_size = len(items) // num_splits                                       
    threads = []
    
    for i in range(num_splits):                                                 
        # determine the indices of the list this thread will handle             
        start = i * split_size                                                  
        # special case on the last chunk to account for uneven splits           
        end = None if i+1 == num_splits else (i+1) * split_size                 
        # create the threads                                                     

        threads.append(
            multiprocessing.Process(target=process, args=(items, start, end, counter, MAX_CPU_Q, MAX_GPU_Q, MIN, AVG, load_list, total_query, interval, queue_length, timestamp, queue_wait_time, )))
        threads[-1].start() # start the thread we just created
        #time.sleep(delay)

    # wait for all threads to finish                                            
    for t in threads:                                                           
        t.join()


MAX = 0
MIN = 100
AVG = 0
load_list = []
total_query = 0
dev_decision_parameter = "alg"

## The function that each thread will run
@profile
def process(query_list, start, end, counter, MAX_CPU_Q, MAX_GPU_Q, MIN, AVG, load_list, total_query, interval, queue_length, timestamp, queue_wait_time):

    ## =============== CREATING AND FITTING THE LR MODELS ================== ##
    
    col_names = ['cpu_usage','cpu_temp','cpu_mem','Q2','Q1','q_length','queue_wait_time','execution_time','max_cpu','max_gpu','interval','device','timestamp']
#    col_names = ['cpu_usage','cpu_temp','Q2','Q1','execution_time']
    Data= pd.read_csv("/home/anes/urgent/idp/GPU_/cpu_train_ml_3_4.csv",header=0, usecols=col_names, names=col_names)
#    feature_cols = ['cpu_usage','cpu_temp','cpu_mem','Q2','Q1','q_length','max_cpu', 'max_gpu', 'interval']
    feature_cols = ['cpu_usage','cpu_temp','cpu_mem','Q2','Q1','q_length','max_cpu','interval']
#    feature_cols = ['gpu_usage','gpu_temp','gpu_mem','cpu_usage','cpu_temperature','Q2','Q1','max_cpu','max_gpu','interval']

    X = Data[feature_cols]
    y = Data['execution_time']

    X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=0.2,random_state=1)

    ## Linear Regression and Fit the Data for CPU
    #regr = linear_model.LinearRegression()
    #regr.fit(X_train,y_train)

    ## RandomForestRegressor
    regressor = RandomForestRegressor(n_estimators=100, random_state=0)
    regressor.fit(X_train, y_train)

    
    print('Score (R-Squared) CPU:',regressor.score(X_test,y_test))
    #print('Intercept (Constant): ', regr.intercept_)
    #print('Coefficients: ', regr.coef_,"\n")
    #coef_list_cpu = regr.coef_

## ============================== END OF CPU MODEL ============================= ##
## ============================== START OF GPU MODEL =========================== ##
    
    col_names_gpu = ['gpu_usage','gpu_temp','gpu_mem','Q2','Q1','q_length','queue_wait_time','execution_time','max_gpu','max_cpu','interval','device','timestamp']
#    col_names_gpu = ['gpu_usage','gpu_temp','Q2','Q1','execution_time']
    Data_gpu= pd.read_csv("/home/anes/urgent/idp/GPU_/gpu_train_ml_3_4.csv",header=0, names=col_names_gpu)
##    feature_cols = ['cpu_usage','cpu_temp','Q2','Q1','max_gpu','max_cpu','interval']

    feature_cols_gpu = ['gpu_usage','gpu_temp','gpu_mem','Q2','Q1','q_length','max_gpu','interval']
##    feature_cols_gpu = ['gpu_usage','gpu_temp','gpu_mem','Q2','Q1','q_length','max_gpu', 'max_cpu', 'interval']

    X_g = Data_gpu[feature_cols_gpu]
    y_g = Data_gpu['execution_time']

    X_train_g, X_test_g, y_train_g, y_test_g = train_test_split(X_g,y_g,test_size=0.2,random_state=1)

    ## Linear Regression and Fit the Data for GPU
    #regr2 = linear_model.LinearRegression()
    #regr2.fit(X_train_g,y_train_g)

    ## RandomForestRegressor
    regressor2 = RandomForestRegressor(n_estimators=100, random_state=0)
    regressor2.fit(X_train_g, y_train_g)
    
    print('Score (R-Squared) GPU:',regressor2.score(X_test_g,y_test_g))
    #print('Intercept (Constant): ', regr2.intercept_)
    #print('Coefficients: ', regr2.coef_,"\n")
    #coef_list_gpu = regr2.coef_
    #response_time_gpu = regr2.intercept_

    q_length = 0
## ============================== END OF CPU MODEL ============================= ##

## ============================== Beginning of the loop ======================== ##    
    ## This loop will be shared between threads and each thread will run its own portion of the query list
    for query in query_list[start:end]:
        counter += 1
        print("QUERY*******",counter)
        info_struct = connector.GET_QUEUE_INFO()
        final_info = ctypes.cast(info_struct, ctypes.POINTER(connector.queue_info))
        condition = final_info.contents.queue_length

        num_threads = 8       
#        if(condition == 0 or condition != 0):
        adhoc_targets = []
        adhoc_keys = []
        adhoc_values = []

        ## Parsing the query for Grafana
        for i in range(0,len(query),2):
            adhoc_key = query[i]
            adhoc_operator = "="
            if len(query) != 1:
                adhoc_value = query[i+1]
                adhoc_values.append(adhoc_value)
                q_id = hashQuery(adhoc_key+adhoc_value)
                #print("Adhoc:" , "key->", adhoc_key, "operator->", adhoc_operator, "value->", adhoc_value)
            else:
                adhoc_values.append("")
                q_id = hashQuery(adhoc_key)
                #print("Adhoc:" , "key->", adhoc_key)
            adhoc_keys.append(adhoc_key)
#           adhoc_values.append(adhoc_value)
                

        start = time.clock_gettime(1)


        if(len(query)==1):
            q_type = 1
        elif len(query) == 2:
            q_type = 2
        else:
            q_type = 3
            
#        try:

            ## Getting the queue info and cast the result to a python struct
#            info_thread = ThreadWithReturnValue(target=connector.GET_QUEUE_INFO, args=( ))
#            info_thread.start()
#            info_struct = info_thread.join()
#            final_info = ctypes.cast(info_struct, ctypes.POINTER(connector.queue_info))

                
#            gpu_thread = ThreadWithReturnValue(target=connector.GET_GPU_INFO, args=( ))
#            gpu_thread.start()
#            gpu_struct = gpu_thread.join()
#            gpu_info_s = ctypes.cast(gpu_struct, ctypes.POINTER(connector.gpu_info))

#        except ValueError:
#            print("problem with getting the queue info thread")


#        gpu_usage = gpu_info_s.contents.gpu_usage
#        gpu_temp = gpu_info_s.contents.gpu_temp
        

        q_id = hashQuery(query[0])
        id_type = q_id +"-"+ str(q_type)
   
        ## Get the queue current info
        info_thread = ThreadWithReturnValue(target=connector.GET_QUEUE_INFO, args=( ))
        info_thread.start()
        info_struct = info_thread.join()
        final_info = ctypes.cast(info_struct, ctypes.POINTER(connector.queue_info))
#        q_length = final_info.contents.queue_length
        
        
        ## Receive the GPU info from C++ api functions
        gpu_thread = ThreadWithReturnValue(target=connector.GET_GPU_INFO, args=( ))
        gpu_thread.start()
        gpu_struct = gpu_thread.join()
        gpu_info_s = ctypes.cast(gpu_struct, ctypes.POINTER(connector.gpu_info))
        
        ## Get the CPU and GPU informations
        cpu_usage = psutil.cpu_percent()
        cpu_temperature = psutil.sensors_temperatures()["coretemp"][0][1]
        cpu_mem = psutil.virtual_memory().percent
        gpu_usage = gpu_info_s.contents.gpu_usage
        gpu_temp = gpu_info_s.contents.gpu_temp
        gpu_mem = gpu_info_s.contents.gpu_mem
        
        ## lists to be used in response time predictions in CPU and GPU LR models 
        
        ##response_time_cpu = regr.intercept_
        ##response_time_gpu = regr2.intercept_

        if len(query) == 1:
            q2, q1 = 0, 1
        elif len(query) == 2:
            q2, q1 = 1, 0
        else:
            q2, q1 = 0, 0
             
        predict_array_cpu = [[cpu_usage , cpu_temperature , q2 , q1 , cpu_mem , q_length , MAX_CPU_Q.value , timestamp]]
        predict_array_gpu = [[gpu_usage , gpu_temp , q2 , q1 , gpu_mem , q_length , MAX_GPU_Q.value , timestamp]]

#        predict_array_cpu = [cpu_usage] + [cpu_temperature] + [q2] + [q1] + [cpu_mem] + [q_length] + [MAX_CPU_Q.value] + [timestamp]
#        predict_array_gpu = [gpu_usage] + [gpu_temp] + [q2] + [q1] + [gpu_mem] + [q_length] + [MAX_GPU_Q.value] + [timestamp]

        ## Predicting
        #for i in range(len(coef_list_cpu)):
        #    response_time_cpu += coef_list_cpu[i] * predict_array_cpu[i]
        #    response_time_gpu += coef_list_gpu[i] * predict_array_gpu[i]

        ## Taking decision ML

#        predict_cpu = regressor.predict(predict_array_cpu)[0]
#        predict_gpu = regressor2.predict(predict_array_gpu)[0]

        #print(response_time_cpu, "|", response_time_gpu)
#        print(predict_cpu, "|", predict_gpu)
#        if predict_cpu < predict_gpu:
#            dev_current = 2
#            dev_decision = 2
#        else:
#            dev_current = 3
#            dev_decision = 3

        ## Algo 2
        if (cpu_usage-gpu_usage) > 25:
            dev_current = 3
        elif (gpu_usage-cpu_usage) > 25:
            dev_current = 2
        elif gpu_usage == cpu_usage :
            if len(query) == 1:
                dev_current = 3
            else:
                dev_current = 2
        else:
            dev_current = 2


	## CPU / GPU
#        dev_current = random.randint(2, 3)#3
#        dev_decision = dev_current


        ## Flag to keep track when queries enters the queue
        entered_q = False
        #if MAX_CPU_Q.value <= 5:
        #if MAX_GPU_Q.value <= 6 and gpu_mem < 40:

        info_thread = ThreadWithReturnValue(target=connector.GET_QUEUE_INFO, args=( ))
        info_thread.start()
        info_struct = info_thread.join()
        final_info = ctypes.cast(info_struct, ctypes.POINTER(connector.queue_info))
#        q_length = final_info.contents.queue_length
        
        print("##############STATS =", q_length, cpu_usage, gpu_usage, "#################")
        ## Check whether there are queries in the queue first
        if q_length > 0 or cpu_usage > 10 or gpu_usage > 10: #or MAX_GPU_Q.value > 30 or MAX_CPU_Q.value > 20:
            if q_length > 0:
                call_rslt = connector.GET_NEXT_QUERY()
                next_query = ctypes.cast(call_rslt, ctypes.POINTER(connector.query_struct)) ## QUERY TO BE PROCESSED
 
#                ## I used the dict structure in python to store the query, its type, and its size (how many queried values)
                query_to_be_processed = {'query':next_query, 'type':next_query.contents.query_type, 'size': next_query.contents.size}
                if query_to_be_processed['type'] == 3:
                    c_cols = (next_query.contents.columns).decode().split(", ")
                    c_vals = next_query.contents.values.decode().split(", ")
#                
                elif query_to_be_processed['type']  == 2:
                    c_vals = next_query.contents.values.decode().split(", ")[0]
                    c_cols = (next_query.contents.columns).decode().split(", ")[0]

                else:
                    c_cols = (next_query.contents.columns).decode().split(", ")[0]
                
                q_ent_time = datetime.strptime((next_query.contents.timestamp).decode(), '%H:%M:%S')
                current_time = datetime.strptime(datetime.now().strftime("%H:%M:%S"), '%H:%M:%S')
                
                queue_wait_time = current_time - q_ent_time
                print(q_ent_time, current_time)
                dev_decision = next_query.contents.decision
                if dev_decision == 1:
                    dev_decision += 1
                    dev_current = dev_decision

                
#                print("###########################DEV DECISION########################", dev_decision)

                #dev_current = dev_decision
#                if queue_length.value > 0:
#                    queue_length.value -= 1
                
                entered_q = True
            ## Inserting the current "query" to queue
                
#            try:

            add_qry_thread = ThreadWithReturnValue(target=connector.INSERT_QUERY, args=(q_id.encode('utf-8').strip(), q_type, adhoc_keys, adhoc_values, dev_current, str(datetime.now().strftime("%H:%M:%S")).encode('utf-8').strip(),))
            add_qry_thread.start()
            add_qry_thread.join()
                
#            except ValueError:
##                print("problem with inserting query thread")


 #       elif dev_current == 2:
        if dev_current == 2:
            dev_decision = 2
            #MAX_CPU_Q.value += 1
            if cpu_usage <= 20:
                num_threads = 8
            elif 20 < cpu_usage <= 40:
                num_threads = 4
            elif 40 < cpu_usage <= 80:
                num_threads = 2
            else:
                num_threads = 1

        else:
            dev_decision = 3
            #MAX_GPU_Q.value += 1

#            if gpu_usage > 15:
#                ## Start measuring the waiting time in the queue
#                start_q = time.clock_gettime(1)
#                entered_q = True
#                queue_length.value += 1
#                ## Inserting the query to queue
#                try:

#                    add_qry_thread = ThreadWithReturnValue(target=connector.INSERT_QUERY, args=(q_id.encode('utf-8').strip(), q_type, adhoc_keys, adhoc_values, dev_decision, str(datetime.now().strftime("%H:%M:%S")).encode('utf-8').strip() ,))

#                    add_qry_thread.start()
#                    add_qry_thread.join()

#                except ValueError:
#                    print("problem with inserting query thread")

        if q_length == 0:
#        if entered_q == False:
            if len(query)==1:
                query_to_be_processed = {'query' : query, 'type' : 1, 'size' : 0}
                c_cols = query[0]
            elif len(query)==2:
                query_to_be_processed = {'query' : query, 'type' : 2, 'size' : 1}
                c_cols = query[0]
                c_vals = query[1]
            else:
                query_to_be_processed = {'query' : query, 'type' : 3, 'size' : len(query)//2}
                c_cols = []
                c_vals = []
                for i in range(0, len(query), 2):
                    c_cols.append(names.index(query[i]))
                    c_vals.append(query[i+1])
                    
                        
#            c_cols = query[0]

        gpu, cpu = False, False
        ## Execute the query based on the decision 
        if(dev_current == 3):  # GPU
            MAX_GPU_Q.value +=1
            start = time.clock_gettime(1)
            pu[0] += 1
            manager = multiprocessing.Manager()
            return_dict = manager.dict()
            gpu = True
#                if q_length > 0: ## WORK ON QUERY TO BE PROCESSED
            if (query_to_be_processed['type'] == 1):
                connector.scan_gpu(c_cols, connector.timeseries, return_dict)
                    

            elif (query_to_be_processed['type'] == 2):
                connector.scan_gpu_with_dependency(c_cols, c_cols, c_vals, connector.timeseries, return_dict)
                    
                
            elif (query_to_be_processed['type'] == 3):
                if (query_to_be_processed['size'] == 2):
                    connector.scan_gpu_mult_dep(c_cols, c_vals, connector.timeseries, return_dict)
                        
                elif (query_to_be_processed['size'] == 3):
                    connector.scan_gpu_mult_dep2(c_cols, c_vals, connector.timeseries, return_dict)
                    
                elif (query_to_be_processed['size'] == 4):
                    connector.scan_gpu_mult_dep3(c_cols, c_vals, connector.timeseries, return_dict)


            if MAX_GPU_Q.value > 0:
                MAX_GPU_Q.value -= 1

                
            execution_time = time.clock_gettime(1) - start
            print("The query's execution time is: ", execution_time)     
            gpu_usage = gpu_info_s.contents.gpu_usage
            gpu_temp = gpu_info_s.contents.gpu_temp


        else: #elif(dev_decision == 2): #CPU
            MAX_CPU_Q.value += 1
            start = time.clock_gettime(1)
            pu[1] += 1
            manager = multiprocessing.Manager()
            return_dict = manager.dict()
            cpu = True
            #print("###############################################\n####################################")

            if (query_to_be_processed['type'] == 1):
                connector.scan(c_cols, connector.timeseries, return_dict, num_threads)
                
            elif (query_to_be_processed['type'] == 2):
                connector.scan_with_filter(c_cols, c_vals, return_dict, num_threads)
                    
            elif (query_to_be_processed['type'] == 3):
                connector.scan_with_dependencies(c_cols, c_vals, connector.timeseries, return_dict, num_threads)


            if MAX_CPU_Q.value > 0:
                MAX_CPU_Q.value -=1        

                    
            cpu_usage = psutil.cpu_percent()
            cpu_temperature = psutil.sensors_temperatures()["coretemp"][0][1]
            
            execution_time = time.clock_gettime(1) - start
            print("The query's execution time is: ", execution_time)

        

#                        elif (len(next_query.contents.values.decode().split(", "))==1):    


        if(dev_current==3): # and gpu:
#            if entered_q:
            if isinstance(queue_wait_time, float) == False:
            #if queue_wait_time.total_seconds() > 0.0:
                q_wait = queue_wait_time.total_seconds()
                response_time = execution_time + q_wait
                queue_wait_time = 0.0
            else:
                q_wait = 0.0
                response_time = execution_time

            t = Texttable()
            t.add_rows([["x_1","Device","GPU"],["x_2","Device Utilization (%)",gpu_usage],["x_3","Number of jobs in the queue",q_length], ["x_4","GPU memory",gpu_mem],["x_8","Query Type",q_type],["x_9","Response Time (s)",response_time],["x_10","Device Temperature (C)",gpu_temp], ["x_12", "Delay", delay], ["x_13", "Queries in CPU", MAX_CPU_Q.value], ["x_14", "Queries in GPU", MAX_GPU_Q.value], ["x_15","Interval", timestamp], ["x_16","Threads", num_threads]])

            print(t.draw())
            print("\n")
            txt_file_name = "gpu_RF_train_30_1.csv"#"gpu_ML_train_45_0.csv"#"gpu_train_ml_3_4.csv"#"gpu_ML_train.csv"
#            if q_type == 1:
            txt_data2 = str(gpu_usage)+","+str(gpu_temp)+","+str(gpu_mem)+","+str(q2)+","+str(q1)+","+str(q_length)+","+str(q_wait)+","+str(response_time)+","+str(MAX_CPU_Q.value)+","+str(MAX_GPU_Q.value)+","+str(timestamp)+","+"1"+","+str(interval)+"\n"
#            elif q_type==2:
#                txt_data2 = str(gpu_usage)+","+str(gpu_temp)+","+str(gpu_mem)+","+"1"+","+"0"+","+str(q_length)+","+str(queue_wait_time)+","+str(execution_time)+","+str(MAX_CPU_Q.value)+","+str(MAX_GPU_Q.value)+","+str(timestamp)+","+"1"+","+str(interval)+"\n"
#            elif (q_type==3 ):
#                txt_data2 = str(gpu_usage)+","+str(gpu_temp)+","+str(gpu_mem)+","+"0"+","+"0"+","+str(q_length)+","+str(queue_wait_time)+","+str(execution_time)+","+str(MAX_CPU_Q.value)+","+str(MAX_GPU_Q.value)+","+str(timestamp)+","+"1"+","+str(interval)+"\n"
                
            #txt_data ="GPU" +","+str(gpu_usage)+","+str(q_type)+","+str(execution_time)+","+str(gpu_temp)+"\n"
            
            
        else: #elif(dev_current ==2): # and cpu:
#            if entered_q > 0:
            if isinstance(queue_wait_time, float) == False:
#            if queue_wait_time > 0.0:
                q_wait = queue_wait_time.total_seconds()
                response_time = execution_time + q_wait
                queue_wait_time = 0.0
            else:
                q_wait = 0.0
                response_time = execution_time
                
            t = Texttable()
            t.add_rows([["x_1","Device","CPU"],["x_2","Device Utilization (%)",cpu_usage],["x_3","Number of jobs in the queue",q_length], ["x_4","GPU memory",gpu_mem],["x_8","Query Type",q_type],["x_9","Response Time (s)",response_time],["x_10","Device Temperature (C)",cpu_temperature],["x_12", "Delay", delay], ["x_13", "Queries in CPU", MAX_CPU_Q.value], ["x_14", "Queries in GPU", MAX_GPU_Q.value],["x_15","Interval",timestamp], ["x_16","Threads", num_threads]])

            print(t.draw())
            print("\n")
            txt_file_name = "cpu_RF_train_30_1.csv"#"cpu_ML_train_45_0.csv"#"cpu_train_ml_3_4.csv" #"cpu_ML_train.csv"
#            if(q_type == 1):

            txt_data2 = str(cpu_usage)+","+str(cpu_temperature)+","+str(cpu_mem)+","+str(q2)+","+str(q1)+","+str(q_length)+","+str(q_wait)+","+str(response_time)+","+str(MAX_CPU_Q.value)+","+str(MAX_GPU_Q.value)+","+str(timestamp)+","+"0"+","+str(interval)+"\n"
#            elif(q_type==2):

#                txt_data2 = str(cpu_usage)+","+str(cpu_temperature)+","+str(cpu_mem)+","+"1"+","+"0"+","+str(q_length)+","+str(queue_wait_time)+","+str(execution_time)+","+str(MAX_CPU_Q.value)+","+str(MAX_GPU_Q.value)+","+str(timestamp)+","+"0"+","+str(interval)+"\n"
#            elif(q_type==3):

#                txt_data2 = str(cpu_usage)+","+str(cpu_temperature)+","+str(cpu_mem)+","+"0"+","+"0"+","+str(q_length)+","+str(queue_wait_time)+","+str(execution_time)+","+str(MAX_CPU_Q.value)+","+str(MAX_GPU_Q.value)+","+str(timestamp)+","+"0"+","+str(interval)+"\n"
            #txt_data = "CPU" +","+str(cpu_usage)+","+str(q_type)+","+str(execution_time)+","+str(cpu_temperature)+"\n"

        mlr_data = open(txt_file_name, "a")
        mlr_data.write(txt_data2)
        mlr_data.close()
#/        if(dev_decision_parameter =="alg"):
#/             f_lb=open("test_algo2_tmp.txt","a")
             #f_lb = open("tau_alg2_2user_.txt","a")
#            f_lb = open("lb_alg_new_bf.txt","a")
#/        elif(dev_decision_parameter =="rand"):
#/            f_lb = open("test_rand_tmp.txt","a")
            #f_lb = open("tau_rand_1user_.txt","a")
#            f_lb = open("lb_rand_new_bf.txt","a")
#/        f_lb.write(txt_data)
#/        f_lb.close()

        print("QUERY -------->",query_list.index(query))
        total_query += 1

#        info_thread = ThreadWithReturnValue(target=connector.GET_QUEUE_INFO, args=( ))
#        info_thread.start()
#        info_struct = info_thread.join()
#        final_info = ctypes.cast(info_struct, ctypes.POINTER(connector.queue_info))
#        q_length = final_info.contents.queue_length

        print("====================== Q_LENGTH =====================", q_length)
@app.route('/query', methods=['POST'])
def query():
   
    #query_list = []
    #query_list = query_gen()
    MAX = 0
    MIN = 100
    AVG = 0
    load_list = []
    total_query = 0
    tau = 15
    g_start = time.clock_gettime(1)
    dev_decision_parameter = "alg" # "rand" or "alg"

    stop = False
    #interval = random.uniform(0.1, 0.9)
    #interval = -0.1*math.log(interval)
#    for i in range(40):
    while not stop:
#        if stop:
#            break
        #time.sleep(interval)
        query_choice = random.randint(1,2)

        if query_choice == 1:
            query_list = gen_query()
        else:
#            query_list = query_gen(350)
            query_list = query_gen(random.randrange(1, 1000))
        if (total_query + len(query_list)) > 14000:
            query_list = query_gen(14000-total_query)
            stop = True
        if len(query_list) <= 10:
            num_splits = len(query_list)
        else:
            num_splits = 30
            
        u = random.uniform(0, 1)
        interval = -1.0 * math.log(u)
        total_query += len(query_list)
        # Generate a random number from an exponential distribution log(u)*(-0.1) when Y ~~ Unif(0,1)
        timestamp = datetime.now()
        split_processing(query_list, MAX_CPU_Q, MAX_GPU_Q, MIN, AVG, load_list, total_query, dev_decision_parameter, num_splits, interval, timestamp, queue_length, counter=0, tau=15)
        time.sleep(interval)
        
    g_finish= time.clock_gettime(1) - g_start
    ## Stores the total time of running all the queries
    if(dev_decision_parameter =="alg"):
        ##f_lb = open("new_algo2_BS.txt","a")
        f_lb = open("new_alg2_times.txt","a")
    elif(dev_decision_parameter =="rand"):
        ##f_lb = open("new_rand_1user.txt","a")
        f_lb = open("new_rand_times.txt","a")
    finish = str(tau) + "," + str(g_finish) +","+str(total_query)+ "\n"
    f_lb.write(finish)
    f_lb.close()

@app.route('/annotations', methods=['POST'])
def annotations():
    req = request.get_json()
    data = [
        {
            "annotation": 'This is the annotation',
        "time": (convert_to_time_ms(req['range']['from']) +
                 convert_to_time_ms(req['range']['to'])) / 2,
            "title": 'Deployment notes',
            "tags": ['tag1', 'tag2'],
            "text": 'Hm, something went wrong...'
        }
    ]
    return jsonify(data)

if __name__ == '__main__':
    app.run(host='127.0.0.1', port=4001, debug=True)
    #for i in range(5):
    #query()

