#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include "string.h"
#include <fstream>
#include <numeric>
#include <unistd.h>
#include <vector>
#include <fstream>
#include <cstring>
#include <sstream>
#include <boost/algorithm/string.hpp>
#include <queue>

using std::queue;
using std::string;

float getAvgExecTime(char* filename);
void init();
double getCurrentValue();

std::vector<size_t> get_cpu_times();

bool get_cpu_times(size_t &idle_time, size_t &total_time);

float time_calculation(queue<string> q, string type,string query);

void setQType(char* QueryType);

int decide(queue<string> cpu_queue, queue<string> gpu_queue,int query);

int deneme();

