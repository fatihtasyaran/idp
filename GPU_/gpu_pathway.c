#include "gpu_wrapper.cu"
//#include "LoadBalancer.cpp"
//#include "data_block.h"

char**  GPU_scan_DB(dataBase* db_addr, char** target_col_num, int ts_col_num, int NO_BLOCK_GROUPS, int col_num, char* col_type, char** timeseries);
//char**  GPU_scan_DB(dataBase* db_addr, int target_col_num, int ts_col_num, int NO_BLOCK_GROUPS, int col_num, char* col_type, char** timeseries);
char**  GPU_scan_DB_DEP(dataBase* db_addr, char** target_col_num, int ts_col_num, int NO_BLOCK_GROUPS, int col_num, int filter_col_num, char* dependency_value, char** timeseries, char* col_type);
//char**  GPU_scan_DB_DEP(dataBase* db_addr, int target_col_num, int ts_col_num, int NO_BLOCK_GROUPS, int col_num, int filter_col_num, char* dependency_value, char** timeseries, char* col_type);
char**  GPU_scan_mult_DEP(dataBase* db_addr, int* target_col_num, char** target1, char** target2, int num_cols, int NO_BLOCK_GROUPS, int col_num, char** dependency_values, char** timeseries);
//char**  GPU_scan_mult_DEP(dataBase* db_addr, int* target_col_num, int num_cols, int NO_BLOCK_GROUPS, int col_num, char** dependency_values, char** timeseries);
char**  GPU_scan_mult_DEP2(dataBase* db_addr, int* target_col_num, char** target1, char** target2, char** target3, int num_cols, int NO_BLOCK_GROUPS, int col_num, char** dependency_values, char** timeseries);
//char**  GPU_scan_mult_DEP2(dataBase* db_addr, int* target_col_num, int num_cols, int NO_BLOCK_GROUPS, int col_num, char** dependency_values, char** timeseries);
char**  GPU_scan_mult_DEP3(dataBase* db_addr, int* target_col_num, char** target1, char** target2, char** target3, char** target4, int num_cols, int NO_BLOCK_GROUPS, int col_num, char** dependency_values, char** timeseries);
//char**  GPU_scan_mult_DEP3(dataBase* db_addr, int* target_col_num, int num_cols, int NO_BLOCK_GROUPS, int col_num, char** dependency_values, char** timeseries);
char**  GPU_scan_mult_DEP4(dataBase* db_addr, int* target_col_num, int num_cols, int NO_BLOCK_GROUPS, int col_num, char** dependency_values, char** timeseries);

//int decision_wrapper(queue<string> cpu_queue, queue<string> gpu_queue, int query, int query_type);

//char** GPU_scan_DB(dataBase* db_addr, int target_col_num, int ts_col_num, int NO_BLOCK_GROUPS, int col_num, char* col_type, char** timeseries){
char** GPU_scan_DB(dataBase* db_addr, char** target_col_num, int ts_col_num, int NO_BLOCK_GROUPS, int col_num, char* col_type, char** timeseries){

  return GPU_scan_db(db_addr, target_col_num, ts_col_num, NO_BLOCK_GROUPS, col_num, col_type, timeseries);

}

char** GPU_scan_DB_DEP(dataBase* db_addr, char** target_col_num, int ts_col_num, int NO_BLOCK_GROUPS, int col_num, int filter_col_num, char* dependency_value, char** timeseries, char* col_type){

  return GPU_scan_db_dep(db_addr,  target_col_num, ts_col_num, NO_BLOCK_GROUPS, col_num, filter_col_num, dependency_value, timeseries, col_type);

}


//char** GPU_scan_mult_DEP(dataBase* db_addr, int* target_col_num, int num_cols, int NO_BLOCK_GROUPS, int col_num, char** dependency_values, char** timeseries){
char** GPU_scan_mult_DEP(dataBase* db_addr, int* target_col_num, char** target1, char** target2, int num_cols, int NO_BLOCK_GROUPS, int col_num, char** dependency_values, char** timeseries){

  return multi_gpu_query(db_addr, target_col_num, target1, target2, num_cols, NO_BLOCK_GROUPS, col_num, dependency_values, timeseries); 

}

//char** GPU_scan_mult_DEP2(dataBase* db_addr, int* target_col_num, int num_cols, int NO_BLOCK_GROUPS, int col_num, char** dependency_values, char** timeseries){
char** GPU_scan_mult_DEP2(dataBase* db_addr, int* target_col_num,  char** target1, char** target2, char** target3, int num_cols, int NO_BLOCK_GROUPS, int col_num, char** dependency_values, char** timeseries){

  return multi_gpu_query2(db_addr, target_col_num,  target1, target2, target3, num_cols, NO_BLOCK_GROUPS, col_num, dependency_values, timeseries); 

}

//char** GPU_scan_mult_DEP3(dataBase* db_addr, int* target_col_num, int num_cols, int NO_BLOCK_GROUPS, int col_num, char** dependency_values, char** timeseries){
char** GPU_scan_mult_DEP3(dataBase* db_addr, int* target_col_num, char** target1, char** target2, char** target3, char** target4, int num_cols, int NO_BLOCK_GROUPS, int col_num, char** dependency_values, char** timeseries){

  return multi_gpu_query3(db_addr, target_col_num, target1, target2, target3, target4, num_cols, NO_BLOCK_GROUPS, col_num, dependency_values, timeseries); 

}

char** GPU_scan_mult_DEP4(dataBase* db_addr, int* target_col_num, int num_cols, int NO_BLOCK_GROUPS, int col_num, char** dependency_values, char** timeseries){

  return multi_gpu_query4(db_addr, target_col_num, num_cols, NO_BLOCK_GROUPS, col_num, dependency_values, timeseries); 

}


/*
int decision_wrapper(queue<string> cpu_queue, queue<string> gpu_queue,int query, int query_type){

	return decide(cpu_queue, gpu_queue, query, query_type);

}
*/

/*extern "C"*/ 
//char** pathway(dataBase* db_addr, int target_col_num, int ts_col_num, int NO_BLOCK_GROUPS, int col_num, char* col_type, char** timeseries){
char** pathway(dataBase* db_addr, char** target_col_num, int ts_col_num, int NO_BLOCK_GROUPS, int col_num, char* col_type, char** timeseries){
  return GPU_scan_DB(db_addr, target_col_num, ts_col_num, NO_BLOCK_GROUPS, col_num, col_type, timeseries);
}

//char** pathway_query(dataBase* db_addr, int target_col_num, int ts_col_num, int NO_BLOCK_GROUPS, int col_num, int filter_col_num, char* dependency_value, char** timeseries, char* col_type){
char** pathway_query(dataBase* db_addr, char** target_col_num, int ts_col_num, int NO_BLOCK_GROUPS, int col_num, int filter_col_num, char* dependency_value, char** timeseries, char* col_type){
  return GPU_scan_DB_DEP(db_addr,  target_col_num, ts_col_num, NO_BLOCK_GROUPS, col_num, filter_col_num, dependency_value, timeseries, col_type);
}

//char** pathway_mult_query(dataBase* db_addr, int* target_col_num, int num_cols, int NO_BLOCK_GROUPS, int col_num, char** dependency_values, char** timeseries){
char** pathway_mult_query(dataBase* db_addr, int* target_col_num, char** target1, char** target2, int num_cols, int NO_BLOCK_GROUPS, int col_num, char** dependency_values, char** timeseries){
  return GPU_scan_mult_DEP(db_addr, target_col_num, target1, target2, num_cols, NO_BLOCK_GROUPS, col_num, dependency_values, timeseries);
}

//char** pathway_mult_query2(dataBase* db_addr, int* target_col_num, int num_cols, int NO_BLOCK_GROUPS, int col_num, char** dependency_values, char** timeseries){
char** pathway_mult_query2(dataBase* db_addr, int* target_col_num,  char** target1, char** target2, char** target3, int num_cols, int NO_BLOCK_GROUPS, int col_num, char** dependency_values, char** timeseries){
  return GPU_scan_mult_DEP2(db_addr, target_col_num, target1, target2, target3, num_cols, NO_BLOCK_GROUPS, col_num, dependency_values, timeseries);
}

//char** pathway_mult_query3(dataBase* db_addr, int* target_col_num, int num_cols, int NO_BLOCK_GROUPS, int col_num, char** dependency_values, char** timeseries){
char** pathway_mult_query3(dataBase* db_addr, int* target_col_num,  char** target1, char** target2, char** target3, char** target4 ,int num_cols, int NO_BLOCK_GROUPS, int col_num, char** dependency_values, char** timeseries){
  return GPU_scan_mult_DEP3(db_addr, target_col_num, target1, target2, target3, target4, num_cols, NO_BLOCK_GROUPS, col_num, dependency_values, timeseries);
}

char** pathway_mult_query4(dataBase* db_addr, int* target_col_num, int num_cols, int NO_BLOCK_GROUPS, int col_num, char** dependency_values, char** timeseries){
  return GPU_scan_mult_DEP4(db_addr, target_col_num, num_cols, NO_BLOCK_GROUPS, col_num, dependency_values, timeseries);
}

