from texttable import Texttable
import sys

arg = int(sys.argv[1])
if(arg == 1):
    txt_name = "lb_alg_new.txt"
elif(arg==2):
    txt_name = "lb_rand_new.txt"

f = open(txt_name,"r")
line_list = f.readlines()
f.close()

cpu_1_count = 0
cpu_2_count = 0
cpu_3_count = 0

gpu_1_count = 0
gpu_2_count = 0
gpu_3_count = 0

total_time_cpu_1 = 0.0
total_time_cpu_2 = 0.0
total_time_cpu_3 = 0.0
total_time_gpu_1 = 0.0
total_time_gpu_2 = 0.0
total_time_gpu_3 = 0.0

total_count = 0

for line in line_list:
    line.strip()
    if "," in line:
        split_list = line.split(",")
        if split_list[0].strip() == "2":        # Device
            if split_list[7].strip() == "1":  # Query type
                total_time_cpu_1 += float(split_list[7])
                cpu_1_count += 1
            elif split_list[6].strip() == "2":      # Query Type
                total_time_cpu_2 += float(split_list[7])
                cpu_2_count += 1
            elif split_list[6].strip() == "3":      # Query Type      
                total_time_cpu_3 += float(split_list[7])
                cpu_3_count += 1
        elif split_list[0].strip() == "GPU":
            if split_list[6].strip() == "1":
                total_time_gpu_1 += float(split_list[7])
                gpu_1_count += 1                
            elif split_list[6].strip() == "2":
                total_time_gpu_2 += float(split_list[7])
                gpu_2_count += 1                
            elif split_list[6].strip() == "3":            
                total_time_gpu_3 += float(split_list[7])
                gpu_3_count += 1                
        total_count += 1
try:
    cpu_q1_avg = total_time_cpu_1 / cpu_1_count
except ZeroDivisionError:
    cpu_q1_avg = 0

try:
    cpu_q2_avg = total_time_cpu_2 / cpu_2_count
except ZeroDivisionError:
    cpu_q2_avg = 0

try:
    cpu_q3_avg = total_time_cpu_3 / cpu_3_count
except ZeroDivisionError:
    cpu_q3_avg = 0
    
try:
    gpu_q1_avg = total_time_gpu_1 / gpu_1_count
except ZeroDivisionError:
    gpu_q1_avg = 0

try:
    gpu_q2_avg = total_time_gpu_2 / gpu_2_count
except ZeroDivisionError:
    gpu_q2_avg = 0

try:
    gpu_q3_avg = total_time_gpu_3 / gpu_3_count
except ZeroDivisionError:
    gpu_q3_avg = 0

t = Texttable()
t.add_rows([['----', 'CPU','GPU'], ['Query Type 1', cpu_q1_avg, gpu_q1_avg], ['Query Type 2', cpu_q2_avg, gpu_q2_avg],['Query Type 3',cpu_q3_avg, gpu_q3_avg]])
print("********************************* Results for",txt_name,"*****************************")
print(t.draw())
print("Total Query: ",total_count)
