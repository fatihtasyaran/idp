import connector
import json
from flask import Flask, request, jsonify
from datetime import datetime
from memory_profiler import profile
import time
import random
import hashlib
import _thread
import psutil
import multiprocessing
from threading import Thread
import ctypes
from texttable import Texttable
import GPUtil
import nvidia_smi
from pynvml import *
app = Flask(__name__)
from query_generator import *
import sys
import statistics
import statsmodels.api as sm
import pandas as pd
from sklearn import linear_model
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import classification_report, confusion_matrix
import numpy as np
import csv
from multiprocessing import Value
import matplotlib.pyplot as plt
import math

#class queue_info(Structure):
#    _fields_ = [("queue_length", c_int),("min_load", c_int), ("max_load", c_int), ("avg_load", c_int), ("stdev", c_int)]


####BF ON-OFF####
BF = int(sys.argv[2])
####BF ON-OFF####
gpu_op = int(sys.argv[3])
global pu
global execution_time
type = 0

filer = open('/home/fatih/idp/chicago-taxi-rides-2016/chicago_taxi_trips_2016_01.csv')
csv_reader = csv.reader(filer)
types = next(csv_reader)
names = next(csv_reader)

#pu = {"GPU":0, "CPU":0}
pu = [0, 0]
average_durations = 0
no_queries = 0

#################
CPU_THRESHOLD = 10
GPU_THRESHOLD = 30
MAX_CPU_Q = Value('i', 0)
MAX_GPU_Q = Value('i', 0)
queue_length = Value('i',0)
################ 

@app.route('/')
def health_check():
    return 'Healthy'

@app.route('/search', methods=['POST'])
def search():
    return jsonify(connector.names)

@app.route('/tag-keys', methods=['POST'])
def tag_keys():
    print("Ad-hoc asking for columns")
    #k = ["dont","forget","your","keys"]
    return jsonify(connector.names)

@app.route('/tag-values', methods=['POST'])
def tag_values():
    values = ["key", "operator", "value"]
    return jsonify(values)

def hashQuery(query):
    result = hashlib.sha256(query.encode())
    result = result.hexdigest()
    return result[0:8]


class ThreadWithReturnValue(Thread):
    def __init__(self, group=None, target=None, name=None,
                 args=(), kwargs={}, Verbose=None):
        Thread.__init__(self, group, target, name, args, kwargs)
        self._return = None
    def run(self):
        #print(type(self._target))
        if self._target is not None:
            self._return = self._target(*self._args,**self._kwargs)
    def join(self, *args):
        Thread.join(self, *args)
        return self._return

    
def monitor(target):
#    manager = multiprocessing.Manager()
#    return_dict = manager.dict()
    worker_process = mp.Process(target=target, args=(return_dict))
    worker_process.start()
    p = psutil.Process(worker_process.pid)

    # log cpu usage of `worker_process` every 10 ms
    cpu_percents = []
    while worker_process.is_alive():
        cpu_percents.append(p.cpu_percent())
        time.sleep(0.01)

    worker_process.join()
    return cpu_percents, return_dict["datarizer"], return_dict["durations"]

delay = 0

#@profile
def split_processing(items, MAX_CPU_Q, MAX_GPU_Q, MIN, AVG, load_list, total_query, dev_decision_parameter, num_splits, timestamp, interval,  queue_length, counter=0, tau=15):

    queue_wait_time = 0.00
    
    split_size = len(items) // num_splits                                       
    threads = []
    
    for i in range(num_splits):                                                 
        # determine the indices of the list this thread will handle             
        start = i * split_size                                                  
        # special case on the last chunk to account for uneven splits           
        end = None if i+1 == num_splits else (i+1) * split_size                 
        # create the threads                                                     

        threads.append(
            multiprocessing.Process(target=process, args=(items, start, end, counter, MAX_CPU_Q, MAX_GPU_Q, MIN, AVG, load_list, total_query, queue_wait_time, interval, queue_length, timestamp, )))
        threads[-1].start() # start the thread we just created
        #time.sleep(delay)

    # wait for all threads to finish                                            
    for t in threads:                                                           
        t.join()


MAX = 0
MIN = 100
AVG = 0
load_list = []
total_query = 0
dev_decision_parameter = "alg"

## The function that each thread will run
#@profile
def process(query_list, start, end, counter, MAX_CPU_Q, MAX_GPU_Q, MIN, AVG, load_list, total_query, queue_wait_time, interval, queue_length, timestamp):


    col_names = ['cpu_usage','cpu_temp','cpu_mem','Q2','Q1','q_length','queue_wait_time','execution_time','max_cpu','max_gpu','interval','device','timestamp']
#    col_names = ['cpu_usage','cpu_temp','Q2','Q1','execution_time']
    Data= pd.read_csv("/home/anes/DOLAP/idp/GPU_/cpu_train_2.csv",header=0, usecols=col_names, names=col_names)
    feature_cols = ['cpu_usage','cpu_temp','cpu_mem','Q2','Q1','q_length','max_cpu','interval']
#    feature_cols = ['gpu_usage','gpu_temp','gpu_mem','cpu_usage','cpu_temperature','Q2','Q1','max_cpu','max_gpu','interval']

    X = Data[feature_cols]
    y = Data['execution_time']

    X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=0.2,random_state=1)

    ## Linear Regression and Fit the Data for CPU
    regr = linear_model.LinearRegression()
    regr.fit(X_train,y_train)
    
    print('Score (R-Squared) CPU:',regr.score(X_test,y_test))
    print('Intercept (Constant): ', regr.intercept_)
    print('Coefficients: ', regr.coef_,"\n")
    coef_list_cpu = regr.coef_

    response_time_cpu = regr.intercept_

#    y_pred = regr.predict(X_test)
#    plt.scatter(y_train, X_train.iloc[:,1].values, color='black')
#    plt.title("Regression line for CPU LR")
#    plt.xlabel("X")
#    plt.ylabel("Y")
#    plt.plot(regr.predict(X_train), X_train.iloc[:,1].values,color='blue', linewidth=3)
#    plt.xticks(())
#    plt.yticks(())
#    plt.savefig('regression_line_cpu.png')



    col_names_gpu = ['gpu_usage','gpu_temp','gpu_mem','Q2','Q1','q_length','queue_wait_time','execution_time','max_gpu','max_cpu','interval','device','timestamp']
#    col_names_gpu = ['gpu_usage','gpu_temp','Q2','Q1','execution_time']
    Data_gpu= pd.read_csv("/home/anes/DOLAP/idp/GPU_/gpu_train_3.csv",header=0, names=col_names_gpu)
##    feature_cols = ['cpu_usage','cpu_temp','Q2','Q1','max_gpu','max_cpu','interval']
    feature_cols_gpu = ['gpu_usage','gpu_temp','gpu_mem','Q2','Q1','q_length','max_gpu','interval']

    X_g = Data_gpu[feature_cols_gpu]
    y_g = Data_gpu['execution_time']

    X_train_g, X_test_g, y_train_g, y_test_g = train_test_split(X_g,y_g,test_size=0.2,random_state=1)

#    # Linear Regression and Fit the Data for GPU
    regr2 = linear_model.LinearRegression()
    regr2.fit(X_train_g,y_train_g)

    print('Score (R-Squared) GPU:',regr2.score(X_test_g,y_test_g))
    print('Intercept (Constant): ', regr2.intercept_)
    print('Coefficients: ', regr2.coef_,"\n")
    coef_list_gpu = regr2.coef_

    response_time_gpu = regr2.intercept_

#    y_pred_g = regr2.predict(X_test_g)
#    plt.scatter(X_test_g.iloc[:,5].values, y_test_g, color='black')
#    plt.plot(X_test_g.iloc[:,5].values, y_pred_g, color='blue', linewidth=3)
#    plt.xticks(())
#    plt.yticks(())
#    plt.savefig('regression_line_gpu.png')

    
    ## This loop will be shared between threads and each thread will run its own portion of the query list
    for query in query_list[start:end]:
        counter += 1
        print("QUERY*******",counter)
        info_struct = connector.GET_QUEUE_INFO()
        final_info = ctypes.cast(info_struct, ctypes.POINTER(connector.queue_info))
        condition = final_info.contents.queue_length

        dev_decision_parameter = "alg"   # "rand" or "alg"
        num_threads = 8       
        if(condition == 0 or condition != 0):
            adhoc_targets = []
            adhoc_keys = []
            adhoc_values = []

            ## Parsing the query for Grafana
            for i in range(0,len(query),2):
                adhoc_key = query[i]
                adhoc_operator = "="
                if len(query) != 1:
                    adhoc_value = query[i+1]
                    adhoc_values.append(adhoc_value)
                    q_id = hashQuery(adhoc_key+adhoc_value)
                    #print("Adhoc:" , "key->", adhoc_key, "operator->", adhoc_operator, "value->", adhoc_value)
                else:
                    adhoc_values.append("")
                    q_id = hashQuery(adhoc_key)
                    #print("Adhoc:" , "key->", adhoc_key)
                adhoc_keys.append(adhoc_key)
#                adhoc_values.append(adhoc_value)
                

            start = time.clock_gettime(1)


            if(len(query)==1):
                parameter = 1
            elif len(query) == 2:
                parameter = 2
            else:
                parameter = 3
            
            try:

                ## Getting the queue info and cast the result to a python struct
                info_thread = ThreadWithReturnValue(target=connector.GET_QUEUE_INFO, args=( ))
                info_thread.start()
                info_struct = info_thread.join()
                final_info = ctypes.cast(info_struct, ctypes.POINTER(connector.queue_info))

                
                gpu_thread = ThreadWithReturnValue(target=connector.GET_GPU_INFO, args=( ))
                gpu_thread.start()
                gpu_struct = gpu_thread.join()
                gpu_info_s = ctypes.cast(gpu_struct, ctypes.POINTER(connector.gpu_info))

            except ValueError:
                print("problem with getting the queue info thread")


            gpu_usage = gpu_info_s.contents.gpu_usage
            gpu_temp = gpu_info_s.contents.gpu_temp
            
            condition = final_info.contents.queue_length

        ## This condition seems erroneous but it is necessary for Gradana results, when sending different adhoc filter queries of different sizes
        if((condition != 0 or condition == 0)  and len(query)==1): ############ QUERY TYPE 1
            q_type = 1
            q_id = hashQuery(query[0])
            id_type = q_id +"-"+ str(q_type)
   

            ## Get the queue current info
            info_thread = ThreadWithReturnValue(target=connector.GET_QUEUE_INFO, args=( ))
            info_thread.start()
            info_struct = info_thread.join()
            final_info = ctypes.cast(info_struct, ctypes.POINTER(connector.queue_info))
            q_length = final_info.contents.queue_length
            
            ## These are old and not used
            #q_min = final_info.contents.min_load
            #q_max = final_info.contents.max_load
            #q_avg = final_info.contents.avg_load
            #q_std = final_info.contents.stdev
                

            ## Receive the GPU info from C++ api functions
            gpu_thread = ThreadWithReturnValue(target=connector.GET_GPU_INFO, args=( ))
            gpu_thread.start()
            gpu_struct = gpu_thread.join()
            gpu_info_s = ctypes.cast(gpu_struct, ctypes.POINTER(connector.gpu_info))

            ## Get the CPU and GPU informations
            cpu_usage = psutil.cpu_percent()
            cpu_temperature = psutil.sensors_temperatures()["coretemp"][0][1]
            cpu_mem = psutil.virtual_memory().percent
            gpu_usage = gpu_info_s.contents.gpu_usage
            gpu_temp = gpu_info_s.contents.gpu_temp
            gpu_mem = gpu_info_s.contents.gpu_mem

            ## lists to be used in response time predictions in CPU and GPU LR models 
            par1_gpu = [gpu_usage,gpu_temp]
            par1_cpu = [cpu_usage,cpu_temperature]

            
            predict_array_cpu = par1_cpu + [0] + [1] + [cpu_mem] + [q_length] + [MAX_CPU_Q.value] + [timestamp]
            predict_array_gpu = par1_gpu + [0] + [1] + [gpu_mem] + [q_length] + [MAX_GPU_Q.value] + [timestamp]

            ## Predicting
            for i in range(len(coef_list_cpu)):
                response_time_cpu += coef_list_cpu[i] * predict_array_cpu[i]
                response_time_gpu += coef_list_gpu[i] * predict_array_gpu[i]

#            print("resp. gpu:",response_time_gpu)

            ## Boolean to keep track when queries enters the queue
            entered_q = False
            #if MAX_CPU_Q.value <= 5:
            #if MAX_GPU_Q.value <= 6 and gpu_mem < 40:


            info_thread = ThreadWithReturnValue(target=connector.GET_QUEUE_INFO, args=( ))
            info_thread.start()
            info_struct = info_thread.join()
            final_info = ctypes.cast(info_struct, ctypes.POINTER(connector.queue_info))
            q_length = final_info.contents.queue_length

            ## Check whether there are queries in the queue first
            if q_length > 0:
                call_rslt = connector.GET_NEXT_QUERY()
                next_query = ctypes.cast(call_rslt, ctypes.POINTER(connector.query_struct))
                c_cols = next_query.contents.columns

                #dev_decision = 3
                if response_time_cpu < response_time_gpu:
                    dev_decision = 2
                    MAX_CPU_Q.value += 1
                    if cpu_usage <= 20:
                        num_threads = 8
                    elif 20 < cpu_usage <= 30:
                        num_threads = 4
                    elif 30 < cpu_usage <= 60:
                        num_threads = 2
                    elif 60 < cpu_usage <= 80:
                        num_threads = 1
                else:
                    dev_decision = 3
                    MAX_GPU_Q.value += 1
                         
                if queue_length.value > 0:
                    queue_length.value -= 1
#            else:
#                c_cols = query[0]

            ## Otherwise
            ## If predicted cpu time is less then predicted gpu time
            elif response_time_cpu < response_time_gpu:
                dev_decision = 2
                MAX_CPU_Q.value += 1
                if cpu_usage <= 20:
                    num_threads = 8
                elif 20 < cpu_usage <= 30:
                    num_threads = 4
                elif 30 < cpu_usage <= 60:
                    num_threads = 2
                elif 60 < cpu_usage <= 80:
                    num_threads = 1
            #    dev_decision = 3
            #    MAX_GPU_Q.value += 1
            else:
                dev_decision = 3
                MAX_GPU_Q.value += 1
            #    dev_decision = 2
            #    MAX_CPU_Q.value += 1
            #    if cpu_usage <= 20:
            #        num_threads = 8
            #    elif 20 < cpu_usage < 30:
            #        num_threads = 4
#                elif 40 < cpu_usage <= 60:
#                    num_threads = 2
#                elif 60 < cpu_usage <= 80:
#                    num_threads = 1
                if gpu_usage > 20:
            #    else:
                    ## Start measuring the waiting time in the queue
                    start_q = time.clock_gettime(1)
                    entered_q = True
                    queue_length.value += 1
                    ## Inserting the query to queue
                    try:

                        add_qry_thread = ThreadWithReturnValue(target=connector.INSERT_QUERY, args=(q_id.encode('utf-8').strip(), parameter, adhoc_keys, adhoc_values,))

                        add_qry_thread.start()
                        add_qry_thread.join()

                    except ValueError:
                        print("problem with inserting query thread")

            c_cols = query[0]
            response_time_cpu = regr.intercept_
            response_time_gpu = regr2.intercept_
            

#            info_thread = ThreadWithReturnValue(target=connector.GET_QUEUE_INFO, args=( ))
#            info_thread.start()
#            info_struct = info_thread.join()
#            final_info = ctypes.cast(info_struct, ctypes.POINTER(connector.queue_info))
#            q_length = final_info.contents.queue_length

            ## Check whether 
#            if q_length > 0:
#                call_rslt = connector.GET_NEXT_QUERY()
#                next_query = ctypes.cast(call_rslt, ctypes.POINTER(connector.query_struct))
#                c_cols = next_query.contents.columns

#                dev_decision = 3
#                if queue_length.value >= 0:
#                    queue_length.value -= 1
#            else:
#                c_cols = query[0]



            ## Execute the query based on the decision 
            if(dev_decision == 3):  # GPU
                start = time.clock_gettime(1)
                pu[0] += 1
                #print([pu[0]])
                #print("Scanning for column",query[0])
                manager = multiprocessing.Manager()
                return_dict = manager.dict()
                if q_length > 0:
                    if (next_query.contents.query_type == 1):
                        connector.scan_gpu(c_cols.decode(), connector.timeseries, return_dict)
                    #print("=====||||||||EXECUTION TIME = ", results[0], " ||||||||=====")
##                    query_process = multiprocessing.Process(target=connector.scan_gpu, args=(c_cols.decode(), connector.timeseries, return_dict))

                    elif (next_query.contents.query_type == 2):
                        c_vals = next_query.contents.values
                        connector.scan_gpu_with_dependency(c_cols.decode().split(", ")[0], c_cols.decode().split(", ")[0], c_vals.decode().split(", ")[0], connector.timeseries, return_dict)
                    #print("=====||||||||EXECUTION TIME = ", results[len(results)-1], " ||||||||=====")
                ##    query_process = multiprocessing.Process(target=connector.scan_gpu_with_dependency, args=((c_cols.decode().split(", ")[0], c_cols.decode().split(", ")[0], c_vals.decode().split(", ")[0], connector.timeseries, return_dict)))
                
                    elif (next_query.contents.query_type == 3):
                        c_vals = next_query.contents.values
                        if (next_query.contents.size == 2):
                            connector.scan_gpu_mult_dep(c_cols.decode().split(", "), c_vals.decode().split(", "), connector.timeseries, return_dict)
                        #print("=====||||||||EXECUTION TIME = ", results[0], " ||||||||=====")
##                        query_process = multiprocessing.Process(target=connector.scan_gpu_mult_dep, args=((c_cols.decode().split(", "), c_vals.decode().split(", "), connector.timeseries, return_dict)))
                        elif (next_query.contents.size == 3):
                            connector.scan_gpu_mult_dep2(c_cols.decode().split(", "), c_vals.decode().split(", "), connector.timeseries, return_dict)
                        #print("=====||||||||EXECUTION TIME = ", results[0], " ||||||||=====")
##                        query_process = multiprocessing.Process(target=connector.scan_gpu_mult_dep2, args=((c_cols.decode().split(", "), c_vals.decode().split(", "), connector.timeseries, return_dict)))
                        elif (next_query.contents.size == 4):
                            connector.scan_gpu_mult_dep3(c_cols.decode().split(", "), c_vals.decode().split(", "), connector.timeseries, return_dict)
                else:
                    connector.scan_gpu(names.index(query[0]), connector.timeseries, return_dict)

                if MAX_GPU_Q.value > 0:
                    MAX_GPU_Q.value -= 1

                
                execution_time = time.clock_gettime(1) - start
                print("The query's execution time is: ", execution_time)     
                gpu_usage = gpu_info_s.contents.gpu_usage
                gpu_temp = gpu_info_s.contents.gpu_temp


            elif(dev_decision == 2):#CPU
                start = time.clock_gettime(1)
                pu[1] += 1
                #print(pu[1])
                manager = multiprocessing.Manager()
                return_dict = manager.dict()

                if q_length > 0:
                    if (next_query.contents.query_type == 1):
                        connector.scan(c_cols.decode(), connector.timeseries, return_dict, num_threads)
                    #print("=====||||||||EXECUTION TIME = ", results[0], " ||||||||=====")
##                    query_process = multiprocessing.Process(target=connector.scan, args=(c_cols.decode(), connector.timeseries, return_dict))

                    elif (next_query.contents.query_type == 2):
                    #print("THE QUERY TYPE IS ======>>", next_query.contents.query_type)
                        c_vals = next_query.contents.values
                        connector.scan_with_filter(c_cols.decode().split(", ")[0], c_vals.decode().split(", ")[0], return_dict, num_threads)
                    #print("=====||||||||EXECUTION TIME = ", results[0], " ||||||||=====")
##                    query_process = multiprocessing.Process(target=connector.scan_with_filter, args=((c_cols.decode().split(", ")[0], c_vals.decode().split(", ")[0], return_dict)))

                    #query_process = multiprocessing.Process(target=connector.scan_with_dependency, args=((c_cols.decode().split(", ")[0], c_cols.decode().split(", ")[0], c_vals.decode().split(", ")[0], connector.timeseries, return_dict)))
                    elif (next_query.contents.query_type == 3):
                        c_vals = next_query.contents.values
                        connector.scan_with_dependencies(c_cols.decode().split(", "), c_vals.decode().split(", "), connector.timeseries, return_dict, num_threads)

                else:
                    connector.scan(names.index(query[0]), connector.timeseries, return_dict, num_threads)

                if MAX_CPU_Q.value > 0:
                    MAX_CPU_Q.value -=1        

                    
                cpu_usage = psutil.cpu_percent()
                cpu_temperature = psutil.sensors_temperatures()["coretemp"][0][1]

                execution_time = time.clock_gettime(1) - start
                print("The query's execution time is: ", execution_time)
                                        


        ## In case of queries of type 2
        elif ((condition != 0 or condition == 0) and len(query) == 2):  ########### QUERY TYPE 2
            q_type = 2
            adhoc_targets = []
            adhoc_key = query[0]
            adhoc_operator = "="
            adhoc_value = query[1]
            q_id = hashQuery(adhoc_key+adhoc_value)
            
            start = time.clock_gettime(1)

            adhoc_keys = []
            adhoc_values = []
            adhoc_keys.append(adhoc_key)
            adhoc_values.append(adhoc_value)
        
            info_thread = ThreadWithReturnValue(target=connector.GET_QUEUE_INFO, args=( ))
            info_thread.start()
            info_struct = info_thread.join()
            final_info = ctypes.cast(info_struct, ctypes.POINTER(connector.queue_info))
            q_length = final_info.contents.queue_length

            gpu_thread = connector.GET_GPU_INFO()
            gpu_struct = gpu_thread
            gpu_info_s = ctypes.cast(gpu_struct, ctypes.POINTER(connector.gpu_info))
                
            cpu_usage = psutil.cpu_percent()
            cpu_temperature = psutil.sensors_temperatures()["coretemp"][0][1]
            cpu_mem = psutil.virtual_memory().percent
            gpu_usage = gpu_info_s.contents.gpu_usage
            gpu_temp = gpu_info_s.contents.gpu_temp
            gpu_mem = gpu_info_s.contents.gpu_mem
            
            par1_gpu = [gpu_usage,gpu_temp]
            par1_cpu = [cpu_usage,cpu_temperature]


            predict_array_cpu = par1_cpu + [1] + [0] + [cpu_mem] + [q_length] + [MAX_CPU_Q.value] + [timestamp]
            predict_array_gpu = par1_gpu + [1] + [0] + [gpu_mem] + [q_length] + [MAX_GPU_Q.value] + [timestamp]

            for i in range(len(coef_list_cpu)):
                response_time_cpu += coef_list_cpu[i] * predict_array_cpu[i]
                response_time_gpu += coef_list_gpu[i] * predict_array_gpu[i]

#            print("resp. gpu:",response_time_gpu)


            info_thread = ThreadWithReturnValue(target=connector.GET_QUEUE_INFO, args=( ))
            info_thread.start()
            info_struct = info_thread.join()
            final_info = ctypes.cast(info_struct, ctypes.POINTER(connector.queue_info))
            q_length = final_info.contents.queue_length
            entered_q = False
            
            if q_length > 0:
                call_rslt = connector.GET_NEXT_QUERY()
                next_query = ctypes.cast(call_rslt, ctypes.POINTER(connector.query_struct))
                c_cols = next_query.contents.columns
                c_vals = next_query.contents.values

                if (response_time_cpu < response_time_gpu):
                     dev_decision = 2
                     MAX_CPU_Q.value += 1
                     if cpu_usage <= 20:
                         num_threads = 8
                     elif 20 < cpu_usage <= 30:
                         num_threads = 4
                     elif 30 < cpu_usage <= 60:
                         num_threads = 2
                     elif 60 < cpu_usage <= 80:
                         num_threads = 1
                else:
                    dev_decision = 3
                    MAX_GPU_Q.value += 1
                         
#                dev_decision = 3
                if queue_length.value > 0:
                    queue_length.value -= 1

            #entered_q = False
            #if MAX_CPU_Q.value <=5:
            #if MAX_GPU_Q.value <= 6 and gpu_mem < 40:
            elif response_time_cpu < response_time_gpu:
                dev_decision = 2
                MAX_CPU_Q.value += 1
                if cpu_usage <= 20:
                    num_threads = 8
                elif 20 < cpu_usage <= 30:
                    num_threads = 4
                elif 30 < cpu_usage <= 60:
                    num_threads = 2
                elif 60 < cpu_usage <= 80:
                    num_threads = 1
            #    dev_decision = 3
            #    MAX_GPU_Q.value += 1
            else:
                dev_decision = 3
                MAX_GPU_Q.value += 1
            #    dev_decision = 2
            #    MAX_CPU_Q.value += 1
            #    if cpu_usage <= 20:
            #        num_threads = 8
            #    elif 20 < cpu_usage < 30:
            #        num_threads = 4
#                elif 40 < cpu_usage <= 60:
#                    num_threads = 2
#                elif 60 < cpu_usage <= 80:
#                    num_threads = 1
                if gpu_usage > 20:
            #    else:
                    start_q = time.clock_gettime(1)
                    entered_q = True
                    queue_length.value += 1
                    try:

                        add_qry_thread = ThreadWithReturnValue(target=connector.INSERT_QUERY, args=(q_id.encode('utf-8').strip(), parameter, adhoc_keys, adhoc_values,))

                        add_qry_thread.start()
                        add_qry_thread.join()

                    except ValueError:
                        print("problem with inserting query thread")
#/            dev_decision = 2
            response_time_cpu = regr.intercept_
            response_time_gpu = regr2.intercept_


#            info_thread = ThreadWithReturnValue(target=connector.GET_QUEUE_INFO, args=( ))
#            info_thread.start()
#            info_struct = info_thread.join()
#            final_info = ctypes.cast(info_struct, ctypes.POINTER(connector.queue_info))
#            q_length = final_info.contents.queue_length

#            if q_length > 0:
#                call_rslt = connector.GET_NEXT_QUERY()
#                next_query = ctypes.cast(call_rslt, ctypes.POINTER(connector.query_struct))
#                c_cols = next_query.contents.columns
#                c_vals = next_query.contents.values

#                dev_decision = 3
#                if queue_length.value >= 0:
#                    queue_length.value -= 1


            
            if(BF == 0 or BF == 3):
                if dev_decision == 2:
                    manager = multiprocessing.Manager()
                    return_dict = manager.dict()
                    
                    if (next_query.contents.query_type == 1):
                        connector.scan(c_cols.decode(), connector.timeseries, return_dict, num_threads)
                        #print("=====||||||||EXECUTION TIME = ", results[0], " ||||||||=====")
##                        query_process = multiprocessing.Process(target=connector.scan, args=(c_cols.decode(), connector.timeseries, return_dict))

                    elif (next_query.contents.query_type == 2):
                        print("THE QUERY TYPE IS ======>>", next_query.contents.query_type)
                        c_vals = next_query.contents.values
                        connector.scan_with_filter(c_cols.decode().split(", ")[0], c_vals.decode().split(", ")[0], return_dict, num_threads)
                        #print("=====||||||||EXECUTION TIME = ", results[0], " ||||||||=====")
##                        query_process = multiprocessing.Process(target=connector.scan_with_filter, args=((c_cols.decode().split(", ")[0], c_vals.decode().split(", ")[0], return_dict)))
    #                    query_process = multiprocessing.Process(target=connector.scan_with_dependency, args=((c_cols.decode().split(", ")[0], c_cols.decode().split(", ")[0], c_vals.decode().split(", ")[0], connector.timeseries, return_dict)))

                    elif (next_query.contents.query_type == 3):
                        c_vals = next_query.contents.values
                        connector.scan_with_dependencies(c_cols.decode().split(", "), c_vals.decode().split(", "), connector.timeseries, return_dict, num_threads)

                    if MAX_CPU_Q.value > 0:
                        MAX_CPU_Q.value -=1
                    
                    cpu_usage = psutil.cpu_percent()
                    #cpu_memory_usage = psutil.virtual_memory()[2]
                    cpu_temperature = psutil.sensors_temperatures()["coretemp"][0][1]
                elif dev_decision == 3:
                    manager = multiprocessing.Manager()
                    return_dict = manager.dict()

                    if (next_query.contents.query_type == 1):
                        connector.scan_gpu(c_cols.decode(), connector.timeseries, return_dict)
                        #print("=====||||||||EXECUTION TIME = ", results[0], " ||||||||=====")
#                        query_process = multiprocessing.Process(target=connector.scan_gpu, args=(c_cols.decode(), connector.timeseries, return_dict))

                    elif (next_query.contents.query_type == 2):
                        c_vals = next_query.contents.values
                        connector.scan_gpu_with_dependency(c_cols.decode().split(", ")[0], c_cols.decode().split(", ")[0], c_vals.decode().split(", ")[0], connector.timeseries, return_dict)
                        #print("=====||||||||EXECUTION TIME = ", results[len(results)-1], " ||||||||=====")
##                        query_process = multiprocessing.Process(target=connector.scan_gpu_with_dependency, args=((c_cols.decode().split(", ")[0], c_cols.decode().split(", ")[0], c_vals.decode().split(", ")[0], connector.timeseries, return_dict)))

                    elif (next_query.contents.query_type == 3):
                        c_vals = next_query.contents.values
                        if (next_query.contents.size == 2):
                            connector.scan_gpu_mult_dep(c_cols.decode().split(", "), c_vals.decode().split(", "), connector.timeseries, return_dict)
                            #print("=====||||||||EXECUTION TIME = ", results[len(results)-1], " ||||||||=====")
##                            query_process = multiprocessing.Process(target=connector.scan_gpu_mult_dep, args=((c_cols.decode().split(", "), c_vals.decode().split(", "), connector.timeseries, return_dict)))
                        elif (next_query.contents.size == 3):
                            connector.scan_gpu_mult_dep2(c_cols.decode().split(", "), c_vals.decode().split(", "), connector.timeseries, return_dict)
                            #print("=====||||||||EXECUTION TIME = ", results[0], " ||||||||=====")
##                            query_process = multiprocessing.Process(target=connector.scan_gpu_mult_dep2, args=((c_cols.decode().split(", "), c_vals.decode().split(", "), connector.timeseries, return_dict)))
                        elif (next_query.contents.size == 4):
                            connector.scan_gpu_mult_dep3(c_cols.decode().split(", "), c_vals.decode().split(", "), connector.timeseries, return_dict)
                    if MAX_GPU_Q.value > 0:
                        MAX_GPU_Q.value -=1

                    gpu_usage = gpu_info_s.contents.gpu_usage
                    gpu_temp = gpu_info_s.contents.gpu_temp
                    
            elif(BF == 1):
                datarizer,durations = connector.scan_with_bf(c_cols.decode().split(", ")[0], c_cols.decode().split(", ")[0], c_vals.decode().split(", ")[0])
                cpu_usage = psutil.cpu_percent()
                #cpu_memory_usage = psutil.virtual_memory()[2]
                cpu_temperature = psutil.sensors_temperatures()["coretemp"][0][1]
            elif(BF == 2):
                
                if dev_decision == 2:
###                    start = time.clock_gettime(1)
                    manager = multiprocessing.Manager()
                    return_dict = manager.dict()

                    if q_length > 0:
                        if (next_query.contents.query_type == 1):
                            connector.scan(c_cols.decode(), connector.timeseries, return_dict, num_threads)
                        #print("=====||||||||EXECUTION TIME = ", results[0], " ||||||||=====")
##                        query_process = multiprocessing.Process(target=connector.scan, args=(c_cols.decode(), connector.timeseries, return_dict))

                        elif (next_query.contents.query_type == 2):
                    ##elif ((len(next_query.contents.columns)/3)==1):
                            print("THE QUERY TYPE IS ======>>", next_query.contents.query_type)
                        #print("BF2-T2-THE COLUMNS SIZE IS ======>>", next_query.contents.columns)
                            c_vals = next_query.contents.values
                            connector.scan_with_filter(c_cols.decode().split(", ")[0], c_vals.decode().split(", ")[0], return_dict, num_threads)
##                        query_process = multiprocessing.Process(target=connector.scan_with_filter, args=((c_cols.decode().split(", ")[0], c_vals.decode().split(", ")[0], return_dict)))
#                        query_process = multiprocessing.Process(target=connector.scan_with_dependency, args=((c_cols.decode().split(", ")[0], c_cols.decode().split(", ")[0], c_vals.decode().split(", ")[0], connector.timeseries, return_dict)))

                        elif (next_query.contents.query_type == 3):
                    ##elif ((len(next_query.contents.columns)/3)>=2):
                        #print("BF2-T3-THE QUERY TYPE IS =====>>", next_query.contents.columns)
                            c_vals = next_query.contents.values
                            connector.scan_with_dependencies(c_cols.decode().split(", "), c_vals.decode().split("\, "), connector.timeseries, return_dict, num_threads)
                    else:
                        connector.scan_with_filter(names.index(query[0]), query[1], return_dict, num_threads)

                    if MAX_CPU_Q.value > 0:
                        MAX_CPU_Q.value -=1        

                    cpu_usage = psutil.cpu_percent()
                    cpu_temperature = psutil.sensors_temperatures()["coretemp"][0][1]
                    execution_time = time.clock_gettime(1) - start

                    print("The query's execution time is: ", execution_time)
                    
                elif dev_decision == 3:
                    start = time.clock_gettime(1)
                    manager = multiprocessing.Manager()
                    return_dict = manager.dict()


                    if q_length > 0:
                        
                        if (next_query.contents.query_type == 1):
                            connector.scan_gpu(c_cols.decode(), connector.timeseries, return_dict)

                        elif (next_query.contents.query_type == 2):
                            c_vals = next_query.contents.values
                            connector.scan_gpu_with_dependency(c_cols.decode().split(", ")[0], c_cols.decode().split(", ")[0], c_vals.decode().split(", ")[0], connector.timeseries, return_dict)

                        elif (next_query.contents.query_type == 3):
                            c_vals = next_query.contents.values

                            if (next_query.contents.size == 2):
                                connector.scan_gpu_mult_dep(c_cols.decode().split(", "), c_vals.decode().split(", "), connector.timeseries, return_dict)

                            elif (next_query.contents.size == 3):
                                connector.scan_gpu_mult_dep2(c_cols.decode().split(", "), c_vals.decode().split(", "), connector.timeseries, return_dict)

                            elif (next_query.contents.size == 4):
                                connector.scan_gpu_mult_dep3(c_cols.decode().split(", "), c_vals.decode().split(", "), connector.timeseries, return_dict)

                    else:
                        connector.scan_gpu_with_dependency(names.index(query[0]), names.index(query[0]), query[1], connector.timeseries, return_dict)

                    if MAX_GPU_Q.value > 0:
                        MAX_GPU_Q.value -=1            

                    gpu_usage = gpu_info_s.contents.gpu_usage
                    gpu_temp = gpu_info_s.contents.gpu_temp
                    execution_time = time.clock_gettime(1) - start
                    print("The query's execution time is: ", execution_time)

            global no_queries
            no_queries += 1
            global average_durations

            execution_time = time.clock_gettime(1) - start
            print("The query's execution time is: ", execution_time)


        ## In case of queries of type 3
        elif ((condition != 0 or condition == 0) and len(query) > 2): ######## QUERY TYPE 3
            q_type = 3
            adhoc_keys = []
            adhoc_values = []
            adhoc_ops = []
            adhoc_targets = []
            #print(len(query))
            if (len(query) > 2):
                for i in range(0, len(query),2):
                    adhoc_keys.append(query[i])
                    adhoc_ops.append("=")
                    adhoc_values.append(query[i+1])
                    adhoc_targets.append(query[i])
            
            q_id = hashQuery(adhoc_keys[0]+adhoc_values[0])

            start = time.clock_gettime(1)
            info_thread = ThreadWithReturnValue(target=connector.GET_QUEUE_INFO, args=( ))
            info_thread.start()
            info_struct = info_thread.join()
            final_info = ctypes.cast(info_struct, ctypes.POINTER(connector.queue_info))
            q_length = final_info.contents.queue_length
                

            gpu_thread = connector.GET_GPU_INFO()
            gpu_struct = gpu_thread
            gpu_info_s = ctypes.cast(gpu_struct, ctypes.POINTER(connector.gpu_info))


            cpu_usage = psutil.cpu_percent()
            cpu_temperature = psutil.sensors_temperatures()["coretemp"][0][1]
            cpu_mem = psutil.virtual_memory().percent
            gpu_usage = gpu_info_s.contents.gpu_usage
            gpu_temp = gpu_info_s.contents.gpu_temp
            gpu_mem = gpu_info_s.contents.gpu_mem
            
            par1_gpu = [gpu_usage,gpu_temp]
            par1_cpu = [cpu_usage,cpu_temperature]

            predict_array_cpu = par1_cpu + [0] + [0] + [cpu_mem] + [q_length] + [MAX_CPU_Q.value] + [timestamp]
            predict_array_gpu = par1_gpu + [0] + [0] + [gpu_mem] + [q_length] + [MAX_GPU_Q.value] + [timestamp]

            
            for i in range(len(coef_list_cpu)):
                response_time_cpu += coef_list_cpu[i] * predict_array_cpu[i]
                response_time_gpu += coef_list_gpu[i] * predict_array_gpu[i]

#            print("resp. gpu:",response_time_gpu)

            info_thread = ThreadWithReturnValue(target=connector.GET_QUEUE_INFO, args=( ))
            info_thread.start()
            info_struct = info_thread.join()
            final_info = ctypes.cast(info_struct, ctypes.POINTER(connector.queue_info))
            q_length = final_info.contents.queue_length
            entered_q = False
            
            if q_length > 0:
                call_rslt = connector.GET_NEXT_QUERY()
                next_query = ctypes.cast(call_rslt, ctypes.POINTER(connector.query_struct))
                c_cols = next_query.contents.columns
                c_vals = next_query.contents.values

                adhoc_keys = c_cols.decode().split(", ")
                adhoc_values = c_vals.decode().split(", ")

#                dev_decision = 3
                if (response_time_cpu < response_time_gpu):
                    dev_decision = 2
                    MAX_CPU_Q.value += 1
                    if cpu_usage <= 20:
                        num_threads = 8
                    elif 20 < cpu_usage <= 30:
                        num_threads = 4
                    elif 30 < cpu_usage <= 60:
                        num_threads = 2
                    elif 60 < cpu_usage <= 80:
                        num_threads = 1

                else:
                    dev_decision = 3
                    MAX_GPU_Q.value += 1
                        
                if queue_length.value >= 0:
                    queue_length.value -= 1            

                    
            #if MAX_CPU_Q.value <=5:
            #if MAX_GPU_Q.value <= 6 and gpu_mem < 40:
            elif response_time_cpu < response_time_gpu:
                dev_decision = 2
                MAX_CPU_Q.value += 1
                if cpu_usage <= 20:
                    num_threads = 8
                elif 20 < cpu_usage <= 30:
                    num_threads = 4
                elif 30 < cpu_usage <= 60:
                    num_threads = 2
                elif 60 < cpu_usage <= 80:
                    num_threads = 1
            #    dev_decision = 3
            #    MAX_GPU_Q.value += 1
            else:
                dev_decision = 3
                MAX_GPU_Q.value += 1
            #    dev_decision = 2
            #    MAX_CPU_Q.value += 1
            #    if cpu_usage <= 20:
            #        num_threads = 8
            #    elif 20 < cpu_usage < 30:
            #        num_threads = 4
#                elif 40 < cpu_usage <= 60:
#                    num_threads = 2
#                elif 60 < cpu_usage <= 80:
#                    num_threads = 1
                if gpu_usage > 20:
            #    else:
                    start_q = time.clock_gettime(1)
                    entered_q = True
                    queue_length.value += 1
                    try:

                        add_qry_thread = ThreadWithReturnValue(target=connector.INSERT_QUERY, args=(q_id.encode('utf-8').strip(), parameter, adhoc_keys, adhoc_values,))
                        
                        add_qry_thread.start()
                        add_qry_thread.join()

                    except ValueError:
                        print("problem with inserting query thread")

            response_time_cpu = regr.intercept_
            response_time_gpu = regr2.intercept_

            
#            info_thread = ThreadWithReturnValue(target=connector.GET_QUEUE_INFO, args=( ))
#            info_thread.start()
#            info_struct = info_thread.join()
#            final_info = ctypes.cast(info_struct, ctypes.POINTER(connector.queue_info))
#            q_length = final_info.contents.queue_length
            
#            if q_length > 0:
#                call_rslt = connector.GET_NEXT_QUERY()
#                next_query = ctypes.cast(call_rslt, ctypes.POINTER(connector.query_struct))
#                c_cols = next_query.contents.columns
#                c_vals = next_query.contents.values
          
#                adhoc_keys = c_cols.decode().split(", ")
#                adhoc_values = c_vals.decode().split(", ")

#                dev_decision = 3
#                if queue_length.value >= 0:
#                    queue_length.value -= 1

                    
            if(BF == 2):
                if dev_decision == 2:
                    #print("IN CPU 2")
                    manager = multiprocessing.Manager()
                    return_dict = manager.dict()

                    ##if ((len(next_query.contents.columns.decode()).split(", "))==1):
                    if q_length > 0:
                        if (next_query.contents.query_type == 1):
                            print("I ENTERED THE FIRST QUERY FUNCTION")
                            connector.scan(c_cols.decode(), connector.timeseries, return_dict, num_threads)
                        #print("=====||||||||EXECUTION TIME = ", results[0], " ||||||||=====")
##                        query_process = multiprocessing.Process(target=connector.scan, args=(c_cols.decode(), connector.timeseries, return_dict))

                        elif (len(next_query.contents.values.decode().split(", "))==1):    
                    ##elif (next_query.contents.query_type == 2):
                            print("BF2-T2-THE QUERY TYPE IS======>>>", len((next_query.contents.values.decode()).split(", ")))
                            c_vals = next_query.contents.values
                        #if ((len(next_query.contents.columns)/3)==1):

                            connector.scan_with_filter(c_cols.decode().split(", ")[0], c_vals.decode().split(", ")[0], return_dict, num_threads)
                        ##connector.scan_with_dependencies(c_cols.decode().split(", "), c_vals.decode().split(", "), connector.timeseries, return_dict)
                        #print("=====||||||||EXECUTION TIME = ", results[0], " ||||||||=====")
##                            query_process = multiprocessing.Process(target=connector.scan_with_filter, args=((c_cols.decode().split(", ")[0], c_vals.decode().split(", ")[0], return_dict)))
                        else:
                            print("BF2-T3-THE QUERY TYPE IS =====>>", len((next_query.contents.values.decode()).split(", ")))
                            c_vals = next_query.contents.values
                            connector.scan_with_dependencies(c_cols.decode().split(", "), c_vals.decode().split(", "), connector.timeseries, return_dict, num_threads)
                    else:
                        cols = []
                        vals = []
                        for i in range(0, len(query),2):
                            cols.append(names.index(query[i]))
                            vals.append(query[i+1])
                        connector.scan_with_dependencies(cols, vals, connector.timeseries, return_dict, num_threads)

                    if MAX_CPU_Q.value > 0:
                        MAX_CPU_Q.value -=1                            
#                        datarizer,durations = connector.scan_with_dependencies(adhoc_keys, adhoc_values, connector.timeseries)
                    cpu_usage = psutil.cpu_percent()
                    cpu_temperature = psutil.sensors_temperatures()["coretemp"][0][1]
                elif dev_decision == 3:
                        #print("IN GPU 3")
                        manager = multiprocessing.Manager()
                        return_dict = manager.dict()

                        if q_length > 0:
                            if (next_query.contents.query_type == 1):
                                connector.scan_gpu(c_cols.decode(), connector.timeseries, return_dict)
                            #print("=====||||||||EXECUTION TIME = ", results[0], " ||||||||=====")
##                            query_process = multiprocessing.Process(target=connector.scan_gpu, args=(c_cols.decode(), connector.timeseries, return_dict))

                            elif (next_query.contents.query_type == 2):
                                c_vals = next_query.contents.values
                                connector.scan_gpu_with_dependency(c_cols.decode().split(", ")[0], c_cols.decode().split(", ")[0], c_vals.decode().split(", ")[0], connector.timeseries, return_dict)
                            #print("=====||||||||EXECUTION TIME = ", results[0], " ||||||||=====")
##                            query_process = multiprocessing.Process(target=connector.scan_gpu_with_dependency, args=((c_cols.decode().split(", ")[0], c_cols.decode().split(", ")[0], c_vals.decode().split(", ")[0], connector.timeseries, return_dict)))

                            elif (next_query.contents.query_type == 3):
                                c_vals = next_query.contents.values

                                if (next_query.contents.size == 2):
                                    connector.scan_gpu_mult_dep(c_cols.decode().split(", "), c_vals.decode().split(", "), connector.timeseries, return_dict)
                                #print("=====||||||||EXECUTION TIME = ", results[0], " ||||||||=====")
##                                query_process = multiprocessing.Process(target=connector.scan_gpu_mult_dep, args=((c_cols.decode().split(", "), c_vals.decode().split(", "), connector.timeseries, return_dict)))
                                elif (next_query.contents.size == 3):
                                    connector.scan_gpu_mult_dep2(c_cols.decode().split(", "), c_vals.decode().split(", "), connector.timeseries, return_dict)
                                #print("=====||||||||EXECUTION TIME = ", results[0], " ||||||||=====")
##                                query_process = multiprocessing.Process(target=connector.scan_gpu_mult_dep2, args=((c_cols.decode().split(", "), c_vals.decode().split(", "), connector.timeseries, return_dict)))
                                elif (next_query.contents.size == 4):
                                    connector.scan_gpu_mult_dep3(c_cols.decode().split(", "), c_vals.decode().split(", "), connector.timeseries, return_dict)
                        else:
                            cols = []
                            vals = []
                            for i in range(0, len(query), 2):
                                cols.append(names.index(query[i]))
                                vals.append(query[i+1])

                            if (len(cols) == 2):
                                connector.scan_gpu_mult_dep(cols, vals, connector.timeseries, return_dict)

                            elif (len(cols) == 3):
                                connector.scan_gpu_mult_dep2(cols, vals, connector.timeseries, return_dict)

                            elif (len(cols) == 4):
                                connector.scan_gpu_mult_dep3(cols, vals, connector.timeseries, return_dict)

                        if MAX_GPU_Q.value > 0:
                            MAX_GPU_Q.value -=1            
                                                     

                        gpu_usage = gpu_info_s.contents.gpu_usage
                        gpu_temp = gpu_info_s.contents.gpu_temp

            elif(BF == 3):
                datarizer, durations = connector.scan_with_dep_bf(adhoc_keys, adhoc_values)
                cpu_usage = psutil.cpu_percent()
                #cpu_memory_usage = psutil.virtual_memory()[2]
                cpu_temperature = psutil.sensors_temperatures()["coretemp"][0][1]

            no_queries += 1

            execution_time = time.clock_gettime(1) - start
            print("The query's execution time is: ", execution_time)

        if(dev_decision==3):

            if entered_q == True and dev_decision == 3:
                queue_wait_time = execution_time - return_dict['durations']
            else:
                queue_wait_time = 0.0
            t = Texttable()
            t.add_rows([["x_1","Device","GPU"],["x_2","Device Utilization (%)",gpu_usage],["x_3","Number of jobs in the queue",q_length], ["x_4","GPU memory",gpu_mem],["x_8","Query Type",q_type],["x_9","Response Time (s)",execution_time],["x_10","Device Temperature (C)",gpu_temp], ["x_11","Queue waiting time", queue_wait_time],["x_12", "Delay", delay], ["x_13", "Queries in CPU", MAX_CPU_Q.value], ["x_14", "Queries in GPU", MAX_GPU_Q.value], ["x_15","Interval", timestamp], ["x_16","Threads", num_threads]])
##            t.add_rows([["x_1","Device","GPU"],["x_2","Device Utilization (%)",gpu_usage],["x_3","Number of jobs in the queue",q_length],["x_4","Min Load in the queue",q_min],["x_5","Max Load in the queue",q_max],["x_6","Avg Load of the queue",q_avg],["x_7","S. Deviation of the queue",q_std],["x_8","Query Type",q_type],["x_9","Response Time (s)",execution_time],["x_10","Device Temperature (C)",gpu_temp]])
            print(t.draw())
            print("\n")
            txt_file_name = "gpu_train_3.csv"
            if q_type == 1:
                txt_data2 = str(gpu_usage)+","+str(gpu_temp)+","+str(gpu_mem)+","+"0"+","+"1"+","+str(q_length)+","+str(queue_wait_time)+","+str(execution_time)+","+str(MAX_CPU_Q.value)+","+str(MAX_GPU_Q.value)+","+str(timestamp)+","+"1"+","+str(interval)+"\n"
            elif q_type==2:
                txt_data2 = str(gpu_usage)+","+str(gpu_temp)+","+str(gpu_mem)+","+"1"+","+"0"+","+str(q_length)+","+str(queue_wait_time)+","+str(execution_time)+","+str(MAX_CPU_Q.value)+","+str(MAX_GPU_Q.value)+","+str(timestamp)+","+"1"+","+str(interval)+"\n"
            elif (q_type==3 ):
                txt_data2 = str(gpu_usage)+","+str(gpu_temp)+","+str(gpu_mem)+","+"0"+","+"0"+","+str(q_length)+","+str(queue_wait_time)+","+str(execution_time)+","+str(MAX_CPU_Q.value)+","+str(MAX_GPU_Q.value)+","+str(timestamp)+","+"1"+","+str(interval)+"\n"
                
            txt_data ="GPU" +","+str(gpu_usage)+","+str(q_type)+","+str(execution_time)+","+str(gpu_temp)+"\n"
            
##            txt_data =str(tau)+","+ "GPU" +","+str(gpu_usage)+","+str(q_min)+","+str(q_max)+","+str(q_avg)+","+str(q_std)+","+str(q_type)+","+str(execution_time)+","+str(gpu_temp)+"\n"
            
        elif(dev_decision==2):
            
            if entered_q == True:
                queue_wait_time = execution_time - return_dict["durations"]
            else:
                queue_wait_time = 0.0
##            if queue_wait_time < 0:
##                queue_wait_time = 0
            t = Texttable()
            t.add_rows([["x_1","Device","CPU"],["x_2","Device Utilization (%)",cpu_usage],["x_3","Number of jobs in the queue",q_length], ["x_4","GPU memory",gpu_mem],["x_8","Query Type",q_type],["x_9","Response Time (s)",execution_time],["x_10","Device Temperature (C)",cpu_temperature], ["x_11", "Queue waiting time", queue_wait_time],["x_12", "Delay", delay], ["x_13", "Queries in CPU", MAX_CPU_Q.value], ["x_14", "Queries in GPU", MAX_GPU_Q.value],["x_15","Interval",timestamp], ["x_16","Threads", num_threads]])
##            t.add_rows([["x_1","Device","CPU"],["x_2","Device Utilization (%)",cpu_usage],["x_3","Number of jobs in the queue",q_length],["x_4","Min Load in the queue",q_min],["x_5","Max Load in the queue",q_max],["x_6","Avg Load of the queue",q_avg],["x_7","S. Deviation of the queue",q_std],["x_8","Query Type",q_type],["x_9","Response Time (s)",execution_time],["x_10","Device Temperature (C)",cpu_temperature]])
            print(t.draw())
            print("\n")
            txt_file_name = "cpu_train_3.csv"
            if(q_type == 1):
#                txt_data2 = str(gpu_usage)+","+str(gpu_temp)+","+str(gpu_mem)+","+str(cpu_usage)+","+str(cpu_temperature)+","+"0"+","+"1"+","+str(execution_time)+","+str(MAX_CPU_Q.value)+","+str(MAX_GPU_Q.value)+","+str(timestamp)+","+"0"+","+str(interval)+"\n"
                txt_data2 = str(cpu_usage)+","+str(cpu_temperature)+","+str(cpu_mem)+","+"0"+","+"1"+","+str(q_length)+","+str(queue_wait_time)+","+str(execution_time)+","+str(MAX_CPU_Q.value)+","+str(MAX_GPU_Q.value)+","+str(timestamp)+","+"0"+","+str(interval)+"\n"
            elif(q_type==2):
#                txt_data2 = str(gpu_usage)+","+str(gpu_temp)+","+str(gpu_mem)+","+str(cpu_usage)+","+str(cpu_temperature)+","+"1"+","+"0"+","+str(execution_time)+","+str(MAX_CPU_Q.value)+","+str(MAX_GPU_Q.value)+","+str(timestamp)+","+"0"+","+str(interval)+"\n"
                txt_data2 = str(cpu_usage)+","+str(cpu_temperature)+","+str(cpu_mem)+","+"1"+","+"0"+","+str(q_length)+","+str(queue_wait_time)+","+str(execution_time)+","+str(MAX_CPU_Q.value)+","+str(MAX_GPU_Q.value)+","+str(timestamp)+","+"0"+","+str(interval)+"\n"
            elif(q_type==3):
#                txt_data2 = str(gpu_usage)+","+str(gpu_temp)+","+str(gpu_mem)+","+str(cpu_usage)+","+str(cpu_temperature)+","+"0"+","+"0"+","+str(execution_time)+","+str(MAX_CPU_Q.value)+","+str(MAX_GPU_Q.value)+","+str(timestamp)+","+"0"+","+str(interval)+"\n"
                txt_data2 = str(cpu_usage)+","+str(cpu_temperature)+","+str(cpu_mem)+","+"0"+","+"0"+","+str(q_length)+","+str(queue_wait_time)+","+str(execution_time)+","+str(MAX_CPU_Q.value)+","+str(MAX_GPU_Q.value)+","+str(timestamp)+","+"0"+","+str(interval)+"\n"
            txt_data = "CPU" +","+str(cpu_usage)+","+str(q_type)+","+str(execution_time)+","+str(cpu_temperature)+"\n"
##            txt_data = str(tau)+","+"CPU" +","+str(cpu_usage)+","+str(q_min)+","+str(q_max)+","+str(q_avg)+","+str(q_std)+","+str(q_type)+","+str(execution_time)+","+str(cpu_temperature)+"\n"
        mlr_data = open(txt_file_name, "a")
        mlr_data.write(txt_data2)
        mlr_data.close()
#/        if(dev_decision_parameter =="alg"):
#/             f_lb=open("test_algo2_tmp.txt","a")
             #f_lb = open("tau_alg2_2user_.txt","a")
#            f_lb = open("lb_alg_new_bf.txt","a")
#/        elif(dev_decision_parameter =="rand"):
#/            f_lb = open("test_rand_tmp.txt","a")
            #f_lb = open("tau_rand_1user_.txt","a")
#            f_lb = open("lb_rand_new_bf.txt","a")
#/        f_lb.write(txt_data)
#/        f_lb.close()

        print("QUERY -------->",query_list.index(query))
        total_query += 1
        
@app.route('/query', methods=['POST'])
def query():
    # SCENARIO
    query_list= [['fare'], ['dropoff_longitude', '511', 'fare', '20', 'extras', '1.5'], ['fare', '61'], ['pickup_latitude', '589', 'dropoff_latitude', '589'], ['dropoff_longitude'], ['dropoff_latitude', '857', 'fare', '20'], ['company', '80', 'fare', '25'], ['fare', '30'], ['company', '151', 'tips', '2'], ['company', '80', 'fare', '25'], ['fare', '50', 'trip_miles', '5.7'], ['tips', '30', 'extras', '8'], ['fare'], ['fare'], ['company', '151', 'tips', '2'], ['fare', '37'], ['pickup_latitude', '416'], ['fare', '39'], ['dropoff_latitude'], ['fare', '39'], ['tips', '30', 'extras', '8'], ['company'], ['pickup_latitude', '589', 'dropoff_latitude', '589'], ['dropoff_longitude', '100'], ['fare', '23'], ['company', '80', 'fare', '45'], ['pickup_latitude'], ['pickup_latitude'], ['pickup_latitude'], ['dropoff_longitude', '511', 'fare', '20', 'extras', '1.5'], ['dropoff_longitude', '511', 'fare', '20', 'extras', '1.5'], ['dropoff_longitude', '381', 'extras', '3', 'fare', '20'], ['dropoff_latitude', '484', 'fare', '50'], ['dropoff_longitude', '418', 'fare', '50', 'extras', '1.8'], ['dropoff_latitude', '857', 'fare', '20'], ['tips', '30', 'extras', '8'], ['fare', '50', 'company', '90'], ['fare', '60'], ['dropoff_longitude', '381', 'extras', '3', 'fare', '20'], ['fare', '37'], ['fare', '30'], ['fare', '28'], ['fare'], ['company', '80', 'fare', '25'], ['extras'], ['dropoff_longitude', '511', 'fare', '20', 'extras', '1.5'], ['fare', '30', 'tips', '7'], ['extras','1', 'fare', '25'], ['fare', '50'], ['fare', '39'], ['fare', '10', 'company', '95'], ['dropoff_latitude'], ['fare', '54'], ['fare', '10', 'company', '95'], ['company', '101'], ['tips', '1.7', 'dropoff_longitude', '601'], ['fare', '40', 'trip_miles', '5.9'], ['pickup_latitude', '416'], ['trip_miles', '5.3', 'tips', '5', 'fare', '25'], ['trip_miles', '1.3', 'tips', '2'], ['company', '101'], ['fare', '10', 'company', '95'], ['fare', '23'], ['fare', '39'], ['tips', '20', 'extras', '4'], ['dropoff_longitude', '511', 'fare', '20', 'extras', '1.5'], ['dropoff_latitude', '484', 'fare', '50'], ['fare', '37'], ['fare', '61'], ['fare', '51'], ['fare', '40'], ['dropoff_latitude', '857', 'fare', '20'], ['dropoff_longitude', '418', 'fare', '50', 'extras', '1.8'], ['fare', '40', 'trip_miles', '5.9'], ['company', '101'], ['pickup_latitude', '416'], ['trip_miles', '5', 'fare', '55'], ['trip_miles', '5', 'fare', '55'], ['fare'], ['company'], ['pickup_latitude', '744', 'dropoff_latitude', '580'], ['fare', '50', 'company', '90'], ['trip_miles', '0.9'], ['dropoff_latitude', '857', 'fare', '20'], ['tips', '20', 'extras', '4'], ['fare', '30'], ['tips', '20', 'extras', '4'], ['pickup_latitude', '589', 'dropoff_latitude', '589'], ['trip_miles', '1', 'fare', '35'],['extras', '3', 'fare', '25', 'tips', '1'], ['fare', '33'], ['fare', '40', 'trip_miles', '5.9'], ['fare'], ['dropoff_longitude', '511'], ['trip_miles', '1.3', 'tips', '2'], ['fare', '40', 'trip_miles', '5.9'],['fare', '50', 'tips', '9', 'company', '85'], ['fare', '50'], ['trip_miles'], ['trip_miles', '1.3', 'tips', '2'], ['fare', '61'], ['fare', '28'], ['fare'], ['trip_miles', '5', 'fare', '55'], ['tips', '30', 'extras', '8'], ['fare'], ['trip_miles', '5.3', 'tips', '5', 'fare', '25'], ['trip_miles', '5.3', 'tips', '5', 'fare', '25'], ['extras'], ['fare', '30', 'tips', '7'], ['fare', '54'], ['fare', '10', 'company', '95'], ['dropoff_latitude', '744', 'fare', '30'], ['fare', '50'], ['fare', '40', 'trip_miles', '5.9'], ['tips', '1.5', 'dropoff_longitude', '615'], ['fare', '61'], ['pickup_latitude', '416'], ['fare', '30', 'tips','7'], ['trip_miles', '5.3', 'tips', '5', 'fare', '25'], ['company', '101'], ['company'], ['dropoff_latitude', '484', 'fare', '50'], ['fare', '54'], ['dropoff_longitude', '418', 'fare', '50', 'extras', '1.8'], ['company', '80', 'fare', '45'], ['company', '101'], ['trip_miles', '5.3', 'tips', '5', 'fare', '25'], ['fare', '33'], ['fare', '40', 'trip_miles', '5.9'], ['fare', '60', 'tips', '20'], ['company', '80', 'fare', '25'], ['fare', '37'], ['company'], ['fare', '26'], ['company', '101'], ['fare', '50', 'tips', '9', 'company', '85'], ['fare'], ['fare', '30'], ['fare', '23'], ['company', '80', 'fare', '45'], ['company', '80', 'fare', '25'], ['trip_miles', '0.3', 'extras', '2', 'tips', '15'], ['dropoff_latitude', '484', 'fare', '50'], ['fare', '40', 'trip_miles', '5.9'], ['company', '101'], ['company', '80', 'fare', '25'], ['fare', '40'], ['dropoff_latitude', '484', 'fare', '50'], ['fare', '50', 'trip_miles', '5.7'], ['dropoff_longitude', '381', 'extras', '3', 'fare', '20'], ['extras', '3', 'fare', '25', 'tips', '1'], ['tips', '1.5', 'dropoff_longitude', '615'], ['dropoff_longitude', '200', 'extras', '2'], ['extras', '1', 'fare', '25'], ['pickup_latitude', '416'], ['fare'], ['fare', '50', 'tips', '9', 'company', '85'], ['trip_miles', '5', 'fare', '55'], ['fare', '39'], ['dropoff_longitude'], ['fare', '28'], ['dropoff_latitude', '744'], ['fare', '30', 'tips', '7'], ['tips', '1.5', 'dropoff_longitude', '615'], ['dropoff_longitude', '200', 'extras', '2'], ['fare', '30', 'tips', '7'], ['fare', '50'], ['extras', '3', 'fare', '25', 'tips', '1'], ['fare', '40'], ['fare', '26'], ['fare', '26'], ['fare', '40', 'trip_miles', '5.9'], ['fare'], ['fare', '28'], ['trip_miles', '1', 'fare', '35'], ['extras', '3', 'fare', '25', 'tips', '1'], ['trip_miles', '0.9'], ['fare', '30'], ['dropoff_longitude', '381', 'extras', '3', 'fare', '20'], ['trip_miles', '0.3', 'extras', '2', 'tips', '15'], ['tips', '30', 'extras', '8'], ['fare'], ['fare', '50', 'tips', '9', 'company', '85'], ['tips', '30', 'extras', '8'], ['dropoff_longitude', '418', 'fare', '50', 'extras', '1.8'], ['fare', '23'], ['fare'], ['fare'], ['company', '101'], ['dropoff_latitude', '744', 'fare', '30'], ['fare', '37'], ['fare'], ['fare', '40'], ['dropoff_longitude', '511'], ['fare', '37'], ['extras'], ['trip_miles', '5.3', 'tips', '5', 'fare', '25'], ['dropoff_longitude', '100'], ['trip_miles', '5', 'fare', '55'], ['fare', '60'], ['company', '80', 'fare', '25'], ['dropoff_longitude', '418', 'fare', '50', 'extras', '1.8'], ['dropoff_longitude', '200', 'extras', '2'], ['pickup_latitude', '589', 'dropoff_latitude', '589'], ['fare', '40', 'trip_miles', '5.9'], ['fare', '28'], ['fare', '24'], ['dropoff_latitude', '744', 'fare', '30'], ['dropoff_longitude', '418', 'fare', '50', 'extras', '1.8'], ['tips', '1.5', 'dropoff_longitude', '615'], ['fare', '24'], ['company', '80', 'fare', '45'], ['fare', '40', 'trip_miles', '5.9'], ['trip_miles', '1', 'fare', '35'], ['fare', '61'], ['extras'], ['fare'], ['tips', '1.5', 'dropoff_longitude', '615'], ['dropoff_longitude'], ['dropoff_longitude', '511', 'fare', '20', 'extras', '1.5'], ['dropoff_latitude', '744', 'fare', '30'], ['fare'], ['trip_miles'], ['fare', '50', 'tips', '9', 'company', '85'], ['tips', '1.7', 'dropoff_longitude', '601'], ['trip_miles', '1', 'fare', '35'], ['trip_miles', '5', 'fare', '55'], ['company', '80','fare', '45'], ['fare'], ['tips', '30', 'extras', '8'], ['fare', '51'], ['tips', '1.5', 'dropoff_longitude', '615'], ['fare', '40'], ['trip_miles'], ['company', '151', 'tips', '2'], ['dropoff_longitude', '200', 'extras', '2'], ['fare', '50', 'company', '90'], ['dropoff_longitude'], ['pickup_latitude', '416'], ['extras', '1', 'fare', '25'], ['company', '80', 'fare', '45'], ['dropoff_latitude', '857', 'fare', '20'], ['fare', '50', 'tips', '9', 'company', '85'], ['tips', '30', 'extras', '8'], ['trip_miles', '1', 'fare', '35'], ['trip_miles', '0.9'], ['trip_miles', '5', 'fare', '55'], ['dropoff_latitude', '857', 'fare', '20'], ['extras', '1', 'fare', '25'], ['dropoff_longitude', '511'], ['fare', '30'], ['fare', '37'], ['company'], ['fare', '40', 'trip_miles', '5.9'], ['extras', '3', 'fare', '25', 'tips', '1'], ['dropoff_longitude','381', 'extras', '3', 'fare', '20'], ['fare', '33'], ['pickup_latitude', '744'], ['company', '80', 'fare', '25'], ['dropoff_longitude', '418', 'fare', '50', 'extras', '1.8'], ['dropoff_latitude', '744'], ['fare', '40', 'trip_miles', '5.9'], ['pickup_latitude', '744'], ['fare', '30'], ['fare', '60'], ['pickup_latitude'], ['fare', '30', 'tips', '7'], ['company', '80', 'fare', '25'], ['fare', '60'], ['company', '151', 'tips', '2'], ['fare', '33'], ['fare', '33'], ['pickup_latitude', '744'], ['trip_miles', '1', 'fare', '35'], ['trip_miles'], ['pickup_latitude', '744', 'dropoff_latitude', '580'], ['dropoff_longitude', '200', 'extras', '2'], ['fare', '24'], ['extras', '1', 'fare', '25'], ['pickup_latitude', '416'], ['dropoff_longitude', '381', 'extras', '3', 'fare', '20'], ['fare', '54'], ['fare', '60', 'tips', '20'], ['tips', '1.7','dropoff_longitude', '601'], ['tips', '1.7', 'dropoff_longitude', '601'], ['company', '101'], ['fare', '50'], ['fare'], ['trip_miles', '0.9'], ['pickup_latitude', '416'], ['extras'], ['fare', '10', 'company', '95'], ['dropoff_latitude', '744'], ['fare', '23'], ['fare', '28'], ['fare', '30', 'tips', '7'], ['trip_miles', '0.3', 'extras', '2', 'tips', '15'], ['fare', '23'], ['tips', '20', 'extras', '4']]
   
    #query_list = []
    #query_list = query_gen()
    MAX = 0
    MIN = 100
    AVG = 0
    load_list = []
    total_query = 0
    tau = 15
    
    ######## THREAD
    #thread_num = 16
    g_start = time.clock_gettime(1)
    ######## QUERY
#    counter = 0
#    tau = 15
    dev_decision_parameter = "alg" # "rand" or "alg"

    
#    interval = random.randint(0, 5)
    #interval = random.uniform(0.1, 0.9)
    #interval = -0.1*math.log(interval)
    for i in range(50):
        
        #time.sleep(interval)
        query_choice = random.randint(1,2)

        if query_choice == 1:
            query_list = gen_query()
        else:
            query_list = query_gen(random.randrange(1000))

        if len(query_list) <= 10:
            num_splits = len(query_list)
        else:
            num_splits = 40

        u = random.uniform(0.1, 0.9)
        interval = -0.1*math.log(u)
        # Generate a random number from an exponential distribution log(u)*(-0.1) when Y ~~ Unif(0,1)
        timestamp = datetime.now()
        split_processing(query_list, MAX_CPU_Q, MAX_GPU_Q, MIN, AVG, load_list, total_query, dev_decision_parameter, num_splits, interval, timestamp, queue_length, counter=0, tau=15)
        time.sleep(interval)
        
    g_finish= time.clock_gettime(1) - g_start
    if(dev_decision_parameter =="alg"):
        ##f_lb = open("new_algo2_BS.txt","a")
        f_lb = open("new_alg2_times.txt","a")
    elif(dev_decision_parameter =="rand"):
        ##f_lb = open("new_rand_1user.txt","a")
        f_lb = open("new_rand_times.txt","a")
    finish = str(tau) + "," + str(g_finish) +","+str(total_query)+ "\n"
    f_lb.write(finish)
    f_lb.close()

@app.route('/annotations', methods=['POST'])
def annotations():
    req = request.get_json()
    data = [
        {
            "annotation": 'This is the annotation',
        "time": (convert_to_time_ms(req['range']['from']) +
                 convert_to_time_ms(req['range']['to'])) / 2,
            "title": 'Deployment notes',
            "tags": ['tag1', 'tag2'],
            "text": 'Hm, something went wrong...'
        }
    ]
    return jsonify(data)

if __name__ == '__main__':
    app.run(host='127.0.0.1', port=4001, debug=True)
