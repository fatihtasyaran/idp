#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include "string.h"
#include "LoadBalancer.h"
#include <fstream>
#include <numeric>
#include <unistd.h>
#include <vector>
#include <fstream>
#include <cstring>
#include <sstream>
#include </usr/include/python2.7/Python.h>
#include "queue.cpp"
#include <time.h>
#include <string>
//#include <python2.7/Python.h>

using namespace std;
//using namespace boost::python;

const int CPU_THRESHOLD = 40;
//#define GPU_THRESHOLD 0.40;
const int DIFF_THRESHOLD = 0.25;

unsigned int micro = 30000;
int QType;
list<string> cpu_queue;
list<string> gpu_queue;

Queue<query>* CPU_QUEUE = new Queue<query>();
Queue<query>* GPU_QUEUE = new Queue<query>();


struct py_query{

	char* q_id;
	int q_type;
	char* columns;
	char* values;
	int device;
	int size;

	py_query(char* Q_id, int Q_type, char* Columns, char* Values, int Device, int Size):
		q_id(Q_id),
		q_type(Q_type),
		columns(Columns),
		values(Values),
		device(Device),
		size(Size){}

};


// int deneme(query_id, query_type, columns, col_values){
int deneme(){
  
/*  cpu_queue.push_back("1");
  cpu_queue.push_back("1");
  cpu_queue.push_back("1");
  int queue_length = cpu_queue.size();
*/

  // CPU_QUEUE->push(query_id, query_type, columns, col_values);
//  CPU_QUEUE->push("2324957235923752", 1, )

  return CPU_QUEUE->queue_length();
}


void add_query1(char* q_id, int q_type, char** columns, int length){
	std::vector<string> cols;
	std::vector<string> vals;
////	cout <<"Entered the add query function" << endl; 
//	cout << "The column is: " << columns[0] << endl;
//	cout << "The value is: " << values[0] << endl;
//	cout << "BEFORE CONVERTING THE CHAR* TO STRINGS " << endl;
	for(int i=0; i < q_type; i++){
//		cout <<"SIZE OF COLS" <<  length << endl;

		stringstream myStreamString, yourStreamString; 
		myStreamString << columns[i];
//		yourStreamString << values[i];
		std::string str = myStreamString.str();
//		std::string val = yourStreamString.str();
//		std::string str(columns[i], sizeof(columns[i])/sizeof(char));
//		std::string val(values[i], sizeof(columns[i])/sizeof(char));
		//stringstream ss(str);
		//int tmp = 0;
		//ss >> tmp;
		cols.push_back(str);
//		vals.push_back(val);
//		cout << cols[i] << " on the "<< i << "th iteration " << endl;
//		cout << vals[i] << " on the "<< i << "th iteration " << endl; 
	}
//	cout << "AFTER CONVERTING "<< endl;

//      BEFORE CREATING AN INSTANCE OF A QUERY, MAKE A DECISION AND PUT IT ALONG WITH THE DECISION
//	MAKE A DECISION BASED ON THE MATRIX GENERATED BEFORE
//	cout << "I am UP until here "<< endl;
	int Q_type = 2;
	if (q_type == 1)
	  Q_type = 1;
	else if (q_type == 2)
	  Q_type = 2;
	else
	  Q_type == 3;

	int device = device_decision(Q_type);
//	cout << "I am UP until here 1"<< endl;
	float CPU_USAGE = cpu_utilization();
	cout << "|||||||||CPU USAGE: "<< CPU_USAGE << " |||||||" << endl;
//	cout << "I am UP until here 2"<< endl;
	gpu_infos* GPU_USAGE = gpu_utilization();
	cout << "|||||||||GPU USAGE: "<< GPU_USAGE->gpu_usage << " |||||||" << endl;
	int qt1_gpu = 0;
	int qt1_cpu = 0;
	int qt1_rnd = 0;
	int qt1_dev = 0;
	if ((CPU_USAGE - GPU_USAGE->gpu_usage) > DIFF_THRESHOLD){
		device = 3;
		qt1_gpu += 1;
	}
	else if (((GPU_USAGE->gpu_usage - CPU_USAGE) > DIFF_THRESHOLD)){
		device = 2;
		qt1_cpu +=1;
	}
	else if ((CPU_USAGE == GPU_USAGE->gpu_usage) || CPU_USAGE == 0 && GPU_USAGE->gpu_usage == 0){
	//	srand (time(NULL));
	//	device = rand() % 2 + 2;
			if (q_type == 1)
	//		if (q_type == 1 || q_type == 2)
				device = 3;
			else if (q_type == 2 || q_type == 3)
	//		else if (q_type == 3)
				device = 2;

		qt1_rnd += 1;
	}
	else{
	  device = device_decision(Q_type);
	  qt1_dev += 1;
	}
	
////	cout << "The cpu usage: " << CPU_USAGE << " | gpu usage: " << GPU_USAGE->gpu_usage << " | device: " << device << endl;
	query* q_i = new query(q_id, Q_type, cols, vals, device);
	CPU_QUEUE->push(*q_i);
////	cout << "The query has been added successfully to the CPU queue " << endl;
	std::ofstream outfile;
	outfile.open("alg1_numbers.txt", std::ios_base::app); // append instead of overwrite
	string data = to_string(qt1_gpu) + to_string(qt1_cpu) + to_string(qt1_rnd) + to_string(qt1_dev) +"25"+ "\n";
	outfile << data; 
	//cout << "qt1 gpu: "<<qt1_gpu<<" , qt1 cpu: "<<qt1_cpu<<" , qt1 rnd: "<< qt1_rnd << " , qt1 dev: " << qt1_dev << endl;
}




void add_query(char* q_id, int q_type, char** columns, char** values, int length){
	std::vector<string> cols;
	std::vector<string> vals;
////	cout <<"Entered the add query function" << endl; 
//	cout << "The column is: " << columns[0] << endl;
//	cout << "The value is: " << values[0] << endl;
//	cout << "BEFORE CONVERTING THE CHAR* TO STRINGS " << endl;
	for(int i=0; i < length; i++){
//		cout <<"SIZE OF COLS" <<  length << endl;

		stringstream myStreamString, yourStreamString; 
		myStreamString << columns[i];
		yourStreamString << values[i];
		std::string str = myStreamString.str();
		std::string val = yourStreamString.str();
//		std::string str(columns[i], sizeof(columns[i])/sizeof(char));
//		std::string val(values[i], sizeof(columns[i])/sizeof(char));
		//stringstream ss(str);
		//int tmp = 0;
		//ss >> tmp;
		cols.push_back(str);
		vals.push_back(val);
//		cout << cols[i] << " on the "<< i << "th iteration " << endl;
//		cout << vals[i] << " on the "<< i << "th iteration " << endl; 
	}
//	cout << "AFTER CONVERTING "<< endl;

//      BEFORE CREATING AN INSTANCE OF A QUERY, MAKE A DECISION AND PUT IT ALONG WITH THE DECISION
//	MAKE A DECISION BASED ON THE MATRIX GENERATED BEFORE
//	cout << "I am UP until here "<< endl;
	int Q_type = 2;
	if (q_type == 1)
	  Q_type = 1;
	else if (q_type == 2)
	  Q_type = 2;
	else
	  Q_type == 3;

	int device = device_decision(Q_type);
//	cout << "I am UP until here 1"<< endl;
	float CPU_USAGE = cpu_utilization();
//	cout << "I am UP until here 2"<< endl;
	gpu_infos* GPU_USAGE = gpu_utilization();

	int qt23_gpu = 0;
	int qt23_cpu = 0;
	int qt23_rnd = 0;
	int qt23_dev = 0;
	
	if ((CPU_USAGE - GPU_USAGE->gpu_usage) > DIFF_THRESHOLD){
		device = 3;
		qt23_gpu += 1;
	}
	else if (((GPU_USAGE->gpu_usage - CPU_USAGE) > DIFF_THRESHOLD)){
		device = 2;
		qt23_cpu += 1;
	}
	else if ((CPU_USAGE == GPU_USAGE->gpu_usage) || CPU_USAGE == 0 && GPU_USAGE->gpu_usage == 0){
	//	srand (time(NULL));
	//	device = rand() % 2 + 2;

		if (q_type == 1)
	//	if (q_type == 1 || q_type == 2)
                        device = 3;
                else if (q_type == 2 || q_type == 3)
	//	else if (q_type == 3)
                        device = 2;
		qt23_rnd += 1;
	}
	else{
	  qt23_dev += 1;
	  device = device_decision(Q_type);
	}
////	cout << "The cpu usage: " << CPU_USAGE << " | gpu usage: " << GPU_USAGE->gpu_usage << " | device: " << device << endl;
	query* q_i = new query(q_id, Q_type, cols, vals, device);
	CPU_QUEUE->push(*q_i);
////	cout << "The query has been added successfully to the CPU queue " << endl;

	std::ofstream outfile;
	outfile.open("alg1_numbers.txt", std::ios_base::app); // append instead of overwrite
	string data = to_string(qt23_gpu) + to_string(qt23_cpu) + to_string(qt23_rnd) + to_string(qt23_dev) + "25"+"\n";
	outfile << data;
	//	cout << "qt23 gpu: "<<qt23_gpu<<" , qt23 cpu: "<<qt23_cpu<<" , qt23 rnd: "<< qt23_rnd << " , qt23 dev: " << qt23_dev << endl;
	
}

gpu_infos* get_gpu_info(){

	gpu_infos* gp_i = gpu_utilization();
//	cout << "GPU usage: "<< gp_i->gpu_usage << " GPU temp: "<< gp_i->gpu_temp << endl;
//	printf("GPU usage: %u | GPU temp: %u\n", gp_i->gpu_usage, gp_i->gpu_mem);
	return gp_i;


}

void remove_query(std::string query_id){

	CPU_QUEUE->remove(query_id);
////	cout << "The query removed successfully from the CPU queue "<< endl;

}

void old_func(char** carray, size_t size)
{
    for(size_t i = 0; i < size; ++i)
        std::cout << carray[i] << '\n';
}

py_query* get_next_query(){

  query q = CPU_QUEUE->pop();
  remove_query(q.query_id);
 // int* cols = q.columns.data();
///  int cols[q.columns.size()];
///  std::copy(q.columns.begin(), q.columns.end(), cols);
///  cout << "COLS[0] = "<< cols[0] << endl;

// char* Cols = reinterpret_cast<char*>(cols);
////  cout << "ALL COLUMNS: "<<  q.columns[0] << endl;
  std::vector<char*> cstrings;
  cstrings.reserve(q.query_strings.size());

  for(auto& s: q.query_strings)
      cstrings.push_back(&s[0]);
//  old_func(cstrings.data(), cstrings.size());
   char* id = (char*)malloc(sizeof(char)*q.query_id.size()+1);
   strcpy(id, q.query_id.c_str());
//#   cout << "CONVERTED STRING : "<< id << endl;
//  char* id = const_cast<char*>(q.query_id.c_str());
   char** vals = cstrings.data();
//#   cout << "THE VALUES[0]: " << vals[0] << endl;

  std::string s;
  int k=0;
  for (const auto &piece : q.query_strings) {
    if (q.query_strings.size() != 1 && k != (q.query_strings.size()-1) && piece != "")
{	s += piece;
	s += ", ";
}else
	s += piece;
    k+=1;
}
////cout << "Size of columns "<< q.columns.size()<< endl;
////cout << "Size of values "<< q.query_strings.size()<< endl;
//#  cout << "Values: "<<  s << endl;
//if (q.query_strings.size() < 3)
//  s = s.substr(0, s.size()-2);
  char* Values_py = (char*)malloc(sizeof(char)* s.size()+1);
  strcpy(Values_py, s.c_str());

//#  cout <<"BEFORE CONVERSION : " << q.columns[0]<< endl;
  std::string cls;
  int i =0;
  for (const auto &piece : q.columns) {
    if (q.columns.size() != 1 && i != (q.columns.size()-1) && piece != "")
 {
 	cls += piece;
	cls += ", ";
 }else
       cls += piece;
    i+=1;
  }
//#  cout << "Columns: "<<  cls << endl;
//if (q.columns.size() < 3)
//  cls = cls.substr(0, cls.size()-2);
//// cout << "NEW CLS= "<<cls << endl;
  char* Cols_py = (char*)malloc(sizeof(char)* cls.size()+1);
  strcpy(Cols_py, cls.c_str());
////  cout <<"VALUES_PY = " << Values_py << endl; 
////  cout <<"COLS_PY = " << Cols_py << endl; 
  cout << "THE QUERY TYPE INSIDE THE LOAD BALANCER IS: "<< q.query_type << endl;
  py_query* nxt_qry = new py_query(id, q.query_type, Cols_py, Values_py, q.device, q.columns.size());

  return nxt_qry;
}
/*
PyObject* foo(query* q)
{
    PyObject* result = PyList_New(0);
    int i;
    
    for (i = 0; i < q->columns.size(); ++i)

    {
        PyList_Append(result, PyInt_FromLong(q->columns[i]));
    }

    return result;
}

*/

struct info{

	int queue_length;
	int min_load;
	int max_load;
	int avg_load;
	int stdev;

	info(int Q_l, int Mn_load, int Mx_load, int AVG_load, int stDev):

		queue_length(Q_l),
		min_load(Mn_load),
		max_load(Mx_load),
		avg_load(AVG_load),
		stdev(stDev){}

};


info* get_queue_info(){

	info* queue_info = new info(CPU_QUEUE->queue_length(),  CPU_QUEUE->get_min_load(),  CPU_QUEUE->get_max_load()
					, CPU_QUEUE->get_avg_load(),  CPU_QUEUE->standard_deviation());
/*
	cout << "CPU queue information : \n";
	cout << "Queue's length: " << CPU_QUEUE->queue_length() << endl;
	cout << "Queue's min load: " << CPU_QUEUE->get_min_load() << endl;
	cout << "Queue's max load: " << CPU_QUEUE->get_max_load() << endl;
	cout << "Queue's avg load: " << CPU_QUEUE->get_avg_load() << endl;
	cout << "Queue's stdv: " << CPU_QUEUE->standard_deviation() << endl;
 */
/*	queue_info->queue_length = CPU_QUEUE->queue_length();
	queue_info->min_load = CPU_QUEUE->get_min_load();
	queue_info->max_load = CPU_QUEUE->get_max_load();
	queue_info->avg_load = CPU_QUEUE->get_avg_load();
	queue_info->std = CPU_QUEUE->standard_deviation();
*/
	return queue_info;

}

/*
std::vector<size_t> get_cpu_times() {
    std::ifstream proc_stat("/proc/stat");
    proc_stat.ignore(5, ' '); // Skip the 'cpu' prefix.
    std::vector<size_t> times;
    for (size_t time; proc_stat >> time; times.push_back(time));
    return times;
}
 
bool get_cpu_times(size_t &idle_time, size_t &total_time) {
    const std::vector<size_t> cpu_times = get_cpu_times();
    if (cpu_times.size() < 4)
        return false;
    idle_time = cpu_times[3];
    total_time = std::accumulate(cpu_times.begin(), cpu_times.end(), 0);
    return true;
}

void setQType(int &QueryType){

        QType = QueryType;

}
*/

//queue<string> getCPUQueue(){

//	return cpu_queue;
//}

//queue<string> getGPUQueue(){ 

//       return gpu_queue;
//}

// ====================00 I have stopped here, cpp function to get the type from datasource.py ==================//

int getQueryType(){
  /*
Py_Initialize();

    try 
    {
        object module = import("__main__");
        object name_space = module.attr("__dict__");
        exec_file("datasource.py", name_space, name_space);

        boost::python::object MyFunc = name_space["get_query_type"];
        boost::python::object result = MyFunc();

        // result is a dictionary
        std::string val = boost::python::extract<std::string>(result["val"]);
    } 
    catch (const std::exception& e) 
    {
        PyErr_Print();
    }

    Py_Finalize();
    return 0;
  */
}

float time_calculation(list<string> q, string type,int query){

	std::string line;
        float cpu_average, gpu_average, cpu_sum = 0.0, gpu_sum = 0.0;
        float cpu_q1_sum, cpu_q2_sum, gpu_q1_sum, gpu_q2_sum;
        float cpu_q1_count = 0.0, cpu_q2_count = 0.0, gpu_q1_count = 0.0, gpu_q2_count = 0.0;
        float c_count = 0.0, g_count = 0.0;
        float cpu_q1_avg, cpu_q2_avg, gpu_q1_avg, gpu_q2_avg;

        vector<string> strs;
        std::ifstream infile("exectime.txt");

        while (std::getline(infile, line))
        {
                strs.clear();
                boost::split(strs,line,boost::is_any_of(","));
//			cout << "Inside calculations 0" << endl;

        //      cout<<"Run. time ="<<strs[1]<< " Device: "<< strs[3] << endl;
                if(strs[3] == " 1"){
//			cout << "Inside calculations" << endl;
				//cout << strs[2] << endl;
		
                        if(strs[2] == " CPU"){
                                cpu_sum += std::stof(strs[0]);
                                c_count += 1;
                                cpu_q1_sum += std::stof(strs[0]);
                                cpu_q1_count += 1;

                        }else if (strs[2] == " GPU"){
                                gpu_sum += std::stof(strs[0]);
                                g_count += 1;
                                gpu_q1_sum += std::stof(strs[0]);
                                gpu_q1_count += 1;
                        }
                 else if(strs[3] == " 2"){

                        if(strs[2] == " CPU"){
                                cpu_sum += std::stof(strs[0]);
                                c_count += 1;
                                cpu_q2_sum += std::stof(strs[0]);
                                cpu_q2_count += 1;
                        }
                        else if (strs[2] == " GPU"){
                                gpu_sum += std::stof(strs[0]);
                                g_count += 1;
                                gpu_q2_sum += std::stof(strs[0]);
                                gpu_q2_count += 1;
                        }
		 }
	}
        }
	cout << "cpu_q1_count: " << cpu_q1_count << endl;
	cout << "gpu_q1_count: " << gpu_q1_count << endl;

        cpu_average = cpu_sum / c_count;
	gpu_average = gpu_sum/ g_count;
        cpu_q1_avg = cpu_q1_sum / cpu_q1_count;
       //cpu_q2_avg = cpu_q2_sum / cpu_q2_count;
        cpu_q2_avg = 0;
	gpu_q1_avg = gpu_q1_sum / gpu_q1_count;
//        gpu_q2_avg = gpu_q2_sum / gpu_q2_count;
        gpu_q2_avg = 0;

        cout << c_count << ", " << g_count << endl;
        cout << "General CPU average: " << cpu_average << ", General GPU average: "<< gpu_average << endl;
        cout << "cpu q1 avg: " << cpu_q1_avg << endl;
//        cout << "cpu q2 avg: " << cpu_q2_avg << endl;
        cout << "gpu q1 avg: " << gpu_q1_avg << endl;
//        cout << "gpu q2 avg: " << gpu_q2_avg << endl;

	int q1_counter = 0;
	int q2_counter = 0;
	float est_time = 0.0;
//if(!q.empty()){	
	while(!q.empty())
	{
//	while(!tmp.empty()){
	  if(q.front() == "1")
	    {
	    q1_counter += 1;
	    q.pop_front();
	    }
	  else if(q.front() == "2")
	  {
	    q2_counter += 1;
	    q.pop_front();
	  }
	}
	
	if(type == "cpu")
	  {
	    cout << "Query type: " << query << endl;
	    if(query == 1)
	      {
		cout << "*****************" << endl;
		if(cpu_q2_avg != 0.0)
		  {
		    est_time = (q1_counter * cpu_q1_avg) + (q2_counter * cpu_q2_avg) + cpu_q1_avg;
		  }
		else
		  {
		    est_time = (q1_counter * cpu_q1_avg) + cpu_q1_avg;
		  }
	       }
	    else if(query == 2)
	      {
		cout << "*****************" << endl;

		est_time = (q1_counter * cpu_q1_avg) + (q2_counter * cpu_q2_avg) + cpu_q2_avg;
	      }
	  }
	else if(type == "gpu")
	  {
	    if(query == 1){
	      if(gpu_q2_avg != 0.0)
		est_time = (q1_counter * gpu_q1_avg) + (q2_counter * gpu_q2_avg) + gpu_q1_avg;
	      else
		est_time = (q1_counter * gpu_q1_avg) + gpu_q1_avg;
	      
	    }
	    else if(query == 2){
	      est_time = (q1_counter * gpu_q1_avg) + (q2_counter * gpu_q2_avg) + gpu_q2_avg;
	      
	    }
	  }

//	cout<<"Estimated response time for query "<<query<<" in "<<type<<" is: "<<est_time<<" seconds"<<endl;
//	return est_time;

//}
else {

	return 0.0;
 }
}


void setQType(int QueryType){
	
	QType = QueryType;

}



void printQueue(list<string> q)
{
	//printing content of queue 
	while (!q.empty()){
		cout<<" "<<q.front();
		q.pop_front();
	}
	cout<<endl;
}

//int decide(queue<string> cpu_queue, queue<string> gpu_queue, int query, char* Query_info){

int decide(char* Query_info){

cout << "GPU queue before :" << endl;
printQueue(gpu_queue);		
cout << "CPU queue before :" << endl;
printQueue(cpu_queue);		


//std::string str_type = std::to_string(query_type);

std::string delimiter = "-";
cout << Query_info << endl;
size_t pos = 0;
std::string q_id;
std::string query_type;
std::string query_info(Query_info);
//while ((pos = query_type.find(delimiter)) != std::string::npos) {
    pos = query_info.find(delimiter);
    q_id = query_info.substr(0, pos);
    cout << q_id << endl;
    query_type = query_info.substr(pos+1, query_info.length()-pos);
//}
cout << query_type << endl;
cout << query_info << endl;
int Query_type = std::stoi(query_type);

//cout << "Inside decide" << endl;
if(!cpu_queue.empty() && !gpu_queue.empty()){
  //cout << "Inside queues verif" << endl;
//  float cpu_time_result = time_calculation(cpu_queue,"cpu", query_type);
  float cpu_time_result = time_calculation(cpu_queue,"cpu", Query_type);
//  float cpu_time_result = (float(rand())/float((RAND_MAX))*3.0);
  //cout << "What about here?" << endl;
  float gpu_time_result = time_calculation(gpu_queue,"gpu", Query_type);
//  float gpu_time_result = (float(rand())/float((RAND_MAX))*3.0);
//  float gpu_time_result = time_calculation(gpu_queue,"gpu", query_type);
cout << cpu_time_result << endl;
cout << gpu_time_result << endl;


	if(cpu_time_result > gpu_time_result){
		cout<<"Decision: GPU"<<endl;
		gpu_queue.push_back(query_info);
		cout << "GPU queue after :" << endl;
		printQueue(gpu_queue);
		return 2;
	}
	else{
		cout<<"Decision: CPU"<<endl;
		cpu_queue.push_back(query_info);
cout << "CPU queue after :" << endl;
		printQueue(cpu_queue);

		return 3;
	}

}else{
	cout << "Empty queues" << endl;
	cout<<"Decision: CPU"<<endl;
	cpu_queue.push_back(query_info);

	return 3;
}
}



void delete_query(int queue){

  
//	if(queue == 1)
//		cpu_queue.pop();

}






/*
    size_t previous_idle_time=0, previous_total_time=0;
    float utilization = 0;
//    for (size_t idle_time, total_time; get_cpu_times(idle_time, total_time); sleep(1)) {
        for(int i=0; i<2;i++, usleep(micro)){
        size_t idle_time, total_time;
        get_cpu_times(idle_time, total_time);
        const float idle_time_delta = idle_time - previous_idle_time;
        const float total_time_delta = total_time - previous_total_time;
        utilization = 100.0 * (1.0 - idle_time_delta / total_time_delta);
        previous_idle_time = idle_time;
        previous_total_time = total_time;
    }
        std::cout << utilization << '%' << std::endl;

std::ifstream ifs("lBalancer.txt");

std::string line;

while(getline(ifs, line, ',')) // read one line from ifs
{

    line.clear();
        std::istringstream iss(line); // access line as a stream

    // we only need the first two columns
    float column1;
//    char* column2;

    //iss >> column1 ; // no need to read further

    cout << line << endl;
    // do what you will with column2
}





        ifstream myReadFile;

        myReadFile.open("lBalancer.txt");
        char output[100];

        if (myReadFile.is_open()) {
          //while (!myReadFile.eof()) {
            while(myReadFile){
             myReadFile.getline(output, 100);
          //  myReadFile >> output;
            cout << output << endl;
            }

        }
myReadFile.close();
*/

