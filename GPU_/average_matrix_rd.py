from texttable import Texttable

txt_name = "cpu_train.csv"

f = open(txt_name,"r")
line_list = f.readlines()
f.close()

cpu_1_count = 0
cpu_2_count = 0
cpu_3_count = 0

gpu_1_count = 0
gpu_2_count = 0
gpu_3_count = 0

total_time_cpu_1 = 0.0
total_time_cpu_2 = 0.0
total_time_cpu_3 = 0.0
total_time_gpu_1 = 0.0
total_time_gpu_2 = 0.0
total_time_gpu_3 = 0.0


for i in range(1,159):
    split_list = line_list[i].split(",")
    if split_list[1].strip() == "CPU":        # CPU
        if split_list[7].strip() == "1":  # Query type 1
            total_time_cpu_1 += float(split_list[8])
            cpu_1_count += 1
        elif split_list[7].strip() == "2":      # Query Type 2
            total_time_cpu_2 += float(split_list[8])
            cpu_2_count += 1
        elif(split_list[7].strip() == "3"):      # Query Type 3
            total_time_cpu_3 += float(split_list[8])
            cpu_3_count += 1
    elif split_list[1].strip() == "GPU":
        if split_list[7].strip() == "1":
            total_time_gpu_1 += float(split_list[8])
            gpu_1_count += 1                
        elif split_list[7].strip() == "2":
            total_time_gpu_2 += float(split_list[8])
            gpu_2_count += 1                
        elif(split_list[7].strip() == "3"):            
            total_time_gpu_3 += float(split_list[8])
            gpu_3_count += 1                

try:
    cpu_q1_avg = total_time_cpu_1 / cpu_1_count
except ZeroDivisionError:
    cpu_q1_avg = 0

try:
    cpu_q2_avg = total_time_cpu_2 / cpu_2_count
except ZeroDivisionError:
    cpu_q2_avg = 0

try:
    cpu_q3_avg = total_time_cpu_3 / cpu_3_count
except ZeroDivisionError:
    cpu_q3_avg = 0
    
try:
    gpu_q1_avg = total_time_gpu_1 / gpu_1_count
except ZeroDivisionError:
    gpu_q1_avg = 0

try:
    gpu_q2_avg = total_time_gpu_2 / gpu_2_count
except ZeroDivisionError:
    gpu_q2_avg = 0

try:
    gpu_q3_avg = total_time_gpu_3 / gpu_3_count
except ZeroDivisionError:
    gpu_q3_avg = 0

#t = Texttable()
#t.add_rows([['----', 'CPU','GPU'], ['Q1 Avg', '3.4', '1.6'], ['Q2 Avg','1.7','1.3'],['Q3 Avg','0.8', '1.4'],['Q1 Count',cpu_1_count,gpu_1_count],['Q2 Count',cpu_2_count,gpu_2_count],['Q3 Count',cpu_3_count,gpu_3_count],['Total Time Q1',total_time_cpu_1, total_time_gpu_1],['Total Time Q2',total_time_cpu_2, total_time_gpu_2],['Total Time Q3',total_time_cpu_3, total_time_gpu_3]])
#print("********************************* Results for",txt_name,"*****************************")
#print(t.draw())
