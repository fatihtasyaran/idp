#include <iostream>
#include "connector.h"
#include "database.hpp"
//#include "main.cpp"
//#include "setup.hpp"
//#include "gpu_struct.hpp"
//#include <cuda.h>

#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>
#include <memory>
#include <assert.h>
#include <stddef.h>
#include <cuda.h>
//#include <stdio.h>
//#include "GPU_funcs.cu"

#include "gpu_pathway.c"
//#include "data_block.h"
//#include "gpu_wrapper.cu"

#define THREADS_PER_BLOCK 1024
#define BLOCKS_PER_BL 900



extern "C" char** getDataBlock(dataBase* db_addr, int target_col_num, int NO_BLOCK_GROUPS, int col_num){


	return copyDataBlock(db_addr, target_col_num, NO_BLOCK_GROUPS, col_num);

}


//extern "C" char** GPU_scan(dataBase* db_addr, int target_col_num, int ts_col_num, int NO_BLOCK_GROUPS, int col_num, char* col_type, char** timeseries){
extern "C" char** GPU_scan(dataBase* db_addr, char** target_col_num, int ts_col_num, int NO_BLOCK_GROUPS, int col_num, char* col_type, char** timeseries){
  std::cout << "IN GPU EXTERNAL WRAPPER" << std::endl;


//  data_bk<char*> *ddb = copyDataBlock(db_addr, target_col_num, NO_BLOCK_GROUPS, col_num); 
//  data_bk<char*> *ddb_timeseries = copyDataBlock(db_addr, ts_col_num, NO_BLOCK_GROUPS, col_num); 

  return pathway(db_addr, target_col_num, ts_col_num, NO_BLOCK_GROUPS, col_num, col_type, timeseries);
  //return GPU_scan_db(db_addr,  target_col_num, ts_col_num, NO_BLOCK_GROUPS, col_num);

//      return GPU_scan_db(db_addr, target_col_num, ts_col_num, NO_BLOCK_GROUPS, col_num);

}

//extern "C" char** GPU_scan_dependency(dataBase* db_addr, int target_col_num, int ts_col_num, int NO_BLOCK_GROUPS, int col_num, int filter_col, char* filter_value, char** timeseries, char* col_type){
extern "C" char** GPU_scan_dependency(dataBase* db_addr, char** target_col_num, int ts_col_num, int NO_BLOCK_GROUPS, int col_num, int filter_col, char* filter_value, char** timeseries, char* col_type){
  std::cout << "GPU Query type 2" << std::endl;

  return pathway_query(db_addr,  target_col_num, ts_col_num, NO_BLOCK_GROUPS, col_num, filter_col, filter_value, timeseries, col_type);

}


//extern "C" char** GPU_scan_mult_dep(dataBase* db_addr, int* target_col_num, int num_cols, int NO_BLOCK_GROUPS, int col_num, char** dependency_values, char** timeseries){
extern "C" char** GPU_scan_mult_dep(dataBase* db_addr, int* target_col_num, char** target1, char** target2 ,int num_cols, int NO_BLOCK_GROUPS, int col_num, char** dependency_values, char** timeseries){
  std::cout << "GPU Query Type 3" << std::endl;

  return pathway_mult_query(db_addr, target_col_num, target1, target2, num_cols, NO_BLOCK_GROUPS, col_num, dependency_values, timeseries);
}


//extern "C" char** GPU_scan_mult_dep2(dataBase* db_addr, int* target_col_num, int num_cols, int NO_BLOCK_GROUPS, int col_num, char** dependency_values, char** timeseries){
extern "C" char** GPU_scan_mult_dep2(dataBase* db_addr, int* target_col_num, char** target1, char** target2, char** target3, int num_cols, int NO_BLOCK_GROUPS, int col_num, char** dependency_values, char** timeseries){
  std::cout << "GPU Query Type 3-Extended" << std::endl;

  return pathway_mult_query2(db_addr, target_col_num, target1, target2, target3,num_cols, NO_BLOCK_GROUPS, col_num, dependency_values, timeseries);
}

extern "C" char** GPU_scan_mult_dep3(dataBase* db_addr, int* target_col_num, char** target1, char** target2, char** target3, char** target4, int num_cols, int NO_BLOCK_GROUPS, int col_num, char** dependency_values, char** timeseries){
  std::cout << "GPU Query Type 3-Extended 2" << std::endl;

  return pathway_mult_query3(db_addr, target_col_num, target1, target2, target3, target4, num_cols, NO_BLOCK_GROUPS, col_num, dependency_values, timeseries);
}


extern "C" char** GPU_scan_mult_dep4(dataBase* db_addr, int* target_col_num, int num_cols, int NO_BLOCK_GROUPS, int col_num, char** dependency_values, char** timeseries){
  std::cout << "GPU Query Type 3-Extended 3" << std::endl;

  return pathway_mult_query4(db_addr, target_col_num, num_cols, NO_BLOCK_GROUPS, col_num, dependency_values, timeseries);
}

