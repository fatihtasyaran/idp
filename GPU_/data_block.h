#include <iostream>
#include <stdio.h>
#include <memory>
#include <assert.h>
#include <stddef.h>
#include "cuda_runtime.h"
#include <omp.h>




template<class T>
struct data_bk {

        T *rows;


        void insert(int index, T &val){
                rows[index] = val;
        }


};


char** copyDataBlock(dataBase* db_addr, int target_col_num, int NO_BLOCK_GROUPS, int col_num){

        data_bk<char*> *ddb;

        ddb = (data_bk<char*>*)malloc(sizeof(data_bk<char*>) * NO_BLOCK_GROUPS);
        for(int i=0; i < NO_BLOCK_GROUPS; i++){
                ddb[i].rows = (char**)malloc(1024 * sizeof(char*));
        }

        std::string::size_type sz;

//        int max_threads = omp_get_max_threads();
	int max_threads = 16;
//      int max_threads = 60;
        int group_per_thread = NO_BLOCK_GROUPS/max_threads;
//      printf("%d", max_threads);
#pragma omp parallel num_threads(max_threads)
 {
        int tid = omp_get_thread_num();
        int start_group = group_per_thread*tid;
        int last_group = start_group + group_per_thread;
        int join_index = start_group * 1024 * 2;

        if(tid == (max_threads - 1))
                last_group = NO_BLOCK_GROUPS - 1;

                for(int b = start_group; b < last_group; b++){
                        for(int i = 0; i < 1024; i++){
                                int target_block_index = b*col_num + target_col_num;
//                              printf("Memory Allocation device 1 %d \n", target_block_index);
                                char* tmp = (char*)malloc(sizeof(char) * 10);
                                //char tmp[10];
                                strcpy(tmp, db_addr->db[target_block_index]->somehow_get(i).c_str());
                                ////tmp = db_addr->db[target_block_index]->somehow_get(i);
				ddb[b].insert(i, tmp);
//                              cout << ddb[b].rows[i] << endl;
//                              ddb[b].insert(i, std::stod(db_addr->db[target_block_index]->somehow_get(i)));
                        }
                }
//#pragma omp barrier
}


//======================U Can Improve Here ===========================//
//Fill the array instead of the data_block//

	char** final = (char**)malloc(sizeof(char*)*NO_BLOCK_GROUPS * 1024);

	for(int i = 0; i < NO_BLOCK_GROUPS*1024; i++){
                final[i] = (char*)malloc(sizeof(char)*100);

        }

	for(int i=0; i<NO_BLOCK_GROUPS; i++){
                for(int j=0; j < 1024; j++){
                       final[j+i*1024] = ddb[i].rows[j];
/*			char tmp[10];
                        strcpy(tmp, db_addr->db[i*col_num + target_col_num]->somehow_get(j).c_str());
			final[j+i*1024] = tmp;

 */     //  		std::cout << final[j+i*1024] << std::endl;
		}
	}

/*

//      if(omp_get_thread_num() == 1){
             int cpt = 0;
                for(int i=0; i<900; i++)
                 for(int j=0; j<1024; j++)
                  //if (ddb[i].rows[j] != " ")
                   {
                    cpt ++;
                    cout << "i= "<< i << " j= "<< j << " value = " << ddb[i].rows[j] << endl;
                   }
//        cout <<"NUMBER IS : " << cpt << endl;
//      }

*/

return final;

}

