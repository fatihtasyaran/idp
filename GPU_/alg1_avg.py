txt_name = "alg1_numbers.txt"

f = open(txt_name,"r")
line_list = f.readlines()
f.close()

gpu_15 = 0
cpu_15 = 0
rnd_15 = 0
dev_15 = 0

gpu_20 = 0
cpu_20 = 0
rnd_20 = 0
dev_20 = 0

gpu_25 = 0
cpu_25 = 0
rnd_25 = 0
dev_25 = 0

for line in line_list:
    if(line[4] == "1" and line[5] == "5"):
        if(line[0] == "1"):
            gpu_15 += 1
        elif(line[1] == "1"):
            cpu_15 +=1
        elif(line[2] == "1"):
            rnd_15 += 1
        elif(line[3] == "1"):
            dev_15 +=1
    elif(line[4] == "2" and line[5] == "0"):
        if(line[0] == "1"):
            gpu_20 += 1
        elif(line[1] == "1"):
            cpu_20 +=1
        elif(line[2] == "1"):
            rnd_20 += 1
        elif(line[3] == "1"):
            dev_20 +=1
    elif(line[4] == "2" and line[5] == "5"):
        if(line[0] == "1"):
            gpu_25 += 1
        elif(line[1] == "1"):
            cpu_25 +=1
        elif(line[2] == "1"):
            rnd_25 += 1
        elif(line[3] == "1"):
            dev_25 +=1
            

print("TAU = 15  GPU: ",gpu_15/3," CPU: ", cpu_15/3, "Random: ", rnd_15/3, "DEV: ", dev_15/3)
print("TAU = 20  GPU: ",gpu_20/3," CPU: ", cpu_20/3, "Random: ", rnd_20/3, "DEV: ", dev_20/3)
print("TAU = 25  GPU: ",gpu_25/3," CPU: ", cpu_25/3, "Random: ", rnd_25/3, "DEV: ", dev_25/3)

