#ifndef DATABASE_H
#define DATABASE_H

#include "common.h"
#include <string.h>
#include <sstream>
//#include "Sketch.h"
#include <bitset>
#include <functional>
//#include <boost/lexical_cast.hpp>
#include <iostream>

// For Host/Device templating
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <memory>
#include <assert.h>
#include <stddef.h>

#define NO_BLOCKS 30
#define BSIZE 1024
using namespace std;

#ifdef __CUDACC__
#define CUDA_HOSTDEV __host__ __device__
#else
#define CUDA_HOSTDEV
#endif


extern int G_SIZE;
//extern int Primes[]; //[4] = {1129, 2063, 3217, 4093};

static uint32_t block_ids = 0;

template <typename T>
inline CUDA_HOSTDEV std::string myString(T val){
  std::stringstream ss;
  ss << val;
  return ss.str();
}

inline char* toChar(std::string val){

  char cstr[val.size() + 1];
  strcpy(cstr, val.c_str());

return cstr;
}


 inline int returnSize_d(std::string type) {
  if(type == "uint8_t")
    return sizeof(UINT8);
  else if(type == "uint16_t")
    return sizeof(UINT16);
  else if(type == "uint32_t")
    return sizeof(UINT32);
  else if(type == "string")
    return SIZEOF_STRING;
  else if(type == "double")
    return sizeof(DOUBLE);
  else if(type == "bool")
    return sizeof(BOOL);
  else if(type == "NONE")
    return 0;
  else
    std::cout << "I don't have " << type << "type" << std::endl;
}

inline uint32_t get_block_id()
{
  return ++block_ids;
}
__host__ __device__
 struct virtual_block{

 // char** elems = new char*[BSIZE]; 

  virtual ~virtual_block(){

  }

   virtual void insert(int index, uint8_t val){
  }

   virtual void insert(int index, uint16_t val){
  }

   virtual void insert(int index, uint32_t val){
  }

   virtual void insert(int index, double val){
  }

   virtual void insert(int index, float val){
  }

   virtual void insert(int index, std::string val){

  }
// ***********************/////////////////////*************************//
   virtual void insert(int index, char* val){

  }

   virtual void insert(int index, bool val){
  }

   virtual void print_get(int index){
  }
//CUDA_HOSTDEV
// virtual std::string somehow_get(int index){
 virtual std::string somehow_get(int index){
//    std::string ret = "D U M M Y";
//    std::cout << "Erroneous" << std::endl;
    return "";
  }

 virtual std::string getType(){

  return "";
}

CUDA_HOSTDEV virtual char* somehow_getG(int index){

  }

 
//CUDA_HOSTDEV virtual void somehow_getG(int index, char* value);


CUDA_HOSTDEV virtual char** getElems(){
 // DUMMY
}


 virtual std::string get_val(int index){
    std::string ret = "D U M M Y 1";
    std::cout << "Erroneous" << std::endl;
    return ret;
  }

 virtual bool bf_query(uint8_t val){
    std::cout << "This is virtual block" << std::endl;
  }
  
 virtual bool bf_query(uint16_t val){
    std::cout << "This is virtual block" << std::endl;
  }

 virtual bool bf_query(uint32_t val){
    std::cout << "This is virtual block" << std::endl;
  }

   virtual bool bf_query(double val){
    std::cout << "This is virtual block" << std::endl;
  }

   virtual bool bf_query(float val){
    std::cout << "This is virtual block" << std::endl;
  }
  
  virtual bool bf_query(std::string val){
//    std::cout << "This is virtual block" << std::endl;
  }

 virtual bool bf_query(bool val){
    std::cout << "This is virtual block" << std::endl;
  }

 virtual int return_accesses(){
    std::cout << "This is virtual block" << std::endl;
  }


/*
CUDA_HOSTDEV virtual char* somehow_getG(int index){

  }

 
//CUDA_HOSTDEV virtual void somehow_getG(int index, char* value);


CUDA_HOSTDEV virtual char** getElems(){
 // DUMMY
}

*/
  
};


extern int Primes[4] = {1129, 2063, 3217, 4093};

template <class T>
 struct ax_b_hash{
private:
  int a;
  int b;
  int p;

public:


  ax_b_hash()
  {}
  
 ax_b_hash(int num){
    a = 71 + num*32;
    b = 18 + num*53;
    p = Primes[num];
  }
  
 int hash(T val){
    
    int sum_val = std::hash<T>{}(val);
    if(sum_val < 0)
      sum_val -= sum_val;
      
    //std::cout << "Sum val: " <<  sum_val <<std::endl;
    int hash_val = ((a*sum_val + b)%p);//%4096;

    if(hash_val < 0){
      hash_val -= hash_val;
      hash_val /= 2;
    }
    
    //std::cout << "Hash val: " << hash_val << std::endl;
    return hash_val;
  }
  
      
};

#define NOBITS 4096
#define NOHASHES 3
template <class T>
 struct BloomFilter {
private:
  //int* bits;
  uint32_t no_bits;
  //uint32_t no_ints;
  std::bitset<NOBITS> bits;
  ax_b_hash<T>* hashes[NOHASHES];
  
public:
  int hash(T val, int k) {
    int index = hashes[k]->hash(val);
    return index;
  }
  
 void insert(T val){
    //std::cout << "Yeeeey first insert!" << std::endl;
    for(int k = 0; k < NOHASHES; k++){
      int index = hash(val, k);
      bits[index] = 1;
    }
  }

  bool query(T val){
    for(int k = 0; k < NOHASHES; k++){
      int index = hash(val, k);
      if(bits[index] == 0)
	return false;
    }
    return true;
  }
  
 BloomFilter(int no_bits) : no_bits(no_bits) {
    //no_ints = ceil(no_bits / (sizeof(int) * 8.0)); 
    //bits = new int[no_ints];
    //memset(bits, 0, sizeof(int) * no_ints);
    
    for(int i = 0; i < 4; i++){
      hashes[i] = new ax_b_hash<T>(i);
    }
  }
  
  ~BloomFilter() {
    //delete [] bits;
    delete [] hashes;
  }
};

template <class T>
//__host__ __device__
 struct block: public virtual_block{
  uint32_t id;
  uint32_t accessed;
  BloomFilter<T>* bloom_filter;
  T elements[BSIZE];
  //char** elems = new char*[BSIZE];

  std::string type;
  
  block() : accessed(0), id(-1) {}
  block(int id) : accessed(0), id(id) {
    bloom_filter = new BloomFilter<T>(1024);
  }
  
  void insert(int index, T val){
    elements[index] = val;
    //elems[index] = toChar(myString(elements[index]));
    bloom_filter->insert(val);
  }
  
  //This is for debug purposes
  //template<typename T>
  void print_get(int index){
    std::cout << elements[index] << std::endl;
    accessed++;
  }

//  CUDA_HOSTDEV
//  inline std::string somehow_get(int index){
  inline std::string somehow_get(int index){
    //    accessed++;
    std::stringstream ss;
    ss << elements[index];
    return ss.str();
//    char* result = (char*)malloc(sizeof(char)*(100));
//    strcpy(result, ss.str().c_str());
//    cout << elements[index] << endl;
//    return result;

  }

CUDA_HOSTDEV char* somehow_getG(int index){
    //std::cout << "I was THERE! I am block: " << id <<std::endl;
    //accessed++;
//    printf("Trying to get the value \n");
    std::string tmp = myString(elements[index]);
      char* value= toChar(tmp);
      //  value[tmp.size() + 1];
      //  tmp.copy(value, tmp.size() + 1);
	//cstr[tmp.size()] = '\0';
    //char* ret = (char*)elements[index];
//    printf("The value is: \%c", value) ;

    return value;
  }

  std::string getType(){
	
  return type;
}

/*CUDA_HOSTDEV char** getElems(){

 return elems;
}
*/

  std::string get_val(int index){
    accessed++;
    std::string out_string;
    std::stringstream ss;
    ss << elements[index];
    out_string = ss.str();
    return out_string;
  }

  bool bf_query(T val){
    return bloom_filter->query(val);
  }

  int return_accesses()
  {
    return accessed;
  }
  
};

struct dataBase{
  virtual_block* db[100000];

  /*dataBase(type_num_col* type_indexes, int col_num)
  {
    //db = new block_ptrs[col_num];
    string type;
    std::cout << "Constructing Database.." << std::endl;
    for(int i = 0; i < col_num*NO_BLOCK_GROUPS; i++){
      type = type_indexes[i%col_num].first;
      
      if(type == "uint8_t"){
	db[i] = new block<uint8_t>(i);
      }
      
      else if(type == "uint16_t"){
	db[i] = new block<uint16_t>(i);
      }
      
      else if(type == "uint32_t"){
	db[i] = new block<uint32_t>(i);
      }
      
      else if(type == "double"){
	db[i] = new block<double>(i);
      }
      
      else if(type == "float"){
	db[i] = new block<float>(i);
      }
      
      else if(type == "string"){
	db[i] = new block<string>(i);
      }
      
      else if(type == "bool"){
	db[i] = new block<bool>(i);
      }
      
      else{
	std::cout << "Undefined type, exiting" << std::endl;
	exit(1);
      }
    }
    }*/

 dataBase(char** col_types, char** col_names, int col_num, int NO_BLOCK_GROUPS)
  {
    //db = new block_ptrs[col_num];
    std::string type;
//    std::cout << "Constructing Database.." << std::endl;
//    std::cout << "NO_BLOCK_GROUPS: " << NO_BLOCK_GROUPS << " TOTAL BLOCKS: " << NO_BLOCK_GROUPS*col_num << std::endl;
    for(int i = 0; i < col_num*NO_BLOCK_GROUPS; i++){
      type = col_types[i%col_num];
      
      if(type == "uint8_t"){
	db[i] = new block<uint8_t>(i);
      }
      
      else if(type == "uint16_t"){
	db[i] = new block<uint16_t>(i);
      }
      
      else if(type == "uint32_t"){
	db[i] = new block<uint32_t>(i);
      }
      
      else if(type == "double"){
	db[i] = new block<double>(i);
      }
      
      else if(type == "float"){
	db[i] = new block<float>(i);
      }
      
      else if(type == "string"){
	db[i] = new block<std::string>(i);
      }
      
      else if(type == "bool"){
	db[i] = new block<bool>(i);
      }
      
      else{
//	std::cout << "Undefined type, exiting" << ": " << type << std::endl;
	exit(1);
      }
    }
//    std::cout << "Constructed Database.." << std::endl;
  }

public:

  int* accesses(int col_num, int NO_BLOCK_GROUPS){
    int* acc = new int[col_num*NO_BLOCK_GROUPS];

    for(int i = 0; i < col_num*NO_BLOCK_GROUPS; i++){
      acc[i] = db[i]->return_accesses();
    }
    return acc;
  }
  
};

namespace database{
  void* declare_dB(char** col_types, char** col_names, int col_num, int NO_BLOCK_GROUPS){
    //std::cout << "In c++: " << col_types[0] << std::endl;
    //dataBase *dB = new dataBase(col_types, col_names, col_num, NO_BLOCK_GROUPS);
    //std::cout << "Constructed Database.." << std::endl;
    return new dataBase(col_types, col_names, col_num, NO_BLOCK_GROUPS);
  }

}


#endif
