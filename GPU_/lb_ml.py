from sklearn.ensemble import AdaBoostClassifier
from sklearn import metrics
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from sklearn import tree
import pandas as pd
import time
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt
from sklearn.ensemble import GradientBoostingClassifier
from sklearn import preprocessing
from sklearn import linear_model
#import statsmodels.api as sm
import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import MinMaxScaler


col_names = ['device','query_type','q_min','q_max','q_avg','q_std','cpu_usage','gpu_usage','cpu_temp','gpu_temp','execution_time']
Data= pd.read_csv("/home/anes/DOLAP/idp/GPU_/LReg.csv",header=0, names=col_names)
feature_cols = ['device','query_type','q_min','q_max','q_avg','q_std','cpu_usage','gpu_usage','cpu_temp','gpu_temp']
X = Data[feature_cols]
y = Data['execution_time']
X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=0.2)

# fit scaler on training data
norm = MinMaxScaler().fit(X_train)

# transform training data
X_train_norm = norm.transform(X_train)

# transform testing dataabs
X_test_norm = norm.transform(X_test)

#sc = StandardScaler()
#X = sc.fit_transform(X)

def cost_function(X,Y,B):
    m =len(Y)
    J = np.sum((X.dot(B)-Y)**2)/(2*m)
    return J

def pred(X_test, newB):
    return X_test.dot(newB)

def gradientDescent(X, Y, B, alpha, iterations):
    cost_history = [0] * iterations
    m = len(Y)

    for iteration in range(iterations):
        # Hypothesis Values
        h = X.dot(B)
        # Difference between hypothesis and actual Y
        loss = h - Y
        # Gradient Calculation
        gradient = X.T.dot(loss) / m
        # Updating theta values using gradient
        B = B - alpha * gradient
        # New cost value
        cost = cost_function(X, Y, B)
        cost_history[iteration] = cost

    return B, cost_history

def r2(y_,y):
    sst = np.sum((y-y.mean())**2)
    ssr = np.sum((y_-y)**2)

    r2 = 1-(ssr/sst)
    print(r2)

#total number of samples for training set
#m = 13000
# number of features
#f = 10

#X_train = X[:m,:f]
#X_train = np.c_[np.ones(len(X_train)),X_train]

#y_train = y[:m]

#X_test = X[m:,:f]
#X_test = np.c_[np.ones(len(X_test)),X_test]

#y_test = y[m:]

# initialize coefficients
B = np.zeros(X_train.shape[1])

# Learning Rate and Number of iterations
alpha = 0.0001
iter_ = 200000

newB, cost_history = gradientDescent(X_train_norm, y_train, B, alpha, iter_)

# Predictions

y_ = pred(X_test_norm,newB)

r2(y_,y_test)


#******************************************************************************************************
#par1 = [[34.2,7,1.2425889328063242,36.926674057649635,19.08463149522798,25.232458572214714,3,48.0,51]]
#Data = pd.read_csv("/home/anes/DOLAP/idp/GPU_/lb_ml.csv")              

#######################################################################################################
#################################### ADABOOST #########################################################
#######################################################################################################
#def adaboost(par1,model):

    #Separating X and y
    #X = Data.iloc[:,1:11]
    #y = Data.iloc[:,0]

    #Split dataset into training set and test set
    #X_train, X_test, y_train, y_test, = train_test_split(X, y, test_size = 0.25) 

    #AdaModel = AdaBoostClassifier(n_estimators = 200, learning_rate = 1)

    #Train Adaboost Classifier
    #model = AdaModel.fit(X_train, y_train)

    #Predict the response for test dataset
    #y_pred = model.predict(X_test)
    #print(model.score(X_test, y_test))

    #Model Accuracy
    #print("Accuracy:",metrics.accuracy_score(y_test, y_pred))
    #print(confusion_matrix(y_test, y_pred))
    
    #row = par1
    #start = time.clock_gettime(1)
    #dev_predict = model.predict(row)
    #finish = time.clock_gettime(1)
    #device = dev_predict[0]
    #print("***********Predicted Device: ",device)
    #print("Adaboost Time:",finish-start)
    #return device

    # -----> Random Method
    # -----> test_size = 0.25, n_estimators = 100, Accuracy = 0.9659178931061193
    # -----> test_size = 0.20, n_estimators = 100, Accuracy = 0.9767666989351403
    
    # -----> Algorithm Method
    # -----> test_size = 0.20, n_estimators = 100, Accuracy: 0.9719710669077758
    # -----> test_size = 0.25, n_estimators = 100, Accuracy = 0.975397973950796
    # -----> test_size = 0.30, n_estimators = 100, Accuracy = 0.972875226039783
    # -----> test_size = 0.25, n_estimators = 200, Accuracy = 0.9775687409551375

########################################################################################################
###################################### DECISION TREE ###################################################
########################################################################################################

#def decisionTree():    
    #col_names = ['device','cpu_usage','gpu_usage','min_load_in_queue','max_load_in_queue','avg_load_of_queue','std_deviation_of_queue','query_type','cpu_temp','gpu_temp']
    #Data= pd.read_csv("/home/anes/DOLAP/idp/GPU_/lb_ml_dt.csv",header=0, names=col_names)
    #feature_cols = ['cpu_usage','gpu_usage','min_load_in_queue','max_load_in_queue','avg_load_of_queue','std_deviation_of_queue','query_type','cpu_temp','gpu_temp']
    #X = Data[feature_cols]
    #y = Data['device']
    #X_train, X_test, y_train, y_test, = train_test_split(X, y, test_size = 0.3, random_state=1) 
    
    #clf = DecisionTreeClassifier(random_state=0)
    #clf = clf.fit(X_train,y_train)
    #y_pred = clf.predict(X_test)
    #print("DT Accuracy:",metrics.accuracy_score(y_test, y_pred))
    #plt.figure()
    #tree.plot_tree(clf)
    #plt.show()

    #row = par1
    #start = time.clock_gettime(1)
    #dev_predict = clf.predict(row)
    #finish = time.clock_gettime(1)
    #device = dev_predict[0]
    #print("Predicted Device: ",device)
    #print("Decision Tree Time:",format((finish-start),'.18f'))
    #return device

    # Random Method
    # -------> test_size = 0.20, Accuracy = 0.968054211035818
    # -------> test_size = 0.25, Accuracy = 0.9620449264136328
    # -------> test_size = 0.30, Accuracy = 0.9612903225806452
    
    # Algorithm Method
    # -------> test_size = 0.20, Accuracy = 0.9701627486437613
    # -------> test_size = 0.25, Accuracy = 0.9681620839363242
    # -------> test_size = 0.30, Accuracy = 0.9710669077757685

#########################################################################################################
##################################### GRADIENT BOOSTING #################################################
#########################################################################################################
#def gradientBoosting(par1,data):

    #Data= pd.read_csv("/home/anes/DOLAP/idp/GPU_/lb_ml.csv")
    #Separating X and y
    #X = Data.iloc[:,1:11]
    #y = Data.iloc[:,0]
    #print(X)
    #print(y)

    #Split dataset into training set and test set
    #X_train, X_test, y_train, y_test, = train_test_split(X, y, test_size = 0.25, random_state=1)
    
    #clf = GradientBoostingClassifier(n_estimators=200, learning_rate=1, random_state=0).fit(X_train, y_train)
    #print("GB ACC.",clf.score(X_test, y_test)) 

    #row = par1
    #start = time.clock_gettime(1)
    #dev_predict = clf.predict(row)
    #finish = time.clock_gettime(1)
    #device = dev_predict[0]
    #print("Predicted Device: ",device)
    #print("Gradient Boosting Time:",finish-start)
    #return device

    # Random Method
    # -------> test_size = 0.25, n_estimator = 200, Accuracy = 0.9767621998450813
    # -------> test_size = 0.20, n_estimator = 200, Accuracy = 0.9690222652468539
    # -------> test_size = 0.20, n_estimator = 100, Accuracy = 0.968054211035818
    
    # Algorithm Method
    # -------> test_size = 0.20, n_estimator = 100, Accuracy = 0.9783001808318263
    # -------> test_size = 0.25, n_estimator = 100, Accuracy = 0.9804630969609262
    # -------> test_size = 0.30, n_estimator = 100, Accuracy = 0.9789029535864979
    # -------> test_size = 0.20, n_estimator = 200, Accuracy = 0.9773960216998192
    # -------> test_size = 0.25, n_estimator = 200, Accuracy = 0.9797395079594791
    # -------> test_size = 0.30, n_estimator = 200, Accuracy = 0.9789029535864979

#print(adaboost(par1,model))
#print(decisionTree())
#print(gradientBoosting(par1,model))


