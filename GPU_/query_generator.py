import random
import socket
import pickle
import time


HOST = "10.36.55.181"
PORT = 65432

columns = ["fare", "tips", "trip_miles", "extras", "company", "dropoff_longitude", "dropoff_latitude", "pickup_latitude"]
values = ["30","5.9","35","1.5","416","80", "2", "744", "1020", "0", "1", "25", "0.9", "7", "1.3", "100", "511", "20", "15", "590", "615", "900", "90"]



class MySocket:
    """demonstration class only
      - coded for clarity, not efficiency
    """

    def __init__(self, sock=None):
        if sock is None:
            self.sock = socket.socket(
                            socket.AF_INET, socket.SOCK_STREAM)
        else:
            self.sock = sock

    def connect(self, host, port):
        self.sock.bind((host, port))

    def mysend(self, msg):
        totalsent = 0
        msg = pickle.dumps(msg)
        self.sock.send(msg)
        #while totalsent < MSGLEN:
        #    sent = self.sock.send(msg[totalsent:])
        #    if sent == 0:
        #        raise RuntimeError("socket connection broken")
        #    totalsent = totalsent + sent

    def myreceive(self):
        chunks = []
        bytes_recd = 0
        while bytes_recd < MSGLEN:
            chunk = self.sock.recv(min(MSGLEN - bytes_recd, 2048))
            if chunk == b'':
                raise RuntimeError("socket connection broken")
            chunks.append(chunk)
            bytes_recd = bytes_recd + len(chunk)
        return b''.join(chunks)




def gen_query():
#    query_type = random.randint(1,3)
    query_type = 1
    col = random.randint(0, len(columns)-1)
    val = random.randint(0, len(values)-1)
    query = []
    
    if query_type == 1:
        query.append(columns[col])
        #query_list.append(tmp_list)


    return [query]


def query_gen(queries_num):
    columns = ["fare", "tips", "trip_miles", "extras", "company", "dropoff_longitude", "dropoff_latitude", "pickup_latitude"]
    values = ["30","5.9","35","1.5","416","80", "2", "744", "1020", "0", "1", "25", "0.9", "7", "1.3", "100", "511", "20", "15", "590", "615", "900", "90"]
    query_list = []

    for i in range(queries_num):
        query_type = random.randint(2,3)
        col = random.randint(0, len(columns)-1)
        val = random.randint(0, len(values)-1)
        tmp_list = []

#        if query_type == 1:
#           tmp_list.append(columns[col])
#            query_list.append(tmp_list)

        if query_type == 2:
            tmp_list.append(columns[col])
            tmp_list.append(values[val])
            query_list.append(tmp_list)
        
        elif query_type == 3:
            for j in range(2):
                col = random.randint(0, len(columns) -1)
                val = random.randint(0, len(values) -1)
                tmp_list.append(columns[col])
                tmp_list.append(values[val])
            
            query_list.append(tmp_list)
    return query_list


if __name__ == "__main__":
    new_sock = MySocket(None)
    
#    while True:
    choice = random.randint(1,2)
    if choice == 1:
        query_data = gen_query()
    else:
        query_data = query_gen(random.randint(2, 300))

#        print(query_data)
    time.sleep(random.randint(1, 5))
    new_sock.connect(HOST, PORT)
    new_sock.mysend(query_data)
#    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
#        s.bind((HOST, PORT))
#        data=pickle.dumps(query_data)
#        s.send(data)


        
