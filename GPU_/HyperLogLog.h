//#if !defined(HYPERLOGLOG_HPP)
//#define HYPERLOGLOG_HPP


#include <vector>
#include <cmath>
#include <sstream>
#include <stdexcept>
#include <algorithm>
#include <cstdlib>
#include "MurmurHash3.h"
#include "omp.h"
#include <math.h>
#include <cstdlib>
#include "commonn.h"
#include "generator.h"

using namespace std;
#define HLL_HASH_SEED 313


inline uint8_t _get_leading_zero_count(unsigned long x, uint8_t b) {

#if defined (_MSC_VER)
	unsigned long leading_zero_len = 32;
	::_BitScanReverse(&leading_zero_len, x);
	--leading_zero_len;
	return std::min(b, (uint8_t)leading_zero_len);
#else
	uint8_t v = 1;
	while (v <= b && !(x & 0x80000000)) {
		v++;
		x <<= 1;
	}
	return v;
#endif

}
#define _GET_LEADING_ZERO_C(x, b) _get_leading_zero_count(x, b)

	// Two const doubles for the card estimation process
	static const double pow_2_32 = 4294967296.0; ///< 2^32
	static const double neg_pow_2_32 = -4294967296.0; ///< -(2^32)

	/** class HyperLogLog

	 */
	template <class T>
	class HyperLogLog {
	public:

		unsigned long long no_stream, no_unique;
		unsigned no_max_threads;
		double epsilon, delta;
		int distNum;
		double zipfAlp;

		int parseInputs(int argc, char** argv) {
			if (argc >= 6) {
				no_stream = pow(2, atoi(argv[1]));
				no_unique = pow(2, atoi(argv[2]));
				epsilon = atof(argv[3]);
				delta = atof(argv[4]);
				no_max_threads = atoi(argv[5]);
				distNum = atoi(argv[6]);
				zipfAlp = atof(argv[7]);
				return 0;
			}
			else {
				return -1;
			}
		}


		/**
		 * Constructor
		 *
		 * @param[in] b bit width (register size will be 2 to the b power).
		 *            This value must be in the range[4,30].Default value is 4.
		 *
		 * @exception std::invalid_argument the argument is out of range.
		 */
		HyperLogLog(uint8_t b = 4) throw (std::invalid_argument) :
			bit_width(b), reg_size(1 << b), M_regs(reg_size, 0) {

			if (b < 4 || 32 < b) {
				throw std::invalid_argument("bit width must be in the range [4,32]");
			}

			double alpha;
			switch (reg_size) {
			case 16:
				alpha = 0.673;
				break;
			case 32:
				alpha = 0.697;
				break;
			case 64:
				alpha = 0.709;
				break;
			default:
				alpha = 0.7213 / (1.0 + 1.079 / reg_size);
				break;
			}
			alphaMM = alpha * reg_size * reg_size;
		}

		/**
		 * Adds element to the estimator
		 *
		 * @param[in] str string to add
		 * @param[in] len length of string
		 */
		void insert(T* item, uint32_t len) {
			uint32_t hash;
			MurmurHash3_x86_32(item, len, HLL_HASH_SEED, (void*)&hash);

			uint32_t index = hash >> (32 - bit_width);
			uint8_t rank = _GET_LEADING_ZERO_C((hash << bit_width), 32 - bit_width);
			if (rank > M_regs[index]) { // M is the register or the buckets where to insert the hashes
				M_regs[index] = rank;
			}
		}

		/*Build the HLL sketch
		* using the insert function
		*/
		void build_sketch(dtype* data, uint32_t len, uint32_t n_elements) {
			
			for (int iter = 0; iter < n_elements; iter++) {
				insert((T*)&data[iter], len);
			}
		}
		
		/**
		 * Estimates cardinality value.
		 *
		 * @return Estimated cardinality value.
		 */
		double Estimate_card() const {
			double estimate;
			double sum = 0.0;
#pragma omp parallel
			for (uint32_t i = 0; i < reg_size; i++) {
				sum += 1.0 / (1 << M_regs[i]);
			}
			estimate = alphaMM / sum; // Using alpha to correct the bias introduced in (reg_size x the harmonic mean)
			if (estimate <= 2.5 * reg_size) {
				uint32_t zeros = 0;
#pragma omp parallel
				for (uint32_t i = 0; i < reg_size; i++) {
					if (M_regs[i] == 0) {
						zeros++;
					}
				}
				if (zeros != 0) {
					estimate = reg_size * std::log(static_cast<double>(reg_size) / zeros);
				}
			}
			else if (estimate > (1.0 / 30.0) * pow_2_32) {
				estimate = neg_pow_2_32 * log(1.0 - (estimate / pow_2_32));
			}
			return estimate;
		}


		/**
		 * Clears all internal registers.
		 */
		void clear() {
			std::fill(M_regs.begin(), M_regs.end(), 0);
		}

		/**
		 * Returns size of register.
		 *
		 * @return Register size
		 */
		uint32_t registerSize() const {
			return reg_size;
		}

		void HyperLogLog_run(int argc, char** argv, uint32_t elements) {

			//Parsing the inputs
			parseInputs(argc, argv);

			//uint32_t elements = 1024;
			dtype* data;
			generateData(*&data, no_stream, no_unique, distNum, zipfAlp);
			HyperLogLog hll(20);
			hll.build_sketch(*&data, 64, elements);

			//Estimating the cardinalty
			double cardinality = hll.Estimate_card();
			std::cout << "The estimated cardinality is:" << cardinality << std::endl;

		}

	protected:
		uint8_t bit_width; // register bit width
		uint32_t reg_size; // register size
		double alphaMM; // alpha * m^2
		std::vector<uint8_t> M_regs; // registers
	};

	


//#endif // !defined(HYPERLOGLOG_HPP)
