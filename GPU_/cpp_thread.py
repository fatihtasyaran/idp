from texttable import Texttable
import statistics

txt_name = "cpp_thread.txt"

f = open(txt_name,"r")
line_list = f.readlines()
f.close()
    
thread_res = {}
thread_res["1_1"] = [0.0,0]
thread_res["1_2"] = [0.0,0]
thread_res["1_3"] = [0.0,0]
thread_res["8_1"] = [0.0,0]
thread_res["8_2"] = [0.0,0]
thread_res["8_3"] = [0.0,0]
thread_res["16_1"] = [0.0,0]
thread_res["16_2"] = [0.0,0]
thread_res["16_3"] = [0.0,0]
thread_res["32_1"] = [0.0,0]
thread_res["32_2"] = [0.0,0]
thread_res["32_3"] = [0.0,0]
thread_res["64_1"] = [0.0,0]
thread_res["64_2"] = [0.0,0]
thread_res["64_3"] = [0.0,0]
thread_res["80_1"] = [0.0,0]
thread_res["80_2"] = [0.0,0]
thread_res["80_3"] = [0.0,0]

thread_1_list = []
thread_8_list = []
thread_16_list = []
thread_32_list = []
thread_64_list = []
thread_80_list = []

for line in line_list:
    line.strip()
    if "," in line:
        split_list = line.split(",")
        if split_list[1].strip() == "Scan":        # Function Name (Query Type)
            if split_list[2].strip() == "1":       # Number of Thread(s)
                thread_res["1_1"][0] += float(split_list[0])
                thread_res["1_1"][1] += 1
                thread_1_list.append(float(split_list[0]))
            elif split_list[2].strip() == "8":
                thread_res["8_1"][0] += float(split_list[0])
                thread_res["8_1"][1] += 1
                thread_8_list.append(float(split_list[0]))                    
            elif split_list[2].strip() == "16":
                thread_res["16_1"][0] += float(split_list[0])
                thread_res["16_1"][1] += 1
                thread_16_list.append(float(split_list[0]))                    
            elif split_list[2].strip() == "32":
                thread_res["32_1"][0] += float(split_list[0])
                thread_res["32_1"][1] += 1
                thread_32_list.append(float(split_list[0]))                    
            elif split_list[2].strip() == "64":
                thread_res["64_1"][0] += float(split_list[0])
                thread_res["64_1"][1] += 1
                thread_64_list.append(float(split_list[0]))                    
            elif split_list[2].strip() == "80":
                thread_res["80_1"][0] += float(split_list[0])
                thread_res["80_1"][1] += 1
                thread_80_list.append(float(split_list[0]))                    
                
        elif split_list[1].strip() == "Scan_with_bf":       # Query Type
            if split_list[2].strip() == "1":     # Number of Thread(s)                
                thread_res["1_2"][0] += float(split_list[0])
                thread_res["1_2"][1] += 1
                thread_1_list.append(float(split_list[0]))
            elif split_list[2].strip() == "8":
                thread_res["8_2"][0] += float(split_list[0])
                thread_res["8_2"][1] += 1
                thread_8_list.append(float(split_list[0]))                    
            elif split_list[2].strip() == "16":
                thread_res["16_2"][0] += float(split_list[0])
                thread_res["16_2"][1] += 1
                thread_16_list.append(float(split_list[0]))                    
            elif split_list[2].strip() == "32":
                thread_res["32_2"][0] += float(split_list[0])
                thread_res["32_2"][1] += 1
                thread_32_list.append(float(split_list[0]))                    
            elif split_list[2].strip() == "64":
                thread_res["64_2"][0] += float(split_list[0])
                thread_res["64_2"][1] += 1
                thread_64_list.append(float(split_list[0]))                    
            elif split_list[2].strip() == "80":
                thread_res["80_2"][0] += float(split_list[0])
                thread_res["80_2"][1] += 1
                thread_80_list.append(float(split_list[0]))
                
        elif split_list[1].strip() == "Scan_with_dep_bf":       # Query Type     
            if split_list[2].strip() == "1":     # Number of Thread(s)
                thread_res["1_3"][0] += float(split_list[0])
                thread_res["1_3"][1] += 1
                thread_1_list.append(float(split_list[0]))
            elif split_list[2].strip() == "8":
                thread_res["8_3"][0] += float(split_list[0])
                thread_res["8_3"][1] += 1
                thread_8_list.append(float(split_list[0]))                    
            elif split_list[2].strip() == "16":
                thread_res["16_3"][0] += float(split_list[0])
                thread_res["16_3"][1] += 1
                thread_16_list.append(float(split_list[0]))                    
            elif split_list[2].strip() == "32":
                thread_res["32_3"][0] += float(split_list[0])
                thread_res["32_3"][1] += 1
                thread_32_list.append(float(split_list[0]))                    
            elif split_list[2].strip() == "64":
                thread_res["64_3"][0] += float(split_list[0])
                thread_res["64_3"][1] += 1
                thread_64_list.append(float(split_list[0]))                    
            elif split_list[2].strip() == "80":
                thread_res["80_3"][0] += float(split_list[0])
                thread_res["80_3"][1] += 1
                thread_80_list.append(float(split_list[0]))                    
                    
###################################### Q1 #################################################
try:
    cpu_q1_t1_avg = thread_res["1_1"][0] / thread_res["1_1"][1]
except ZeroDivisionError:
    cpu_q1_t1_avg = 0

try:
    cpu_q1_t8_avg = thread_res["8_1"][0] / thread_res["8_1"][1]
except ZeroDivisionError:
    cpu_q1_t8_avg = 0

try:
    cpu_q1_t16_avg = thread_res["16_1"][0] / thread_res["16_1"][1]
except ZeroDivisionError:
    cpu_q1_t16_avg = 0

try:
    cpu_q1_t32_avg = thread_res["32_1"][0] / thread_res["32_1"][1]
except ZeroDivisionError:
    cpu_q1_t32_avg = 0

try:
    cpu_q1_t64_avg = thread_res["64_1"][0] / thread_res["64_1"][1]
except ZeroDivisionError:
    cpu_q1_t64_avg = 0

try:
    cpu_q1_t80_avg = thread_res["80_1"][0] / thread_res["80_1"][1]
except ZeroDivisionError:
    cpu_q1_t80_avg = 0
###################################### Q2 #################################################
try:
    cpu_q2_t1_avg = thread_res["1_2"][0] / thread_res["1_2"][1]
except ZeroDivisionError:
    cpu_q2_t1_avg = 0

try:
    cpu_q2_t8_avg = thread_res["8_2"][0] / thread_res["8_2"][1]
except ZeroDivisionError:
    cpu_q2_t8_avg = 0

try:
    cpu_q2_t16_avg = thread_res["16_2"][0] / thread_res["16_2"][1]
except ZeroDivisionError:
    cpu_q2_t16_avg = 0

try:
    cpu_q2_t32_avg = thread_res["32_2"][0] / thread_res["32_2"][1]
except ZeroDivisionError:
    cpu_q2_t32_avg = 0

try:
    cpu_q2_t64_avg = thread_res["64_2"][0] / thread_res["64_2"][1]
except ZeroDivisionError:
    cpu_q2_t64_avg = 0

try:
    cpu_q2_t80_avg = thread_res["80_2"][0] / thread_res["80_2"][1]
except ZeroDivisionError:
    cpu_q2_t80_avg = 0
###################################### Q3 #################################################
try:
    cpu_q3_t1_avg = thread_res["1_3"][0] / thread_res["1_3"][1]
except ZeroDivisionError:
    cpu_q3_t1_avg = 0

try:
    cpu_q3_t8_avg = thread_res["8_3"][0] / thread_res["8_3"][1]
except ZeroDivisionError:
    cpu_q3_t8_avg = 0

try:
    cpu_q3_t16_avg = thread_res["16_3"][0] / thread_res["16_3"][1]
except ZeroDivisionError:
    cpu_q3_t16_avg = 0

try:
    cpu_q3_t32_avg = thread_res["32_3"][0] / thread_res["32_3"][1]
except ZeroDivisionError:
    cpu_q3_t32_avg = 0

try:
    cpu_q3_t64_avg = thread_res["64_3"][0] / thread_res["64_3"][1]
except ZeroDivisionError:
    cpu_q3_t64_avg = 0

try:
    cpu_q3_t80_avg = thread_res["80_3"][0] / thread_res["80_3"][1]
except ZeroDivisionError:
    cpu_q3_t80_avg = 0    


thread_1_total = thread_res["1_1"][0] + thread_res["1_2"][0] + thread_res["1_3"][0]
thread_8_total = thread_res["8_1"][0] + thread_res["8_2"][0] + thread_res["8_3"][0]
thread_16_total = thread_res["16_1"][0] + thread_res["16_2"][0] + thread_res["16_3"][0]
thread_32_total = thread_res["32_1"][0] + thread_res["32_2"][0] + thread_res["32_3"][0]
thread_64_total = thread_res["64_1"][0] + thread_res["64_2"][0] + thread_res["64_3"][0]
thread_80_total = thread_res["80_1"][0] + thread_res["80_2"][0] + thread_res["80_3"][0]

thread_1_noq = thread_res["1_1"][1] + thread_res["1_2"][1] + thread_res["1_3"][1]
thread_8_noq = thread_res["8_1"][1] + thread_res["8_2"][1] + thread_res["8_3"][1]
thread_16_noq = thread_res["16_1"][1] + thread_res["16_2"][1] + thread_res["16_3"][1]
thread_32_noq = thread_res["32_1"][1] + thread_res["32_2"][1] + thread_res["32_3"][1]
thread_64_noq = thread_res["64_1"][1] + thread_res["64_2"][1] + thread_res["64_3"][1]
thread_80_noq = thread_res["80_1"][1] + thread_res["80_2"][1] + thread_res["80_3"][1]

print("******************************* Resulst for",txt_name,"****************************************")    
t = Texttable()
t.add_rows([['Number of Threads', '1','8','16','32','64','80'], ['Scan(Query Type 1)', cpu_q1_t1_avg, cpu_q1_t8_avg, cpu_q1_t16_avg, cpu_q1_t32_avg, cpu_q1_t64_avg, cpu_q1_t80_avg], ['Scan_with_bf (Query Type 2)', cpu_q2_t1_avg, cpu_q2_t8_avg, cpu_q2_t16_avg, cpu_q2_t32_avg, cpu_q2_t64_avg, cpu_q2_t80_avg],['Scan_with_dep_bf (Query Type 3)', cpu_q3_t1_avg, cpu_q3_t8_avg, cpu_q3_t16_avg, cpu_q3_t32_avg, cpu_q3_t64_avg, cpu_q3_t80_avg],['Number of query',thread_1_noq, thread_8_noq, thread_16_noq, thread_32_noq, thread_64_noq, thread_80_noq],['Total Time', thread_1_total, thread_8_total, thread_16_total, thread_32_total, thread_64_total, thread_80_total],['Max',max(thread_1_list),max(thread_8_list),max(thread_16_list),max(thread_32_list),max(thread_64_list),max(thread_80_list)],['Min',min(thread_1_list),min(thread_8_list),min(thread_16_list),min(thread_32_list),min(thread_64_list),min(thread_80_list)],['Median',statistics.median(thread_1_list),statistics.median(thread_8_list),statistics.median(thread_16_list),statistics.median(thread_32_list),statistics.median(thread_64_list),statistics.median(thread_80_list)],['Mean',statistics.mean(thread_1_list),statistics.mean(thread_8_list),statistics.mean(thread_16_list),statistics.mean(thread_32_list),statistics.mean(thread_64_list),statistics.mean(thread_80_list)]])
print(t.draw())
