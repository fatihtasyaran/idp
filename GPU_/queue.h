#include <iostream>
#include <vector>
#include <algorithm>
#include <string>

using namespace std;

struct query{
	std::string query_id; 
	uint8_t query_type; 
	vector<string> columns; 
	string query_string;

	query(string Query_id, uint8_t Query_type, vector<string> Columns, string Query):
  		query_id(Query_id),
		query_type(Query_type),
		columns(Columns),
		query_string(Query){}

};


template <class T>
class queue{

public:
  queue(){};
  ~queue(){};
  T pop();
  void push(query& element);
  void remove(query Q);
  int get_min_load();
  int get_max_load();
  int get_avg_load();
  int standart_deviation();
  int queue_length();
  
  vector <query> Queue;

};


template <class T>
T queue<T>::pop(){
  return Queue.front();
}

template <class T>
void queue<T>::push(query& element){
  Queue.push_back(element);
}

template <class T>
void queue<T>::remove(query Q){
  Queue.erase(std::remove(Queue.begin(), Queue.end(), Q.query_id), Queue.end());
}

template <class T>
int queue<T>::queue_length(){
  return Queue.size();
}


int main(){


vector<string> vec;
vec.push_back("fare");
vec.push_back("tips");

query* q1 = new query("23SDFM34", 1, vec, "20");

queue<query>* Queue = new queue<query>();
Queue->push(*q1);

}
